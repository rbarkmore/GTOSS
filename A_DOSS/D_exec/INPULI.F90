! ROUTINE: INPULI
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!********************************
!********************************
!
      SUBROUTINE INPULI
!
!********************************
!********************************

! THIS SUBROUTINE PERFORMS VARIOUS APPLICATION
! SPECIFIC INITIALIZATION


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_DOSS.h"
      include "../../A_HDR/COM_ROSS.h"




! CHARACTER STORAGE FOR TRANSFER OF ASCII RUN ID RDB FILE
      CHARACTER CHRTRN(131)

!****************************************************
! FOR MULTIPAGE PRINT FACILITY, INITIALIZE THE OUTPUT
! FILE WITH THE RUN GEOMETRY FILE CREATED AS PART OF
! THE RESULTS DATA BASE AT GTOSS EXECUTION TIME
!****************************************************
100    CONTINUE

!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
! FOR MAC VERSION, SEND  ESC Q TO SET PRINTER TO SUPER
! CONDENSED PRINT
!@MAC       WRITE(IUOUT,299) CHAR(27),'Q'
!@MAC299    FORMAT(1X,2A1)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@


! OPEN THE ASCII RDB RUN GEOMETRY FILE
!-------------------------------------

! MAKE A FACILTY-DEPENDENT PATH TO THE OUTPUT FILE
           CALL GETPTH (DBROOT, RDBFNI,   DBPATH)

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! CYBER PERMANENT FILE ACCESS
!@NOS      CALL PF('ATTACH', RDBFNI, RDBFNI)

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!@NOS      OPEN(IOUXXI, FILE = RDBFNI, STATUS='OLD')
      OPEN(IOUXXI, FILE = DBPATH, STATUS='OLD')
!@VAX      OPEN(IOUXXI, FILE = DBPATH, STATUS='OLD')
!@DOS      OPEN(IOUXXI, FILE = DBPATH, STATUS='OLD')
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
!@MAC      OPEN(IOUXXI, FILE = DBPATH, STATUS='OLD')
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@

      REWIND(IOUXXI)



30    READ(IOUXXI,300,END=40) (CHRTRN(J),J=1,131)
300   FORMAT(131A1)
      WRITE(IUOUT,300) (CHRTRN(J),J=1,131)
      GO TO 30

40    CONTINUE

! NOW CLOSE THE ASCII RDB RUN GEOMETRY FILE
      CLOSE(IOUXXI, STATUS='KEEP')

      RETURN
      END
