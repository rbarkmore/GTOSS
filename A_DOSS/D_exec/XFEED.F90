! ROUTINE: XFEED
! %Z%GTOSS %M% H.7 code v01.02
!              H.7 code v01.02 Eliminated un-used variable FFF
!-------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!********************************
!********************************
!
      SUBROUTINE XFEED(IOU)
!
!********************************
!********************************
! THIS SUBROUTINE INSTIGATES OR INHIBITS A
! FORM FEED TO THE ASCII RESULTS OUTPUT FILE

! THE COMMON PARAMETER FFOFF DETERMINES IF A
! FORM FEED IS TO BE EMITTED

! THE COMMON PARAMETER LFACIL DETERMINES FORM FEED TYPE:
!     IF LDFAC = 0, USE IMBEDDED FORM FEED
!     IF LDFAC = 1, USE 1H1 WRITE


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_DOSS.h"




! IF USER WANTS NO FORM-FEEDS IN OUTPUT, JUST LEAVE
      IF(FFOFF .EQ. 0.0) CALL NEWPAG(LDFAC, IOU)

      RETURN
      END
