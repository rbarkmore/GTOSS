! ROUTINE: YFM0110
! %Z%GTOSS %M% H.12 code v01.00
!              H.12 code v01.00   (baseline for vH12 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM110 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! SPECIAL SKYDER LOGGING MOVE LINE DEPLOYED LENGTH DISPLAY


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_DOSS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"




!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! WRITE HEADING TO PRINTOUT PAGE
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE DATA FOR PRINTOUT PAGE
      IF(JFUNC .EQ. 2) GO TO 2000

! PUT OUT A LINE OF DATA TO PRINTOUT PAGE
      IF(JFUNC .EQ. 3) GO TO 3000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM0108-0'


!******************************************
!******************************************
!    WRITE PAGE HEAD TO PRINTOUT FILE
!******************************************
!******************************************
1000  CONTINUE

      CALL XFEED(IUOUT)

      WRITE(IUOUT,50)
   50 FORMAT(3X,   &
     &'DISPLAY FMT 110:  MOVE LINE DEPLOY RATE SUMMARY FOR ', &
     &'SKYDER LOGGING SYSTEM "LANDING BASED WINCH CONFIGURATION"'/)

! ACTIVATE FORMATS OF UNITS CHOICE
      IF(NUNIT .EQ. 1) WRITE(IUOUT,51)
      IF(NUNIT .EQ. 1) WRITE(IUOUT,52)
      IF(NUNIT .EQ. 1) WRITE(IUOUT,53)

      IF(NUNIT .EQ. 2) WRITE(IUOUT,54)
      IF(NUNIT .EQ. 2) WRITE(IUOUT,55)
      IF(NUNIT .EQ. 2) WRITE(IUOUT,56)

!            |<========= N.W.  DEPLOY RATE ===========>|     S.E.      MASTER      S.W.    |<========= N.E.  DEPLOY RATE ===========>|
!    Q-TIME    MOVE 1     MOVE 2    AUXILRY     SHEAVE       MOVE       MOVE       MOVE       MOVE 1     MOVE 2    AUXILRY     SHEAVE
!     (SEC)    (M/S)      (M/S)      (M/S)       (M/S)       (M/S)      (M/S)      (M/S)      (M/S)      (M/S)      (M/S)       (M/S)
!     (SEC)    (FPS)      (FPS)      (FPS)       (FPS)       (FPS)      (FPS)      (FPS)      (FPS)      (FPS)      (FPS)       (FPS)
!
!
!1234567.90   456.0001   45678.01   45678.01   45678.01   45678.01   45678.01   45678.01x   45678.01   45678.01   45678.01   45678.01


! ENGLISH UNITS FORMAT (IE. KG FORCE FORMAT)
!---------------------
   51 FORMAT(3X,'         |<========= N.W.  DEPLOY RATE =======', &
     &          '====>|     S.E.      MASTER      S.W.    |<===', &
     &          '====== N.E.  DEPLOY RATE ===========>|')

   52 FORMAT(3X,' Q-TIME','    MOVE 1     MOVE 2    AUXILRY     ', &
     &                    'SHEAVE       MOVE       MOVE       MO', &
     &                    'VE       MOVE 1     MOVE 2    AUXILRY', &
     &                    '     SHEAVE')

   53 FORMAT(3X,'  (SEC)','    (F/S)      (F/S)      (F/S)      ', &
     &          ' (F/S)       (F/S)      (F/S)      (F/S)      (', &
     &          'F/S)      (F/S)      (F/S)       (F/S)'/)

! METRIC  UNITS FORMAT
!---------------------
   54 FORMAT(3X,'         |<========= N.W.  DEPLOY RATE =======', &
     &          '====>|     S.E.      MASTER      S.W.    |<===', &
     &          '====== N.E.  DEPLOY RATE ===========>|')

   55 FORMAT(3X,' Q-TIME','    MOVE 1     MOVE 2    AUXILRY     ', &
     &                    'SHEAVE       MOVE       MOVE       MO', &
     &                    'VE       MOVE 1     MOVE 2    AUXILRY', &
     &                    '     SHEAVE')

   56 FORMAT(3X,'  (SEC)','    (M/S)      (M/S)      (M/S)      ', &
     &          ' (M/S)       (M/S)      (M/S)      (M/S)      (', &
     &          'M/S)      (M/S)      (M/S)       (M/S)'/)
      RETURN


!******************************************
!******************************************
!    CALCULATE DATA FOR PRINTOUT PAGE
!******************************************
!******************************************
2000  CONTINUE

! DISPLAY FOR TETHER FORCE PARAMETERS

      OUTP( 1) = QTIME

! DISPLAY DEPLOYED LENGTHS OF MOVE LINES
         OUTP( 2) =  XXLNS * TLENTD( 9)
         OUTP( 3) =  XXLNS * TLENTD(22)
         OUTP( 4) =  XXLNS * TLENTD(25)
         OUTP( 5) =  XXLNS * TLENTD(27)

         OUTP( 6) =  XXLNS * TLENTD(17)
         OUTP( 7) =  XXLNS * TLENTD(15)
         OUTP( 8) =  XXLNS * TLENTD(18)

         OUTP( 9) =  XXLNS * TLENTD(10)
         OUTP(10) =  XXLNS * TLENTD(19)
         OUTP(11) =  XXLNS * TLENTD(24)
         OUTP(12) =  XXLNS * TLENTD(26)

! STATE # OF OUTPUT ITEMS IN ABOVE ARRAY FOR USE IN XPLASO
      OUTP(25) = 12.0

      RETURN




!******************************************
!******************************************
!   WRITE LINE OF DATA TO PRINTOUT PAGE
!******************************************
!******************************************
3000  CONTINUE

      IF(NUNIT .EQ. 1) WRITE(IUOUT,57) (OUTP(J),J=1,LASITM)
      IF(NUNIT .EQ. 2) WRITE(IUOUT,58) (OUTP(J),J=1,LASITM)

! ENGLISH UNITS FORMAT
!---------------------
   57  FORMAT(F10.2, 4F11.4, 3F11.4, 1X, 4F11.4)

! METRIC  UNITS FORMAT
!---------------------
   58  FORMAT(F10.2, 4F11.4, 3F11.4, 1X, 4F11.4)

      RETURN
      END
