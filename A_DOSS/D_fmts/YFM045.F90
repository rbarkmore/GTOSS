! ROUTINE: YFM045
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM045 (JFUNC)
!**************************************************
!**************************************************
!**************************************************


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_DOSS.h"




!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! WRITE HEADING TO PRINTOUT PAGE
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE DATA FOR PRINTOUT PAGE
      IF(JFUNC .EQ. 2) GO TO 2000

! PUT OUT A LINE OF DATA TO PRINTOUT PAGE
      IF(JFUNC .EQ. 3) GO TO 3000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM045-0'


!******************************************
!******************************************
!    WRITE PAGE HEAD TO PRINTOUT FILE
!******************************************
!******************************************
1000  CONTINUE

      CALL XFEED(IUOUT)

      WRITE(IUOUT,10) NFTSHO
   10 FORMAT(3X,   &
     & 'DISPLAY FMT 45:  <BEAD MODEL TETHER>  BEAD POSITION IN REF ',   &
     & 'END ORB FRAME  (X-COMP AT SPECIFIED BEADS):  SOLN #', I3 /)

! ACTIVATE FORMATS OF UNITS CHOICE
      IF(NUNIT .EQ. 1) WRITE(IUOUT,11) (NINT(PNBSHO(J)),J=1,11)
      IF(NUNIT .EQ. 1) WRITE(IUOUT,12)

      IF(NUNIT .EQ. 2) WRITE(IUOUT,13) (NINT(PNBSHO(J)),J=1,11)
      IF(NUNIT .EQ. 2) WRITE(IUOUT,14)

! ENGLISH UNITS FORMAT
!---------------------
   11 FORMAT(3X,' F-TIME', 11('   XORB-B',I3) )

   12 FORMAT(3X,'  (SEC)', 11('      (FT)  ') /)

! METRIC  UNITS FORMAT
!---------------------
   13 FORMAT(3X,' F-TIME', 11('   XORB-B',I3) )

   14 FORMAT(3X,'  (SEC)', 11('      (M)   ') /)

      RETURN


!******************************************
!******************************************
!    CALCULATE DATA FOR PRINTOUT PAGE
!******************************************
!******************************************
2000  CONTINUE

! MAKE CALL TO GET ORB FRAME POS COMP OF SELECTED BEADS
      CALL YPDFBB(1)

      RETURN


!******************************************
!******************************************
!   WRITE LINE OF DATA TO PRINTOUT PAGE
!******************************************
!******************************************
3000  CONTINUE

      IF(NUNIT .EQ. 1) WRITE(IUOUT,15) (OUTP(J),J=1,LASITM)
      IF(NUNIT .EQ. 2) WRITE(IUOUT,16) (OUTP(J),J=1,LASITM)

! ENGLISH UNITS FORMAT
!---------------------
15    FORMAT(F10.2, 11F12.1 )

! METRIC  UNITS FORMAT
!---------------------
16    FORMAT(F10.2, 11F12.1 )

      RETURN
      END
