! ROUTINE: YPDFTT
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!****************************
      SUBROUTINE YPDFTT
!****************************


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS

! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_DOSS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"

!      include "../../A_HDR/EQU_OBJ.i"  (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.h"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.h"



!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"





! DEFINE SCRATCH FOR ROUTINE
       DIMENSION VELDUM(3), POSDUM(3), PADUM(3), PADDUM(3)
       DIMENSION PODUM(3), PODDUM(3)


!---------------------------------------------
! PRELIMINARY ASCERTAINMENTS AND SUPPORT CALCS
!---------------------------------------------

! MAKE SURE THAT DISPLAY TOSS TETHER NUMBER MAKES SENSE
      JTETH  = NTTSHO
      IF((JTETH .LT. 1) .OR. (JTETH .GT. NTETH)) GO TO 100

! GET FINITE SOLN ASSIGNED TO THE DISPLAYED TOSS TETHER
      JFTETH = LASIGN(JTETH)

! FIND OBJ AND ATTACH PT NUMBER AT X-END OF TOSS TETHER
      JOX = NOBJX(JTETH)
      JAX = LATTX(JTETH)

! FIND OBJ AND ATTACH PT NUMBER AT Y-END OF TOSS TETHER
      JOY = NOBJY(JTETH)
      JAY = LATTY(JTETH)

! IF FINITE SOLN IS NOT ASSIGNED OR IS IN-ACTIVE, SO SPECIFY
! (ASSUME ASSIGNED AND ACTIVE, THEN CHECK THIS ASSUMPTION)
      JFSDUM = 1
      IF( (JFTETH.LT.1) .OR. (JFTETH.GT.NFINIT) ) JFSDUM = 0

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
! (IF APPROPRIATE)
      IF(JFSDUM .EQ. 1) CALL TISLDW(JFTETH)


! FIND INERTIAL TO ORB FRAME XFORMATION FOR OBJECT AT X-END
!             (SAVE AS SCRATCH MATRIX TOSMX1)
! ALSO FIND THE INERTIAL POSITION AND VELOCITY OF THE X-END
!----------------------------------------------------------

! FIRST, ASSUME X-END OBJECT IS THE REF PT (SET TOSMX1 = GOI)
      CALL MATMOV (0,RPGOI,  TOSMX1)
      CALL VECMOV (RPI,  POSDUM)
      CALL VECMOV (RPID, VELDUM)

! IF X-END OBJ IS A TOSS OBJECT INSTEAD, ACCESS THE OBJECTS DATA
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
      IF(JOX .GT. 1) CALL TOSLDW (JOX)
! THEN CALL ROUTINE TO DEFINE ORB FRAME ASSOCIATED W/OBJ STATE
      IF(JOX .GT. 1) CALL ORBFRM (RI,RID,   TOSMX1)
! SET IN INERTIAL POSITION OF OBJECT AT X-END
      IF(JOX .GT. 1) CALL VECMOV (RI,   POSDUM)
! SET IN INERTIAL VELOCITY OF OBJECT AT X-END
      IF(JOX .GT. 1) CALL VECMOV (RID,  VELDUM)


! FIND TETHER FRAME TO ORB FRAME (GOT) TRANSFORMATION
!     ( SAVE AS SCRATCH MATRIX TOSMX2 )
!----------------------------------------------------
      IF(JFSDUM .EQ. 1) CALL MATMUL (0,TOSMX1,GIT, TOSMX2)


!***********************************
! START PARAMETER CALC FOR EACH ITEM
!***********************************

! SET IN APPROPRIATE SIMULATION TIME FOR THIS TETHER
      OUTP( 1) = QTIME
      IF(JFSDUM .EQ. 1) OUTP( 1) = TISTIM

!-----------------------
! SET IN REF END TENSION
!-----------------------
      OUTP( 2) = XXFORS * XELOAD(JTETH)

!--------------------------------
! SET IN RANGE BETWEEN ATT POINTS
!--------------------------------
      OUTP( 3) = XXLNS * TDIST(JTETH)

!------------------------------
! SET IN TETHER DEPLOYMENT RATE
!------------------------------
      OUTP( 4) = XXLNS * TLENTD(JTETH)

!------------------------------
! SET IN DEPLOYED TETHER LENGTH
!------------------------------
      OUTP( 5) = XXLNS * TLENT(JTETH)

!---------------------------------------
! SET IN GROSS SLACK (= - GROSS STRETCH)
!---------------------------------------
      OUTP( 6) = - XXLNS * (TDIST(JTETH) - TLENT(JTETH))

!------------------------
! SET IN GROSS SLACK RATE
!------------------------
      OUTP( 7) = - XXLNS * (TDISTD(JTETH) - TLENTD(JTETH))

!----------------------------------------------------
! CALC LIBRATION ANGLE OF OBJ WR/T ORB FRAME AT X-END
!----------------------------------------------------
! GET POS AND VEL VECTOR BETWEEN ATT PTS
      DO 22 J = 1,3
         PADUM(J)  = APS(J+6,JAY,JOY) - APS(J+6,JAX,JOX)
         PADDUM(J) = APS(J+9,JAY,JOY) - APS(J+9,JAX,JOX)
22    CONTINUE

! TRANSFORM TO X-END ORB FRAME
      CALL MATVEC (0,TOSMX1,PADUM,   PODUM)

! EXTRACT LIBRATION ANGLES
      CALL XYZLIB(NTPLIB,PODUM(1),PODUM(2),PODUM(3),  THTDUM,PHIDUM)

! SET IN OUTPUT ARRAY IN DEGREES
      OUTP( 8) = XRDXDG * THTDUM
      OUTP(10) = XRDXDG * PHIDUM



! FIND DERIV OF X-END "ORBITAL FRAME" COMP OF AP RANGE VEC
!---------------------------------------------------------
! POSDUM IS POS VEC OF X-END OBJECT WR/T INER PT (INER FRAME COMP)
! PADUM IS POS VEC BETWEEN ATT PTS (INER FRAME COMP)
! PODUM IS POS VEC BETWEEN ATT PTS (X-END ORB FRAME COMP)
! PADDUM IS VEL BETWEEN ATT PTS (INER FRAME COMP)
! PODDUM IS RATE OF CHANGE OF ELEMENTS OF PODUM

! CALCULATE ORB RATE OF X-END OBJECT
!-----------------------------------
! NOTE: VELDUM IS INER VEL OF X-END OBJECT (INER FRAME COMP)
      CALL MATVEC (0, TOSMX1,VELDUM,  TOSVX3)

      TOSVX2(1) = 0.0
      TOSVX2(2) = - TOSVX3(1)/VECMAG(POSDUM)
      TOSVX2(3) = 0.0

      CALL MATVEC (0, TOSMX1,PADDUM,  PODDUM)

! EXTRACT ORB RATE CONTRIBUTION TO GET COMPONENT DERIVATIVES
      CALL CROSS (TOSVX2,PODUM,  TOSVX3)
      CALL VECDIF (PODDUM,TOSVX3,  PODDUM)


!--------------------------
! CALCULATE LIBRATION RATES  =====> (FOR TYPE 1 LIBRATION ANGLES)
!--------------------------
      OUTP(9) = ( PODDUM(1)*PODUM(3)-PODUM(1)*PODDUM(3) )/(PODUM(3)**2)
      OUTP( 9) = XRDXDG * (COS(THTDUM)**2)*OUTP(9)

      OUTP(11) = 0.0
      IF(COS(PHIDUM) .EQ. 0.0) GO TO 33
          OUTP(11) = ( PODDUM(2)*TDIST(JTETH)-PODUM(2)*TDISTD(JTETH) )/   &
     &                (TDIST(JTETH)**2)
          OUTP(11) = - XRDXDG * OUTP(11)/COS(PHIDUM)
33    CONTINUE


!-----------------------------------------
! FIND MAX BEAD DEFLECTIONS IF APPROPRIATE
!-----------------------------------------
! ASSUME NO ACTIVE BEAD SOLN
      OUTP(12) = 0.0
      OUTP(13) = 0.0
      OUTP(14) = 0.0
      OUTP(15) = 0.0
      OUTP(16) = 0.0
      OUTP(17) = 0.0

! GET OUT OF THERE IS NO ACTIVE FINITE SOLUTION
      IF(JFSDUM .EQ. 0) GO TO 50

! GET OUT IF FINITE SOLN IS NOT A BEAD MODEL
      IF(NFTYP .EQ. 0) GO TO 50


! SET BEAD NUMBER OF MAX DEFLECTIONS
          OUTP(13) = 1.
          OUTP(15) = 1.
          OUTP(17) = 1.

! EXAMINE EACH BEAD IN TETHER
          DO 40 JBEAD = 1,NBEAD
             JBX = 3*(JBEAD-1) + 1
             JBY = JBX + 1
             JBZ = JBX + 2

! FIND MAX TETHER FRAME Z BEAD DEFLECTION
              IF(ABS(BUT(JBZ)) .GT. ABS(OUTP(12))) OUTP(13)=REAL(JBEAD)
              IF(ABS(BUT(JBZ)) .GT. ABS(OUTP(12))) OUTP(12) = BUT(JBZ)

! FIND MAX TETHER FRAME Y BEAD DEFLECTION
              IF(ABS(BUT(JBY)) .GT. ABS(OUTP(14))) OUTP(15)=REAL(JBEAD)
              IF(ABS(BUT(JBY)) .GT. ABS(OUTP(14))) OUTP(14) = BUT(JBY)

! FIND MAX TETHER FRAME X BEAD DEFLECTION
              IF(ABS(BUT(JBX)) .GT. ABS(OUTP(16))) OUTP(17)=REAL(JBEAD)
              IF(ABS(BUT(JBX)) .GT. ABS(OUTP(16))) OUTP(16) = BUT(JBX)

! END OF BEAD LOOP
40    CONTINUE

! CONVERT DEFLECTIONS TO METRIC
      OUTP(12) = XXLNS * OUTP(12)
      OUTP(14) = XXLNS * OUTP(14)
      OUTP(16) = XXLNS * OUTP(16)


! END OF ACTIVE BEAD SOLN DATA PROCESSING
!----------------------------------------
50    CONTINUE


!----------------------------
! SET IN OPPOSITE END TENSION
!----------------------------
      OUTP(18) = XXFORS * YELOAD(JTETH)


! STATE # OF OUTPUT ITEMS IN ABOVE ARRAY FOR USE IN XPLASO
!---------------------------------------------------------
100   OUTP(25) = 18.0

      RETURN
      END
