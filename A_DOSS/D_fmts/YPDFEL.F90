! ROUTINE: YPDFEL
! %Z%GTOSS %M% H.7 code v02.03
!              H.7 code v02.03 Modified calc of elec current
!---------------------------------------------------------------------
!              H.5 code v02.02 modified apogee/perigee for all planets
!---------------------------------------------------------------------
!              H.1 code v02.01 (baseline for vH1 inertial bead model)
!***************************
      SUBROUTINE YPDFEL
!***************************
! THIS ROUTINE DEFINES ORBITAL ELEMENTS
! AND OTHER USEFUL ELECTROMAGNETIC DATA


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS

! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_DOSS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.h"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.h"



!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"






! SCRATCH FOR REQUESTED OBJECT POS AND VELOCITY, GRAV ACCEL
      DIMENSION ROBDUM(3), VOBDUM(3), DUMELC(3), DUMARO(3), AMODUM(3)

!*********************************************
! PRELIMINARY ASCERTAINMENTS AND SUPPORT CALCS
!*********************************************
! MAKE SURE THE REQUESTED OBJECT NUMBER MAKES SENSE
      IF(NOBJZ .LT. 1) GO TO 100

! MAKE SURE THE REQUESTED TOSS TETHER NUMBER MAKES SENSE
      JTETH  = NTTSHO
      IF(JTETH .GT. NTETH) GO TO 100

! IF NO TOSS TETHER IS SPECIFIED, DONT DO FINITE SOLN STUFF
!----------------------------------------------------------
      JFSDUM = 0
      IF(JTETH .EQ. 0) GO TO 70

! GET FINITE SOLN ASSIGNED TO THE DISPLAYED TOSS TETHER
           JFSOL = LASIGN(JTETH)

! FIND OBJ AND ATT PT NUMBERS AT X-END/Y-END OF THIS TOSS TETHER
           JOX = NOBJX(JTETH)
           JAX = LATTX(JTETH)
           JOY = NOBJY(JTETH)
           JAY = LATTY(JTETH)

! IF A FINITE SOLN IS ASSIGNED AND ACTIVE, SO SPECIFY
           IF((JFSOL.GE.1).AND.(JFSOL.LE.NFINIT)) JFSDUM = 1

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
! (IF APPROPRIATE)
           IF(JFSDUM .EQ. 1) CALL TISLDW(JFSOL)

70    CONTINUE
! END OF FINITE SOLUTION SET-UP
!------------------------------



!-----------------------------------------------------------
! FIND INERTIAL TO ORB FRAME XFORMATION FOR REQUESTED OBJECT
!             (SAVE AS SCRATCH MATRIX TOSMX1)
!-----------------------------------------------------------

! FIRST, ASSUME REQUESTED OBJECT IS THE REF PT (SET TOSMX1 = GOI)
      CALL MATMOV (0,RPGOI,  TOSMX1)

! IF REQUESTED OBJECT IS A TOSS OBJECT INSTEAD, ACCESS ITS OBJECT DATA
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
      IF(NOBJZ .GE. 2) CALL TOSLDW (NOBJZ)
! THEN CALL ROUTINE TO DEFINE ORB FRAME ASSOCIATED W/OBJECT STATE
      IF(NOBJZ .GE. 2) CALL ORBFRM (RI,RID,   TOSMX1)



!----------------------------------------------------
! FIND TETHER FRAME TO ORB FRAME (GOT) TRANSFORMATION
!     ( SAVE AS SCRATCH MATRIX TOSMX2 )
!----------------------------------------------------
      IF(JFSDUM .EQ. 1) CALL MATMUL (0,TOSMX1,GIT, TOSMX2)



!----------------------------------------------
! FIND APPROPRIATE OBJECT POSITION AND VELOCITY
!----------------------------------------------
      IF(NOBJZ .EQ. 1) CALL VECMOV (RPI,   ROBDUM)
      IF(NOBJZ .EQ. 1) CALL VECMOV (RPID,  VOBDUM)

      IF(NOBJZ .GE. 2) CALL VECMOV (RI,    ROBDUM)
      IF(NOBJZ .GE. 2) CALL VECMOV (RID,   VOBDUM)



!**************************************
! SET IN GENERAL REF PT SIMULATION TIME
!**************************************
      OUTP(1) = RPTIME


!*******************************************************
! SET IN ALTITUDE OF TOSS OBJ OR REF PT (AS APPROPRIATE)
!*******************************************************
      ALTDUM = RPALT
      IF(NOBJZ .GE. 2) ALTDUM = ALTOBL

      OUTP(2) = XXLNB1 * ALTDUM


!**********************************************************
! CALC OBJECTS LATITUDE AND LONGITUDE WR/T PLANET FIXED REF
!**********************************************************
! FIRST, GET PLANET FIXED COMP OF REQUESTED OBJECTS INERTIAL POS
      IF(NOBJZ.EQ.1) CALL VECMOV (RPIP,   TOSVX5)
      IF(NOBJZ.GT.1) CALL VECMOV (RIP,    TOSVX5)


!--------------------------------------------------------------------
! FIND PLANET RADIUS CORRESPONDING TO OBJECT POSITION IN PLANET FRAME
!--------------------------------------------------------------------
      CALL GEOD (LGLBOP,RPTIME,TOSVX5,  DUMRP,DUMLAT,DUMALT)


! MAKE SOME CONVENIENT DEFINITIONS
      DUMMAG = VECMAG(TOSVX5)
      DUMX   = TOSVX5(1)
      DUMY   = TOSVX5(2)
      DUMZ   = TOSVX5(3)

! SET IN BOGUS DEFAULT VALUES
      DUMLA = 0.0
      DUMMU = 0.0

! DONT EVALUATE AT LONG/LAT SINGULARITIES
      IF(ABS(DUMZ/DUMMAG) .LE. 1.0)  DUMLA = ASIN ( DUMZ/DUMMAG )
      IF( (DUMX + DUMY)   .NE. 0.0)  DUMMU = ATAN2( DUMY, DUMX  )

! SET IN LATITUDE AND LONGITUDE
!------------------------------
      OUTP(3) = XRDXDG * DUMLA
      OUTP(4) = XRDXDG * DUMMU



!****************************************************
! SET IN ORBITAL INCLINATION (WR/T NORTH INER VECTOR)
!****************************************************
      OUTP(5) = XRDXDG * ACOS( -TOSMX1(2,3) )



!*******************************
! CALCULATE ORBITAL ECCENTRICITY
!*******************************

! EVALUATE GRAVITATIONAL CONSTANT FOR PLANET
!-------------------------------------------
! FIRST, DETERMINE ACCEL OF GRAV AT REF RADIUS ALONG POSITION VECTOR
      CALL VECNRM(TOSVX5, TOSVX1)
      CALL VECSCL(DUMRP, TOSVX1,    TOSVX1)

      CALL GRAV (LGRVOP,RPTIME,TOSVX1,  TOSVX4)

! THEN CALC THE EFFECTIVE GRAVITATIONAL CONSTANT (MU)
      DUMMU = VECMAG(TOSVX4)*DUMRP**2


! CALC ANGULAR MOMENTUM VECTOR (PER UNIT MASS) AND SQUARE IT
!-----------------------------------------------------------
! FIND MAGNITUDE OF POSITION VECTOR
      RDMDUM = SQRT(ROBDUM(1)**2 + ROBDUM(2)**2 + ROBDUM(3)**2)

! CALC ANGULAR MOMENTUM (CROSS PRODUCT)
      AMODUM(1) = + ROBDUM(2)*VOBDUM(3) - VOBDUM(2)*ROBDUM(3)
      AMODUM(2) = - ROBDUM(1)*VOBDUM(3) + VOBDUM(1)*ROBDUM(3)
      AMODUM(3) = + ROBDUM(1)*VOBDUM(2) - VOBDUM(1)*ROBDUM(2)

! THEN SQUARE IT
      DUMHSQ = AMODUM(1)**2 + AMODUM(2)**2 + AMODUM(3)**2


! CALC TOTAL ENERGY (PER UNIT MASS)
!----------------------------------
! KINETIC ENERGY
      DUMKE = 0.5*( VOBDUM(1)**2 + VOBDUM(2)**2 +VOBDUM(3)**2 )

! POTENTIAL ENERGY
      DUMPE = -DUMMU/RDMDUM

! THEN TOTAL ENERGY
      DUMTE = DUMKE + DUMPE


! CALC ECCENTRICITY, BUT CHECK FOR: SQRT(-SMALL NEG NUMBER)
!----------------------------------------------------------
      ESQDUM = 1.0 + 2.0*DUMTE*DUMHSQ/DUMMU**2
      DUMECC = SQRT(ABS(ESQDUM))

! SET IN ECCENTRICITY
!--------------------
      OUTP(6) = SIGN(DUMECC,ESQDUM)


!---------------------------------------------------------
! JUMP OVER A LOT OF STUFF IF THERE IS NO TETHER SPECIFIED
!---------------------------------------------------------
! FIRST, ASSUME NO TETHER IS SPECIFIED (FOR AFFECTED ITEMS)
      OUTP( 7) = 0.0
      OUTP( 8) = 0.0
      OUTP( 9) = 0.0
      OUTP(10) = 0.0
      OUTP(11) = 0.0
      OUTP(12) = 0.0
      OUTP(13) = 0.0
      OUTP(14) = 0.0

! THEN SEE IF NO TETHER IS INVOLVED (IN WHICH CASE JUMP OVER)
      IF(JTETH .EQ. 0) GO TO 888



!***************************************************
! SET IN ELECTRO + AERO FORCE ORBIT FRAME COMPONENTS
!***************************************************

! FIND ELECTRO + AERO FORCE IN ORB FRAME OF REQUESTED OBJECT
!-----------------------------------------------------------
! FIRST SEE IF: FINITE SOLN,  OR MASSLESS TETHER IS APPLICABLE
      IF(JFSDUM .EQ. 1) GO TO 5

! MASSLESS TETHER ELECTRO/AERO FORCES ARE BEING USED
!---------------------------------------------------
! MASSLESS TETHER ELECTRO/AERO-FORCE EXTRACTION DEPENDS ON FACT THAT
!   1. .5*(ELECTRO+AERO) APPEARS AS ATT PT TARE (IN ADDITION TO TENSION)
!   2. A SINGLE TETHER ONLY IS ATTACHED TO THE ATT PT (FOR UNIQUENESS)

! FIRST, GET POS VECTOR FROM X-END ATT PT TO Y-END ATT PT
      DO 23 J = 1,3
         TOSVX1(J) = APS(J+6,JAY,JOY) - APS(J+6,JAX,JOX)
23    CONTINUE

! FORM A UNIT TANGENT FROM X-END TO Y-END OF TOSS TETHER
      CALL VECNRM(TOSVX1,TOSVX1)

! MULTIPLY THIS BY TENSION AT X-END TO GET TENSION VECTOR
      CALL VECSCL(XELOAD(JTETH),TOSVX1,  TOSVX1)

! SUBTRACT FROM TOTAL ATTACH PT FORCE (WHICH INCLUDES ELECTRO+AERO)
! (NOTE: TOTAL ELEC+AERO FORCE SHOULD BE TWICE THIS ATT PT-FORCE)
      TOSVX2(1) = 2.0*( APS(1,JAX, JOX) - TOSVX1(1) )
      TOSVX2(2) = 2.0*( APS(2,JAX, JOX) - TOSVX1(2) )
      TOSVX2(3) = 2.0*( APS(3,JAX, JOX) - TOSVX1(3) )

! TRANSFORM ELECTRO+AERO FORCE INER COMP TO ORB FRAME (OF REQUESTED OBJ)
      CALL MATVEC (0,TOSMX1,TOSVX2,   TOSVX1)

! CALCULATE LENGTH-AVERAGE CURRENT FOR MASSLESS TETHER
      DUMAVG = CURRF(JTETH)

      GO TO 10


! START HERE FOR FINITE SOLN ELECTRO/AERO FORCES
!-----------------------------------------------
5     CONTINUE

      CALL MATVEC (1,GIT,FBMEI,  DUMELC)
      CALL MATVEC (1,GIT,FBMAI,  DUMARO)

      CALL MATSUM (0, DUMELC, DUMARO,  TOSVX1)


! NOW TRANSFORM TO ORB FRAME ASSOCIATED WITH OBJECT AT X-END
      CALL MATVEC (0,TOSMX2,TOSVX1,   TOSVX1)

! CALC LENGTH-AVERAGE OF CURRENT FOR FINITE SOLN
      DUMAVG = 0.0
      DO 111 JSEG = 1, NBEAD+1
             DUMAVG = DUMAVG + CURRT + CURRTS(JSEG)
111   CONTINUE

      DUMAVG = DUMAVG/REAL(NBEAD+1)


! SET IN THE IN-PLANE/OUT-OF-PLANE (ELECTRO + AERO) FORCE COMPONENTS
!-------------------------------------------------------------------
10    CONTINUE
      OUTP(7) = XXFORS * TOSVX1(1)
      OUTP(8) = XXFORS * TOSVX1(2)



!*********************************************************
! SET IN SATELLITE LIBRATION ANGLES WR/T X-END ORBIT FRAME
!*********************************************************

! ANGLE OF TETHER Y-END REL-TO X-END WR/T REQUEST OBJ ORB FRAME
!--------------------------------------------------------------
! GET POS VECTOR BETWEEN ATT PTS (FROM X-END TO Y-END)
      DO 22 J = 1,3
         TOSVX1(J) = APS(J+6,JAY,JOY) - APS(J+6,JAX,JOX)
22    CONTINUE

! TRANSFORM TO X-END ORB FRAME
      CALL MATVEC (0,TOSMX1,TOSVX1,   TOSVX2)

! EXTRACT LIBRATION ANGLES (IN-PLANE AND OUT-OF-PLANE)
      CALL XYZLIB(NTPLIB,TOSVX2(1),TOSVX2(2),TOSVX2(3), OILDUM,OOLDUM)

! SET IN LIBRATION ANGLES
!------------------------
      OUTP( 9) = XRDXDG * OILDUM
      OUTP(10) = XRDXDG * OOLDUM



!********************************************
! SET IN TETHER ANGLES WR/T X-END ORBIT FRAME
!********************************************

! CALC TETHER ANGLE AT X-END WR/T ORB FRAME OF REQUESTED OBJECT
!--------------------------------------------------------------
! FIRST SEE IF FINITE SOLN OR MASSLESS TETHER IS APPLICABLE
      IF(JFSDUM .EQ. 1) GO TO 25

! MASSLESS TETHER ANGLES ARE IN ORDER
!------------------------------------
! JUST USE OBJECT LIBRATION ANGLES FROM ABOVE (TETH ANGLE NOT KNOWN)
      TILDUM = OILDUM
      TOLDUM = OOLDUM

      GO TO 40


! FINITE SOLN TETHER ANGLES ARE TO BE USED
!-----------------------------------------
25    CONTINUE

! FIRST PUT UNIT TANGENT AT X-END (TETHER FRAME COMP) INTO TOSVX3

! USE BEAD MODEL TANGENT VECTOR
      IF(NFTYP .GT. 0) THEN
          TOSVX2(1) = BUT(1) + ELMG/REAL(NBEAD+1)
          TOSVX2(2) = BUT(2)
          TOSVX2(3) = BUT(3)
          CALL VECNRM(TOSVX2,  TOSVX3)
      END IF


! THEN, TRANSFORM UNIT TANGENT TO ORB FRAME
      CALL MATVEC (0,TOSMX2,TOSVX3,   TOSVX1)

! NOW, EXTRACT TETHER ANGLES (IN-PLANE AND OUT-OF-PLANE)
      CALL XYZLIB(NTPLIB,TOSVX1(1),TOSVX1(2),TOSVX1(3), TILDUM,TOLDUM)

! SET IN DIFF BETWEEN SATELLITE LIBRATION AND TETHER ANGLES
!----------------------------------------------------------
40    CONTINUE
      OUTP(11) = XRDXDG * (TILDUM - OILDUM)
      OUTP(12) = XRDXDG * (TOLDUM - OOLDUM)

! END OF TETHER ANGLE DETERMINATION
!----------------------------------



!***************************
! SET IN VOLTAGE AND CURRENT
!***************************
      OUTP(13) = VOLTF(JTETH)
      OUTP(14) = DUMAVG

!----------------------------------------
! RESUME HERE, IF NO TETHER WAS SPECIFIED
!----------------------------------------
888   CONTINUE


!***********************************************
! CALC INSTANTANEOUS PERIGEE AND APOGEE ALTITUDE
!***********************************************
! CALC SEMI-AXIS MAJOR
      DUMSAM = - 0.5*DUMMU/DUMTE

! CALC SPHERICAL PERIGEE AND APOGEE ALTITUDE
      DUMPER = DUMSAM*(1.0 - DUMECC) - DUMRP
      DUMAPR = DUMSAM*(1.0 + DUMECC) - DUMRP

! SET IN INSTANTANEOUS PERIGEE AND APOGEE ALTITUDE
!-------------------------------------------------
      OUTP(15) = XXLNB1 * DUMPER
      OUTP(16) = XXLNB1 * DUMAPR


!*****************************************************
! CALC RIGHT ASCENSION ANGLE (WR/T INERTIAL GREENWICH)
!*****************************************************
! FIRST, FORM VECTOR NORMAL TO ORBITAL PLANE
      TOSVX4(1) = TOSMX1(2,1)
      TOSVX4(2) = TOSMX1(2,2)
      TOSVX4(3) = TOSMX1(2,3)

! FORM NORTHERN POINTING INERTIAL UNIT VECTOR
      TOSVX5(1) = 0.0
      TOSVX5(2) = 0.0
      TOSVX5(3) = 1.0

! CALC RIGHT ASCENSION ANGLE (IF DEFINED)
!----------------------------------------
      CALL CROSS (TOSVX4,TOSVX5,  TOSVX6)

      IF(VECMAG(TOSVX6) .NE. 0.0)  CALL VECNRM(TOSVX6,   TOSVX6)

      OUTP(17) = 0.0
      IF((TOSVX6(2).NE.0.0) .OR. (TOSVX6(1).NE.0.0) ) THEN

           OUTP(17) = XRDXDG * ATAN2( TOSVX6(2), TOSVX6(1) )

      END IF


! STATE # OF OUTPUT ITEMS IN ABOVE ARRAY FOR USE IN XPLASO
!---------------------------------------------------------
100   OUTP(25) = 17.0

      RETURN
      END
