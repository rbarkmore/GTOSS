! ROUTINE: YFM014
! %Z%GTOSS %M% H.10 code v01.02
!              H.10 code v01.02 Added fuel usage output.
!----------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM014 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! THIS FORMAT DISPLAYS TOSS OBJECT MASS AND INERTIA
! PROPERTIES AND REF PT MASS


! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_DOSS.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_TOSS.h"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.h"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.h"







!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! WRITE HEADING TO PRINTOUT PAGE
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE DATA FOR PRINTOUT PAGE
      IF(JFUNC .EQ. 2) GO TO 2000

! PUT OUT A LINE OF DATA TO PRINTOUT PAGE
      IF(JFUNC .EQ. 3) GO TO 3000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM014-0'


!******************************************
!******************************************
!    WRITE PAGE HEAD TO PRINTOUT FILE
!******************************************
!******************************************
1000  CONTINUE

      CALL XFEED(IUOUT)

      WRITE(IUOUT,9) NOBJZ
    9 FORMAT(3X,   &
     &'DISPLAY FMT 14:   <TOSS OBJECT> MASS (OF AND DEPLOYED FROM),',   &
     &' AND INERTIA/GEOMETRY',   9X,'TOSS OBJECT #',I2/)


! ACTIVATE FORMATS OF UNITS CHOICE
      IF(NUNIT .EQ. 1) WRITE(IUOUT,51)
      IF(NUNIT .EQ. 1) WRITE(IUOUT,52)

      IF(NUNIT .EQ. 2) WRITE(IUOUT,53)
      IF(NUNIT .EQ. 2) WRITE(IUOUT,54)

!    OB-TIME  <------ MASS  (SLUGS) ------>       IXX          IYY          IZZ         IXY         IXZ         IYZ       X-CG   Y-CG    Z-CG
!      (SEC)  IN OBJECT....DEPLOYED....FUEL    SLUG-FT*2    SLUG-FT*2    SLUG-FT*2   SLUG-FT*2   SLUG-FT*2   SLUG-FT*2    (FT)   (FT)    (FT)
!
!       .00     4999.74       .260  xxxxx.x   0.0000E+00   0.0000E+00   0.0000E+00   0.000E+00   0.000E+00   0.000E+00      .0     .0      .0
!
!
!    OB-TIME  <-------- MASS (KG) -------->       IXX          IYY          IZZ         IXY         IXZ         IYZ       X-CG   Y-CG    Z-CG
!      (SEC)  IN OBJECT....DEPLOYED....FUEL     KG-M**2      KG-M**2      KG-M**2     KG-M**2     KG-M**2     KG-M**2      (M)    (M)     (M)


! ENGLISH UNITS FORMAT
!---------------------
   51 FORMAT(3X,' OB-TIME  <------ MASS  (SLUGS) ------>       IXX',   &
     &          '          IYY          IZZ         IXY         IXZ',   &
     &          '         IYZ       X-CG   Y-CG    Z-CG'  )

   52 FORMAT(3X,'   (SEC)  IN OBJECT....DEPLOYED....FUEL    SLUG-FT*2',   &
     &          '    SLUG-FT*2    SLUG-FT*2   SLUG-FT*2   SLUG-FT*2  ',   &
     &          ' SLUG-FT*2    (FT)   (FT)    (FT)' /)

! METRIC  UNITS FORMAT
!---------------------
   53 FORMAT(3X,' OB-TIME  <------ MASS  (SLUGS) ------>       IXX',   &
     &          '          IYY          IZZ         IXY         IXZ',   &
     &          '         IYZ       X-CG   Y-CG    Z-CG'  )

   54 FORMAT(3X,'   (SEC)  IN OBJECT....DEPLOYED....FUEL     KG-M**2',   &
     &          '      KG-M**2      KG-M**2     KG-M**2     KG-M**2 ',   &
     &          '    KG-M**2      (M)    (M)     (M)' /)

      RETURN



!******************************************
!******************************************
!    CALCULATE DATA FOR PRINTOUT PAGE
!******************************************
!******************************************
2000  CONTINUE

! IF TOSS OBJECT NUMBER HAS NOT BEEN DEFINED, GET OUT
      IF(NOBJZ .EQ. 0) GO TO 100

! IF REF PT OBJECT NUMBER IS REQUESTED, GET OUT
      IF(NOBJZ .EQ. 1) GO TO 100

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
      CALL TOSLDW (NOBJZ)

! DISPLAY TOSS OBJECT TIME
      OUTP( 1) = TOSTIM

! DISPLAY MASS OF THIS TOSS OBJECT
      OUTP( 2) = XXMS1*DMASS

! CALCULATE TETHER MASS DEPLOYED FROM THIS TOSS OBJECT
      OUTP( 3) = 0.0
      DO 10 JAP = 1,NATPAT
            OUTP( 3) = OUTP( 3) + XXMS1*APS(32,JAP,NOBJZ)
10    CONTINUE

! DISPLAY FUEL MASS LOST DUE  TO THRUSTING
      OUTP( 4) = XXMS1 * DMASINC

! DISPLAY TOSS OBJECT INERTIA PROPERTIES
      OUTP( 5) = (XXMS1*XXLNS**2) * DIXX
      OUTP( 6) = (XXMS1*XXLNS**2) * DIYY
      OUTP( 7) = (XXMS1*XXLNS**2) * DIZZ

      OUTP( 8) = (XXMS1*XXLNS**2) * DIXY
      OUTP( 9) = (XXMS1*XXLNS**2) * DIXZ
      OUTP(10) = (XXMS1*XXLNS**2) * DIYZ

! DISPLAY TOSS OBJECT CG LOCATION
      OUTP(11) = XXLNS * PCGB(1)
      OUTP(12) = XXLNS * PCGB(2)
      OUTP(13) = XXLNS * PCGB(3)


! STATE # OF OUTPUT ITEMS IN ABOVE ARRAY FOR USE IN XPLASO
      OUTP(25) = 13.0

! STANDARD RETURN
100   CONTINUE

      RETURN


!******************************************
!******************************************
!   WRITE LINE OF DATA TO PRINTOUT PAGE
!******************************************
!******************************************
3000  CONTINUE

      IF(NUNIT .EQ. 1) WRITE(IUOUT,55) (OUTP(J),J=1,LASITM)
      IF(NUNIT .EQ. 2) WRITE(IUOUT,56) (OUTP(J),J=1,LASITM)

! ENGLISH UNITS FORMAT
!---------------------
55    FORMAT(F10.2, F12.2, F11.3, F9.1,   &
     &              1P, 3(E13.4),3(E12.3),  0P, F8.1, F7.1, F8.1)

! METRIC  UNITS FORMAT
!---------------------
56    FORMAT(F10.2, F12.2, F11.3, F9.1,   &
     &              1P, 3(E13.4),3(E12.3),  0P, F8.2, F7.2, F8.2)

      RETURN
      END
