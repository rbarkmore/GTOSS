
INCLUDE=A_HDR
FC=gfortran
FFLAGS= 
LDFLAGS = 
CPPFLAGS=-I$(INCLUDE) 

utilfiles=\
A_UTIL/ADAM.o \
A_UTIL/CROSS.o \
A_UTIL/DAYNIT.o \
A_UTIL/DLINT.o \
A_UTIL/DMOUSE.o \
A_UTIL/DOT.o \
A_UTIL/EULICS.o \
A_UTIL/EULINT.o \
A_UTIL/EULMAT.o \
A_UTIL/LIBXYZ.o \
A_UTIL/MATDIF.o \
A_UTIL/MATEUL.o \
A_UTIL/MATFIX.o \
A_UTIL/MATMOV.o \
A_UTIL/MATMUL.o \
A_UTIL/MATSCL.o \
A_UTIL/MATSNV.o \
A_UTIL/MATSUM.o \
A_UTIL/MATVEC.o \
A_UTIL/NCROSS.o \
A_UTIL/NEWPAG.o \
A_UTIL/ORBFRM.o \
A_UTIL/RIBXYZ.o \
A_UTIL/QLTERP.o \
A_UTIL/SHOERR.o \
A_UTIL/SLINT.o \
A_UTIL/TCROSS.o \
A_UTIL/TDOT.o \
A_UTIL/TOPOGA.o \
A_UTIL/TOPOGR.o \
A_UTIL/TR3SOL.o \
A_UTIL/TR4SOL.o \
A_UTIL/TR5SOL.o \
A_UTIL/VECDIF.o \
A_UTIL/VECMAG.o \
A_UTIL/VECMAT.o \
A_UTIL/VECMOV.o \
A_UTIL/VECNRM.o \
A_UTIL/VECSCL.o \
A_UTIL/VECSUM.o \
A_UTIL/XYZLIB.o \
A_UTIL/SEEVECMAT.o \
A_UTIL/XYZRIB.o

ross0files=\
A_ROSS/R_aux/PULAXA.o \
A_ROSS/R_aux/PULAXB.o \
A_ROSS/R_aux/PULAXF.o \
A_ROSS/R_aux/PULAXG.o \
A_ROSS/R_aux/PULAXJ.o \
A_ROSS/R_aux/PULAXK.o \
A_ROSS/R_aux/PULAXT.o \
A_ROSS/R_basic/PULBSA.o \
A_ROSS/R_basic/PULBSB.o \
A_ROSS/R_basic/PULBST.o \
A_ROSS/R_exec/PULCLO.o \
A_ROSS/R_exec/PULDB.o \
A_ROSS/R_exec/PULNAM.o \
A_ROSS/R_exec/PULOPN.o \
A_ROSS/R_wild/PULBSF.o \
A_ROSS/R_wild/PULBSG.o \
A_ROSS/R_wild/PULBSJ.o \
A_ROSS/R_wild/PULBSK.o \
A_ROSS/R_wild/PULF1.o \
A_ROSS/R_wild/PULG1.o \
A_ROSS/R_wild/PULJ1.o \
A_ROSS/R_wild/PULK1.o

ross1files=\
A_ROSS/R_ascii/POPIDB.o \
A_ROSS/R_ascii/POPIDO.o \
A_ROSS/R_ascii/POPIDS.o \
A_ROSS/R_ascii/POPIDT.o

ross2files=\
A_ROSS/R_aux/POPAXA.o \
A_ROSS/R_aux/POPAXB.o \
A_ROSS/R_aux/POPAXF.o \
A_ROSS/R_aux/POPAXG.o \
A_ROSS/R_aux/POPAXJ.o \
A_ROSS/R_aux/POPAXK.o \
A_ROSS/R_aux/POPAXT.o

ross3files=\
A_ROSS/R_basic/POPBSA.o \
A_ROSS/R_basic/POPBSB.o \
A_ROSS/R_basic/POPBST.o

ross4files=\
A_ROSS/R_exec/FIXPTH.o \
A_ROSS/R_exec/GETPTH.o \
A_ROSS/R_exec/GTROOT.o \
A_ROSS/R_exec/MACPTH.o \
A_ROSS/R_exec/NEWNAM.o \
A_ROSS/R_exec/POPDB.o \
A_ROSS/R_exec/POPDBO.o \
A_ROSS/R_exec/POPID.o \
A_ROSS/R_exec/POPIDX.o \
A_ROSS/R_exec/RDBCLO.o \
A_ROSS/R_exec/RDBNME.o \
A_ROSS/R_exec/RDBOPN.o

ross5files=\
A_ROSS/R_init/RDBIC.o \
A_ROSS/R_init/RDBSET.o \
A_ROSS/R_init/RDBZZ.o

ross6files=\
A_ROSS/R_wild/POPBSF.o \
A_ROSS/R_wild/POPBSG.o \
A_ROSS/R_wild/POPBSJ.o \
A_ROSS/R_wild/POPBSK.o \
A_ROSS/R_wild/POPF1.o \
A_ROSS/R_wild/POPG1.o \
A_ROSS/R_wild/POPJ1.o \
A_ROSS/R_wild/POPK1.o

vossfiles=\
A_VOSS/V_exec/ALLCLO.o \
A_VOSS/V_exec/CRTDIS.o \
A_VOSS/V_exec/CRTPUL.o \
A_VOSS/V_exec/DECIDE.o \
A_VOSS/V_exec/INCHEV.o \
A_VOSS/V_exec/INFOPN.o \
A_VOSS/V_exec/INPULV.o \
A_VOSS/V_exec/REPOPN.o \
A_VOSS/V_exec/VTOSS.o \
A_VOSS/V_exec/VTOSUB.o \
A_VOSS/V_exec/XFCHUZ.o \
A_VOSS/V_fmts/YFMT01.o \
A_VOSS/V_fmts/YFMT02.o \
A_VOSS/V_fmts/YFMT03.o \
A_VOSS/V_fmts/YFMT04.o \
A_VOSS/V_fmts/YFMT50.o

# Declare new suffix relationship
.SUFFIXES : .o .F90 

# Specify source compilation via new suffix rule
.F90.o :
	$(FC) -s -O1 -c -o $@ $?

all: VTOSSvH12

VTOSSvH12 : A_VOSS/V_exec/VTOSS.o \
$(vossfiles) \
A_ROSS/R_exec/GETPTH.o \
A_ROSS/R_exec/GTROOT.o \
A_ROSS/R_exec/FIXPTH.o \
A_UTIL/DMOUSE.o
	$(FC) $(LDFLAGS) -o VTOSSvH12 $^

clean:
	/bin/rm -f *.o VTOSSvH12
cleanwell:
	@/bin/rm -f A_VOSS/V_exec/VTOSS.o
	@/bin/rm -f $(vossfiles)

