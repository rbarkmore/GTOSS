#!/bin/bash -ex
# -e means: exit this script if an error occurs on any command
# -x means: print each command just before it is executed, good for debugging

# ==================== INSTRUCTIONS FOR USE =======================
#
# 1. Set the directory to the one in which the GTOSS applications
#    and input data resides.
#
# 2. The applications must be named GTOSS, DTOSS, UTOSS, and VTOSS.
#
# 3. Input files must be Unix fmt (ie. \n   for line termination).
#
# 4. The verification input files, GRUN00, GRUN01, ..., GRUN35
#                                  DRUN00, DRUN01, ..., DRUN35
#                                  URUN00, URUN01, ..., URUN35
#
#    must reside in directories called: GRUNS, DRUNS, URUNS, respectively,
#    said directories, all residing in a single directory called A__RUN.
#
# The user then:
#
#   Enters 2 arguments on the command line, which are,
#
#       1. The name of the directory into which all the results 
#          of the run verifications will be archived.
#
#       2. a 4 char identification string to tag the family of results output.
#          (example H101, which will be appended to a "v", vH101)
#
#   Then,
#
#       3. makes the script file an executable Unix file (chmod +x filename).
#
#       4. Executes the script on the terminal command line.



# Arg 1 is User-defined corral name. Make the corral to contain test
#-------------------------------------------------------------------
    Test_Folder=$1
    mkdir $Test_Folder

# Arg 2 is User-defined 4 character test-family ID
#-------------------------------------------------
    TestID=$2
    echo 'Test Family ID is: '$TestID

# Determine the $working directory and save
#-----------------------------------------
    working=`pwd`
    echo 'Working directory is: ' $working
 
echo 'Step.....0'

# Within this corral, make folders to archive specific sub-results
	mkdir $working/$Test_Folder/OUTDISv$TestID
	mkdir $working/$Test_Folder/VERIFYv$TestID

echo 'Step.....1'

#Define a sequence of GTOSS verification run numbers
#---------------------------------------------------
runs="00 01 02 03 04 05 06 07 08 09 \
 	  10 11 12 13 14 15 16 17 18 19 \
 	  20 21 22 23 24 25 26 27 28 29 \
 	  30 32 33 34 35"

echo 'Step.....2'


#Fetch each verification run
#---------------------------
for xxxx in $runs ; do

echo 'Step.....3'
echo $xxxx

#move $working dir to environment for configuring run-corral
#----------------------------------------------------------
	cd $working/

#Make a corral to contain this GTOSS verification run 
#----------------------------------------------------
    RDB=RDB_$TestID_$xxxx
	mkdir $working/$Test_Folder/$RDB

echo 'Step.....4'

# Copy various GTOSS applications into the corral
#------------------------------------------------
	cp GTOSS $working/$Test_Folder/$RDB/
	cp DTOSS $working/$Test_Folder/$RDB/
	cp UTOSS $working/$Test_Folder/$RDB/
	
echo 'Step.....5'

# Copy verification run file into $working folder (w/name change)
#---------------------------------------------------------------
	cp $working/A__RUN/GRUNS/GRUN$xxxx $working/$Test_Folder/$RDB/INGOSS
	cp $working/A__RUN/DRUNS/DRUN$xxxx $working/$Test_Folder/$RDB/INDOSS
	cp $working/A__RUN/URUNS/URUN$xxxx $working/$Test_Folder/$RDB/INUOSS	

echo 'Step.....6'

#move $working dir to test apl area for run stream inheritance
#------------------------------------------------------------
	cd $working/$Test_Folder/$RDB/
	
# Run GTOSS against its input file
#---------------------------------
	$working/$Test_Folder/$RDB/GTOSS

echo 'Step.....7'

# Run DTOSS against RDB and archive result (w/name change)
#---------------------------------------------------------
	$working/$Test_Folder/$RDB/DTOSS
	mv $working/$Test_Folder/$RDB/OUTDIS ../OUTDISv$TestID/OUTDIS_$xxxx

echo 'Step.....8'

# Run UTOSS against RDB and archive result (w/name change)
#---------------------------------------------------------
	$working/$Test_Folder/$RDB/UTOSS
	mv VERIFY ../VERIFYv$TestID/V${xxxx}$TestID

echo 'Step.....9'

# Now clean up the remains
#-------------------------
	rm $working/$Test_Folder/$RDB/GTOSS
	rm $working/$Test_Folder/$RDB/DTOSS
	rm $working/$Test_Folder/$RDB/UTOSS

	rm $working/$Test_Folder/$RDB/GERROR
	rm $working/$Test_Folder/$RDB/DERROR
	rm $working/$Test_Folder/$RDB/UERROR

done


echo 'Successfully Finished'
