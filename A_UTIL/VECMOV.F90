! ROUTINE: VECMOV
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!************************************
!
      SUBROUTINE VECMOV (VA, VC)
!
!************************************
! TRANSFER OR MOVE OF AN INPUT VECTOR TO AN OUTPUT VECTOR

      include "../A_HDR/COM_ALL.h"

      DIMENSION VA(3), VC(3)

! TRANSFER THE INPUT VECTOR TO THE OUTPUT VECTOR
      VC(1) = VA(1)
      VC(2) = VA(2)
      VC(3) = VA(3)

      RETURN
      END
