! ROUTINE: VECSCL
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************
!
      SUBROUTINE VECSCL(SCL,VA, VC)
!
!*************************************
! MULTIPLY VECTOR BY A REAL SCALAR
! (CALCULATION IS PROTECTED ALLOWING INPUT TO BE OVERWRITTEN)

      include "../A_HDR/COM_ALL.h"

      DIMENSION VA(3),  VC(3)

! SCALE THE OUTPUT VECTOR BY GAIN
      VC(1) = SCL*VA(1)
      VC(2) = SCL*VA(2)
      VC(3) = SCL*VA(3)

      RETURN
      END
