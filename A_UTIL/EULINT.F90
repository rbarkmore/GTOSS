! ROUTINE: EULINT
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!***********************************************
!
      SUBROUTINE EULINT (DELT, N, DYDX,  Y)
!
!***********************************************
! THIS SUBROUTINE PERFORMS AN EULER INTEGRATION OF
! "N" FIRST ORDER DIFF EQNS WITH DERIVATIVES DYDX,
! INTO VARIABLES Y.

      include "../A_HDR/COM_ALL.h"

      DIMENSION DYDX(N), Y(N)

! PERFORM INTEGRATION
      DO 10 J = 1,N
         Y(J) = Y(J) + DYDX(J)*DELT
  10  CONTINUE

      RETURN
      END
