! ROUTINE: CRTHD
! %Z%GTOSS %M% H.11 code v01.04
!              H.11 code v01.04 Added output option 4
!----------------------------------------------------------------------
!              H.10 code v01.03 Upped display resolution on tens/range
!                               and added Y-End tension display.
!                               Added tension displays for tether 2.
!                               Added stop logic for max tens specif by user.
!                               Added selectable CRT display formats.
!                               Increased time display width.
!                               Replaced RPGIM->RPGBI (cosmetic consistency).
!---------------------------------------------------------------------------
!----------------------------------------------------------------------------
!              H.5 code v01.02 modified CRT display for interactive ops
!                              Eliminated RPRTD in lie of RTDALL
!----------------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!*****************************
!*****************************
!
      SUBROUTINE CRTHD
!
!*****************************
!*****************************
! THIS SUBROUTINE PUTS A HEADING ON THE CRT OUTPUT

! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS

      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_HOST.h"
      include "../../A_HDR/EQU_HOST.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.h"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.h"




      DIMENSION UDUMR(3), RAP_DUM(3), RS_DUM(3), RI_DUM(3), RL_DUM(3)

! DO LOGIC TO PUT HEADING ON CRT DISPLAY
!---------------------------------------
      IF(NCRT .NE. 0) GO TO 50

! PUT AN IDENTIFIER HEADER OUT TO CRT DEPENDING UPON USER SELECTION
          IF(PCRTFMT .EQ. 0.0) WRITE (IOUCRO,11)
          IF(PCRTFMT .EQ. 1.0) WRITE (IOUCRO,22)
          IF(PCRTFMT .EQ. 2.0) WRITE (IOUCRO,33)
          IF(PCRTFMT .EQ. 3.0) WRITE (IOUCRO,44)
          IF(PCRTFMT .EQ. 4.0) WRITE (IOUCRO,55)
          IF(PCRTFMT .EQ. 5.0) WRITE (IOUCRO,66)
          IF(PCRTFMT .EQ. 6.0) WRITE (IOUCRO,77)
          IF(PCRTFMT .EQ. 7.0) WRITE (IOUCRO,88)

! LAYOUT FOR FMT OPTION 0 (DEFAULT)
! NOMINAL DEFAULT CRT FORMAT
   11     FORMAT (/   &
     &      '     TIME    RP-ALT  RP-VELI  LONGI LATI    RANGE-2',   &
     &      '   RNGD-2  ILIB-2  OLIB-2   XE-TETH_1    YE-TETH_1   ', &
     &                                  'XE-TETH_2    YE-TETH_2'/   &
     &      '      SEC     (FT)    (FT/S)   DEG   DEG      (FT) ',   &
     &      '   (FT/S)    DEG     DEG       (LBS)        (LBS)     ',&
     &                                     '  (LBS)        (LBS) ')



! LAYOUT FOR FMT OPTION 1
! CRT FORMAT FOR MULTI-OBJECTS AND TETHERS
   22     FORMAT (/'             <------------TETHER 1---------->  ',&
     &         '<------------TETHER 2---------->  <------------',   &
     &         'TETHER 3---------->   <-----LIBRATION ANG (DEG)---->'/&
     &        '      TIME   XE_TEN  YE_TEN   AP_RANG  RANG-D  XE_TEN',&
     &         '  YE_TEN   AP_RANG  RANG-D  XE_TEN  YE_TEN   ',   &
     &         'AP_RANG  RANG-D   OBJECT-2   OBJECT-3   OBJECT-4'/   &
     &         '       SEC    (LBS)   (LBS)      (FT)   (FPS)   ',   &
     &         '(LBS)   (LBS)      (FT)   (FPS)   (LBS)   (LBS)',   &
     &         '      (FT)   (FPS)    IP   OP    IP   OP    IP   OP')



! LAYOUT FOR FMT OPTION 2
!            <------------------ OBJECT 2 ---------------->   <------------------ OBJECT 3 ----------------->   <------------- TETHER 1 ---------->   INTEG
!     TIME       ALT        HD   VH_RP  E_LONG  E_LAT  MASS       ALT        HD   VH_RP   E_LONG  E_LAT  MASS    X-TENS   Y-TENS    DEP-L    DEP-LD    STEP
!      SEC      (FT)      (F/S)  (F/S)   (DEG)  (DEG)  (SG)      (FT)      (F/S)  (F/S)    (DEG)  (DEG)  (SG)     (LBS)    (LBS)    (FT)      (F/S)   (SEC)
!
!         0.  117342665.  -115.0      0.    .00    .00  100.  117408265.     4.0      0.    .00    .00   5000.      .00     .00      65600.  119.00  1.0E-03

! CRT FORMAT FOR DUAL-OBJECTS AND ONE TETHER FMT
   33     FORMAT (/'            <------------------ OBJECT 2 -----',  &
     &             '----------->   <------------------ OBJECT 3 ----',&
     &             '------------->   <------------- TETHER 1 -------',&
     &             '--->   INTEG'/   &
     &             '     TIME       ALT        HD   VH_RP  E_LONG ',  &
     &             ' N_LAT  MASS       ALT        HD   VH_RP   E_LON',&
     &            'G  N_LAT  MASS    X-TENS   Y-TENS    DEP-L    DEP',&
     &             '-LD    STEP'/   &
     &             '      SEC      (FT)      (F/S)  (F/S)   (DEG) ',  &
     &             ' (DEG)  (SG)      (FT)      (F/S)  (F/S)    (DEG',&
     &             ')  (DEG)  (SG)     (LBS)    (LBS)    (FT)      (',&
     &             'F/S)   (SEC)')



! LAYOUT FOR FMT OPTION 3
!             <----------------- MAINSTAYS --------------->       <-------------- MOVE LINES ------------->      <-------------- PULL-IN ------------->
!     TIME       NE       SE     MASTER        SW       NW         NE       SE      MASTER      SW       NW         NE      SE    MASTER     SW     NW
!      SEC      (KG)     (KG)     (KG)        (KG)     (KG)       (KG)     (KG)      (KG)      (KG)     (KG)       DEG     DEG      DEG     DEG    DEG
!
!         0.   16174.    9080.     8841.      9080.   16174.      1221.    1221.        0.     1221.    1221.        0.      0.      0.      0.      0.
!               F9.0,    F9.0,    F10.0,     F11.0,    F9.0,     F11.0,    F9.0,    F10.0,    F10.0,    F9.0,    F10.0,   F8.0,   F8.0,   F8.0,    F8.0
!

! MULTIPLE TETHER/ELEV ANGLE REPORT FORMAT (FOR SKYDER LOGGING SYSTEM)
   44     FORMAT (/'             <----------------- MAINSTAYS ------',&
     & '--------->       <-------------- MOVE LINES ------------->  ',&
     & '    <-------------- PULL-IN ------------->'/   &
     &             '     TIME       NE       SE     MASTER        SW',&
     & '       NW         NE       SE      MASTER      SW       NW  ',&
     & '       NE      SE    MASTER     SW     NW'/   &
     &             '      SEC      (KG)     (KG)     (KG)        (KG',&
     & ')     (KG)       (KG)     (KG)      (KG)      (KG)     (KG) ',&
     & '      DEG     DEG      DEG     DEG    DEG')

!     TIME   NE TEN   SE TEN   MAST TEN    SW TEN   NW TEN    TEN_BL   NE-MOVL  SE-MOVL   MAS-MOVL  SW-MOVL  NW-MOVL   PUL-NE  PUL-SE  PUL-MS  PUL-SW  PUL-NW
!      SEC     (KG)     (KG)     (KG)        (KG)     (KG)     (KG)      (KG)     (KG)      (KG)      (KG)     (KG)      DEG     DEG      DEG     DEG    DEG
!
!         0.  12000.   12000.    12000.     12000.   12000.    12000.   12000.   12000.    12000.    12000.   12000.      XX.     XX.     XX.     XX.     XX.
!    XXXXXXX   F8.0,    F9.0,    F10.0,     F11.0,    F9.0,    F10.0,    F9.0,    F9.0,    F10.0,     F10.0,   F9.0,    F9.0,   4F8.0

! MULTIPLE TETHER/ELEV ANGLE REPORT FORMAT (FOR NO STEP LOGGING SYSTEM)
!   44     FORMAT (/'     TIME   NE TEN   SE TEN   MAST TEN    ',
!     1                  'SW TEN   NW TEN    TEN_BL   NE-MOVL  ',
!     2                  'SE-MOVL   MAS-MOVL  SW-MOVL  NW-MOVL ',
!     3                  '  PUL-NE  PUL-SE  PUL-MS  PUL-SW  PUL-NW'/
!     4             '      SEC     (KG)     (KG)     (KG)      ',
!     5                  '  (KG)     (KG)     (KG)      (KG)   ',
!     6                  '  (KG)      (KG)      (KG)     (KG)  ',
!     7                  '    DEG     DEG      DEG     DEG    DEG')



! LAYOUT FOR FMT OPTION 4

!            <-- MOVE HEAD POSITION -->   <------------ MOVE LINE TENSIONS -------------->   <---------- MOVE LINE DEPLOY-RATES ----------->   <-------- MOVE LINE DEPLOYED LENGTH  -------->
!     TIME     NORTH    EAST     ALT         LIFT     NW      NE      SE      MS      SW        LIFT     NW      NE      SE      MS      SW       LIFT     NW      NE      SE      MS      SW
!    (SEC)      (M)      (M)     (M)         (KG)    (KG)    (KG)    (KG)    (KG)    (KG)      (M/S)   (M/S)   (M/S)   (M/S)   (M/S)   (M/S)       (M)    (M)     (M)     (M)     (M)     (M)
!
!     0.00      2.4      0.0    400.0        5996.   1824.   1824.   2532.    481.   2532.      0.00    0.00    0.00    0.00    0.00    0.00    0000.0  0000.0  0000.0  0000.0  0000.0  0000.0


! SPECIAL MOVE-HEAD CONTROL FORMAT (FOR SKYDER LOGGING)
   55     FORMAT (/'            <-- MOVE HEAD POSITION -->   <------',&
     &             '------ MOVE LINE TENSIONS -------------->   <---',&
     &             '------- MOVE LINE DEPLOY-RATES ----------->'/ &
     &             '     TIME     NORTH    EAST     ALT         LIFT',&
     &             '     NW      NE      SE      MS      SW        L',&
     &             'IFT     NW      NE      SE      MS      SW'/  &
     &             '    (SEC)      (M)      (M)     (M)         (KG)',&
     &             '    (KG)    (KG)    (KG)    (KG)    (KG)      (M',&
     &             '/S)   (M/S)   (M/S)   (M/S)   (M/S)   (M/S)'/)





! LAYOUT FOR FMT OPTION 5

!            <-- MOVE HEAD POSITION -->   <------------ MOVE LINE TENSIONS -------------->   <---------- MOVE LINE DEPLOY-RATES ----------->   <-------- MOVE LINE DEPLOYED LENGTH  -------->
!     TIME     NORTH    EAST     ALT         LIFT     NW      NE      SE      MS      SW        LIFT     NW      NE      SE      MS      SW       LIFT     NW      NE      SE      MS      SW
!    (SEC)      (FT)    (FT)    (FT)         (LB)    (LB)    (LB)    (LB)    (LB)    (LB)      (F/S)   (F/S)   (F/S)   (F/S)   (F/S)   (F/S)      (FT)    (FT)    (FT)    (FT)    (FT)    (FT)
!
!     0.00      2.4      0.0    400.0        5996.   1824.   1824.   2532.    481.   2532.      0.00    0.00    0.00    0.00    0.00    0.00    0000.0  0000.0  0000.0  0000.0  0000.0  0000.0


! SPECIAL MOVE-HEAD CONTROL FORMAT (FOR SKYDER LOGGING)
   66     FORMAT (/'            <-- MOVE HEAD POSITION -->   <------',&
     &             '------ MOVE LINE TENSIONS -------------->   <---',&
     &             '------- MOVE LINE DEPLOY-RATES ----------->'/ &
     &             '     TIME     NORTH    EAST     ALT         LIFT',&
     &             '     NW      NE      SE      MS      SW        L',&
     &             'IFT     NW      NE      SE      MS      SW'/  &
     &             '    (SEC)      (FT)    (FT)    (FT)         (LB)',&
     &             '    (LB)    (LB)    (LB)    (LB)    (LB)      (F',&
     &             '/S)   (F/S)   (F/S)   (F/S)   (F/S)   (F/S)'/)





! LAYOUT FOR FMT OPTION 6

!                                        <-- NE STAY -->    <-- SE STAY -->    <-- MS STAY -->    <-- SW STAY -->    <-- NW STAY -->
!     TIME   BALL-LN   LIFT-LN  LOG-LN    UPPER   LOWER      UPPER   LOWER      UPPER   LOWER      UPPER   LOWER      UPPER    LOWER
!    (SEC)     (KG)     (KG)    (KG)       (KG)    (KG)       (KG)    (KG)       (KG)    (KG)       (KG)    (KG)       (KG)     (KG)
!
!     0.00    XXXXX.    XXXXX.  XXXXX.    XXXXX.  XXXXX.     XXXXX.  XXXXX.     XXXXX.  XXXXX.     XXXXX.  XXXXX.     XXXXX.   XXXXX.


! SPECIAL MOVE-HEAD CONTROL FORMAT (FOR SKYDER LOGGING)
   77     FORMAT (/'                                        <-- NE', &
     &             ' STAY -->    <-- SE STAY -->    <-- MS STAY --', &
     &             ' >    <-- SW STAY -->    <-- NW STAY -->'/ &
     &             '     TIME   BALL-LN   LIFT-LN  LOG-LN    UPPER', &
     &             '    LOWER      UPPER   LOWER      UPPER   LOWE', &
     &             'R      UPPER   LOWER      UPPER    LOWER'/  &
     &             '    (SEC)     (KG)     (KG)    (KG)       (KG)', &
     &             '     (KG)       (KG)    (KG)       (KG)    (KG', &
     &             ')       (KG)    (KG)       (KG)     (KG)'/)




! LAYOUT FOR FMT OPTION 7
!             <------- MAINSTAY TENSION ------->   CLIMB-LINE  BALLOON-LN    <------- APEX POS. ------>  <----- BALLOON POS. ----->
!     TIME       NE       SE       SW       NW       TENSION    TENSION        NORTH     EAST      ALT     NORTH     EAST      ALT
!      SEC      (KG)     (KG)     (KG)     (KG)       (KG)        (KG)          (M)      (M)       (M)      (M)      (M)       (M)
!
!         0.   16174.   9080.    8841.     9080.       6174.      1221.       1221.2   2340.5    1221.3   1221.2   2340.5    1221.3
!               F9.0,   F8.0,    F9.0,    F10.0,      F12.0,     F11.0,       F13.1,    F9.1,   F10.1,    F9.1,     F9.1,    F10.1,
!

! 2008 SPACE ELEV GAMES BALLOON MOORING (FOR SPACEWARD FOUNDATION)
   88     FORMAT (/'             <------- MAINSTAY TENSION ------->', &
     &             '   CLIMB-LINE  BALLOON-LN    <------- APEX POS.', &
     &             ' ------>  <----- BALLOON POS. ----->'/   &
     &             '     TIME       NE       SE       SW       NW  ', &
     &             '     TENSION    TENSION        NORTH     EAST  ', &
     &             '    ALT     NORTH     EAST      ALT '/   &
     &             '      SEC      (KG)     (KG)     (KG)     (KG) ', &
     &             '      (KG)        (KG)          (M)      (M)   ', &
     &             '    (M)      (M)      (M)       (M) ')





! SET HEADER DISPLAY FLAG SO HEADER WON'T REPEAT
!----------------------------------------------
          NCRT = 1

!**************************************************
!--------------------------------------------------
!**************************************************
! ALLOW USER TO DECIDE HOW OFTEN OUTPUT GOES TO CRT
!**************************************************
!--------------------------------------------------
!**************************************************
50     IF(NBCRT .GE. NINT(BATGON)) THEN
            NBCRT = 0

! BUMP CRT COUNTER AND PUT OUT DATA LINE
            NCRT = NCRT + 1




!************************************************
!************************************************
!-----------------------------------------------
! SEE IF STANDARD CRT DISPLAY FORMAT IS SELECTED
!-----------------------------------------------
!************************************************
!************************************************
            IF(PCRTFMT .EQ. 0.0) THEN

! USE EULER ANGLE TYPE SPECIFIED BY JEUL
                CALL MATMUL(2,RPGBI, RPGOI,   TOSMX1 )
                CALL MATEUL (JEUL,TOSMX1,  PHODUM,THODUM,PSODUM)

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
                CALL TOSLDW(2)
                CALL XYZLIB(1,RBOR(1),RBOR(2),RBOR(3), ELDUM,AZDUM)

! OUTPUT THIS DATA
                WRITE (IOUCRO,30) T, RFPALT, RPVELI, DEGMUI, DEGLAI,&
     &                            RANGE, RANGED,   &
     &                            RTDALL*ELDUM, RTDALL*AZDUM,   &
     &                            XELOAD(1), YELOAD(1),   &
     &                            XELOAD(2), YELOAD(2)

30      FORMAT(/F11.2,F10.0,F7.0,F6.0,F6.1,F13.0,F7.1,F8.1,F7.1,4F13.4)

                IF(NCRT .GT. 9) NCRT = 0

            END IF


!****************************************************************
!****************************************************************
!-------------------------------------------------------------
! SEE IF MULTIPLE OBJECT/TETHER CRT DISPLAY FORMAT IS SELECTED
!-------------------------------------------------------------
!****************************************************************
!****************************************************************
           IF(PCRTFMT .EQ. 1.0) THEN

! CALC LIBRATION ANGLES WR/T REF PT FOR OBJECTS
               ELDUM2 = 0.0
               AZDUM2 = 0.0

               ELDUM3 = 0.0
               AZDUM3 = 0.0

               ELDUM4 = 0.0
               AZDUM4 = 0.0

               IF(LASOBJ .GE. 2) THEN

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
                  CALL TOSLDW(2)
                  CALL XYZLIB(1,RBOR(1),RBOR(2),RBOR(3),ELDUM2,AZDUM2)
               END IF

               IF(LASOBJ .GE. 3) THEN

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
                  CALL TOSLDW(3)
                  CALL XYZLIB(1,RBOR(1),RBOR(2),RBOR(3),ELDUM3,AZDUM3)
               END IF

               IF(LASOBJ .GE. 4) THEN
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
                  CALL TOSLDW(4)
                  CALL XYZLIB(1,RBOR(1),RBOR(2),RBOR(3),ELDUM4,AZDUM4)
               END IF

! EXTRACT ATTACH POINT RANGES AND RANGE RATES
               DUMR1  = TDIST(1)
               DUMRD1 = TDISTD(1)

               DUMR2  = TDIST(2)
               DUMRD2 = TDISTD(2)

               DUMR3  = TDIST(3)
               DUMRD3 = TDISTD(3)

! OUTPUT THIS DATA
               WRITE (IOUCRO,40) T,   &
     &                XELOAD(1), YELOAD(1), DUMR1, DUMRD1,   &
     &                XELOAD(2), YELOAD(2), DUMR2, DUMRD2,   &
     &                XELOAD(3), YELOAD(3), DUMR3, DUMRD3,   &
     &                RTDALL*ELDUM2, RTDALL*AZDUM2,   &
     &                RTDALL*ELDUM3, RTDALL*AZDUM3,   &
     &                RTDALL*ELDUM4, RTDALL*AZDUM4

40      FORMAT(/F11.1, 3(F8.0,F8.0,F11.0,F7.1), F7.0,F5.0, 2(F6.0,F5.0))

               IF(NCRT .GT. 9) NCRT = 0

            END IF




!****************************************************************
!****************************************************************
!----------------------------------------------------------------
! SEE IF DUAL OBJECT AND SINGLE TETHER DISPLAY FORMAT IS SELECTED
!----------------------------------------------------------------
!****************************************************************
!****************************************************************
            IF(PCRTFMT .EQ. 2.0) THEN

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
                CALL TOSLDW(2)

                DUMH2   = ALT

! CALC ALTITUDE RATE OF OBJECT
                CALL VECNRM(RI,UDUMR)
                DUMHD2 = DOT(UDUMR,RID)

                DUMV2   = SQRT(RBORD(1)**2 +RBORD(2)**2)
                DUMMUE2 = DMUE*RTDALL
                DUMLAE2 = DLAE*RTDALL
                DUMAS2  = DMASS

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
                CALL TOSLDW(3)

                DUMH3   = ALT

! CALC ALTITUDE RATE OF OBJECT
                CALL VECNRM(RI,UDUMR)
                DUMHD3 = DOT(UDUMR,RID)

                DUMV3   = SQRT(RBORD(1)**2 +RBORD(2)**2)
                DUMMUE3 = DMUE*RTDALL
                DUMLAE3 = DLAE*RTDALL
                DUMAS3  = DMASS

! OUTPUT THIS DATA
               WRITE (IOUCRO,60) T,DUMH2,DUMHD2,DUMV2,DUMMUE2,DUMLAE2,&
     &                             DUMAS2,   &
     &                             DUMH3,DUMHD3,DUMV3,DUMMUE3,DUMLAE3,&
     &                             DUMAS3,   &
     &                      XELOAD(1), YELOAD(1), TLENT(1), TLENTD(1),&
     &                      RPDELT

60      FORMAT(/F11.0,  F12.0, F8.1, F8.0, F7.2, F7.2, F6.0,   &
     &                  F12.0, F8.1, F8.0, F7.2, F7.2, F8.0,   &
     &                  F9.2, F8.2, F12.0, F8.2,  1PE9.1)

                IF(NCRT .GT. 9) NCRT = 0

            END IF


!*********************************************
!---------------------------------------------
! SEE IF ITS MULTI-TETHER OUTPUT FOR THIS RUN
!---------------------------------------------
!*********************************************
            IF(PCRTFMT .EQ. 3.0) THEN

                DUM_NE   = 0.45359 * XELOAD(1)
                DUM_SE   = 0.45359 * XELOAD(2)
                DUM_MS   = 0.45359 * XELOAD(6)
                DUM_SW   = 0.45359 * XELOAD(3)
                DUM_NW   = 0.45359 * XELOAD(4)

                DUM_BL   = 0.45359 * XELOAD(5)

                DUM_NEML   = 0.45359 * XELOAD(10)
                DUM_SEML   = 0.45359 * XELOAD(17)
                DUM_MSML   = 0.45359 * XELOAD(15)
                DUM_SWML   = 0.45359 * XELOAD(18)
                DUM_NWML   = 0.45359 * XELOAD(9)

! CALCULATE LOWER NE MAINSTAY PULL-IN ANGLE
!------------------------------------------
! GET ATT PT STATE FOR REF PT, ATT PT 1 (INER FRAME)
                RAP_DUM(1) = APS( 7, 1, 1)
                RAP_DUM(2) = APS( 8, 1, 1)
                RAP_DUM(3) = APS( 9, 1, 1)

! GET NE SHEAVE-OBJECT POSITION STATE WR/T REF PT (INER FRAME)
                RS_DUM(1) = APS( 7, 2, 6)
                RS_DUM(2) = APS( 8, 2, 6)
                RS_DUM(3) = APS( 9, 2, 6)

! FIND VECTOR FROM GROUND ATT PT TO SHEAVE OBJECT
                CALL VECDIF(RS_DUM, RAP_DUM,   RI_DUM)

! TRANSFORM THIS TO REF PT TOPO FRAME
                CALL MATVEC(1, RPGIL,RI_DUM,   RL_DUM)

! CALC THE ELEVATION ANGLE
                DUMPLN = SQRT(RL_DUM(1)**2 + RL_DUM(2)**2)
                DUM_VERT_ANG = RTDALL*ATAN(ABS(RL_DUM(3))/DUMPLN)

                DUM_ELV_NE = DUM_VERT_ANG
                DUM_PUL_NE = 45.0 - DUM_ELV_NE


! CALCULATE LOWER SE MAINSTAY PULL-IN ANGLE
!------------------------------------------
! GET ATT PT STATE FOR REF PT, ATT PT 2 (INER FRAME)
                RAP_DUM(1) = APS( 7, 2, 1)
                RAP_DUM(2) = APS( 8, 2, 1)
                RAP_DUM(3) = APS( 9, 2, 1)

! GET SE SHEAVE-OBJECT POSITION STATE WR/T REF PT (INER FRAME)
                RS_DUM(1) = APS( 7, 2, 7)
                RS_DUM(2) = APS( 8, 2, 7)
                RS_DUM(3) = APS( 9, 2, 7)

! FIND VECTOR FROM GROUND ATT PT TO SHEAVE OBJECT
                CALL VECDIF(RS_DUM, RAP_DUM,   RI_DUM)

! TRANSFORM THIS TO REF PT TOPO FRAME
                CALL MATVEC(1, RPGIL,RI_DUM,   RL_DUM)

! CALC THE ELEVATION ANGLE
                DUMPLN = SQRT(RL_DUM(1)**2 + RL_DUM(2)**2)
                DUM_VERT_ANG = RTDALL*ATAN(ABS(RL_DUM(3))/DUMPLN)

                DUM_ELV_SE = DUM_VERT_ANG
                DUM_PUL_SE = 45.0 - DUM_ELV_SE


! CALCULATE LOWER SW MAINSTAY PULL-IN ANGLE
!------------------------------------------
! GET ATT PT STATE FOR REF PT, ATT PT 3 (INER FRAME)
                RAP_DUM(1) = APS( 7, 3, 1)
                RAP_DUM(2) = APS( 8, 3, 1)
                RAP_DUM(3) = APS( 9, 3, 1)

! GET SW SHEAVE-OBJECT POSITION STATE WR/T REF PT (INER FRAME)
                RS_DUM(1) = APS( 7, 2, 8)
                RS_DUM(2) = APS( 8, 2, 8)
                RS_DUM(3) = APS( 9, 2, 8)

! FIND VECTOR FROM GROUND ATT PT TO SHEAVE OBJECT
                CALL VECDIF(RS_DUM, RAP_DUM,   RI_DUM)

! TRANSFORM THIS TO REF PT TOPO FRAME
                CALL MATVEC(1, RPGIL,RI_DUM,   RL_DUM)

! CALC THE ELEVATION ANGLE
                DUMPLN = SQRT(RL_DUM(1)**2 + RL_DUM(2)**2)
                DUM_VERT_ANG = RTDALL*ATAN(ABS(RL_DUM(3))/DUMPLN)

                DUM_ELV_SW = DUM_VERT_ANG
                DUM_PUL_SW = 45.0 - DUM_ELV_SW



! CALCULATE LOWER NW MAINSTAY PULL-IN ANGLE
!------------------------------------------
! GET ATT PT STATE FOR REF PT, ATT PT 4 (INER FRAME)
                RAP_DUM(1) = APS( 7, 4, 1)
                RAP_DUM(2) = APS( 8, 4, 1)
                RAP_DUM(3) = APS( 9, 4, 1)

! GET NE SHEAVE-OBJECT POSITION STATE WR/T REF PT (INER FRAME)
                RS_DUM(1) = APS( 7, 2, 9)
                RS_DUM(2) = APS( 8, 2, 9)
                RS_DUM(3) = APS( 9, 2, 9)

! FIND VECTOR FROM GROUND ATT PT TO SHEAVE OBJECT
                CALL VECDIF(RS_DUM, RAP_DUM,   RI_DUM)

! TRANSFORM THIS TO REF PT TOPO FRAME
                CALL MATVEC(1, RPGIL,RI_DUM,   RL_DUM)

! CALC THE ELEVATION ANGLE
                DUMPLN = SQRT(RL_DUM(1)**2 + RL_DUM(2)**2)
                DUM_VERT_ANG = RTDALL*ATAN(ABS(RL_DUM(3))/DUMPLN)

                DUM_ELV_NW = DUM_VERT_ANG
                DUM_PUL_NW = 45.0 - DUM_ELV_NW


! CALCULATE LOWER MASTER MAINSTAY PULL-IN ANGLE
!----------------------------------------------
! GET ATT PT STATE FOR REF PT, ATT PT 5 (INER FRAME)
                RAP_DUM(1) = APS( 7, 5, 1)
                RAP_DUM(2) = APS( 8, 5, 1)
                RAP_DUM(3) = APS( 9, 5, 1)

! GET NE SHEAVE-OBJECT POSITION STATE WR/T REF PT (INER FRAME)
                RS_DUM(1) = APS( 7, 2, 10)
                RS_DUM(2) = APS( 8, 2, 10)
                RS_DUM(3) = APS( 9, 2, 10)

! FIND VECTOR FROM GROUND ATT PT TO SHEAVE OBJECT
                CALL VECDIF(RS_DUM, RAP_DUM,   RI_DUM)

! TRANSFORM THIS TO REF PT TOPO FRAME
                CALL MATVEC(1, RPGIL,RI_DUM,   RL_DUM)

! CALC THE ELEVATION ANGLE
                DUMPLN = SQRT(RL_DUM(1)**2 + RL_DUM(2)**2)
                DUM_VERT_ANG = RTDALL*ATAN(ABS(RL_DUM(3))/DUMPLN)

                DUM_ELV_MAS = DUM_VERT_ANG
                DUM_PUL_MAS = 47.405 - DUM_ELV_MAS

! OUTPUT THIS DATA
             WRITE (IOUCRO,70) T,DUM_NE,DUM_SE,DUM_MS,DUM_SW,DUM_NW, &
!cc     2                              DUM_BL,   &
     &           DUM_NEML, DUM_SEML, DUM_MSML, DUM_SWML, DUM_NWML,   &
     &           DUM_PUL_NE, DUM_PUL_SE, DUM_PUL_MAS,   &
     &           DUM_PUL_SW,DUM_PUL_NW

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!@VAX70      FORMAT(/F11.0,  F9.0, F9.0, F10.0, F11.0, F9.0, F11.0, F9.0,
!@VAX70     1        F10.0, F10.0, F9.0, F10.0, F8.0, F8.0, F8.0, F8.0)
70      FORMAT(/F11.0,  F9.0, F9.0, F10.0, F11.0, F9.0, F11.0, F9.0,   &
     &          F10.0, F10.0, F9.0, F10.0, F8.0, F8.0, F8.0, F8.0)
!@DOS70      FORMAT(/F11.0,  F9.0, F9.0, F10.0, F11.0, F9.0, F11.0, F9.0,
!@DOS70     1        F10.0, F10.0, F9.0, F10.0, F8.0, F8.0, F8.0, F8.0)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
!@MAC70      FORMAT(/F11.0,  F9.0, F9.0, F10.0, F11.0, F9.0, F11.0, F9.0,
!@MAC70     1        F10.0, F10.0, F9.0, F10.0, F8.0, F8.0, F8.0, F8.0)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@

                IF(NCRT .GT. 9) NCRT = 0

            END IF





!*********************************************
!---------------------------------------------
! SEE IF ITS MULTI-TETHER OUTPUT FOR THIS RUN (METRIC UNITS)
!---------------------------------------------
!*********************************************
            IF(PCRTFMT .EQ. 4.0) THEN

! DEFINE MOVE-HEAD POSITION
!--------------------------
! EXTRACT COORDINATES
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
      CALL TOSLDW(4)

! TRANSFORM THE OBJECT INER POS COORDS TO REF PT TOPO FRAME
      CALL MATVEC(1, RPGIL,RBI,   RL_DUM)

      DUM_NOR =  0.3048 * RL_DUM(1)
      DUM_EAS =  0.3048 * RL_DUM(2)
      DUM_ALT = -0.3048 * RL_DUM(3)

! TRANSFORM THE OBJECT INER VELOCITY TO REF PT TOPO FRAME
!      CALL MATVEC(1, RPGIL,RBID,   RL_DUM)
!
!      HMOV_ALTD = - RL_DUM(3)
!      HMOV_NORD = RL_DUM(1)
!      HMOV_EASD = RL_DUM(2)

! SET IN VARIOUS LINES MOVE LINE TENSIONS (IN KG)
      DUMT_LL     = 0.45359 * XELOAD(7)
      DUMT_NWML   = 0.45359 * XELOAD(9)
      DUMT_NEML   = 0.45359 * XELOAD(10)
      DUMT_SEML   = 0.45359 * XELOAD(17)
      DUMT_MSML   = 0.45359 * XELOAD(15)
      DUMT_SWML   = 0.45359 * XELOAD(18)

! SET IN VARIOUS LINES MOVE LINE DEPLOY RATES
      DUMLD_LL     = 0.3048 * TLENTD(7)
      DUMLD_NWML   = 0.3048 * TLENTD(9)
      DUMLD_NEML   = 0.3048 * TLENTD(10)
      DUMLD_SEML   = 0.3048 * TLENTD(17)
      DUMLD_MSML   = 0.3048 * TLENTD(15)
      DUMLD_SWML   = 0.3048 * TLENTD(18)

! SET IN VARIOUS LINES MOVE LINE DEPLOYED LENGTHS
      DUML_LL     = 0.3048 * TLENT(7)
      DUML_NWML   = 0.3048 * TLENT(9)
      DUML_NEML   = 0.3048 * TLENT(10)
      DUML_SEML   = 0.3048 * TLENT(17)
      DUML_MSML   = 0.3048 * TLENT(15)
      DUML_SWML   = 0.3048 * TLENT(18)

! OUTPUT THIS DATA
             WRITE (IOUCRO,80) T, DUM_NOR, DUM_EAS, DUM_ALT,         &

     &                            DUMT_LL, DUMT_NWML, DUMT_NEML,     &
     &                            DUMT_SEML, DUMT_MSML, DUMT_SWML,   &

     &                            DUMLD_LL, DUMLD_NWML, DUMLD_NEML,  &
     &                            DUMLD_SEML, DUMLD_MSML, DUMLD_SWML

!     &                            DUML_LL, DUML_NWML, DUML_NEML,     &
!     &                            DUML_SEML, DUML_MSML, DUML_SWML

80      FORMAT( F9.2,  3F9.1, 4X,  F9.0,5F8.0, 2X,  6F8.2, 2X, 6F8.1)

                IF(NCRT .GT. 19) NCRT = 0

            END IF





!*********************************************
!---------------------------------------------
! SEE IF ITS MULTI-TETHER OUTPUT FOR THIS RUN (ENGLISH UNITS)
!---------------------------------------------
!*********************************************
            IF(PCRTFMT .EQ. 5.0) THEN

! DEFINE MOVE-HEAD POSITION
!--------------------------
! EXTRACT COORDINATES
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
      CALL TOSLDW(4)

! TRANSFORM THE OBJECT INER POS COORDS TO REF PT TOPO FRAME
      CALL MATVEC(1, RPGIL,RBI,   RL_DUM)

      DUM_NOR =   RL_DUM(1)
      DUM_EAS =   RL_DUM(2)
      DUM_ALT = - RL_DUM(3)

! TRANSFORM THE OBJECT INER VELOCITY TO REF PT TOPO FRAME
!      CALL MATVEC(1, RPGIL,RBID,   RL_DUM)
!
!      HMOV_ALTD = - RL_DUM(3)
!      HMOV_NORD = RL_DUM(1)
!      HMOV_EASD = RL_DUM(2)

! SET IN VARIOUS LINES MOVE LINE TENSIONS (IN KG)
      DUMT_LL     = XELOAD(7)
      DUMT_NWML   = XELOAD(9)
      DUMT_NEML   = XELOAD(10)
      DUMT_SEML   = XELOAD(17)
      DUMT_MSML   = XELOAD(15)
      DUMT_SWML   = XELOAD(18)

! SET IN VARIOUS LINES MOVE LINE DEPLOY RATES
      DUMLD_LL     = TLENTD(7)
      DUMLD_NWML   = TLENTD(9)
      DUMLD_NEML   = TLENTD(10)
      DUMLD_SEML   = TLENTD(17)
      DUMLD_MSML   = TLENTD(15)
      DUMLD_SWML   = TLENTD(18)

! SET IN VARIOUS LINES MOVE LINE DEPLOYED LENGTHS
      DUML_LL     = TLENT(7)
      DUML_NWML   = TLENT(9)
      DUML_NEML   = TLENT(10)
      DUML_SEML   = TLENT(17)
      DUML_MSML   = TLENT(15)
      DUML_SWML   = TLENT(18)

! OUTPUT THIS DATA
             WRITE (IOUCRO,89) T, DUM_NOR, DUM_EAS, DUM_ALT,         &

     &                            DUMT_LL, DUMT_NWML, DUMT_NEML,     &
     &                            DUMT_SEML, DUMT_MSML, DUMT_SWML,   &

     &                            DUMLD_LL, DUMLD_NWML, DUMLD_NEML,  &
     &                            DUMLD_SEML, DUMLD_MSML, DUMLD_SWML

!     &                            DUML_LL, DUML_NWML, DUML_NEML,     &
!     &                            DUML_SEML, DUML_MSML, DUML_SWML

89      FORMAT( F9.2,  3F9.1, 4X,  F9.0,5F8.0, 2X,  6F8.2, 2X, 6F8.1)

                IF(NCRT .GT. 19) NCRT = 0

            END IF




!*********************************************
!---------------------------------------------
! SEE IF ITS MULTI-TETHER OUTPUT FOR THIS RUN (ENGLISH UNITS)
!---------------------------------------------
!*********************************************
            IF(PCRTFMT .EQ. 6.0) THEN

              DUM_T_BALN =  0.45359 * XELOAD(5)
              DUM_T_LFLN =  0.45359 * XELOAD(7)
              DUM_T_LGLN =  0.45359 * XELOAD(8)

              DUM_T_NE_U =  0.45359 * XELOAD(1)
              DUM_T_NE_L =  0.45359 * XELOAD(11)

              DUM_T_SE_U =  0.45359 * XELOAD(2)
              DUM_T_SE_L =  0.45359 * XELOAD(12)

              DUM_T_MS_U =  0.45359 * XELOAD(6)
              DUM_T_MS_L =  0.45359 * XELOAD(16)

              DUM_T_SW_U =  0.45359 * XELOAD(3)
              DUM_T_SW_L =  0.45359 * XELOAD(13)

              DUM_T_NW_U =  0.45359 * XELOAD(4)
              DUM_T_NW_L =  0.45359 * XELOAD(14)


! OUTPUT THIS DATA
             WRITE (IOUCRO,99) T, DUM_T_BALN, DUM_T_LFLN, DUM_T_LGLN,  &
     &                            DUM_T_NE_U, DUM_T_NE_L, &
     &                            DUM_T_SE_U, DUM_T_SE_L, &
     &                            DUM_T_MS_U, DUM_T_MS_L, &
     &                            DUM_T_SW_U, DUM_T_SW_L, &
     &                            DUM_T_NW_U, DUM_T_NW_L

99      FORMAT( F9.2,  2F10.0, F8.0, 5(3X,2F8.0))

                IF(NCRT .GT. 19) NCRT = 0

            END IF





! LAYOUT FOR FMT OPTION 7
!             <------- MAINSTAY TENSION ------->   CLIMB-LINE  BALLOON-LN    <------- APEX POS. ------>  <----- BALLOON POS. ----->
!     TIME       NE       SE       SW       NW       TENSION    TENSION        NORTH     EAST      ALT     NORTH     EAST      ALT
!      SEC      (KG)     (KG)     (KG)     (KG)       (KG)        (KG)          (M)      (M)       (M)      (M)      (M)       (M)
!
!         0.   16174.   9080.    8841.     9080.       6174.      1221.       1221.2   2340.5    1221.3   1221.2   2340.5    1221.3
!               F9.0,   F8.0,    F9.0,    F10.0,      F12.0,     F11.0,       F13.1,    F9.1,   F10.1,    F9.1,     F9.1,    F10.1,
!

!*********************************************
!---------------------------------------------
! SEE IF ITS MULTI-TETHER OUTPUT FOR 2008 ELEV CONTEST
!---------------------------------------------
!*********************************************
            IF(PCRTFMT .EQ. 7.0) THEN

! FETCH TENSIONS
                DUM_NE   = 0.45359 * XELOAD(1)
                DUM_SE   = 0.45359 * XELOAD(2)
                DUM_SW   = 0.45359 * XELOAD(3)
                DUM_NW   = 0.45359 * XELOAD(4)

                DUM_MS   = 0.45359 * XELOAD(6)
                DUM_BAL  = 0.45359 * XELOAD(5)

! DEFINE APEX POSITION
!---------------------
! EXTRACT COORDINATES
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
      CALL TOSLDW(3)

! TRANSFORM THE OBJECT INER POS COORDS TO REF PT TOPO FRAME
      CALL MATVEC(1, RPGIL,RBI,   RL_DUM)

      DUM_APEX_N =  0.3048 * RL_DUM(1)
      DUM_APEX_E =  0.3048 * RL_DUM(2)
      DUM_APEX_H = -0.3048 * RL_DUM(3)


! DEFINE BALLOON POSITION
!------------------------
! EXTRACT COORDINATES
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
      CALL TOSLDW(2)

! TRANSFORM THE OBJECT INER POS COORDS TO REF PT TOPO FRAME
      CALL MATVEC(1, RPGIL,RBI,   RL_DUM)

      DUM_BALL_N =  0.3048 * RL_DUM(1)
      DUM_BALL_E =  0.3048 * RL_DUM(2)
      DUM_BALL_H = -0.3048 * RL_DUM(3)

! OUTPUT THIS DATA
             WRITE (IOUCRO,81) T, DUM_NE, DUM_SE, DUM_SW, DUM_NW, &
     &                            DUM_MS, DUM_BAL,   &
     &                            DUM_APEX_N, DUM_APEX_E, DUM_APEX_H, &
     &                            DUM_BALL_N, DUM_BALL_E, DUM_BALL_H

81      FORMAT(/F11.0,  F9.0, F8.0, F9.0, F10.0,   F12.0, F11.0, &
     &                  F13.1, F9.1, F10.2,    F9.1, F9.1, F10.2)

                IF(NCRT .GT. 9) NCRT = 0

            END IF





!**************************************************
!--------------------------------------------------
!**************************************************
!         END OF OUTPUT SELECTION LOGIC
!**************************************************
!--------------------------------------------------
!**************************************************

      END IF


! BUMP THE OUTPUT DISPLAY COUNTER
      NBCRT = NBCRT + 1


! STOP RUN IF TENSION EXCEEDS USER SPECIFIED (NON ZERO) MAX TENS LEVEL
!---------------------------------------------------------------------
      IF((PTENSTOP .GT. 0.0) .AND. (XELOAD(1) .GT. PTENSTOP)) THEN

          PROCED = .FALSE.

          WRITE (IOUERR,1500)
1500         FORMAT ('RUN STOPPED DUE TO EXCEEDING A USER SPECIFIED',&
     &               ' MAXIMUM TENSION LEVEL')
             STOP 'IN CRTHD, RUN STOPPED VIA USER SPECIF MAX TENSION'
      END IF


! STOP RUN IF (NON ZERO) USER SPECIFIED ATTACH PT RANGE IS LESS THAN VALUE
!-------------------------------------------------------------------------
      IF((PLENSTOP .GT. 0.0) .AND. (PTTNSTOP .GT. 0.0) ) THEN

          IF(TDIST(NINT(PTTNSTOP)) .LT. PLENSTOP)  THEN
                PROCED = .FALSE.

                WRITE (IOUERR,1501) NINT(PTTNSTOP)
1501            FORMAT (   &
     &               'RUN STOPPED DUE TO A USER SPECIFIED MINIMUM',&
     &               ' ALLOWED DEPLOYED LENGTH FOR TTN #', I3)
                STOP 'IN CRTHD, RUN STOP VIA USER SPEC DEPLOY LENGTH'
          END IF

      END IF

      RETURN
      END
