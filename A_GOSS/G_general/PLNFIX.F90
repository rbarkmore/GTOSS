! ROUTINE: PLNFIX
! %Z%GTOSS %M% H.10 code v02.00
!              H.10 code v02.00 Added ability to move Planet fixed pt.
!                               Changed .729 to RPOME.
!                               Replaced RPGIM->RPGBI (cosmetic consistency).
!----------------------------------------------------------------------------
!              H.8 code v01.00 (baseline for vH.8 delivery)
!*************************************
!*************************************
!
      SUBROUTINE PLNFIX
!
!*************************************
!*************************************
! THIS SUBROUTINE DETERMINES THE EFFECTIVE INTEGRATED STATES OF THE
! RP OBJECT BASED ON ITS BEING FIXED TO THE PLANET FRAME. IT IS
! CALLED SO AS TO PREEMPT RESULTS OF A NORMAL RP OBJECT INTEGRATION
!
! FURTHERMORE, THIS ROUTINE INCORPORATES 3 "PERTURBATION" COMPONENTS
! OF RP FIXED STATES; THESE PERTURBATION STATES INCLUDE POS, VEL,
! ACCEL IN THE TOPO FRAME DEFINED BY THE USER AS THE "NOMINALLY
! FIXED" REFERENCE POINT POSITION. THESE STATES ARE PROVIDED BY
! A VISIT TO THE PROGRAM "BSMOVR"


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_HOST.h"
      include "../../A_HDR/EQU_HOST.h"
      include "../../A_HDR/COM_RPS.h"





!-----------------------
! LOCAL VOLATILE STORAGE
!-----------------------
      DIMENSION DUMGPL(3,3), DUMGIL(3,3), POSDUM(3)
      DIMENSION OMPDUM(3), OMLDUM(3), OCRDUM(3), OOCRDUM(3)

! POS FROM INERTIAL PT  -TO-  MOVED BASE PT (TOPO-FRAME)
      DIMENSION DUMR(3)

! POS FROM INERTIAL PT  -TO-  UN-MOVED BASE PT (TOPO-FRAME)
      DIMENSION DUMRO(3)

! POS FROM UN-MOVED BASE -TO- MOVED BASE PT (TOPO-FRAME)
      DIMENSION DUMRM(3)

! RELATIVE DERIVATIVE VECTORS: UN-MOVED -TO- MOVED BASE PT (TOPO-FRAME)
      DIMENSION DUMRAD(3), DUMRADD(3)



!******************************************************************
! VISIT PROGRAM TO PROVIDE USER SPECIFIED PERTURBATIONS TO RP STATE
!******************************************************************
      CALL BSMOVR


!********************************************
! FORM BASIC TOPO FRAME VECTORS NEEDED BY ALL
!********************************************
! POS VEC FROM "UN-MOVED" -TO- "MOVED" BASE PT
      DUMRM(1) = BSPER(1)
      DUMRM(2) = BSPER(2)
      DUMRM(3) = BSPER(3)

! DERIVATIVES WR/T TOPO FRAME OF VEC FROM "UN-MOVED -TO- "MOVED" BASE
      DUMRAD(1) = BSPERD(1)
      DUMRAD(2) = BSPERD(2)
      DUMRAD(3) = BSPERD(3)

      DUMRADD(1) = BSPERDD(1)
      DUMRADD(2) = BSPERDD(2)
      DUMRADD(3) = BSPERDD(3)

! USE SPECIFIED PLANET FRAME LAT/LONG IC, FIND TOPO-TO-PLANET TRANSFORM
      CALL TOPOGA (RPCMUO*DTRALL, RPCLAO*DTRALL,   DUMGPL)


! FORM ANGULAR VELOCITY VECTOR OF PLANET (IN THE PLANET FRAME)
      OMPDUM(1) = 0.0
      OMPDUM(2) = 0.0
      OMPDUM(3) = RPOME

! TRANSFORM ANGULAR VEL VEC TO TOPO FRAME
      CALL MATVEC(1, DUMGPL, OMPDUM,     OMLDUM)

! FIND RADIUS OUT TO THE UN-MOVED BASE PT
!----------------------------------------
! FORM A TOPO FRAME UNIT RADIUS VECTOR (OUT ALONG TOPO VERTICAL AXIS)
      VECB(1) =  0.0
      VECB(2) =  0.0
      VECB(3) = -1.0

! TRANSFORM THIS TO PLANET FRAME
      CALL MATVEC(0, DUMGPL, VECB,     VECB)

! GET THE GEODETIC PLANET RADIUS ALONG THIS DIRECTION
! (NOTE, THIS CALL IS MADE ONLY TO DETERMINE PLANET RADIUS, DUMRPL)
      CALL GEOD (NINT(PGLBOP),T,VECB,  DUMRPL,DUMLAT,DUMALT)

! FORM VEC FROM INER PT -TO- (USER DEFINED) UN-MOVED BASE ALT (TOPO FRM)
      DUMRO(1) = 0.0
      DUMRO(2) = 0.0
      DUMRO(3) = - (DUMRPL + RPHO)




!*******************************************************
!*******************************************************
! PREEMPT THE INTEGRATED STATE OF THE RP OBJECT POSITION
!*******************************************************
!*******************************************************
! FORM POS VEC FROM INER PT -TO- MOVED BASE (TOPO FRAME)
      CALL VECSUM(DUMRO, DUMRM,   DUMR)

! TRANSFORM THIS TO PLANET FRAME
      CALL MATVEC(0, DUMGPL, DUMR,     POSDUM)


! GET INERTIAL COMPONENTS OF VEC TO MOVED BASE PT
!------------------------------------------------
! (PREEMPTING INTEGRATED VALUE OF REF PT POS STATE)
      CALL EFTEI(NINT(PJULOP),T, POSDUM,   RPRI)



!*******************************************************
!*******************************************************
! PREEMPT THE INTEGRATED STATE OF THE RP OBJECT VELOCITY
!*******************************************************
!*******************************************************
! COMBINE VELOCITY CONTRIBUTIONS
      CALL CROSS(OMLDUM,  DUMR,    OCRDUM)

      CALL VECSUM(OCRDUM, DUMRAD,    VECI)

! TRANSFORM THIS TO PLANET FRAME
      CALL MATVEC(0, DUMGPL, VECI,   VECI)

! GET INERTIAL COMPONENTS OF THIS VELOCITY VECTOR
!------------------------------------------------
! (PREEMPTING INTEGRATED VALUE OF REF PT VEL STATE)
      CALL EFTEI (NINT(PJULOP),T, VECI,   RPRID)



!************************************************
!************************************************
! PROVIDE AN ACCELERATION STATE FOR THE RP OBJECT
!************************************************
!************************************************
! COMBINE ALL THE ACCEL CONTRIBUTIONS
      CALL CROSS(OMLDUM,  OCRDUM,    OOCRDUM)

      CALL CROSS(OMLDUM,  DUMRAD,    VECB)
      CALL VECSCL(2.0, VECB,         VECB)

      CALL VECSUM(OOCRDUM, VECB,     VECB)
      CALL VECSUM(VECB, DUMRADD,     VECB)

! TRANSFORM THIS TO PLANET FRAME
      CALL MATVEC(0, DUMGPL, VECB,   VECB)

! GET INERTIAL COMPONENTS OF THIS ACCEL VECTOR
!---------------------------------------------
! (THUS SUPPLYING CONSISTENT VALUES OF REF PT ACCEL STATE)
      CALL EFTEI (NINT(PJULOP),T, VECB,   RPRIDD)



!******************************************
!******************************************
! PREEMPT THE STATE OF THE RP BODY ATTITUDE
!******************************************
!******************************************
! EMPLOY USER-SPECIFIED ATTITUDE IC (WR/T TOPO FRAME) AS ATT DEFINITOR

! GET TRANSFORMATION BETWEEN BODY AND TOPO FRAME
      CALL EULMAT(JEUL,DTRALL*RPOLLO,DTRALL*RPITCO,DTRALL*RPYAWO, R3X3)

! FIND CURRENT LAT/LONG AND XFORM BETWEEN TOPO (AT THAT PT) INER FRAME
      DUMLON = ATAN2( RPRI(2), RPRI(1) )
      DUMLAT = ASIN(  RPRI(3)/VECMAG(RPI) )
      CALL TOPOGA (DUMLON,DUMLAT,         DUMGIL)

! PREEMPT THE GIM MATRIX
      CALL MATMUL (2, R3X3,DUMGIL,  RPGBI)



!**************************************************
!**************************************************
! PREEMPT THE STATE OF THE RP BODY ANGULAR VELOCITY
!**************************************************
!**************************************************
! NOTE THAT ANGULAR VEOCITY OF OBJECT IS ONLY DUE TO PLANET ROTATION

! FORM ANGULAR VELOCITY VECTOR OF PLANET (IN THE PLANET FRAME)
      VECB(1) = 0.0
      VECB(2) = 0.0
      VECB(3) = RPOME

! TRANSFORM THIS TO THE INERTIAL FRAME
!-------------------------------------
! GET INERTIAL COMPONENTS OF THIS ANGULAR VELOCITY VECTOR
      CALL EFTEI (NINT(PJULOP),T, VECB,   VECI)

! TRANSFORM THIS TO OBJECT BODY FRAME
      CALL MATVEC(0,RPGBI, VECI,   RPOM)

      RETURN
      END
