! ROUTINE: DERIV
! %Z%GTOSS %M% H.10 code v01.2
!              H.10 code v01.2 Moved RP-Fixed accel calc into PLNFIX.
!                              Replaced RPGIM->RPGBI (cosmetic consistency).
!                              Added TOSS Obj grav attraction to RP obj accel.
!-----------------------------------------------------------------------------
!              H.8 code v01.10 Added accel calc for planet-fixed object
!----------------------------------------------------------------------
!              H.6 code v01.03 Added subr call for host arbitrary force
!                              Added call to PGCALC
!----------------------------------------------------------------------
!              H.5 code v01.02 Changed EARTH to PLANET in comments
!-----------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************
!*************************************
!
      SUBROUTINE DERIV
!
!*************************************
!*************************************

!><><><><><><><><><><><><><><><><><><><><><><><><><><><<>
!>   TITLE: TETHERED OBJECT SIMULATION SYSTEM (TOSS)   <>
!>   AUTHOR: DAVID D. LANG       PHN: (206) 232 5451   <>
!>   DATE: 15 SEPT 83     NASA CONTRACT # NAS9-16715   <>
!><><><><><><><><><><><><><><><><><><><><><><><><><><><<>

!==============================================
!==============================================
! THIS PROGRAM IS NOT AN EXPLICIT PART OF TOSS
! (BUT RATHER PART OF THE "STANDALONE" FUNCTION
!  OF REF POINT GENERATION AND RESULT DISPLAY)
!==============================================
!==============================================

!***********************************************
! WARNING: CALCULATIONAL ORDERING/PHASING IS OF
!          GREAT SIGNIFICANCE IN THE CODE BELOW!
!***********************************************

! THIS SUBROUTINE EVALUATES DERIVATIVES OF ALL INTEGRATED
! VARIABLES, AND SETS UP THE TOSS INTERFACE


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_HOST.h"
      include "../../A_HDR/EQU_HOST.h"
      include "../../A_HDR/COM_RPS.h"




      DIMENSION RPAGIDUM(3)

! DONT UPDATE ATTITUDE STATES UNLESS 6 DOF REF PT DYNAMICS IS ACTIVE
!-------------------------------------------------------------------
      IF(PNOMOM .EQ. 0.0) THEN

! FORM BODY AXIS ANGULAR VELOCITY MATRIX (FOR ALL TO USE)
           RPOMG(1,1) =   0.0
           RPOMG(1,2) =  RPOM(3)
           RPOMG(1,3) = -RPOM(2)
           RPOMG(2,1) = -RPOM(3)
           RPOMG(2,2) =   0.0
           RPOMG(2,3) =  RPOM(1)
           RPOMG(3,1) =  RPOM(2)
           RPOMG(3,2) = -RPOM(1)
           RPOMG(3,3) =   0.0

! FORM DERIVATIVE OF ANG VEL MATRIX (FOR AP UPDATES)
           RPOMGD(1,1) =   0.0
           RPOMGD(1,2) =  RPOMD(3)
           RPOMGD(1,3) = -RPOMD(2)
           RPOMGD(2,1) = -RPOMD(3)
           RPOMGD(2,2) =   0.0
           RPOMGD(2,3) =  RPOMD(1)
           RPOMGD(3,1) =  RPOMD(2)
           RPOMGD(3,2) = -RPOMD(1)
           RPOMGD(3,3) =   0.0

      END IF


!********************************************************
! INVOKE LATE START DATA SNAP SHOT OF REF PT IF REQUESTED
!********************************************************
      IF(JSNAPT .EQ. 1) CALL COMSNP


!<><><><><><><><><><><><><><><><><><><><>
! INVOKE TOSS OBJECT DYNAMICS EXECUTION
!<><><><><><><><><><><><><><><><><><><><>

! ALLOW BYPASS OF TOSS DYNAMICS FOR  REF-PT-ONLY  C/O
      IF(TOSOFF .EQ. 0.0)  CALL TOSEXE(JSNAPT)

!<><><><><><><><><><><><><><><><><><><>
! END OF TOSS DYNAMICS EXECUTION CALL
!<><><><><><><><><><><><><><><><><><><>


!<><><><><><><><><><><><><><><><><><><><><><><><>
! EXTRACT ATTACH PT FORCES ON RP FROM "RPS" ARRAY
!<><><><><><><><><><><><><><><><><><><><><><><><>
      CALL APSGET


!****************************************************
! CALCULATE MISCELLANEOUS VARIABLES FOR USE BY OTHERS
!****************************************************

! REF PT RADIUS VECTOR MAGNITUDE
      RPR = VECMAG(RPRI)

! REF PT POSITION VECTOR IN PLANET FIXED FRAME
      CALL EITEF (LJULOP, T, RPRI,     RPRIP)

! INVOKE PLANET SHAPE ROUTINE TO GET REF PT ALTITUDE
      CALL GEOD (LGLBOP,T,RPRIP,  RPGEOD,RPGLAT,RFPALT)

! CALC MAGNITUDE OF REF PT INERTIAL VELOCITY
      RPVELI = VECMAG(RPRID)

! DO OTHER MISCELLANEOUS SUPPORT CALCS FOR ALL
      CALL PGCALC


!**************************************************
! CALC SIMPLE ORBITAL DRAG ON REF PT IF APPROPRIATE
!**************************************************
      IF(DRGFLG .GT. 0.0) CALL DRAGO


!*****************************************
! CALC PROPERTIES RELATED TO VARIABLE MASS
!*****************************************
      CALL DERMAS


!***********************************
! CALC GRAVITATIONAL ACCEL AT REF PT
!***********************************
! NOTE: GRAVITY CAN BE TOTALLY INHIBITED BY SPECIAL USER INPUT
      IF(GRVFLG .GE. 0.0) THEN

! GET PLANET FRAME COMPONENTS OF ACCEL OF GRAV (IF COMMANDED THIS PASS)
            IF(LEVGRV .EQ. 1) THEN

                CALL GRAV (LGRVOP,T,RPRIP,    RPAGP)


! TRANSFORM PLANET FRAME COMPONENTS OF ACCELERATION TO INER FRAME
                CALL EFTEI (LJULOP,T,RPAGP,    RPAGI)

! SEE IF OTHER TOSS OBJS ARE ALLOWED TO PROVIDE GRAVITATIONAL ATTRACTION
! NOTE 1: 2ND AGR =1, IS A DUMMY IN THE CASE OF GRAV ACCEL CALC ON REF PT
! NOTE 2: LEFT INTENTIONALLY OUT OF ENVIR EVAL LOOP ABOVE SINCE IT MUST
!         BE EVALUATED EVERYTIME SINCE NOT A SAVED VARIABLE AT THIS TIME.
!-----------------------------------------------------------------------
                CALL TOGRAV(RPRI, 1,   RPAGIDUM)

! ADD THIS TOSS OBJ CONTRIBUTION INTO REGULAR ENVIRON GRAV ACCL
                CALL VECSUM(RPAGI,RPAGIDUM,    RPAGI)

            END IF

      END IF




!------------------------------------------------------------
!------------------------------------------------------------
! INTRODUCE A SPECIAL FORCE WHICH CAN BE ACTIVATED VIA SECRET
! USER INPUT AND MANIFESTS ITSELF VIA EXTERNAL EFFECTS VECTOR
!
! THIS FORCE IS ALONG THE LINE-OF-SIGHT DIRECTION OF ATTACH
! PT 1 ON THE REF PT OBJECT AND AP 1 ON TOSS OBJECT 2
!------------------------------------------------------------
!------------------------------------------------------------
! TRIGGER CALCULATION ONLY IF SECRET FORCE IS REQUESTED
      IF(SECFOR .NE. 0.0) THEN

! FIND VECTOR FROM ATT PT 1 ON REF PT TO ATT PT 1 ON TOSS OBJECT 2
             VECI(1)  = APS( 7,1,2) - APS( 7,1,1)
             VECI(2)  = APS( 8,1,2) - APS( 8,1,1)
             VECI(3)  = APS( 9,1,2) - APS( 9,1,1)

! FORM A UNIT VECTOR ALONG THIS DIRECTION
             CALL VECNRM(VECI, VECI)

! FORM THE FORCE (POSITIVE 'SECFOR' MEANS FORCE IS AWAY FROM TOSS OBJ 2)
             CALL VECSCL(-SECFOR,VECI,      VECI)

! PUT THIS FORCE INTO EXTERNAL EFFECTS VECTOR
             CALL VECMOV(VECI,  RPFEI)

      END IF
!------------------------------------------------------------
!------------------------------------------------------------


!---------------------------------------------------------------------
! CALL ROUTINE TO PROVIDE ARBITRARY FORCES ON HOST OBJECT, RPFCXI, ETC
!---------------------------------------------------------------------
! ZERO THE CONTROL FORCES FOR POSSIBLE ACCUMULATION
      RPFCI(1) = 0.0
      RPFCI(2) = 0.0
      RPFCI(3) = 0.0

! IF DESIRED, PROVIDE SOEM ARBITRARY FORCES
      IF(ARBFON .GT. 0.0) CALL RPAFOR


!********************************************************
! CALC TRANS RATE & ACCEL(USING AP FORCES FROM LAST PASS)
!********************************************************

!----------------------------------
! SUM COMPONENTS OF FORCE ON REF PT
!----------------------------------
      RPFXI = RPFTXI + RPFAXI + RPFCXI + RPFEXI
      RPFYI = RPFTYI + RPFAYI + RPFCYI + RPFEYI
      RPFZI = RPFTZI + RPFAZI + RPFCZI + RPFEZI

! CALCULATE TRANSLATIONAL ACCELERATION
!-------------------------------------
      RPRIDD(1) = RPIOM*RPFXI + RPAGXI
      RPRIDD(2) = RPIOM*RPFYI + RPAGYI
      RPRIDD(3) = RPIOM*RPFZI + RPAGZI


! FOR OBJECT FIXED-TO-PLANET-FRAME OPT, FORCE TRANS ACCEL CONSISTENCY
!********************************************************************
! THIS IS FOR EVALUATING AN ACCEL CONSISTENT WITH RP-PLANET FIXED RP
! INCIDENTALLY, ????????????????????????????????????????????????????????
      IF(PFREEZ .GT. 0.0) CALL PLNFIX



! SET IN TRANS RATES FOR INTEGRATION SCHEME
!------------------------------------------
      DYDX(5) = RPRID(1)
      DYDX(6) = RPRID(2)
      DYDX(7) = RPRID(3)


!************************************************
! PERFORM REF PT ROTATIONAL DYNAMICS CALCULATIONS
!************************************************
! (ALLOW FOR MOMENT CALC ON USER SPECIFICATION)
      IF(PNOMOM .EQ. 0.0) CALL DERROT


!<><><><><><><><><><><><><><><><><><><><><><><>
! UPDATE THE REF PT STATE IN THE "RPS"  ARRAY
!<><><><><><><><><><><><><><><><><><><><><><><>
      CALL NEWRPS


!<><><><><><><><><><><><><><><><><><><><><><>
! CODE TO INVOKE THE FLEXIBLE BOOM SIMULATION
!<><><><><><><><><><><><><><><><><><><><><><>
! IF FLEXIBLE BOOM IS ACTIVE ADVANCE SOLUTION
      IF(BNNACT .EQ. 1.0) THEN

! CALCULATE POSITION VECTOR FROM CG TO BOOM BASE
          BMBASB(1) = BRFBAS(1) - RPXBCG
          BMBASB(2) = BRFBAS(2) - RPYBCG
          BMBASB(3) = BRFBAS(3) - RPZBCG

! CALL ROUTINE TO SIMULATE FELXIBLE BOOM
! (RETURNS FORCES/MOMENTS AND TIP STATE)
!---------------------------------------
          CALL BOMDER (BMBASB, RPGBI, RPOM,RPOMD,   &
     &                 BOOMFI, BOOMCI,   &
     &                 BOOMPI, BOOMVI, BOOMAI,   &
     &                 BGPI,   BOMP,   BOMPD     )

      END IF



!<><><><><><><><><><><><><><><><><><><>
! UPDATE ATTACH PT STATE IN "APS" ARRAY
!<><><><><><><><><><><><><><><><><><><>
      CALL APSPUT

!<><><><><><><><><><><><><><><><><><><><><><><>
! DO GENERAL TOSS INTERPRETIVE VARIABLES CALCS
!<><><><><><><><><><><><><><><><><><><><><><><>
      CALL TOSHOW

      RETURN
      END
