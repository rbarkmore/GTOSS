! COMDECK COM_TOSS
! %Z%GTOSS %M% H.12 code v02.00
!              H.12 code v02.00 Increased NLQ, NJQ, NFQ for expanded number of
!                               Finite solns, Attach Points, TOSS tethers
!-----------------------------------------------------------------------------
!              H.10 code v01.20 Increased LTOSFQ size from  360 to 500.
!                               Increased FTOSFQ size from 2600 to 3500.
!                               Increased FTOSW size from 195 to 300.
!                               Added params specifying max no. of Objects.
!                               Added array for Object states.
!                               Increased DTOSXW size to 250.
!                               Increased FTOSFW size from 300 to 400.
!                               Warning added for Obj(2) specific storage.
!                               Increased Max Object number to 15.
!-------------------------------------------------------------------------
!              H.7 code v01.10 Increased size of NQF from 1600 to 2600
!----------------------------------------------------------------------
!              H.5 code v01.02 Modified allocation for nu MMDS scenario
!                              Eliminated RTD and DTR variables
!--------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!********************************************************
!********************************************************
!
!          "GENERAL TOSS" COMMON DEFINITION
!
!********************************************************
!********************************************************
! STATE MAX NUMBER OF TOSS TETHERS ALLOWED
      PARAMETER ( MAXTTN =  58          )


!********************************************************
! SPECIFY THE MAXIMUM NUMBER OF OBJECTS CURRENTLY ALLOWED
!********************************************************
      PARAMETER ( MAXOBJ =  15          )
      PARAMETER ( MAX5OJ =  5 * MAXOBJ  )


!*******************************************************
!                "GENERAL" TOSS COMMON
!*******************************************************
      PARAMETER ( NLQ =  1700          )
      PARAMETER ( NJQ =  1520          )
      PARAMETER ( NFQ = 10000          )
      PARAMETER ( NDQ = 15384 + MAX5OJ )
      PARAMETER ( NBQ =   226          )

      COMMON /TOSCMQ/  FTOSQ(NFQ), DTOSQ(NDQ), LTOSQ(NLQ), JTOSQ(NJQ)


!*******************************************************
!                "WORKING" TOSS COMMON
!*******************************************************
! NOTE: THESE ARRAYS ARE ARTIFACTS OF THE ORIGINAL TOSS SCHEME
!       AREA" USED TO REPLICATE TOSS OBJECTS, THAT BEING THE
!       "WORKING INTO WHICH AN OFFICIAL DATA IMAGE OF A TOSS
!       OBJECT WAS LOADED FOR SOLUTION ADVANCEMENT. THIS REMAINS
!       INTACT IN THE NEW vH11 THAT USES POINTERS BECAUSE IT IS
!       AN INTEGRAL PART OF THE SCHEME TO READ IN DATA FROM THE
!       INPUT STREAM AND MAP IT TO LOCATIONS IN THE NEW DATA
!       STRUCTURE DEFINITION OF A TOSS OBJECT.
!****************************************************************
      PARAMETER ( NLW =  50 )
      PARAMETER ( NJW =  15 )
      PARAMETER ( NFW = 400 )
      PARAMETER ( NDW = 720 )
      PARAMETER ( NBW =  51 )

      COMMON /TOSCMW/  FTOSW(NFW), DTOSW(NDW), LTOSW(NLW), JTOSW(NJW)

!****************************************************************
! ARRAYS TO ENTER/MAP TOSS OBJECT INPUT STREAM ITEMS TO OFFICIAL VARIABLES
!****************************************************************
      PARAMETER ( NLI_IN =  100 )
      PARAMETER ( NLR_IN =  700 )
      COMMON /TOSOBIN/  L_TOBJ_IN(NLI_IN), R_TOBJ_IN(NLR_IN)





!************************************************
!************************************************
!  EQUIVALENCES DEFINING ARRAY SIZE DEFINITORS
!************************************************
!************************************************
! NOTE: THESE DATA ITEMS RELATE TO THE SCHEME FOR MANAGING
!       INPUT ARRAYS USED TO READ IN DATA FOR TOSS OBJECTS

! EQUIVALENCE FOR "GENERAL" ARRAY SIZE BOOKKEEPING AND PROTECTION
!----------------------------------------------------------------
      EQUIVALENCE (LTOSQ( 1), NLZZQ )

!     EQUIVALENCE (LTOSQ( 2 TO 5),     UNASSIGNED )

      EQUIVALENCE (LTOSQ( 6), NLQOFF )
      EQUIVALENCE (LTOSQ( 7), NJQOFF )
      EQUIVALENCE (LTOSQ( 8), NFQOFF )
      EQUIVALENCE (LTOSQ( 9), NDQOFF )

      EQUIVALENCE (LTOSQ(10), NLQMIN )
      EQUIVALENCE (LTOSQ(11), NJQMIN )
      EQUIVALENCE (LTOSQ(12), NFQMIN )
      EQUIVALENCE (LTOSQ(13), NDQMIN )

      EQUIVALENCE (LTOSQ(14), NLQMAX )
      EQUIVALENCE (LTOSQ(15), NJQMAX )
      EQUIVALENCE (LTOSQ(16), NFQMAX )
      EQUIVALENCE (LTOSQ(17), NDQMAX )

      EQUIVALENCE (LTOSQ(18), LUI    )
      EQUIVALENCE (LTOSQ(19), LUO    )
      EQUIVALENCE (LTOSQ(20), LUC    )
      EQUIVALENCE (LTOSQ(21), LUE    )


!     EQUIVALENCE (LTOSQ(NLZZQ = 22) IS 1ST ZEROED L ARRAY TERM

!     WHERE: NLQMIN = LOWER LIMIT ON "READ-IN-INDEX" FOR "LTOSQ"
!            NLQMAX = UPPER LIMIT ON "READ-IN-INDEX" FOR "LTOSQ"
!                     ETC, FOR EACH ARRAY

!            NLQOFF = ARRAY INDEX OFFSET FOR READ-IN DEFINED AS:
!                     "LTOSQ(READ-IN-INDEX + NLQOFF)" = READ-IN VALUE
!                     ETC, FOR EACH ARRAY


!****************************************************************
!****************************************************************
!
!       BELOW DEFINES THE DIMENSIONS AND EQUIVALENCES FOR
!       THE SUB-ARRAY LINKAGES INTO OFFICIAL TOSS COMMON
!       ARRAYS AS SPECIFIED ABOVE
!
!****************************************************************
!****************************************************************

!*****************************************************************
!             LINKAGES TO "GENERAL" TOSS COMMON
!*****************************************************************
! NOTE: ARRAYS JTOSVQ AND DTOSZQ ARE EXCLUSIVELY USED BY THE THE TSS
!       DEPLOYER SIMULATION. REFER TO FILE: EQUD TO DETERMINE STORAGE
!       REQUIREMENTS FOR THESE ARRAYS

! DIMENSION OF "GENERAL" ARRAY LINKAGE SUB-ARRAYS
      DIMENSION LTOSFQ(1700),  JTOSUQ(1500),  FTOSFQ(10000), DTOSUQ( 11000)
      DIMENSION                JTOSVQ(  20),                 DTOSVQ(  4000)
      DIMENSION                                              DTOSXQ(    54)
      DIMENSION                                              DTOSYQ(   180)
      DIMENSION                                              DTOSZQ(   150)
      DIMENSION                                              DTOSOQ(MAX5OJ)
!                      ----          ----            ----           --------
!                NLQ = 1700     NJQ = 1520     NFQ = 10000   NDQ = 15384 +MAX5OJ

! EQUIVALENCE OF "GENERAL" LINKAGE SUB-ARRAY TO CONSTANT INTEGER ARRAY
      EQUIVALENCE (LTOSQ(  1), LTOSFQ(1) )

! EQUIVALENCE OF "GENERAL" LINKAGE SUB-ARRAYS TO VARIABLE INTEGER ARRAY
      EQUIVALENCE (JTOSQ(   1), JTOSUQ(1) )
      EQUIVALENCE (JTOSQ(1501), JTOSVQ(1) )

! EQUIVALENCE OF "GENERAL" LINKAGE SUB-ARRAY TO CONSTANT REAL ARRAY
      EQUIVALENCE (FTOSQ(  1), FTOSFQ(1) )

! EQUIVALENCE OF "GENERAL" LINKAGE SUB-ARRAYS TO VARIABLE REAL ARRAY
      EQUIVALENCE (DTOSQ(    1), DTOSUQ(1) )
      EQUIVALENCE (DTOSQ(11001), DTOSVQ(1) )
      EQUIVALENCE (DTOSQ(15001), DTOSXQ(1) )
      EQUIVALENCE (DTOSQ(15055), DTOSYQ(1) )
      EQUIVALENCE (DTOSQ(15235), DTOSZQ(1) )
      EQUIVALENCE (DTOSQ(15385), DTOSOQ(1) )



!*********************************************************************
!               LINKAGES TO "WORKING" TOSS COMMON
!*********************************************************************
! FOR THE MOMENT, DATA READ-IN TO INDIVIDUAL TOSS OBJECTS IS INTIMATELY
! CONNECTED TO THIS DEFINITION OF THE XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

! DIMENSION OF "WORKING" ARRAY LINKAGE SUB-ARRAYS
      DIMENSION  LTOSFW( 50),  JTOSUW( 10),  FTOSFW(400),  DTOSUW( 40)
      DIMENSION                JTOSVW(  5),                DTOSVW(320)
      DIMENSION                                            DTOSXW(250)
      DIMENSION                                            DTOSYW( 50)
      DIMENSION                                            DTOSZW( 60)
!                       ----          ----          ----          ----
!                  NLW = 50      NJW = 15      NFW = 400    NDW = 720


! EQUIVALENCE OF "WORKING" LINKAGE SUB-ARRAYS TO CONSTANT INTEGER ARRAY
      EQUIVALENCE (LTOSW(  1), LTOSFW(1) )

! EQUIVALENCE OF "WORKING" LINKAGE SUB-ARRAY TO VARIABLE INTEGER ARRAY
      EQUIVALENCE (JTOSW(  1), JTOSUW(1) )
      EQUIVALENCE (JTOSW( 11), JTOSVW(1) )

! EQUIVALENCE OF "WORKING" LINKAGE SUB-ARRAY TO CONSTANT REAL ARRAY
      EQUIVALENCE (FTOSW( 1 ), FTOSFW(1) )

! EQUIVALENCE OF "WORKING" LINKAGE SUB-ARRAYS TO VARIABLE REAL ARRAY
      EQUIVALENCE (DTOSW(  1), DTOSUW(1) )
      EQUIVALENCE (DTOSW( 41), DTOSVW(1) )
      EQUIVALENCE (DTOSW(361), DTOSXW(1) )
      EQUIVALENCE (DTOSW(611), DTOSYW(1) )
      EQUIVALENCE (DTOSW(661), DTOSZW(1) )



!******************************************************
!******************************************************
!  EQUIVALENCES FOR GLOBAL CONSTANTS AND SCRATCH AREAS
!******************************************************
!******************************************************

! IF READ-IN INTEGER >0; TOSS INPUT IS PRINTED OUT
      EQUIVALENCE (LTOSFQ(22),  ISHOIN    )
! IF READ-IN INTEGER >0; TOSS DOES ONLY PARTICLE DYN (GLOBAL)
      EQUIVALENCE (LTOSFQ(23),  NOMOM     )
! IF READ-IN INTEGER >0; GRAV GRAD TORQ IS ON ALL TOSS OBJECTS (GLOBAL)
      EQUIVALENCE (LTOSFQ(126), IGGTON    )


! EQUIVALENCES FOR UNIVERSAL CONTANTS
!     EQUIVALENCE (FTOSFQ(1),                  OPEN )
!     EQUIVALENCE (FTOSFQ(2),                  OPEN )

! SOME BASIC PLANET ATTRIBUTES FOR USER-SPECIFIED PLANET
      EQUIVALENCE (FTOSFQ(3), OMEGAE )
      EQUIVALENCE (FTOSFQ(4), RE     )
      EQUIVALENCE (FTOSFQ(5), GE     )

!     EQUIVALENCE (FTOSFQ(6),        )
!     EQUIVALENCE (FTOSFQ(7),        )
!     EQUIVALENCE (FTOSFQ(8),        )
!     EQUIVALENCE (FTOSFQ(9), TO HERE RESERVED FOR UNIVERSAL CONSTANTS



! SUB-ARRAY DTOSXQ - EXCLUSIVELY SCRATCH VECTORS/MATRICES FOR ALL TOSS
      DIMENSION TOSVX1(3),   TOSVX2(3),   TOSVX3(3),   &
     &          TOSVX4(3),   TOSVX5(3),   TOSVX6(3)

      DIMENSION TOSMX1(3,3), TOSMX2(3,3), TOSMX3(3,3), TOSMX4(3,3),   &
     &          TOSCX1(9),   TOSCX2(9),   TOSCX3(9),   TOSCX4(9)

      EQUIVALENCE  (DTOSXQ(  1),  TOSVX1(1)   )
      EQUIVALENCE  (DTOSXQ(  4),  TOSVX2(1)   )
      EQUIVALENCE  (DTOSXQ(  7),  TOSVX3(1)   )
      EQUIVALENCE  (DTOSXQ( 10),  TOSVX4(1)   )
      EQUIVALENCE  (DTOSXQ( 13),  TOSVX5(1)   )
      EQUIVALENCE  (DTOSXQ( 16),  TOSVX6(1)   )

      EQUIVALENCE  (DTOSXQ( 19),  TOSMX1(1,1) )
      EQUIVALENCE  (DTOSXQ( 19),  TOSCX1(1)   )

      EQUIVALENCE  (DTOSXQ( 28),  TOSMX2(1,1) )
      EQUIVALENCE  (DTOSXQ( 28),  TOSCX2(1)   )

      EQUIVALENCE  (DTOSXQ( 37),  TOSMX3(1,1) )
      EQUIVALENCE  (DTOSXQ( 37),  TOSCX3(1)   )

      EQUIVALENCE  (DTOSXQ( 46),  TOSMX4(1,1) )
      EQUIVALENCE  (DTOSXQ( 46),  TOSCX4(1)   )

!     EQUIVALENCE  (DTOSXQ( 54), LAST ELEMENT - FULL )
