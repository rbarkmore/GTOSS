! COMDECK COM_BOOM
! %Z%GTOSS %M% H.10 code v01.02
!                        v01.02 Replaced RPGBI->GRPGBI (cosmetic consistency)
!---------------------------------------------------------------------------
!                        v02.00 added time bias variable TBIASB
!                              added prev stepsize variable DELTPB
!                              removed NHFCNT, and unused JBSTAR
!-----------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************************
!*************************************************
!
!   COMMON AND EQUIVALENCES FOR BOOM SUBSYSTEM
!
!*************************************************
!*************************************************
! THIS IS AN 'INCLUDE' FILE SPECIFICALLY USED FOR THE
! IMPLEMENTATION OF FLEXIBLE BOOM DYNAMICS IN THE GTOSS
! REF PT SIMULATION. IT IS ENCAPSULATED HERE TO FACILITATE
! A PARALLEL TYPE OF INCLUSION INTO OTHER HOST SIMULATIONS

!***********************************************
! COMMON ALLOCATION FOR FLEX BOOM REAL VARIABLES
!***********************************************
!-------------------------
! FLEX BOOM REAL VARIABLES
!-------------------------
! STATE SIZE OF THIS COMMON ARRAY
      PARAMETER (      NBOMP = 800 )
      COMMON /BOOM/ BP(NBOMP)

!----------------------------
! FLEX BOOM INTEGER VARIABLES
!----------------------------
! STATE SIZE OF THIS COMMON ARRAY
      PARAMETER (        NBOMI = 20 )
      COMMON /BOOM/ INTB(NBOMI)



!**************************
! EQUIVALENCES FOR INTEGERS
!**************************

! FLAG PASSED TO RTOSS TO REQUEST ARCHIVING OF BOOM DATA
      EQUIVALENCE (INTB( 1), LRDBON )

! NUMBER OF MODAL COORDINATES ACTIVATED
      EQUIVALENCE (INTB( 2), NBOMY )
      EQUIVALENCE (INTB( 3), NBOMZ )

! ATTACH PT NUMBER ASSOCIATED WITH BOOM
      EQUIVALENCE (INTB( 4), NAPBOM )

! MAX NUMBER OF MODAL COORDINATES ALLOWED IN BOOM MODEL
      EQUIVALENCE (INTB( 5), NMMAX )

! OPTION FLAG TO ACTIVATE DUAL REGION JOINT STIFFNESS EFFECTS
      EQUIVALENCE (INTB( 6), NOPT12 )

! OPTION FLAG FOR PREEMPTING TO IDENTICAL Y/Z AXIS MODAL CONFIGURATION
      EQUIVALENCE (INTB( 7), NOPTXY )

! SOLUTION TIME COUNTER
      EQUIVALENCE (INTB( 8), NTMBOM )




!**********************************************
! EQUIVALENCES FOR INTEGRATED MODAL COORDINATES
!**********************************************
      DIMENSION            QBY(5), QBYD(5),QBYDP(5), QBYDD(5),QBYDDP(5)
      EQUIVALENCE (BP( 1), QBY(1)    )

      EQUIVALENCE (BP( 6), QBYD(1)   )
      EQUIVALENCE (BP(11), QBYDP(1)  )

      EQUIVALENCE (BP(16), QBYDD(1)  )
      EQUIVALENCE (BP(21), QBYDDP(1) )

      DIMENSION            QBZ(5), QBZD(5),QBZDP(5), QBZDD(5),QBZDDP(5)
      EQUIVALENCE (BP(26), QBZ(1)    )

      EQUIVALENCE (BP(31), QBZD (1)  )
      EQUIVALENCE (BP(36), QBZDP(1)  )

      EQUIVALENCE (BP(41), QBZDD(1)  )
      EQUIVALENCE (BP(46), QBZDDP(1) )


!     EQUIVALENCE (BP(51),            UN-ASSIGNED )
!           "                              "
!     EQUIVALENCE (BP(75),            UN-ASSIGNED )


!************************************************
! EQUIVALENCES FOR DYNAMICAL EQUATION INGREDIENTS
!************************************************
! MODAL OSCILATOR TERMS
      DIMENSION             SUQY(5), SUQYD(5),  SUQZ(5), SUQZD(5)
      EQUIVALENCE (BP( 76), SUQY(1)   )
      EQUIVALENCE (BP( 81), SUQYD(1)  )
      EQUIVALENCE (BP( 86), SUQZ(1)   )
      EQUIVALENCE (BP( 91), SUQZD(1)  )

! BOOM FRAME Y AXIS COUPLING TERMS
      DIMENSION             CUPY1(5), CUPY2(5), CUPY3(5), CUPY4(5)
      DIMENSION             CUPY5(5), CUPY6(5), CUPY7(5), CUPY8(5)
      EQUIVALENCE (BP( 96), CUPY1(1)  )
      EQUIVALENCE (BP(101), CUPY2(1)  )
      EQUIVALENCE (BP(106), CUPY3(1)  )
      EQUIVALENCE (BP(111), CUPY4(1)  )
      EQUIVALENCE (BP(116), CUPY5(1)  )
      EQUIVALENCE (BP(121), CUPY6(1)  )
      EQUIVALENCE (BP(126), CUPY7(1)  )
      EQUIVALENCE (BP(131), CUPY8(1)  )

! BOOM FRAME Z AXIS COUPLING TERMS
      DIMENSION             CUPZ1(5), CUPZ2(5), CUPZ3(5), CUPZ4(5)
      DIMENSION             CUPZ5(5), CUPZ6(5), CUPZ7(5), CUPZ8(5)
      EQUIVALENCE (BP(136), CUPZ1(1)  )
      EQUIVALENCE (BP(141), CUPZ2(1)  )
      EQUIVALENCE (BP(146), CUPZ3(1)  )
      EQUIVALENCE (BP(151), CUPZ4(1)  )
      EQUIVALENCE (BP(156), CUPZ5(1)  )
      EQUIVALENCE (BP(161), CUPZ6(1)  )
      EQUIVALENCE (BP(166), CUPZ7(1)  )
      EQUIVALENCE (BP(171), CUPZ8(1)  )

! GENERALIZED FORCES (DUE TO GRAVITY)
      DIMENSION             PUFGY(5), PUFGZ(5)
      EQUIVALENCE (BP(176), PUFGY(1)  )
      EQUIVALENCE (BP(181), PUFGZ(1)  )

! GENERALIZED FORCES (DUE TO TETHER ATT PT TENSION)
      DIMENSION             PUFTY(5), PUFTZ(5)
      EQUIVALENCE (BP(186), PUFTY(1)  )
      EQUIVALENCE (BP(191), PUFTZ(1)  )

! GENERALIZED FORCES (DUE TO TETHER ATTACH PT COUPLE)
      DIMENSION             PUFCY(5), PUFCZ(5)
      EQUIVALENCE (BP(196), PUFCY(1)  )
      EQUIVALENCE (BP(201), PUFCZ(1)  )

! TETHER TENSION AND COUPLE COMPONENTS AT BOOM TIP (BOOM FRAME)
      DIMENSION             TENSF(3), COUPF(3)
      EQUIVALENCE (BP(206), TENSF(1)  )
      EQUIVALENCE (BP(211), COUPF(1)  )


!     EQUIVALENCE (BP(216),            UN-ASSIGNED )
!           "                              "
!     EQUIVALENCE (BP(225),            UN-ASSIGNED )


!------------------------
! MISCELLANEOUS VARIABLES
!------------------------
! BOOM MODEL INTEGRATION STEP SIZE
      EQUIVALENCE (BP(226), DELTBM  )

! BOOM MODEL INTEGRATION TIME VARIABLE
      EQUIVALENCE (BP(227), TIMBOM  )

! REF PT BODY AXIS ANGULAR VELOCITY COMPONENTS (BODY FRAME)
      DIMENSION             OOMB(3),    OOMBD(3)
      EQUIVALENCE (BP(228), OOMB(1)  )
      EQUIVALENCE (BP(231), OOMBD(1) )

! REF PT BODY AXIS ANGULAR VELOCITY COMPONENTS (BOOM FRAME)
      DIMENSION             OMF(3),    OMFD(3)
      EQUIVALENCE (BP(234), OMF(1)  )
      EQUIVALENCE (BP(237), OMFD(1) )

! UTILITY UNIT VECTORS
      DIMENSION             FRMI(3),   FRMJ(3),  FRMK(3)
      EQUIVALENCE (BP(240), FRMI(1) )
      EQUIVALENCE (BP(243), FRMJ(1) )
      EQUIVALENCE (BP(246), FRMK(1) )

! OFT OCCURING ANGULAR VEL COMPOSITE TERMS
      DIMENSION             OOMDI(3),  OOMDJ(3), OOMDK(3)
      EQUIVALENCE (BP(249), OOMDI(1) )
      EQUIVALENCE (BP(252), OOMDJ(1) )
      EQUIVALENCE (BP(255), OOMDK(1) )

! BOOM TIP FRAME ANGULAR VELOCITY (IN TIP FRAME)
      DIMENSION             OMP(3),    OMPD(3)
      EQUIVALENCE (BP(258), OMP(1)  )
      EQUIVALENCE (BP(261), OMPD(1) )

! VARIABLES ASSOCIATED WITH HOST DRIVEN STEPSIZE CAPABILITY
      EQUIVALENCE (BP(264), TBIASB  )
      EQUIVALENCE (BP(265), DELTPB  )



!     EQUIVALENCE (BP(266),            UN-ASSIGNED )
!           "                              "
!     EQUIVALENCE (BP(275),            UN-ASSIGNED )

!--------------------------------------
! TERMS USED IN BASE FORCE CALCULATIONS
!--------------------------------------
! FORCE ON HOST BODY AT BASE OF FLEX BOOM (BOOM FRAME)
      DIMENSION             FORBF(3)
      EQUIVALENCE (BP(276), FORBF(1) )

! GROSS CONTRIBUTION TO FORCE AT BASE
      DIMENSION             FRRF(3)
      EQUIVALENCE (BP(279), FRRF(1)  )

! FLEXIBLE CONTRIBUTION TO FORCE AT BASE
      DIMENSION             FUDF(3)
      EQUIVALENCE (BP(282), FUDF(1)  )

! COUPLING COEFFICIENTS
      EQUIVALENCE (BP(285), SMY   )
      EQUIVALENCE (BP(286), SMYD  )
      EQUIVALENCE (BP(287), SMYDD )

      EQUIVALENCE (BP(288), SMZ   )
      EQUIVALENCE (BP(289), SMZD  )
      EQUIVALENCE (BP(290), SMZDD )

!     EQUIVALENCE (BP(291),            UN-ASSIGNED )
!           "                              "
!     EQUIVALENCE (BP(300),            UN-ASSIGNED )

!---------------------------------------
! TERMS USED IN BASE MOMENT CALCULATIONS
!---------------------------------------
! COUPLE ON HOST BODY AT BASE OF FLEX BOOM (BOOM FRAME)
      DIMENSION             CUPBF(3)
      EQUIVALENCE (BP(301), CUPBF(1) )

! PRODUCT OF MASS AND POSITION VECTOR FORM BASE TO BOOM CG
      DIMENSION             EMRCG(3)
      EQUIVALENCE (BP(304), EMRCG(1)  )

! MOMENT CONTRIBUTION FROM TIP LOAD
      DIMENSION             GTIPF(3)
      EQUIVALENCE (BP(307), GTIPF(1)  )

! MOMENT CONTRIBUTION DUE TO GRAVITY
      DIMENSION             GGBSF(3)
      EQUIVALENCE (BP(310), GGBSF(1)  )

! MOMENT CONTRIBUTION INDUCED BY BASE ACCELERATION
      DIMENSION             GABSF(3)
      EQUIVALENCE (BP(313), GABSF(1)  )

! COUPLING COEFFICIENTS
      EQUIVALENCE (BP(316), SMUY   )
      EQUIVALENCE (BP(317), SMUYD  )
      EQUIVALENCE (BP(318), SMUYDD )

      EQUIVALENCE (BP(319), SMUZ   )
      EQUIVALENCE (BP(320), SMUZD  )
      EQUIVALENCE (BP(321), SMUZDD )

      EQUIVALENCE (BP(322), SS1  )
      EQUIVALENCE (BP(323), SS2  )
      EQUIVALENCE (BP(324), SS3  )
      EQUIVALENCE (BP(325), SS4  )
      EQUIVALENCE (BP(326), SS5  )
      EQUIVALENCE (BP(327), SS6  )
      EQUIVALENCE (BP(328), SS7  )
      EQUIVALENCE (BP(329), SS8  )
      EQUIVALENCE (BP(330), SS9  )
      EQUIVALENCE (BP(331), SS10 )

! MOMENT CONTRIBUTIONS DUE TO U*UDD AND MU*UDD
      DIMENSION             HUUDD(3), HMUDD(3)
      EQUIVALENCE (BP(332), HUUDD(1)  )
      EQUIVALENCE (BP(335), HMUDD(1)  )

! MISC MOMENT CONTRIBUTIONS
      DIMENSION             HWY(3), HWZ(3), HWC(3)
      EQUIVALENCE (BP(338), HWY(1)  )
      EQUIVALENCE (BP(341), HWZ(1)  )
      EQUIVALENCE (BP(344), HWC(1)  )

! TOTAL DERIVATIVE OF ANGULAR MOMENTUM
      DIMENSION             HDBSF(3)
      EQUIVALENCE (BP(347), HDBSF(1)  )

!     EQUIVALENCE (BP(350),            UN-ASSIGNED )
!           "                              "
!     EQUIVALENCE (BP(368),            UN-ASSIGNED )

!--------------------------------------------------
! SPATIAL BOOM TIP FLEXIBLE DISPLACEMENT COMPONENTS
!--------------------------------------------------
! BOOM TIP I-K PLANE SLOPE, RATE, ACCEL
      EQUIVALENCE (BP(369), PSIP   )
      EQUIVALENCE (BP(370), PSIPD  )
      EQUIVALENCE (BP(371), PSIPDD )

! BOOM TIP I-J PLANE SLOPE, RATE, ACCEL
      EQUIVALENCE (BP(372), THTP    )
      EQUIVALENCE (BP(373), THTPD   )
      EQUIVALENCE (BP(374), THTPDD  )

! BOOM TIP POS, RATE, ACCEL, IN BOOM FRAME
      DIMENSION  TIPF(3), TIPDF(3), TIPDDF(3)
      EQUIVALENCE (BP(375), TIPF(1)   )
      EQUIVALENCE (BP(378), TIPDF(1)  )
      EQUIVALENCE (BP(381), TIPDDF(1) )

! BOOM TIP POS, RATE, ACCEL, IN BODY FRAME
      DIMENSION  TIPB(3), TIPDB(3), TIPDDB(3)
      EQUIVALENCE (BP(384), TIPB(1)   )
      EQUIVALENCE (BP(387), TIPDB(1)  )
      EQUIVALENCE (BP(390), TIPDDB(1) )


!---------------------------------
! TRANSFORMATION FRAME DEFINITIONS
!---------------------------------
! BODY FRAME TO BOOM FRAME DIR COS MATRIX
      DIMENSION             GFB(3,3)
      EQUIVALENCE (BP(393), GFB(1,1)  )

! BOOM FRAME TO INERTIAL FRAME DIR COS MATRIX
      DIMENSION             GFI(3,3)
      EQUIVALENCE (BP(402), GFI(1,1)  )

! REF POINT BODY FRAME TO INERTIAL FRAME DIR COS MATRIX
      DIMENSION             BRPGBI(3,3)
      EQUIVALENCE (BP(411), BRPGBI(1,1)  )

! BOOM TIP FRAME TO INERTIAL FRAME DIR COS MATRIX
      DIMENSION             GPI(3,3)
      EQUIVALENCE (BP(420), GPI(1,1)  )

! BOOM TIP FRAME TO BOOM FRAME DIR COS MATRIX
      DIMENSION             GPF(3,3)
      EQUIVALENCE (BP(429), GPF(1,1)  )


!     EQUIVALENCE (BP(438),            UN-ASSIGNED )


!**********************************
! VARIOUS BOOM BASE STATE VARIABLES
!**********************************
! POS VECTOR FROM INER PT TO BASE OF BOOM (INER FRAME/PLANET FRAME)
      DIMENSION             RBSI(3),   RBSP(3)
      EQUIVALENCE (BP(439), RBSI(1) )
      EQUIVALENCE (BP(442), RBSP(1) )

! ACCEL OF GRAV AT BASE OF BOOM (PLANET/INER/BODY/BOOM FRAME/)
      DIMENSION             AGBSP(3),  AGBSI(3),  AGBSF(3)
      EQUIVALENCE (BP(445), AGBSP(1) )
      EQUIVALENCE (BP(448), AGBSI(1) )
      EQUIVALENCE (BP(451), AGBSF(1) )

! VELOCITY COMPONENTS (BODY FRAME)
      DIMENSION             VBSB(3)
      EQUIVALENCE (BP(454), VBSB(1)  )

! VELOCITY, ACCEL (AND DERIVATIVE) COMPONENTS (BOOM FRAME)
      DIMENSION             VBSF(3),  AABSF(3),   VBSDF(3)
      EQUIVALENCE (BP(457), VBSF(1) )
      EQUIVALENCE (BP(460), AABSF(1))
      EQUIVALENCE (BP(463), VBSDF(1))

! POSITION VECTOR FROM CG TO BOOM BASE MOUNTING (BODY FRAME)
      DIMENSION             RBASEB(3)
      EQUIVALENCE (BP(466), RBASEB(1)  )

! BOOM LONGITUDINAL LENGTH VECTOR (BODY FRAME)
      DIMENSION             RELB(3)
      EQUIVALENCE (BP(469), RELB(1)    )

! POSITION VECTOR FROM CG TO UN-DEFLECTED BOOM TIP (BODY FRAME)
      DIMENSION             RTUPB(3)
      EQUIVALENCE (BP(472), RTUPB(1)   )


!     EQUIVALENCE (BP(475),            UN-ASSIGNED )
!           "                              "
!     EQUIVALENCE (BP(500),            UN-ASSIGNED )

!***************************************
! BOOM GEOMETRY DEFINITION AND VARIABLES
!***************************************
! GEOMETRY: LENGTH OF BOOM ABOVE MOUNTING
      EQUIVALENCE (BP(501), ELBOOM  )

! GEOMETRY: BOOM MASS
      EQUIVALENCE (BP(502), BOOMAS  )

! GEOMETRY: 1ST AND 2ND BOOM MASS MOMENTS
      EQUIVALENCE (BP(503), CRU1    )
      EQUIVALENCE (BP(504), CRU2    )

! GEOMETRY: EULER ANGLES OF BOOM FRAME WR/T BODY FRAME
      EQUIVALENCE (BP(505), THTBOM  )
      EQUIVALENCE (BP(506), PHIBOM  )
      EQUIVALENCE (BP(507), PSIBOM  )

!     EQUIVALENCE (BP(508),            UN-ASSIGNED )
!     EQUIVALENCE (BP(509),            UN-ASSIGNED )

! GEOMETRY: MODAL DATA FOR Y BOOM FRAME AXIS
!*******************************************
! FREQUENCIES AND DAMPING
!------------------------
      DIMENSION             FREQY1(5),FREQY2(5),  ZETAY1(5),ZETAY2(5)
      EQUIVALENCE (BP(510), FREQY1(1) )
      EQUIVALENCE (BP(515), FREQY2(1) )
      EQUIVALENCE (BP(520), ZETAY1(1) )
      EQUIVALENCE (BP(525), ZETAY2(1) )

! JOINT STIFFNESS TRANSITION POINT
!---------------------------------
      DIMENSION             QTRANY(5)
      EQUIVALENCE (BP(530), QTRANY(1) )

! DYNAMIC COUPLING DATA
!----------------------
      DIMENSION             CMMY1(5), CMMUY1(5), HYL1(5), SYL1(5)
      EQUIVALENCE (BP(535), CMMY1(1)  )
      EQUIVALENCE (BP(540), CMMUY1(1) )
      EQUIVALENCE (BP(545), HYL1(1)   )
      EQUIVALENCE (BP(550), SYL1(1)   )


! GEOMETRY: MODAL DATA FOR Z BOOM FRAME AXIS
!*******************************************
! FREQUENCIES AND DAMPING
!------------------------
      DIMENSION             FREQZ1(5), FREQZ2(5),  ZETAZ1(5),ZETAZ2(5)
      EQUIVALENCE (BP(555), FREQZ1(1) )
      EQUIVALENCE (BP(560), FREQZ2(1) )
      EQUIVALENCE (BP(565), ZETAZ1(1) )
      EQUIVALENCE (BP(570), ZETAZ2(1) )

! JOINT STIFFNESS TRANSITION POINT
!---------------------------------
      DIMENSION             QTRANZ(5)
      EQUIVALENCE (BP(575), QTRANZ(1) )

! DYNAMIC COUPLING DATA
!----------------------
      DIMENSION             CMMZ1(5),  CMMUZ1(5), HZL1(5), SZL1(5)
      EQUIVALENCE (BP(580), CMMZ1(1)  )
      EQUIVALENCE (BP(585), CMMUZ1(1) )
      EQUIVALENCE (BP(590), HZL1(1)   )
      EQUIVALENCE (BP(595), SZL1(1)   )


! GEOMETRY: Y-Z AXIS MODAL COUPLING DATA
!***************************************
      DIMENSION             CMMYZ1(5,5)
      EQUIVALENCE (BP(600), CMMYZ1(1,1)  )


!     EQUIVALENCE (BP(625),            UN-ASSIGNED )
!           "                              "
!     EQUIVALENCE (BP(634),            UN-ASSIGNED )

!-------------------
! DERIVED MODAL DATA
!-------------------
! CIRCLAR FREQUENCIES
      DIMENSION             CIRCY1(5), CIRCZ1(5), CIRCY2(5), CIRCZ2(5)
      EQUIVALENCE (BP(635), CIRCY1(1) )
      EQUIVALENCE (BP(640), CIRCZ1(1) )
      EQUIVALENCE (BP(645), CIRCY2(1) )
      EQUIVALENCE (BP(650), CIRCZ2(1) )

! CIRCLAR FREQUENCIES SQUARED
      DIMENSION             CISSY1(5), CISSZ1(5), CISSY2(5), CISSZ2(5)
      EQUIVALENCE (BP(655), CISSY1(1) )
      EQUIVALENCE (BP(660), CISSZ1(1) )
      EQUIVALENCE (BP(665), CISSY2(1) )
      EQUIVALENCE (BP(670), CISSZ2(1) )

! DAMPING COEFFICIENTS
      DIMENSION             DMPY1(5),  DMPZ1(5),  DMPY2(5),  DMPZ2(5)
      EQUIVALENCE (BP(675), DMPY1(1)  )
      EQUIVALENCE (BP(680), DMPZ1(1)  )
      EQUIVALENCE (BP(685), DMPY2(1)  )
      EQUIVALENCE (BP(690), DMPZ2(1)  )


!     EQUIVALENCE (BP(695),            UN-ASSIGNED )
!           "                              "
!     EQUIVALENCE (BP(800),            UN-ASSIGNED )
