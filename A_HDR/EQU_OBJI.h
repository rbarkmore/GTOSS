! COMDECK EQU_OBJI
! %Z%GTOSS %M% H.12 code v02.00
!              H.12 code v02.00 Added user input for 8 more Att Pts defns
!------------------------------------------------------------------------
!              H.11 code v02.00 Baseline of vH11 TOSS Objects w/Ptrs
!--------------------------------------------------------------------
!              H.10 code v01.05 Added user input for object grav body.
!                               Added user input of Isp for thrusting.
!                               Added user input for Control Opt 6.
!                               Added user input for Object buoyancy.
!                               Added user input for Control Opt 7.
!                               Added input for Type 6 attitude cntrl
!----------------------------------------------------------------------
!              H.9 code v01.04 added input items for Type 5 att control
!----------------------------------------------------------------------
!              H.5 code v01.03 added start/stop times for lib control
!--------------------------------------------------------------------
!              H.3 code v01.02 Removed FTIMO, FDELTO
!----------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!********************************************************
!********************************************************
!
!  >>>>>> OBJECT CLASS WORKING ARRAY EQUIVALENCES  <<<<<
!               --------------------
!        >>>>>> INPUT PARAMETERS ONLY <<<<<<
!
!
!     THIS FILE CONTAINS DETAIL EQUIVALENCES FOR
!     ALL  "READ-IN DATA" , HENCE IT REPRESENTS
!     THE BASIC INPUT DATA DEFINITION COMMON TO
!     ALL TOSS OBJECTS
!
!********************************************************
!********************************************************

! NOTE: THE FOLLOWING EQUIVALENCES DEFINES THE "MAPPING" BETWEEN
!       INTEGER IDENTIFICATIONS AND THE INPUT ITEM VARIABLE NAMES
!       AS THEY ULTIMATELY MAINIFEST THEMSELVS IN THE TOSS OBJECT
!       DATA  STRUCTURE

!*************************************************************
!*************************************************************
!
!    EQUIVALENCES TO FIXED INTEGER WORKING ARRAY "LTOSW"
!                    ----- ------- -------
!
!    READ-IN INTEGER VALUES WHICH ARE CONSTANT FOR A RUN
!
!*************************************************************
!*************************************************************
!-----------------------------------------------
! SUB-ARRAY LTOSFW - FIXED INTEGERS (IE READ-IN)
!-----------------------------------------------
!     EQUIVALENCE  (L_TOBJ_IN(  1), RESERVED, NO READ-IN )
!          "           "            "
!          "           "            "
!     EQUIVALENCE  (L_TOBJ_IN( 17), RESERVED, NO READ-IN )

      EQUIVALENCE  (L_TOBJ_IN( 18), NATPAT_in  )
      EQUIVALENCE  (L_TOBJ_IN( 19), LAERO_in   )
      EQUIVALENCE  (L_TOBJ_IN( 20), LCONT_in   )
      EQUIVALENCE  (L_TOBJ_IN( 21), LICTRN_in  )
      EQUIVALENCE  (L_TOBJ_IN( 22), LICROT_in  )
      EQUIVALENCE  (L_TOBJ_IN( 23), LICSPL_in  )
      EQUIVALENCE  (L_TOBJ_IN( 24), LNOMOM_in  )
      EQUIVALENCE  (L_TOBJ_IN( 25), LEULER_in  )
      EQUIVALENCE  (L_TOBJ_IN( 26), LGRVON_in  )
      EQUIVALENCE  (L_TOBJ_IN( 27), LGGTON_in  )
      EQUIVALENCE  (L_TOBJ_IN( 28), LIBTYP_in  )
      EQUIVALENCE  (L_TOBJ_IN( 29), LICOMT_in  )
      EQUIVALENCE  (L_TOBJ_IN( 30), LINERT_in  )

! THESE ITEMS ARE ASSOCIATED WITH THE SKIP ROPE DAMPER FEATURE
!-------------------------------------------------------------
! ATTACH PT AND TOSS OBJECT NUMBER TO WHICH DAMPER BASE IS INSTALLED
      EQUIVALENCE  (L_TOBJ_IN( 31), LOBJSR_in  )
      EQUIVALENCE  (L_TOBJ_IN( 32), LAPPSR_in  )
! NUMBER OF ANCHOR POINTS FOR DAMPER LINK ARMS
      EQUIVALENCE  (L_TOBJ_IN( 33), LNCHOR_in  )
! TOSS TETHER CONNECTING DAMPER CENTER-PIECE TO DAMPER BASE ATTACH PT
      EQUIVALENCE  (L_TOBJ_IN( 34), LTHBAS_in  )
! TOSS TETHER WHOSE SKIP ROPE MOTION IS TO BE DAMPED
      EQUIVALENCE  (L_TOBJ_IN( 35), LTHDMP_in  )
! TYPE OF INTEGRATOR TO BE USED FOR TOSS OBJECT CENTERPIECE (1=EULER)
      EQUIVALENCE  (L_TOBJ_IN( 36), LSRDIN_in  )

! OBJ AND AP WR/T WHICH CONTROL OPT 4 PROVIDES INLINE-THRUST/LIB CONTROL
!-----------------------------------------------------------------------
      EQUIVALENCE  (L_TOBJ_IN( 37), LREFOB_in  )
      EQUIVALENCE  (L_TOBJ_IN( 38), LREFAT_in  )
      EQUIVALENCE  (L_TOBJ_IN( 39), LAPLIB_in  )
! TOSS TETHER NUMBER WHICH CONTROL OPT 4 USES TO ACTIVATE INLINE-THRUST
      EQUIVALENCE  (L_TOBJ_IN( 40), LTTNNL_in  )


! ATTITUDE CONTROL FRAME-REF SPECIFIER FOR CONTROL OPT 5
!-------------------------------------------------------
      EQUIVALENCE  (L_TOBJ_IN( 41), L05TYP_in  )

! FLAG TO ACTIVE THIS OBJECTS MASS AS A GRAVITY BODY TO OTHERS
!-------------------------------------------------------------
      EQUIVALENCE  (L_TOBJ_IN( 42), LGRAVBOD_in  )

! ATTITUDE CONTROL FRAME-REF SPECIFIER FOR CONTROL OPT 7
!-------------------------------------------------------
      EQUIVALENCE  (L_TOBJ_IN( 43), L07TYP_in  )

!     EQUIVALENCE  (L_TOBJ_IN( 44), NEXT AVAILABLE  )
!     EQUIVALENCE  (L_TOBJ_IN(100), LAST ELEMENT    )




!***************************************************************
!***************************************************************
!
!      EQUIVALENCES TO FIXED DATA WORKING ARRAY "FTOSW"
!                      ----- ---- -------
!
!      READ-IN REAL VALUES WHICH ARE CONSTANT FOR A RUN
!
!***************************************************************
!***************************************************************
!----------------------------------------------------------
! SUB-ARRAY FTOSFW - MASS PROPERTIES, KINEMATICS, STATE ICS
!----------------------------------------------------------
      DIMENSION  PCGBO_in(3), PDPB_in(3),  POSO_in(3),   RATEO_in(3), &
     &                        OMBO_in(3),  GBIO_in(3,3), PBOYB_in(3)

!     EQUIVALENCE  (R_TOBJ_IN(  1),                 OPEN )
!     EQUIVALENCE  (R_TOBJ_IN(  2),                 OPEN )

! MASS PROPERTIES AND GEOMETRY
      EQUIVALENCE  (R_TOBJ_IN(  3), FMASSO_in  )
      EQUIVALENCE  (R_TOBJ_IN(  4), FIXXO_in   )
      EQUIVALENCE  (R_TOBJ_IN(  5), FIYYO_in   )
      EQUIVALENCE  (R_TOBJ_IN(  6), FIZZO_in   )
      EQUIVALENCE  (R_TOBJ_IN(  7), FIXYO_in   )
      EQUIVALENCE  (R_TOBJ_IN(  8), FIXZO_in   )
      EQUIVALENCE  (R_TOBJ_IN(  9), FIYZO_in   )

      EQUIVALENCE  (R_TOBJ_IN( 10), PCGBO_in(1) )

! ATTACH POINT POSITIONS WR/T BODY GEOMETRY REF PT
! (NOTE: ADDITIONAL 8 ATT PT USER INPUT ITEMS ARE PROVIDED FURTHER BELOW)
      DIMENSION PXTB_in(8),  PYTB_in(8),   PZTB_in(8)
      EQUIVALENCE  (R_TOBJ_IN( 13), PXTB_in(1) )
      EQUIVALENCE  (R_TOBJ_IN( 21), PYTB_in(1) )
      EQUIVALENCE  (R_TOBJ_IN( 29), PZTB_in(1) )

! AERO REFERENCE POINT
      EQUIVALENCE  (R_TOBJ_IN( 37), PDPB_in(1) )

! INITIAL CONDITIONS ON TRANSLATION POSITION STATE
      EQUIVALENCE  (R_TOBJ_IN( 40), POSO_in(1) )

! INITIAL CONDITIONS ON TRANSLATION RATE STATE
      EQUIVALENCE  (R_TOBJ_IN( 43), RATEO_in(1) )

! INITIAL CONDITONS ON BODY ANGULAR VELOCITY STATE
      EQUIVALENCE  (R_TOBJ_IN( 46), OMBO_in(1) )

! INITIAL EULER ANGLES (FOR ATTITUDE INITIALIZATION)
      EQUIVALENCE  (R_TOBJ_IN( 49), PITCHO_in  )
      EQUIVALENCE  (R_TOBJ_IN( 50), ROLLO_in   )
      EQUIVALENCE  (R_TOBJ_IN( 51), YAWO_in    )

! INITIAL CONDITIONS ON DIRECTION COSINE STATE
      EQUIVALENCE  (R_TOBJ_IN( 52), GBIO_in(1,1) )


!----------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 2
!----------------------------------------------
! ISP VALUE FOR DETERMINING MASS LOSS DUE TO THRUST
      EQUIVALENCE  (R_TOBJ_IN( 61),    FCNISP_in )

! A CONSTANT COUPLE
      EQUIVALENCE  (R_TOBJ_IN( 62),    GXCO_in   )
      EQUIVALENCE  (R_TOBJ_IN( 63),    GYCO_in   )
      EQUIVALENCE  (R_TOBJ_IN( 64),    GZCO_in   )

! A SEQUENCE OF TIMED, CONSTANT FORCE PERIODS
      EQUIVALENCE  (R_TOBJ_IN( 65), CFT1_in      )

      EQUIVALENCE  (R_TOBJ_IN( 66), CFORX1_in    )
      EQUIVALENCE  (R_TOBJ_IN( 67), CFORY1_in    )
      EQUIVALENCE  (R_TOBJ_IN( 68), CFORZ1_in    )

      EQUIVALENCE  (R_TOBJ_IN( 69), CFT2_in      )
      EQUIVALENCE  (R_TOBJ_IN( 70), CFORX2_in    )
      EQUIVALENCE  (R_TOBJ_IN( 71), CFORY2_in    )
      EQUIVALENCE  (R_TOBJ_IN( 72), CFORZ2_in    )

      EQUIVALENCE  (R_TOBJ_IN( 73), CFT3_in      )
      EQUIVALENCE  (R_TOBJ_IN( 74), CFORX3_in    )
      EQUIVALENCE  (R_TOBJ_IN( 75), CFORY3_in    )
      EQUIVALENCE  (R_TOBJ_IN( 76), CFORZ3_in    )

      EQUIVALENCE  (R_TOBJ_IN( 77), CFT4_in      )
      EQUIVALENCE  (R_TOBJ_IN( 78), CFORX4_in    )
      EQUIVALENCE  (R_TOBJ_IN( 79), CFORY4_in    )
      EQUIVALENCE  (R_TOBJ_IN( 80), CFORZ4_in    )

      EQUIVALENCE  (R_TOBJ_IN( 81), CFT5_in      )
      EQUIVALENCE  (R_TOBJ_IN( 82), CFORX5_in    )
      EQUIVALENCE  (R_TOBJ_IN( 83), CFORY5_in    )
      EQUIVALENCE  (R_TOBJ_IN( 84), CFORZ5_in    )

      EQUIVALENCE  (R_TOBJ_IN( 85), CFT6_in      )
      EQUIVALENCE  (R_TOBJ_IN( 86), CFORX6_in    )
      EQUIVALENCE  (R_TOBJ_IN( 87), CFORY6_in    )
      EQUIVALENCE  (R_TOBJ_IN( 88), CFORZ6_in    )

      EQUIVALENCE  (R_TOBJ_IN( 89), CFT7_in      )
      EQUIVALENCE  (R_TOBJ_IN( 90), CFORX7_in    )
      EQUIVALENCE  (R_TOBJ_IN( 91), CFORY7_in    )
      EQUIVALENCE  (R_TOBJ_IN( 92), CFORZ7_in    )

      EQUIVALENCE  (R_TOBJ_IN( 93), CFT8_in      )

! A SINUSOIDAL PSUEDO-CONTROL FORCE
! (SPECIFIED AS DISPLACEMENT HALF-AMPL AND FREQ-CPS)
      EQUIVALENCE  (R_TOBJ_IN( 94), COSTRT_in    )

      EQUIVALENCE  (R_TOBJ_IN( 95), CDELX_in     )
      EQUIVALENCE  (R_TOBJ_IN( 96), CFREQX_in   )

      EQUIVALENCE  (R_TOBJ_IN( 97), CDELY_in     )
      EQUIVALENCE  (R_TOBJ_IN( 98), CFREQY_in    )

      EQUIVALENCE  (R_TOBJ_IN( 99), CDELZ_in     )
      EQUIVALENCE  (R_TOBJ_IN(100), CFREQZ_in    )

      EQUIVALENCE  (R_TOBJ_IN(101), COSTOP_in    )



! CONSTANT DRAG COEFFICIENT DEFAULT AERO OPTION DATA
!---------------------------------------------------
      EQUIVALENCE  (R_TOBJ_IN(102), AAERO_in     )
      EQUIVALENCE  (R_TOBJ_IN(103), CDRGO_in     )


! SECRET FORCE WHICH CAN BE GENERATED ONLY BETWEEN ATT POINTS 1 ON
! REF PT AND TOSS OBJECT 2 (ACTIVATED WITH NON-ZERO VALUE 'SECRET')
      EQUIVALENCE  (R_TOBJ_IN(104), SECRET_in     )



!------------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 3
! (PROVIDES BOOM TIP SKIP ROPE DAMPER CAPABILITY)
!------------------------------------------------
      DIMENSION  XASRA_in(6),   YASRA_in(6),   ZASRA_in(6),  &
     &           SRPL_in(6),    SRK_in(6),     SRKD_in(6),   &
     &           HYSRPL_in(6),  HYSRK_in(6),   HYSRKD_in(6), &
     &           REFSRL_in(6)

! DAMPER ARM ANCHOR POINT COORDS WR/T ATTACH PT (IN AP FRAME)
      EQUIVALENCE  (R_TOBJ_IN(105), XASRA_in(1)  )
      EQUIVALENCE  (R_TOBJ_IN(111), YASRA_in(1)  )
      EQUIVALENCE  (R_TOBJ_IN(117), ZASRA_in(1)  )

! REFERENCE LENGTH OF DAMPER ARM (FOR SPRING CONSTANT CALCS)
      EQUIVALENCE  (R_TOBJ_IN(123), REFSRL_in(1) )

! PRELOAD (ASSOCIATED WITH POSITIVE LINK EXTENSION RATE)
      EQUIVALENCE  (R_TOBJ_IN(129),   SRPL_in(1) )

! SPRING CONSTANT (ASSOCIATED WITH POSITIVE LINK EXTENSION RATE)
      EQUIVALENCE  (R_TOBJ_IN(135),    SRK_in(1) )

! DAMPING CONSTANT (ASSOCIATED WITH POSITIVE LINK EXTENSION RATE)
      EQUIVALENCE  (R_TOBJ_IN(141),   SRKD_in(1) )

! PRELOAD HYSTERESIS FACTOR (ASSOCIATED WITH NEG LINK EXTENSION RATE)
      EQUIVALENCE  (R_TOBJ_IN(147), HYSRPL_in(1) )

! SPRING HYSTERESIS FACTOR (ASSOCIATED WITH NEG LINK EXTENSION RATE)
      EQUIVALENCE  (R_TOBJ_IN(153),  HYSRK_in(1) )

! DAMPING HYSTERESIS FACTOR (ASSOCIATED WITH LINK ARM EXTENSION RATE)
      EQUIVALENCE  (R_TOBJ_IN(159), HYSRKD_in(1) )

! COEFF OF FRICTION BETWEEN TETHER AND DAMPER CENTER-PIECE
      EQUIVALENCE  (R_TOBJ_IN(165), COEFMU_in    )

! EXTENSION RATE AT WHICH FULL HYSTERESIS ONSET WILL HAVE OCCURED
      EQUIVALENCE  (R_TOBJ_IN(166), HYSONS_in    )

! CENTERPIECE/TETHER REL RATE AT WHICH FULL FRIC ONSET WILL HAVE OCCURED
      EQUIVALENCE  (R_TOBJ_IN(167), FRCONS_in    )


!     EQUIVALENCE  (R_TOBJ_IN(168),                 OPEN )
!     EQUIVALENCE  (R_TOBJ_IN(169),                 OPEN )


!-------------------------------------------------------------
! INITIAL VALUES FOR TRANSL IC OPT VIA LIBRATION SPECIFICATION
!-------------------------------------------------------------
! INITIAL LIBRATION IN-PLANE AND OUT-OF-PLANE ANGLES AND RATES
      EQUIVALENCE  (R_TOBJ_IN(170),  AIPLO_in  )
      EQUIVALENCE  (R_TOBJ_IN(171),  AOPLO_in  )
      EQUIVALENCE  (R_TOBJ_IN(172),  AIPLDO_in )
      EQUIVALENCE  (R_TOBJ_IN(173),  AOPLDO_in )

! INITIAL RANGE AND RANGE RATE
      EQUIVALENCE  (R_TOBJ_IN(174),  ARNGO_in  )
      EQUIVALENCE  (R_TOBJ_IN(175),  ARNGDO_in )


!----------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 4
!----------------------------------------------
! OPERATIONAL LIBRATION ANGLE AND DEADBAND SPECIFICATION
      EQUIVALENCE  (R_TOBJ_IN(176),  DBLIBD_in   )
      EQUIVALENCE  (R_TOBJ_IN(177),  OPLIBD_in   )

! INLINE THRUST LEVEL AND ON-OFF TIME SPECIFICATION
      EQUIVALENCE  (R_TOBJ_IN(178),  FINLIN_in   )
      EQUIVALENCE  (R_TOBJ_IN(179),  FINON_in    )
      EQUIVALENCE  (R_TOBJ_IN(180),  FINOFF_in   )

! LENGTH OF TETHER INSIDE OF WHICH IN-LINES WILL BE TURNED ON
      EQUIVALENCE  (R_TOBJ_IN(181),  FLONOF_in   )


! ITEM OUT OF ORDER, PERTAINS TO THERMAL CONDUCTION AT OBJECT
!------------------------------------------------------------
! INITIAL BODY TEMPERATURE <---- OUT OF NUMERICAL ORDER
! (NOTE, LINAKAGE BETWEEN OBJECT TEMP AND TETHER THERMAL IS NOT ACTIVE)
      EQUIVALENCE  (R_TOBJ_IN(182),  BTEMPO_in   )


!----------------------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 4 (CONTINUED)
!----------------------------------------------------------
! START/STOP TIMES FOR LIBRATION CONTROL
      EQUIVALENCE  (R_TOBJ_IN(183),  TLIBON_in   )
      EQUIVALENCE  (R_TOBJ_IN(184),  TLIBOF_in   )


!-------------------------------------------------
! DATA FOR TOSS OBJECT ATTITUDE CONTROL OPTION = 5
!-------------------------------------------------
! ATT ERROR-RATE GAINS
      EQUIVALENCE  (R_TOBJ_IN(185),  GOREX_in   )
      EQUIVALENCE  (R_TOBJ_IN(186),  GOREY_in   )
      EQUIVALENCE  (R_TOBJ_IN(187),  GOREZ_in   )
! ATT ERROR GAINS
      EQUIVALENCE  (R_TOBJ_IN(188),  GOAEX_in   )
      EQUIVALENCE  (R_TOBJ_IN(189),  GOAEY_in   )
      EQUIVALENCE  (R_TOBJ_IN(190),  GOAEZ_in   )


!-------------------------------------------
! DATA INPUT FOR TOSCN3,  CONTROL OPTION = 8
!-------------------------------------------
! HERE IS SOME SIMPLE THRUST DATA SPECIFICALLY FOR OBJECT 3 (TOSCN3)
      EQUIVALENCE  (R_TOBJ_IN(191), CISP3_in    )
      EQUIVALENCE  (R_TOBJ_IN(192), TTIMON3_in  )
      EQUIVALENCE  (R_TOBJ_IN(193), TTIMOFF3_in )
      EQUIVALENCE  (R_TOBJ_IN(194), VTHRUS3_in  )
      EQUIVALENCE  (R_TOBJ_IN(195), P_AREA3_in  )


!----------------------------------------
! DATA FOR TOSS OBJECT CONTROL OPTION = 6
!----------------------------------------
! GENERAL DATA FOR ALL OPT 6 OPERATIONS
      EQUIVALENCE  (R_TOBJ_IN(196), ORBTISP_in    )

! DATA RELATED TO ORBITAL VERTICAL-THRUST APPLICATION
      EQUIVALENCE  (R_TOBJ_IN(197), OVT_ENABL_in  )

      EQUIVALENCE  (R_TOBJ_IN(198), OVT_T_ON_in   )
      EQUIVALENCE  (R_TOBJ_IN(199), OVT_T_OFF_in  )
      EQUIVALENCE  (R_TOBJ_IN(200), OVT_H_OFF_in  )

      EQUIVALENCE  (R_TOBJ_IN(201), OVT_DB_in     )

      EQUIVALENCE  (R_TOBJ_IN(202), OVT_CONTVA_in )

      EQUIVALENCE  (R_TOBJ_IN(203), OVT_OBJ_in    )
      EQUIVALENCE  (R_TOBJ_IN(204), OVT_FAC_in    )

      EQUIVALENCE  (R_TOBJ_IN(205), OVT_TZERO_in  )
      EQUIVALENCE  (R_TOBJ_IN(206), OVT_HZERO_in  )
      EQUIVALENCE  (R_TOBJ_IN(207), OVT_THRUS_in  )

! DATA RELATED TO ORBITAL HORIZONTAL-THRUST APPLICATION
      EQUIVALENCE  (R_TOBJ_IN(208), OHT_ENABL_in    )

      EQUIVALENCE  (R_TOBJ_IN(209), OHT_T_ON_in     )
      EQUIVALENCE  (R_TOBJ_IN(210), OHT_T_OFF_in    )

      EQUIVALENCE  (R_TOBJ_IN(211), OHT_CONTVA_in   )
      EQUIVALENCE  (R_TOBJ_IN(212), OHT_CONAZ_in    )
      EQUIVALENCE  (R_TOBJ_IN(213), OHT_CONLAT_in   )
      EQUIVALENCE  (R_TOBJ_IN(214), OHT_CONLON_in   )

      EQUIVALENCE  (R_TOBJ_IN(215), OHT_LL_DB_in    )
      EQUIVALENCE  (R_TOBJ_IN(216), OHT_VEL_DB_in   )

      EQUIVALENCE  (R_TOBJ_IN(217), OHT_THRUS_in    )

      EQUIVALENCE  (R_TOBJ_IN(218), OHT_T_LLDZ_in   )
      EQUIVALENCE  (R_TOBJ_IN(219), OHT_H_LLDZ_in   )

! DATA RELATED TO ORBITAL TENSION-ALIGNED THRUST APPLICATION
      EQUIVALENCE  (R_TOBJ_IN(220), OTT_ENABL_in )

      EQUIVALENCE  (R_TOBJ_IN(221), OTT_T_ON_in  )
      EQUIVALENCE  (R_TOBJ_IN(222), OTT_T_OFF_in )

      EQUIVALENCE  (R_TOBJ_IN(223), OTT_FAC_in   )

! TABLE FOR VERTICAL PROFILE (EITHER VELOCITY OR THRUST)
      DIMENSION OVTVATAB_in(27)
      EQUIVALENCE  (R_TOBJ_IN(224), OVTVATAB_in(1) )

      DIMENSION OHTVTAB_in(21), OHAZTAB_in(21)
      EQUIVALENCE  (R_TOBJ_IN(251), OHTVTAB_in(1)  )
      EQUIVALENCE  (R_TOBJ_IN(272), OHAZTAB_in(1)  )

! DATA INPUT, GENERAL ERROR AND RATE GAINS ASSOCIATED WITH VERT CONTROL
      EQUIVALENCE  (R_TOBJ_IN(293), GAIN_VERR_in    )
      EQUIVALENCE  (R_TOBJ_IN(294), GAIN_VERRD_in   )

! DATA INPUT, GENERAL ERROR AND RATE GAINS ASSOCIATED WITH HORIZ CONTROL
      EQUIVALENCE  (R_TOBJ_IN(295), GAIN_HERR_in    )
      EQUIVALENCE  (R_TOBJ_IN(296), GAIN_HERRD_in   )

! DATA INPUT, TIME INTERVAL TO ASSESS TENSION FOR VERTICAL CONTROL
      EQUIVALENCE  (R_TOBJ_IN(297), ALT_T_INTVL_in )
      EQUIVALENCE  (R_TOBJ_IN(298), TENF_TTSPAN_in )
      EQUIVALENCE  (R_TOBJ_IN(299), TENF_NSAMPL_in )
      EQUIVALENCE  (R_TOBJ_IN(300), ALT_ALT_FLG_in )
      EQUIVALENCE  (R_TOBJ_IN(301), TEN_FILTER_in  )
      EQUIVALENCE  (R_TOBJ_IN(302), ALT_FILTER_in  )



!------------------------------------------------
! DATA INPUT, TO DEFINE BUOYANCY FOR TOSS OBEJCTS
!------------------------------------------------
! NOTE: THIS IS A WORK IN PROGRESS, SO CURRENTLY, A FIXED CONTAINER SIZE
!       IS ASSUMED, EG. ENVELOP EXPANSION EFFECTS ARE NOT INCLUDED
!       (IT IS ALSO ASSUMED THAT THE USER HAS INCLUDED IN THE TOSS OBJECT
!        MASS ACCOUNTING, THE MASS OF ANY CHOSEN GASEOUS "FILL")
      EQUIVALENCE  (R_TOBJ_IN(303), OBVOL_MAX )

!      EQUIVALENCE  (R_TOBJ_IN(304), RESERVED FOR NEW FUTURE FEATURES )
!      EQUIVALENCE  (R_TOBJ_IN(305), RESERVED FOR NEW FUTURE FEATURES )
!      EQUIVALENCE  (R_TOBJ_IN(306), RESERVED FOR NEW FUTURE FEATURES )
!      EQUIVALENCE  (R_TOBJ_IN(307), RESERVED FOR NEW FUTURE FEATURES )
!      EQUIVALENCE  (R_TOBJ_IN(308), RESERVED FOR NEW FUTURE FEATURES )

! CENTER OF BUOYANCY REFERENCE POINT
      EQUIVALENCE  (R_TOBJ_IN(309), PBOYB_in(1) )



!-------------------------------------------------
! DATA FOR TOSS OBJECT ATTITUDE CONTROL OPTION = 7
!-------------------------------------------------
! ATT CONTROL ERROR-RATE GAINS
      EQUIVALENCE  (R_TOBJ_IN(312),  GOREX7_in  )
      EQUIVALENCE  (R_TOBJ_IN(313),  GOREY7_in  )
      EQUIVALENCE  (R_TOBJ_IN(314),  GOREZ7_in  )
! ATT CONTROL ERROR GAINS
      EQUIVALENCE  (R_TOBJ_IN(315),  GOAEX7_in  )
      EQUIVALENCE  (R_TOBJ_IN(316),  GOAEY7_in  )
      EQUIVALENCE  (R_TOBJ_IN(317),  GOAEZ7_in  )

! AOA VALUE, GREATER THAN WHICH, AOA CONTROL STARTS
      EQUIVALENCE  (R_TOBJ_IN(318),  AOA_MX7_in )

! TIME TO START CONTROL OPTION 6
      EQUIVALENCE  (R_TOBJ_IN(319),  TSTR_CO7_in )

! AOA CONTROL ERROR AND RATE GAINS
      EQUIVALENCE  (R_TOBJ_IN(320),  AOAERR7_in  )
      EQUIVALENCE  (R_TOBJ_IN(321),  AOARAT7_in  )

! DELTA-TIME TO TRANSITION TO FINAL COMMANDED AOA OF 180 DEG
      EQUIVALENCE  (R_TOBJ_IN(322),  DELT7_AOA_in )

! BODY RATE .LT. WHICH AOA TRANSITION IS ALLOWED TO PROCEED
      EQUIVALENCE  (R_TOBJ_IN(323),  TST_OMD7_in )

! DELTA-TIME TO DRIVE OMB TO ZERO
      EQUIVALENCE  (R_TOBJ_IN(324),  DELT7_OMB_in )



!---------------------------------------------------------
! DATA FOR TOSS OBJECT ATTITUDE CONTROL OPTIONS = 9 AND 10
!---------------------------------------------------------
! DEFINE NUMBER OF TETHERS INVOKED TO CONTROL THIS OBJECT
      EQUIVALENCE  (R_TOBJ_IN(325),  C9_TTCNT_in )

! DEFINE WHETHER SHEAVE RATES ARE CONSIDERED IN CONTROL CALCS
      EQUIVALENCE  (R_TOBJ_IN(326),  C9_SHVONOF_in )

! DEFINE RATE-HOLD CONTROL FEEDBACK GAINS
      EQUIVALENCE  (R_TOBJ_IN(327),  C9_RTH_KERR_in )
      EQUIVALENCE  (R_TOBJ_IN(328),  C9_RTH_KRAT_in )

! DEFINE POSITION-HOLD CONTROL FEEDBACK GAINS
      EQUIVALENCE  (R_TOBJ_IN(329),  C9_PSH_KERR_in )
      EQUIVALENCE  (R_TOBJ_IN(330),  C9_PSH_KRAT_in )

! DEFINE TIME TO START/STOP CONTROLLER
      EQUIVALENCE  (R_TOBJ_IN(331),  C9_TSTART_in )
      EQUIVALENCE  (R_TOBJ_IN(332),  C9_TSTOP_in  )

! DEFINE LIBRATION CONTROL FEEDBACK GAINS AND "ULTILITY FACTOR"
      EQUIVALENCE  (R_TOBJ_IN(333),  C9_PSH_KLRAT_in )
      EQUIVALENCE  (R_TOBJ_IN(334),  C9_PSH_KLACC_in )
      EQUIVALENCE  (R_TOBJ_IN(335),  C9_PSH_KFACT_in )
      EQUIVALENCE  (R_TOBJ_IN(336),  C9_LHPOS_DB_in  )
      EQUIVALENCE  (R_TOBJ_IN(337),  C9_LHVEL_DB_in  )

! RESERVED FOR ADDITIONAL CONTROLLER PARAMETERS
!     EQUIVALENCE  (R_TOBJ_IN(338),                OPEN)
!     EQUIVALENCE  (R_TOBJ_IN(339),                OPEN)

! DEFINE TOSS TETHER NUMBERS DELEGATED TO CONTROLLING OBJECT
      DIMENSION C9_TT_in(10)
      EQUIVALENCE  (R_TOBJ_IN(340),  C9_TT_in(1)     )

! DEFINE MAXIMUM ALLOWED TENSION FOR TETHER
      DIMENSION C9_TMAX_in(10)
      EQUIVALENCE  (R_TOBJ_IN(350),  C9_TMAX_in(1)   )

! DEFINE MINIMUM ALLOWED TENSION FOR TETHER
      DIMENSION C9_TMIN_in(10)
      EQUIVALENCE  (R_TOBJ_IN(360),  C9_TMIN_in(1)   )

! DEFINE MAXIMUM ALLOWED DEPLOY RATE FOR TETHER
      DIMENSION C9_LDMAX_in(10)
      EQUIVALENCE  (R_TOBJ_IN(370),  C9_LDMAX_in(1)  )

! DEFINE MAXIMUM ALLOWED DEPLOY ACCELERATION FOR TETHER
      DIMENSION C9_LDDMAX_in(10)
      EQUIVALENCE  (R_TOBJ_IN(380),  C9_LDDMAX_in(1) )

! DEFINE XD (NORTH), YD (EAST), AND ALT-RATE (UP) COMMAND TABLES VS TIME
      DIMENSION C9_XD_TAB_in(30),  C9_YD_TAB_in(30),  C9_HD_TAB_in(30)
      EQUIVALENCE  (R_TOBJ_IN(390),  C9_XD_TAB_in(1) )
      EQUIVALENCE  (R_TOBJ_IN(420),  C9_YD_TAB_in(1) )
      EQUIVALENCE  (R_TOBJ_IN(450),  C9_HD_TAB_in(1) )

! DEFINE USER-PREEMPT OF RATE COMMANDS VIA CONSTANT VALUES
      EQUIVALENCE  (R_TOBJ_IN(480),  C9_XDCON_CMD_in )
      EQUIVALENCE  (R_TOBJ_IN(481),  C9_YDCON_CMD_in )
      EQUIVALENCE  (R_TOBJ_IN(482),  C9_HDCON_CMD_in )

! DEFINE USER-SPECIFIED GO-TO COMMANDS VIA CONSTANT VALUES
      EQUIVALENCE  (R_TOBJ_IN(483),  C9_XCON_CMD_in )
      EQUIVALENCE  (R_TOBJ_IN(484),  C9_YCON_CMD_in )
      EQUIVALENCE  (R_TOBJ_IN(485),  C9_HCON_CMD_in )

! DEFINE USER-SPECIFIED MULTIPLIER-FACTORS ON TABLE LOOKUP COMMANDS
      EQUIVALENCE  (R_TOBJ_IN(486),  C9_XDFAC_in )
      EQUIVALENCE  (R_TOBJ_IN(487),  C9_YDFAC_in )
      EQUIVALENCE  (R_TOBJ_IN(488),  C9_HDFAC_in )

! DEFINE MINIMUM TENSION TABLE -VS- TETHER LENGTH
      DIMENSION C9_TSAG_TAB_in(20)
      EQUIVALENCE  (R_TOBJ_IN(489),  C9_TSAG_TAB_in(1) )

! DEFINE USER-SPECIFIED MULTIPLIER-FACTOR ON MIN TENSION TABLE LOOKUP
      EQUIVALENCE  (R_TOBJ_IN(509),  C9_TSAGFAC_in )

! DEFINE USER-SPECIFIED TENSION ERROR GAINS FOR THE TENSION CONTROLLER
      EQUIVALENCE  (R_TOBJ_IN(510),  C9_TERR_GAIN_in  )
      EQUIVALENCE  (R_TOBJ_IN(511),  C9_TERRR_GAIN_in )
      EQUIVALENCE  (R_TOBJ_IN(512),  C9_TERRI_GAIN_in )
      EQUIVALENCE  (R_TOBJ_IN(513),  C9_LBW_FLG_in    )

! DEFINE USER-SPECIFIED DEBUG FLAG
      EQUIVALENCE  (R_TOBJ_IN(514),  C9_DEBUG_FLG_in  )
      EQUIVALENCE  (R_TOBJ_IN(515),  C9_DEBUG_TTN1_in )
      EQUIVALENCE  (R_TOBJ_IN(516),  C9_DEBUG_TTN2_in )

! DEFINE MASS-LOADING EVENT 1
      EQUIVALENCE  (R_TOBJ_IN(517),  C9_TIME_MSEV_1_in )
      EQUIVALENCE  (R_TOBJ_IN(518),  C9_DELT_MSEV_1_in )
      EQUIVALENCE  (R_TOBJ_IN(519),  C9_MASS_MSEV_1_in )

! DEFINE MASS-LOADING EVENT 2
      EQUIVALENCE  (R_TOBJ_IN(520),  C9_TIME_MSEV_2_in)
      EQUIVALENCE  (R_TOBJ_IN(521),  C9_DELT_MSEV_2_in)
      EQUIVALENCE  (R_TOBJ_IN(522),  C9_MASS_MSEV_2_in)

! DEFINE MASS-LOADING EVENT 3
      EQUIVALENCE  (R_TOBJ_IN(523),  C9_TIME_MSEV_3_in )
      EQUIVALENCE  (R_TOBJ_IN(524),  C9_DELT_MSEV_3_in )
      EQUIVALENCE  (R_TOBJ_IN(525),  C9_MASS_MSEV_3_in )

! DEFINE OBJECT TO WHICH MASS-LOADING IS APPLIED
      EQUIVALENCE  (R_TOBJ_IN(526),  C9_MSEV_OBJ_in )

! DEFINE USER-SPECIFIED DEBUG TIME-INTERVAL FOR OUTPUT
      EQUIVALENCE  (R_TOBJ_IN(527),  C9_DEBUG_DT_in  )

!     EQUIVALENCE  (R_TOBJ_IN(528),                   OPEN )
!     EQUIVALENCE  (R_TOBJ_IN(529),                   OPEN )

! DEFINE TIME INTERVAL, NO. OF SAMPLES, FOR TENSION AVERAGING FILTER
      EQUIVALENCE  (R_TOBJ_IN(530), C9_TENF_TTSPAN_in )
      EQUIVALENCE  (R_TOBJ_IN(531), C9_TENF_NSAMPL_in )

! DEFINE PARAMETERS CONTROLLING SAG TENSION LIMITS AND DEADBANDS
      EQUIVALENCE  (R_TOBJ_IN(532), C9_SAG_LIMIT_FRAC_in )
      EQUIVALENCE  (R_TOBJ_IN(533), C9_SAG_LIMIT_MIN_in  )
      EQUIVALENCE  (R_TOBJ_IN(534), C9_SAG_DB_FRAC_in  )
      EQUIVALENCE  (R_TOBJ_IN(535), C9_SAG_DB_VALUE_in  )

!     EQUIVALENCE  (R_TOBJ_IN(536),                   OPEN )
!     ...........................
!     EQUIVALENCE  (R_TOBJ_IN(599),                   OPEN )


! ADDITIONAL 8 ATT POINT POSITIONS WR/T BODY GEOMETRY REF PT
      DIMENSION PXTB_in_(8),  PYTB_in_(8),   PZTB_in_(8)
      EQUIVALENCE  (R_TOBJ_IN(600), PXTB_in_(1) )
      EQUIVALENCE  (R_TOBJ_IN(608), PYTB_in_(1) )
      EQUIVALENCE  (R_TOBJ_IN(616), PZTB_in_(1) )


!     EQUIVALENCE  (R_TOBJ_IN(624), NEXT AVAILABLE  )
!     EQUIVALENCE  (R_TOBJ_IN(700), LAST ELEMENT    )


!********************************************************************
!********************************************************************
!********************************************************************
!
! IMPORTANT NOTE: ENTRIES INTO THIS FILE DEFINING INPUT CONSTANTS FOR
!                 TOSS OBJECTS MUST BE MAINTAINED SUCH THAT CONSISTENCY
!                 IS REALIZED AMONGST CORRESPONDING DATA ITEMS IN THE
!                 FOLLOWING CODES:
!
!                 ROUTINE:  TOSS_OBJ_IN
!
!                 MODULE:   TOSS_OBJECT_DATA_STRUCTURE
!                           CONTAINING: ZERO_OBJ_IN_DATA
!********************************************************************
!********************************************************************
!********************************************************************





