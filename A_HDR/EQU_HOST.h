! COMDECK EQU_HOST
! %Z%GTOSS %M% H.12 code v02.30
!              H.12 code v02.30 Added storage for additional Attach Points
!-----------------------------------------------------------------------------------
!              H.10 code v02.10 Added base motion data for RP Planet-fix.
!                               Added max-tens input parameter to stop run.
!                               Added user input item for CRT format selection.
!                               Added user input item Max tension stop.
!                               Added user input item for min deploy length stop.
!                               Added user input items for Scen 1 vert base motion.
!                               Added user input items for Scenario 2 base motion.
!                               Added user input items to control On-line animation.
!                               Added permenant earth refer location for anim. calcs
!                               Replaced RPGIM->RPGBI (cosmetic consistency).
!                               Added user input item for variable stepsize control.
!----------------------------------------------------------------------------------
!              H.9 code v02.05 Added new items for subr RPATTC
!                              Fixed err in arrays TABCX,Y,Z by
!                              decreasing dimension 43->41.
!-------------------------------------------------------------
!              H.8 code v02.04 Added flag to freeze RP obj to planet frm
!-----------------------------------------------------------------------
!              H.7 code v02.03 Added input items for plasma environment
!                              fidelity and evaluation interval
!                              Added timer for plasam eval option
!                              Added input itmes for ref date
!-------------------------------------------------------------------
!              H.6 code v02.02 Eliminated matrix variable RPGTP
!                              Reorganized common to fix input problem
!                              Added data for host att control and table
!                              lookups of force and moment
!                              Added inertial euler angs
!---------------------------------------------------------------------
!              H.5 code v02.01 Added user input of const planet radius
!                              Eliminated RPRTD and RPDTR variables
!------------------------------------------------------------------
!              H.3 code v02.00 Added time bias variable TBIASH
!                              Added prev stepsize variable DELTPH
!                              Removed boom stepsize input item
!                              Added input items, PSD1, PSD2, TPSD12
!-------------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************************
!*************************************************
!
!      HOST SIMULATION COMMON EQUIVALENCES
!
!*************************************************
!*************************************************
! NOTE: VARIOUS AREAS OF GTOSS COMMON IS BEING RESERVED TO
!       PRESERVE THE GTOSS INPUT DECKS SO THAT THEIR PRE-
!       RESULTS-DATA-BASE FORM WILL STILL WORK.  PARTS OF
!       THIS AREA HAS BEEN CONSISTENTLY PRESSED INTO SERVICE
!       FOR CONTROL OF QUICK-LOOK OUTPUT.  THE AREAS BEING
!       INTENTIONALLY LEFT BLANK ARE DENOTED AS "RESERVED".

!==============================================
!==============================================
! THIS COMMON  IS NOT AN EXPLICIT PART OF TOSS
! (BUT RATHER PART OF THE "STANDALONE" FUNCTION
!   OF REF POINT GENERATION AND RESULT DISPLAY)
!==============================================
!==============================================

! THIS IS AN 'INCLUDE' FILE FOR THE APPLICATION DEPENDENT
! COMMON EQUIVALENCES FOR THE REF POINT HOST SIMULATION

!*************************************
! EQUIVALENCES OF INTEGRATED VARIABLES
!*************************************
! RIGID BODY TRANSLATIONAL STATE
      DIMENSION           RPRID(3),                RPRIDD(3)
      EQUIVALENCE (Y( 2), RPRID(1)),    (DYDX( 2), RPRIDD(1))

      DIMENSION           RPRI(3)
      EQUIVALENCE (Y( 5), RPRI(1))

! RIGID BODY ANGULAR VELOCITY STATE VECTORS
      DIMENSION           RPOM(3),                 RPOMD(3)
      EQUIVALENCE (Y( 8), RPOM(1)),     (DYDX( 8), RPOMD(1))

! RIGID BODY DIRECTION COSINE AND DERIVATIVE STATE MATRICES
      DIMENSION            RPGBI(3,3),  RPGI(9)
      EQUIVALENCE (Y(11),  RPGBI(1,1),  RPGI(1))

      DIMENSION              RPGBID(3,3)
      EQUIVALENCE (DYDX(11), RPGBID(1,1))



!***************************************
! EQUIVALENCES OF GENERAL REAL VARIABLES
!***************************************

!     P(1)...THRU..P(17)      SEE COM_HOST.i FOR DEFINITION


! VARIABLES ASSOCIATED WITH HOST DRIVEN STEPSIZE CAPABILITY
      EQUIVALENCE (P(18), TBIASH  )
      EQUIVALENCE (P(19), DELTPH  )

!======================================
! INPUT DATA FOR REF PT OBJECT GEOMETRY
!======================================
!=====INPUT=====> MASS AND MOMENTS OF INERTIA
      EQUIVALENCE (P(20), RPMASO  )
      EQUIVALENCE (P(21), RPIXXO  )
      EQUIVALENCE (P(22), RPIYYO  )
      EQUIVALENCE (P(23), RPIZZO  )

!=====INPUT=====> LOCATION OF REF PT CENTER OF MASS
      EQUIVALENCE (P(24), RPXCGO  )
      EQUIVALENCE (P(25), RPYCGO  )
      EQUIVALENCE (P(26), RPZCGO  )

!=====INPUT=====> PRODUCTS OF INERTIA
      EQUIVALENCE (P(27), RPIXYO  )
      EQUIVALENCE (P(28), RPIXZO  )
      EQUIVALENCE (P(29), RPIYZO  )

!=====INPUT=====> CONSTANT/LINEAR INERTIA VARIATION FLAG
      EQUIVALENCE (P(30), PINOPT  )

!     EQUIVALENCE (P(31),           OPEN  )



!-------------------------------------------
! REFERENCE POINT FORCE AND MOMENT VARIABLES
!-------------------------------------------

! TOTAL APPLIED FORCES ON REF PT IN INERTIAL FRAME (EXCLUDING GRAV)
!---------------------
      DIMENSION                  RPFI(3)
      EQUIVALENCE (P(32), RPFXI, RPFI(1) )
      EQUIVALENCE (P(33), RPFYI          )
      EQUIVALENCE (P(34), RPFZI          )

! TOTAL TETHER FORCE ON REF POINT (IE.FROM ALL ATTACH POINTS)
      DIMENSION                   RPFTI(3)
      EQUIVALENCE (P(35), RPFTXI, RPFTI(1) )
      EQUIVALENCE (P(36), RPFTYI           )
      EQUIVALENCE (P(37), RPFTZI           )

! TOTAL AERODYNAMIC APPLIED FORCE ON REF POINT
      DIMENSION                   RPFAI(3)
      EQUIVALENCE (P(38), RPFAXI, RPFAI(1) )
      EQUIVALENCE (P(39), RPFAYI           )
      EQUIVALENCE (P(40), RPFAZI           )

! TOTAL CONTROL FORCE ON REF POINT
      DIMENSION                   RPFCI(3)
      EQUIVALENCE (P(41), RPFCXI, RPFCI(1) )
      EQUIVALENCE (P(42), RPFCYI           )
      EQUIVALENCE (P(43), RPFCZI           )

! TOTAL EXTERNAL EFFECTS FORCES ON REF POINT
      DIMENSION                   RPFEI(3)
      EQUIVALENCE (P(44), RPFEXI, RPFEI(1) )
      EQUIVALENCE (P(45), RPFEYI           )
      EQUIVALENCE (P(46), RPFEZI           )

! ACCEL DUE TO GRAVITY AT REF PT
      DIMENSION                   RPAGI(3)
      EQUIVALENCE (P(47), RPAGXI, RPAGI(1) )
      EQUIVALENCE (P(48), RPAGYI           )
      EQUIVALENCE (P(49), RPAGZI           )


! TOTAL APPLIED MOMENT ON REF POINT BODY (BODY FRAME)
!---------------------
      DIMENSION                  RPGB(3)
      EQUIVALENCE (P(50), RPGXB, RPGB(1) )
      EQUIVALENCE (P(51), RPGYB          )
      EQUIVALENCE (P(52), RPGZB          )

! TOTAL TETHER MOMENT ON BODY (IE.FROM ALL ATTACH POINTS)
      DIMENSION                   RPGTB(3)
      EQUIVALENCE (P(53), RPGTXB, RPGTB(1) )
      EQUIVALENCE (P(54), RPGTYB           )
      EQUIVALENCE (P(55), RPGTZB           )

! TOTAL AERODYNAMIC MOMENT ON BODY
      DIMENSION                   RPGAB(3)
      EQUIVALENCE (P(56), RPGAXB, RPGAB(1) )
      EQUIVALENCE (P(57), RPGAYB           )
      EQUIVALENCE (P(58), RPGAZB           )

! TOTAL CONTROL MOMENT ON BODY
      DIMENSION                   RPGCB(3)
      EQUIVALENCE (P(59), RPGCXB, RPGCB(1) )
      EQUIVALENCE (P(60), RPGCYB           )
      EQUIVALENCE (P(61), RPGCZB           )

! TOTAL EXTERNAL EFFECTS MOMENT ON BODY
      DIMENSION                   RPGEB(3)
      EQUIVALENCE (P(62), RPGEXB, RPGEB(1) )
      EQUIVALENCE (P(63), RPGEYB           )
      EQUIVALENCE (P(64), RPGEZB           )

! TOTAL GRAVITY GRADIENT MOMENT ON BODY
      DIMENSION                   RPGGB(3)
      EQUIVALENCE (P(65), RPGGXB, RPGGB(1) )
      EQUIVALENCE (P(66), RPGGYB           )
      EQUIVALENCE (P(67), RPGGZB           )

! GRAVITY ACCEL IN PLANET FRAME
      DIMENSION                   RPAGP(3)
      EQUIVALENCE (P(68), RPAGP(1) )



!=====INPUT=====> STEPSIZE STAGING SKIP INTERVALS
!------------------------------------------------
      DIMENSION TSKIP(4)
      EQUIVALENCE (P(71), TSKIP(1) )

!=====INPUT=====> VARIABLE STEPSIZE ENABLE/FREQ SPECIFICATION
!------------------------------------------------------------
      EQUIVALENCE (P(75), GOV_STEP_FREQ )

! TIME FOR OUTPUT OF DATA BASED ON VARIABLE STEPSIZE OPERATION
      EQUIVALENCE (P(76), TOUTVS )


!     EQUIVALENCE (P(77),           OPEN  )
!          "
!     EQUIVALENCE (P(79),           OPEN  )



!=================================================
! INPUT DATA FOR INITIAL CONDITION OF REF PT STATE
!=================================================
!=====INPUT=====> VEL AZIM WR/T TOPO FRAME (TRANSL STATE CIRC ORB IC OPT)
      EQUIVALENCE (P(80), RPCIRZ  )

!=====INPUT=====> INERTIAL FRAME TRANSLATIONAL STATE IC DATA
      EQUIVALENCE (P(81), RPXIO   )
      EQUIVALENCE (P(82), RPYIO   )
      EQUIVALENCE (P(83), RPZIO   )
      EQUIVALENCE (P(84), RPXIDO  )
      EQUIVALENCE (P(85), RPYIDO  )
      EQUIVALENCE (P(86), RPZIDO  )

!=====INPUT=====> ROTATIONAL STATE IC DATA
      EQUIVALENCE (P(87), RPOMXO  )
      EQUIVALENCE (P(88), RPOMYO  )
      EQUIVALENCE (P(89), RPOMZO  )

!=====INPUT=====> ROTATION ATTITUDE STATE IC OPTION SELECTOR
      EQUIVALENCE (P(90), CROSEL  )

!=====INPUT=====> GRAVITY FORCE INHIBIT FLAG
      EQUIVALENCE (P(91), GRVFLG  )

!=====INPUT=====> OBJECT GRAVITY GRADIENT TORQUE ACTIVATE FLAG
      EQUIVALENCE (P(92), GRTFLG  )

!=====INPUT=====> ROTATION ATTITUDE RATE STATE IC OPTION SELECTOR
      EQUIVALENCE (P(93), COMSEL  )

!=====INPUT=====> FLAG TO INHIBIT DOTS TO CRT DURING EXECUTION
      EQUIVALENCE (P(94), PNODOT  )

!=====INPUT=====> FLAG TO SELECT CRT FORMAT DURING EXECUTION
      EQUIVALENCE (P(95), PCRTFMT )


!     EQUIVALENCE (P(96),           OPEN  )

!=====INPUT=====> FLAG/TENSION LEVEL FOR USER REQUESTED RUN-ABORT
      EQUIVALENCE (P( 97), PTENSTOP )

!=====INPUT=====> FLAG TO SPECIFY PLANET-FIXED RP BASE PERTURB SCENARIOS
      EQUIVALENCE (P( 98), PBSPER )

!=====INPUT=====> FLAG TO FIX THE RP OBJECT WR/T PLANET FRAME
      EQUIVALENCE (P( 99), PFREEZ  )

!=====INPUT=====> TRANSLATION STATE IC OPTION SELECTOR
      EQUIVALENCE (P(100), CTRSEL  )

!=====INPUT=====> INITIAL ALT, LONG, LAT FOR TRANSL IC OPTION
      EQUIVALENCE (P(101), RPHO   )
      EQUIVALENCE (P(102), RPCMUO )
      EQUIVALENCE (P(103), RPCLAO )

!=====INPUT=====> INITIAL EULER ANGLES OF BODY FRAME
      EQUIVALENCE (P(104), RPITCO )
      EQUIVALENCE (P(105), RPOLLO )
      EQUIVALENCE (P(106), RPYAWO )

!=====INPUT=====> TOPOCENTRIC VEL COMP FOR TRANSL IC OPTION
      EQUIVALENCE (P(107), RPVXTO )
      EQUIVALENCE (P(108), RPVYTO )
      EQUIVALENCE (P(109), RPVZTO )

!=====INPUT=====> DEFINE GTOSS EXECUTION CONTROL
      EQUIVALENCE (P(110), TOSOFF  )
      EQUIVALENCE (P(111), PNOMOM  )
      EQUIVALENCE (P(112), PEULRP  )
      EQUIVALENCE (P(113), PLASOB  )
      EQUIVALENCE (P(114), PNAP    )

!     P(115)          RESERVED FOR EARLIER VERSION COMPATIBILITY
!     P(116)          RESERVED FOR EARLIER VERSION COMPATIBILITY

!=====INPUT=====> DEFINE QUICK LOOK FORMAT
      EQUIVALENCE (P(117), PQSEL    )


!     P(118)...P(133) RESERVED FOR EARLIER VERSION COMPATIBILITY


!=====INPUT=====> ATTACH POINT LOCATIONS IN REF PT BODY FRAME
! NOTE 1: SEE BELOW (NEAR END) FOR INPUTS TO ADDITIONAL ATT PTS (9-16)
! NOTE 2: THESE VARIABLES ARE RENAMED AND A NEW VARIABLE IS DECLARED
!         INPUT TRIAGE TO THE NEW VARIABLE IS DONE IN INITIA.F
      DIMENSION RPPXBT_(8),   RPPYBT_(8),   RPPZBT_(8)
      EQUIVALENCE (P(134), RPPXBT_(1) )
      EQUIVALENCE (P(142), RPPYBT_(1) )
      EQUIVALENCE (P(150), RPPZBT_(1) )


!=====INPUT=====> FLAG TO ACTIVATE ARBITRARY BODY MOMENTS
      EQUIVALENCE (P(158), ARBMON    )

!=====INPUT=====> TABLE DATA DEFINING ARB HOST BODY AXIS COUPLES VS TIME
      DIMENSION TABCX(41)
      EQUIVALENCE (P(159), TABCX( 1) )
!     EQUIVALENCE (P(199), TABCX(41) )

!     EQUIVALENCE (P(200),                unused)

      DIMENSION TABCY(41)
      EQUIVALENCE (P(201), TABCY( 1) )
!     EQUIVALENCE (P(241), TABCY(41) )

!     EQUIVALENCE (P(242),                unused)
!     EQUIVALENCE (P(243),                unused)

      DIMENSION TABCZ(41)
      EQUIVALENCE (P(244), TABCZ( 1) )
!     EQUIVALENCE (P(284), TABCZ(41) )


!=====INPUT=====> FLAG/TOSS TETH DEPLOY LEN FOR USER REQUESTED RUN-ABORT
      EQUIVALENCE (P(285), PLENSTOP )
      EQUIVALENCE (P(286), PTTNSTOP )


!=====INPUT=====> REFERENCE DATE FOR RUN (CORRESPONDS TO ZERO SIM TIME)
      EQUIVALENCE (P(287), PPYRR  )
      EQUIVALENCE (P(288), PPMON  )
      EQUIVALENCE (P(289), PPDAY  )
      EQUIVALENCE (P(290), PPHRR  )
      EQUIVALENCE (P(291), PPMIN  )
      EQUIVALENCE (P(292), PPSEC  )


!-----------------------------------------
! BODY AXIS FRAME ATTITUDE STATE VARIABLES
!-----------------------------------------
! RIGID BODY FRAME ANGULAR VEL AND ACCEL MATRICES
      DIMENSION            RPOMG(3,3),   RPOMGD(3,3)
      EQUIVALENCE (P(293), RPOMG(1,1)  )
      EQUIVALENCE (P(302), RPOMGD(1,1) )

! RIGID BODY ANGULAR MOMENTUM VECTOR (IN INERTIAL FRAME)
      DIMENSION            RPAMI(3)
      EQUIVALENCE (P(311), RPAMI(1)    )


!     EQUIVALENCE (P(314),               OPEN  )
!          "
!     EQUIVALENCE (P(334),               OPEN  )


!-------------------------------------------------------
! ATTACH POINT FORCES AND COUPLES USED FOR CHECKOUT, ETC
!-------------------------------------------------------
! NOTE: NAMES SUCH AS FXAP1 ARE NOT USED, RATHER, LOOK FOR:  P(K+334)
!     EQUIVALENCE (P(335),  FXAP1   RESERVED FOR SPECIAL PURPOSES
!     EQUIVALENCE (P(336),  FYAP1   RESERVED FOR SPECIAL PURPOSES
!     EQUIVALENCE (P(337),  FZAP1   RESERVED FOR SPECIAL PURPOSES

!     EQUIVALENCE (P(338),  CXAP1   RESERVED FOR SPECIAL PURPOSES
!     EQUIVALENCE (P(339),  CYAP1   RESERVED FOR SPECIAL PURPOSES
!     EQUIVALENCE (P(340),  CZAP1   RESERVED FOR SPECIAL PURPOSES

!     EQUIVALENCE (P(341),  FXAP2   RESERVED FOR SPECIAL PURPOSES
!     EQUIVALENCE (P(342),  FYAP2   RESERVED FOR SPECIAL PURPOSES
!     EQUIVALENCE (P(343),  FZAP2   RESERVED FOR SPECIAL PURPOSES

!     EQUIVALENCE (P(344),  CXAP2   RESERVED FOR SPECIAL PURPOSES
!     EQUIVALENCE (P(345),  CYAP2   RESERVED FOR SPECIAL PURPOSES
!     EQUIVALENCE (P(346),  CZAP2   RESERVED FOR SPECIAL PURPOSES


!-------------------------------
! MISCELLANEOUS REF PT VARIABLES
!-------------------------------

!     EQUIVALENCE (P(347),               OPEN  )
!           "
!     EQUIVALENCE (P(353),               OPEN  )

! SOME BASIC PLANET ATTRIBUTES FOR USER SPECIFIED PLANET
      EQUIVALENCE (P(354), RPOME  )
      EQUIVALENCE (P(355), RPRE   )
      EQUIVALENCE (P(356), RPGE   )

! REF PT POS VECTOR MAGNITUDE, PLANET GLOBE ALT
      EQUIVALENCE (P(357), RPR    )
      EQUIVALENCE (P(358), RFPALT )

! INER VEL MAGNITUDE, SPHERICAL ALT RATE
      EQUIVALENCE (P(359), RPVELI )
      EQUIVALENCE (P(360), RPHD   )

! MAGNI OF VECTOR SUM OF ATTACH PT FORCES ON REF PT BODY
      EQUIVALENCE (P(361), TENSON )

! BODY EULER ANGLES WR/T INERTIAL FRAME (DEG)
      EQUIVALENCE (P(362), DGTHTI )
      EQUIVALENCE (P(363), DGPHII )
      EQUIVALENCE (P(364), DGPSII )

! REF PT PLANET GLOBE GEOMETRY ATTRIBUTES
      EQUIVALENCE (P(365), RPGEOD )
      EQUIVALENCE (P(366), RPGLAT )

! BODY EULER ANGLES WR/T INERTIAL FRAME (RAD)
      EQUIVALENCE (P(367), RPTHTI )
      EQUIVALENCE (P(368), RPPHII )
      EQUIVALENCE (P(369), RPPSII )


!     EQUIVALENCE (P(370),            OPEN  )


! PLANET AND INERTIAL LONG/LATITUDE (DEGREES)
      EQUIVALENCE (P(371), DEGMUE )
      EQUIVALENCE (P(372), DEGLAE )
      EQUIVALENCE (P(373), DEGMUI )
      EQUIVALENCE (P(374), DEGLAI )


! REF POSITION VECTOR IN PLANET FIXED FRAME
      DIMENSION            RPRIP(3)
      EQUIVALENCE (P(375), RPRIP(1) )

! RETENTIVE STORAGE FOR REFERENCE POSITION FOR ANIMATION CODE GHAINALD
      DIMENSION            RPEANIM(3)
      EQUIVALENCE (P(378), RPEANIM(1))


!=====INPUT=====> PARAMETERS TO CONTROL THE QUICK LOOK PAGE OUTPUT
      DIMENSION  POBJQ(6)
      EQUIVALENCE (P(381), POBJQ(1) )
!         "
!     EQUIVALENCE (P(386), POBJQ(6) )


!=====INPUT=====> PARAMETERS TO CONTROL LATE START SNAP SHOT TIMES
      DIMENSION  TPLATE(10)
      EQUIVALENCE (P(387), TPLATE( 1) )
!         "
!     EQUIVALENCE (P(396), TPLATE(10) )

!=====INPUT=====> ASKS FOR LATE START INITIALIZATION IF = 1.0
      EQUIVALENCE (P(397), PLATE      )

!=====INPUT=====> ASKS FOR SNAP SHOT (LATE START) DUMPS IF = 1.0
      EQUIVALENCE (P(398), PSNAP      )

!=====INPUT=====> CONTROLS TO CREATE ON-LINE ANIMATION FILE
      EQUIVALENCE (P(399), ANIMAT )
      EQUIVALENCE (P(400), ARBVAL )
      EQUIVALENCE (P(401), AFTYPE )
      EQUIVALENCE (P(402), RENDPT )


!     EQUIVALENCE (P(403),                OPEN  )
!          "
!     EQUIVALENCE (P(414),                OPEN  )


!=====INPUT=====> USED TO REQUEST AUX DATA INCLUSION IN RDB
      EQUIVALENCE (P(415), FLAXA  )
      EQUIVALENCE (P(416), FLAXG  )
      EQUIVALENCE (P(417), FLAXT  )
!     EQUIVALENCE (P(418),                OPEN  )
      EQUIVALENCE (P(419), FLAXB  )

!     EQUIVALENCE (P(420),                OPEN  )


!=====INPUT=====> PARAMETERS TO CONTROL THE QUICK LOOK PAGE OUTPUT
      DIMENSION            PTSHQ(12)
      EQUIVALENCE (P(421), PTSHQ( 1) )
!          "
!     EQUIVALENCE (P(432), PTSHQ(12) )


!     EQUIVALENCE (P(433),                OPEN  )
!     EQUIVALENCE (P(434),                OPEN  )


!=====INPUT=====> PARAMETER TO CONTROL THE QUICK LOOK PAGE OUTPUT
      EQUIVALENCE (P(435), PFTSHQ   )

!     P(436)...P(441) RESERVED FOR EARLIER VERSION COMPATIBILITY


!=====INPUT=====> PARAMETERS TO CONTROL THE QUICK LOOK PAGE OUTPUT
      DIMENSION            PBSHQ(3)
      EQUIVALENCE (P(442), PBSHQ(1) )
!          "
!     EQUIVALENCE (P(444), PBSHQ(3) )


!     P(445)...P(451) RESERVED FOR EARLIER VERSION COMPATIBILITY


!=====INPUT=====> PARAMETERS TO CONTROL TOSS PASSIVE SKIP DAMPER
!                 (MAINLY FOR TEST AND CHECKOUT, BUT USE IF YOU LIKE)
      EQUIVALENCE (P(452), PSD1   )
      EQUIVALENCE (P(453), PSD2   )
      EQUIVALENCE (P(454), TPSD12 )


!---------------------------------------
! VARIABLES FOR REF PT AERO DRAG FEATURE
!---------------------------------------
      DIMENSION RPVWE(3), RPVWP(3), RPVWI(3), RPWND(3)

!=====INPUT=====> AERO DRAG ACTIVATION FLAG, REF AREA, DRAG COEFF
      EQUIVALENCE (P(455), DRGFLG   )
      EQUIVALENCE (P(456), RPAARF   )
      EQUIVALENCE (P(457), RPCDRG   )

! DYNAMIC PRESSURE, DENS, SPEED OF SOUND
      EQUIVALENCE (P(458), RPQBAR   )
      EQUIVALENCE (P(459), RPDENS   )
      EQUIVALENCE (P(460), RPSSS    )
      EQUIVALENCE (P(461), RPHOT    )

! RELATIVE WIND DUE TO PLANET ROTATION (INERTIAL FRAME)
      EQUIVALENCE (P(462), RPVWE(1) )

! RELATIVE WIND PERTURBATIONS (INERTIAL FRAME)
      EQUIVALENCE (P(465), RPVWP(1) )

! TOTAL RELATIVE WIND (INERTIAL FRAME)
      EQUIVALENCE (P(468), RPVWI(1) )

! TOTAL PLANET WIND (PLANET FRAME)
      EQUIVALENCE (P(471), RPWND(1) )


!=========================================
! INPUT DATA CONTROLLING RESULTS DATA BASE
!=========================================
!=====INPUT=====> INHIBIT RDB TIME-SNAP-SHOT TO SPECIFIED FILE
      EQUIVALENCE (P(474), FLMISA  )
      EQUIVALENCE (P(475), FLMISG  )
      EQUIVALENCE (P(476), FLMIST  )
!     EQUIVALENCE (P(477),              OPEN  )
      EQUIVALENCE (P(478), FLMISB  )

!     EQUIVALENCE (P(479),              OPEN  )
!     EQUIVALENCE (P(480),              OPEN  )

!====================================================
! INPUT DATA DEFINING ENVIRONMENT SIMULATION FIDELITY
!====================================================
!=====INPUT=====> DEFINE ENVIRONMENT SIMULATION FIDELITY
      EQUIVALENCE (P(481), PGRVOP  )
      EQUIVALENCE (P(482), PATMOP  )
      EQUIVALENCE (P(483), PMAGOP  )
      EQUIVALENCE (P(484), PGLBOP  )
      EQUIVALENCE (P(485), PWNDOP  )
      EQUIVALENCE (P(486), PJULOP  )
      EQUIVALENCE (P(487), PEULMS  )
      EQUIVALENCE (P(488), PPLSOP  )

!     EQUIVALENCE (P(489),              OPEN  )


!=====INPUT=====> USER DEFINED CONSTANT PLANET RADIUS
! (OTHERWISE GTOSS SETS CONSTANT RADIUS TO THAT FROM GEOD AT TIME = O)
      EQUIVALENCE (P(490), RCONU   )


! REF PT MASS MATRIX, ITS INVERSE, AND 1/(REF PT MASS)
!-----------------------------------------------------
      DIMENSION            RPMSX(3,3),    RPMSXI(3,3)
      EQUIVALENCE (P(491), RPMSX(1,1)  )
      EQUIVALENCE (P(500), RPMSXI(1,1) )
      EQUIVALENCE (P(509), RPIOM       )


! BODY TO ORB FRAME TRANSFORMATION
!---------------------------------
      DIMENSION            RPGBO(3,3)
      EQUIVALENCE (P(510), RPGBO(1,1) )



!===========================================================
! INPUT DATA DEFINING PLANET-FIXED RP BASE MOTION SCENARIO 1
!===========================================================
!=====INPUT=====> START <== INPUT DATA DEFINING BASE MOTION SCENARIO 1
      EQUIVALENCE (P(519), S1START )
      EQUIVALENCE (P(520), S1STOP  )
      EQUIVALENCE (P(521), S1AZSCN )
! DATA DEFINING 1ST COSINE WAVE
      EQUIVALENCE (P(522), S1PER1 )
      EQUIVALENCE (P(523), S1RAT1 )
      EQUIVALENCE (P(524), S1ACC1 )
      EQUIVALENCE (P(525), S1TDL1 )
      EQUIVALENCE (P(526), S1AMP1 )
! DEAD TIME BETWEEN 1ST AND 2ND COSINE WAVE
      EQUIVALENCE (P(527), S1DEAD )
! DATA DEFINING 2ND COSINE WAVE
      EQUIVALENCE (P(528), S1PER2 )
      EQUIVALENCE (P(529), S1RAT2 )
      EQUIVALENCE (P(530), S1ACC2 )
      EQUIVALENCE (P(531), S1TDL2 )
      EQUIVALENCE (P(532), S1AMP2 )
! DATA DEFINING VERTICAL MOTION
      EQUIVALENCE (P(533), S1VPER )
      EQUIVALENCE (P(534), S1VAMP )
!=====INPUT=====> END <== INPUT DATA DEFINING BASE MOTION SCENARIO 1


!===========================================================
! INPUT DATA DEFINING PLANET-FIXED RP BASE MOTION SCENARIO 2
!===========================================================
!=====INPUT=====> START <== INPUT DATA DEFINING BASE MOTION SCENARIO 2
      EQUIVALENCE (P(535), S2START  )
      EQUIVALENCE (P(536), S2STOP   )
      EQUIVALENCE (P(537), S2AZSCN  )
! DATA DEFINING VERTICAL MOTION
      EQUIVALENCE (P(538), S2VPER   )
      EQUIVALENCE (P(539), S2VAMP   )
! BASE PALTFORM PERFORMANCE SPECIFICATION
      EQUIVALENCE (P(540), S2BASMS  )
      EQUIVALENCE (P(541), S2MAXSP  )
      EQUIVALENCE (P(542), S2MAXTM  )
! DATA DEFINING MAX PERF TIME DURATIONS, COAST DURATIONS
      EQUIVALENCE (P(543), S2MXPDT1 )
      EQUIVALENCE (P(544), S2COSDT1 )

      EQUIVALENCE (P(545), S2MXPDT2 )
      EQUIVALENCE (P(546), S2COSDT2 )

      EQUIVALENCE (P(547), S2MXPDT3 )
      EQUIVALENCE (P(548), S2COSDT3 )

      EQUIVALENCE (P(549), S2MXPDT4 )
      EQUIVALENCE (P(550), S2COSDT4 )

      EQUIVALENCE (P(551), S2MXPDT5 )
      EQUIVALENCE (P(552), S2COSDT5 )

      EQUIVALENCE (P(553), S2MXPDT6 )
      EQUIVALENCE (P(554), S2COSDT6 )

      EQUIVALENCE (P(555), S2MXPDT7 )
      EQUIVALENCE (P(556), S2COSDT7 )

      EQUIVALENCE (P(557), S2MXPDT8 )
      EQUIVALENCE (P(558), S2COSDT8 )

      EQUIVALENCE (P(559), S2CDOVR  )
!=====INPUT=====> END <== INPUT DATA DEFINING BASE MOTION SCENARIO 2


!     EQUIVALENCE (P(560),               OPEN  )



!=====================================================
! INPUT DATA DEFINING ENVIRONMENT SIMULATION FREQUENCY
!=====================================================
!=====INPUT=====> PREEMPT ALL ENVIRON'S TO EVAL EVERY REF PT TIME STEP
      EQUIVALENCE (P(561), PLOCEV  )

!=====INPUT=====> DEFINE SPECIFIC ENVIRONMENT SIMULATION TIME INTERVALS
      EQUIVALENCE (P(562), FRTGRV  )
      EQUIVALENCE (P(563), FRTATM  )
      EQUIVALENCE (P(564), FRTMAG  )
      EQUIVALENCE (P(565), FRTWND  )
      EQUIVALENCE (P(566), FRTPLS  )

!     EQUIVALENCE (P(567),              OPEN  )
!     EQUIVALENCE (P(568),              OPEN  )
!     EQUIVALENCE (P(569),              OPEN  )
!     EQUIVALENCE (P(570),              OPEN  )
!     EQUIVALENCE (P(571),              OPEN  )


! INTEGRATED BASE MOTION VARIABLES FOR BASE MOTION SCENARIO 2
      EQUIVALENCE (P(572),  BS2X   )
      EQUIVALENCE (P(573),  BS2XD  )
      EQUIVALENCE (P(574),  BS2XDD )


!     EQUIVALENCE (P(575),              OPEN  )
!          "
!     EQUIVALENCE (P(595),              OPEN  )


! TIMERS FOR ENVIRONMENT EVALUATION COMMANDS
      EQUIVALENCE (P(596), TIMPLS  )
      EQUIVALENCE (P(597), TIMGRV  )
      EQUIVALENCE (P(598), TIMATM  )
      EQUIVALENCE (P(599), TIMMAG  )
      EQUIVALENCE (P(600), TIMWND  )


!<><><><><><><><><><><><><><><><><><><><><><><><><><>><><><><><><><><>
! THE FOLLOWING DATA IS ASSOCIATED WITH THE FLEXIBLE BOOM ATTACH    <>
! POINT CAPABILITY IN GTOSS. IT CAN SERVE AS A TEMPLATE FOR OTHERS  <>
!<><><><><><><><><><><><><><><><><><><><><><><><><><>><><><><><><><><>

!=====INPUT=====> FLAG ACTIVATING FLEXIBLE BOOM SIMULATION
      EQUIVALENCE (P(601), BNNACT )

!=====INPUT=====> FLAG ACTIVATING DUAL STIFFNESS (JOINT FREQ) OPER.
      EQUIVALENCE (P(602), BNN12 )

!=====INPUT=====> FLAG TO SLAVE Y AXIS GEOMETRY TO Z AXIS DATA
      EQUIVALENCE (P(603), BNNYZ )

!=====INPUT=====> ATTACH POINT SERVICED BY FLEXIBLE BOOM
      EQUIVALENCE (P(604), BNNAP )

!=====INPUT=====> NUMBER OF BOOM MODAL COORDINATES IN Y AND Z AXES
      EQUIVALENCE (P(605), BNNQY )
      EQUIVALENCE (P(606), BNNQZ )

!     EQUIVALENCE (P(607),              OPEN)

!=====INPUT=====> BOOM LENGTH
      EQUIVALENCE (P(608), BOOMLN )

!=====INPUT=====> BOOM MASS
      EQUIVALENCE (P(609), BOOMS )

!=====INPUT=====> BOOM IST AND 2ND MASS MOMENTS WR/T BASE
      EQUIVALENCE (P(610), BOOMC1 )
      EQUIVALENCE (P(611), BOOMC2 )

!=====INPUT=====> BOOM BASE LOCATION IN REF PT BODY GEOMETRY FRAME
      DIMENSION            BRFBAS(3)
      EQUIVALENCE (P(612), BRFBAS(1))

! VARIABLE: POSITION VECTOR FROM CG TO BOOM BASE (BODY FRAME)
      DIMENSION            BMBASB(3)
      EQUIVALENCE (P(615), BMBASB(1))

!=====INPUT=====> BOOM EULER ANGLES REF PT BODY GEOMETRY FRAME (DEG)
      EQUIVALENCE (P(618), BOMTHT )
      EQUIVALENCE (P(619), BOMPHI )
      EQUIVALENCE (P(620), BOMPSI )

!=====INPUT=====> INITIAL VALUES OF BOOM MODAL COORDINATES
      DIMENSION            QBYO(5),    QBZO(5)
      EQUIVALENCE (P(621), QBYO(1) )
      EQUIVALENCE (P(626), QBZO(1) )

!=====INPUT=====> INITIAL VALUES OF BOOM MODAL COORDINATE RATES
      DIMENSION            QBYDO(5),    QBZDO(5)
      EQUIVALENCE (P(631), QBYDO(1) )
      EQUIVALENCE (P(636), QBZDO(1) )

!     EQUIVALENCE (P(641),              OPEN  )
!          "
!     EQUIVALENCE (P(645),              OPEN  )

!=====INPUT=====> SMALL DEFLECTION MODAL FREQ AND PERCENT CRIT DAMPING
      DIMENSION            QFREY1(5),QDMPY1(5),QFREZ1(5),QDMPZ1(5)
      EQUIVALENCE (P(646), QFREY1(1))
      EQUIVALENCE (P(651), QDMPY1(1))
      EQUIVALENCE (P(656), QFREZ1(1))
      EQUIVALENCE (P(661), QDMPZ1(1))

!=====INPUT=====> LARGE DEFLECTION MODAL FREQ AND PERCENT CRIT DAMPING
      DIMENSION            QFREY2(5),QDMPY2(5),QFREZ2(5),QDMPZ2(5)
      EQUIVALENCE (P(666), QFREY2(1))
      EQUIVALENCE (P(671), QDMPY2(1))
      EQUIVALENCE (P(676), QFREZ2(1))
      EQUIVALENCE (P(681), QDMPZ2(1))

!=====INPUT=====> MODAL COUPLING TERMS, AND TIP MODE SHAPE: Y AXIS
      DIMENSION            BMMY1(5), BMMUY1(5), BMTPY1(5)
      EQUIVALENCE (P(686), BMMY1(1))
      EQUIVALENCE (P(691), BMMUY1(1))
      EQUIVALENCE (P(696), BMTPY1(1))

!=====INPUT=====> MODAL COUPLING TERMS, AND TIP MODE SHAPE: Z AXIS
      DIMENSION            BMMZ1(5), BMMUZ1(5), BMTPZ1(5)
      EQUIVALENCE (P(701), BMMZ1(1))
      EQUIVALENCE (P(706), BMMUZ1(1))
      EQUIVALENCE (P(711), BMTPZ1(1))

!=====INPUT=====> MODAL COORDINATE BREAKPOINTS FOR JOINT STIFFNESS
      DIMENSION            QBRKY(5), QBRKZ(5)
      EQUIVALENCE (P(716), QBRKY(1) )
      EQUIVALENCE (P(721), QBRKZ(1) )

!=====INPUT=====> Y-Z MODAL COUPLING MATRIX (I,J ELEMENTS AS COL MATRIX)
      DIMENSION            BMMYZ1(25)
      EQUIVALENCE (P(726), BMMYZ1(1))


! VARIABLE: BOOM BASE FORCE AND COUPLE (INER FRAME)
      DIMENSION            BOOMFI(3), BOOMCI(3)
      EQUIVALENCE (P(751), BOOMFI(1))
      EQUIVALENCE (P(754), BOOMCI(1))

! VARIABLE: BOOM BASE FORCE AND COUPLE (BODY FRAME)
      DIMENSION            BOOMFB(3), BOOMCB(3)
      EQUIVALENCE (P(757), BOOMFB(1))
      EQUIVALENCE (P(760), BOOMCB(1))

! VARIABLE: BOOM TIP STATE WR/T TOSS REF PT (INER FRAME)
      DIMENSION            BOOMPI(3), BOOMVI(3), BOOMAI(3)
      EQUIVALENCE (P(763), BOOMPI(1))
      EQUIVALENCE (P(766), BOOMVI(1))
      EQUIVALENCE (P(769), BOOMAI(1))


! NOTE: ITEM 772 IS UN-RELATED TO BOOM MODE, BUT IS PRESERVED
!       FOR HISTORICAL CONSISTENCY OF INPUT DATA ITEMS
!=====INPUT=====> SECRET C/0 FORCE BETWEEN REF PT AND TOSS OBJECT 2
!                 (+ FORCE MAG MEANS FORCE IS AWAY FROM TOSS OBJECT)
!                 (- FORCE MAG MEANS FORCE IS   TOWARD  TOSS OBJECT)
! SIGNED-MAGNITUDE OF CONSTANT FORCE (.NE. ACTIVATES FORCE CALC)
      EQUIVALENCE (P(772), SECFOR  )


!=====INPUT=====> TIP MODAL SLOPE: Y AXIS
      DIMENSION            BSTPY1(5)
      EQUIVALENCE (P(773), BSTPY1(1))

!=====INPUT=====> TIP MODAL SLOPE: Z AXIS
      DIMENSION            BSTPZ1(5)
      EQUIVALENCE (P(778), BSTPZ1(1))


! VARIABLE: BOOM FRAME TO BODY FRAME TRANSFORMATION
      DIMENSION            RPGFB(3,3)
      EQUIVALENCE (P(783), RPGFB(1,1))

! VARIABLE: BOOM TIP FRAME TO INER FRAME TRANSFORMATION
      DIMENSION            BGPI(3,3)
      EQUIVALENCE (P(792), BGPI(1,1))

! VARIABLE: ANG VEL OF BOOM TIP FRAME (IN TIP FRAME)
      DIMENSION            BOMP(3), BOMPD(3)
      EQUIVALENCE (P(801), BOMP(1))
      EQUIVALENCE (P(804), BOMPD(1))

!<><><><><><><><><><><><><><><><><><><><><><><><><><>><><>
! END OF COMMON RELATED TO FLEXIBLE BOOM ATTACH POINTS  <>
!<><><><><><><><><><><><><><><><><><><><><><><><><><>><><>



!========================================================
! INPUT DATA DEFINING SIMPLE HOST ATTITUDE CONTROL SYSTEM
!========================================================
!=====INPUT=====> FLAG TO ACTIVATE/SPECIFY TYPE OF HOST ATT CONT SYSTEM
!                 (0.=OFF, 1.= WR/T HOST ORB FRAME, 2.= WR/T INER FRAME
!
      EQUIVALENCE (P( 807), ATTYPE )

!=====INPUT=====> RATE AND ERROR FEEDBACK GAINS
      EQUIVALENCE (P( 808), GRATEX )
      EQUIVALENCE (P( 809), GRATEY )
      EQUIVALENCE (P( 810), GRATEZ )
      EQUIVALENCE (P( 811), GERPHI )
      EQUIVALENCE (P( 812), GERTHT )
      EQUIVALENCE (P( 813), GERPSI )


!     EQUIVALENCE (P(814),               OPEN  )



!=====INPUT=====> DEFINE TOSS TETHER NO. TO USE IN LIBRATION CALCS
!                 WHICH ARE PROVIDED FOR ARB FORCE AND MOMENT CAPABILITY
      EQUIVALENCE (P( 815), PJTLIB    )

!=====INPUT=====> DEFINE LIBRATION ANGLE TYPE FOR HA ABOVE CALCS
      EQUIVALENCE (P( 816), PJLTYP    )

!=====INPUT=====> FLAG TO ACTIVATE ARBITRARY BODY FORCES
      EQUIVALENCE (P( 817), ARBFON    )

!=====INPUT=====> TABLE DATA DEFINING ARB HOST BODY AXIS FORCES VS TIME
      DIMENSION TABFX(43)
      EQUIVALENCE (P( 818), TABFX( 1) )
!     EQUIVALENCE (P( 860), TABFX(43) )

      DIMENSION TABFY(43)
      EQUIVALENCE (P( 861), TABFY( 1) )
!     EQUIVALENCE (P( 903), TABFY(43) )

      DIMENSION TABFZ(43)
      EQUIVALENCE (P( 904), TABFZ( 1) )
!     EQUIVALENCE (P( 946), TABFZ(43) )

! NOTE, DATA FOR CORRESPONDING MOMENT TABLE DEFS CAN BE FOUND AT P(158)



!=====INPUT=====> ATTACH POINT LOCATIONS IN REF PT BODY FRAME
! NOTE: THIS IS FOR ATT PTS 9-16
      DIMENSION RPPXBT__(8),   RPPYBT__(8),   RPPZBT__(8)
      EQUIVALENCE (P(947), RPPXBT__(1) )
      EQUIVALENCE (P(955), RPPYBT__(1) )
      EQUIVALENCE (P(963), RPPZBT__(1) )


!     EQUIVALENCE (P( 971),              OPEN  )
!          "
!     EQUIVALENCE (P(1042),              OPEN  )


!-------------------------------------------
! REF PT OBJECT MASS PROPERTIES AND GEOMETRY
!-------------------------------------------
! MASS AND MOMENTS OF INERTIA
      EQUIVALENCE (P(1043), RPMASS  )

      EQUIVALENCE (P(1044), RPIXX   )
      EQUIVALENCE (P(1045), RPIYY   )
      EQUIVALENCE (P(1046), RPIZZ   )

! PRODUCTS OF INERTIA
      EQUIVALENCE (P(1047), RPIXY   )
      EQUIVALENCE (P(1048), RPIXZ   )
      EQUIVALENCE (P(1049), RPIYZ   )

! LOCATION OF REF PT CENTER OF MASS
      EQUIVALENCE (P(1050), RPXBCG  )
      EQUIVALENCE (P(1051), RPYBCG  )
      EQUIVALENCE (P(1052), RPZBCG  )


!--------------------------------------------------------------------
! RP OBJECT EULER ANGLES AND BODY AXIS COMP OF ANGULAR WR/T ORB FRAME
!--------------------------------------------------------------------
      DIMENSION                     RPEULO(3)
      EQUIVALENCE (P(1053), RPPHIO, RPEULO(1) )
      EQUIVALENCE (P(1054), RPTHTO            )
      EQUIVALENCE (P(1055), RPPSIO            )

      DIMENSION             RPOMO(3)
      EQUIVALENCE (P(1056), RPOMO(1) )


! REFERENCE DIRCOS MATRIX FOR DATA RETENTION BY SUBR RPATTC.F
!------------------------------------------------------------
      DIMENSION             DIRHOL(3,3)
      EQUIVALENCE (P(1059), DIRHOL(1,1))

! ATT CONTROL ANGLE ERRORS AND ERROR RATES USED BY SUBR RPATTC.F
      EQUIVALENCE (P(1068), RFPHIE  )
      EQUIVALENCE (P(1069), RFTHTE  )
      EQUIVALENCE (P(1070), RFPSIE  )

      EQUIVALENCE (P(1071), RFRTXE  )
      EQUIVALENCE (P(1072), RFRTYE  )
      EQUIVALENCE (P(1073), RFRTZE  )


!--------------------------------------------
! DATA RELATING TO PLANET-FIXED REF PT OPTION
!--------------------------------------------
! POS, VEL, ACCEL PERTURBATIONS ON PLANET-FIXED RP BASE (IN TOPO-FRAME)
      DIMENSION             BSPER  (3)
      EQUIVALENCE (P(1074), BSPER  (1) )

      DIMENSION             BSPERD (3)
      EQUIVALENCE (P(1077), BSPERD (1) )

      DIMENSION             BSPERDD(3)
      EQUIVALENCE (P(1080), BSPERDD(1) )

! CYCLE MULTIPLIER/FLAG FOR RP FIXED BASE-MOTION SCENARIO 1
      EQUIVALENCE (P(1083), S1CYFL )


!     EQUIVALENCE (P(1084),              OPEN  )
!          "
!     EQUIVALENCE (P(1094),              OPEN  )




!---------------------------------------
! SCRATCH VECTORS AND MATRICES FOR GTOSS
!---------------------------------------
      DIMENSION VECI(3), VECB(3), R3X3(3,3), P3X3(3,3), Q3X3(3,3)
      DIMENSION                   R1X9(9)
      EQUIVALENCE (P(1095), VECI(1)   )
      EQUIVALENCE (P(1098), VECB(1)   )
      EQUIVALENCE (P(1101), R3X3(1,1),  R1X9(1) )
      EQUIVALENCE (P(1110), P3X3(1,1) )
      EQUIVALENCE (P(1119), Q3X3(1,1) )


!-------------------------------------------
! VARIABLES FOR HOST ATTACH POINT KINEMATICS
!-------------------------------------------
      DIMENSION RPXBT(16),    RPYBT(16),    RPZBT(16),    &
     &          RPFXBT(16),   RPFYBT(16),   RPFZBT(16),   &
     &          RPMXBT(16),   RPMYBT(16),   RPMZBT(16),   &
     &          RPGXBT(16),   RPGYBT(16),   RPGZBT(16)

! ACTUAL ATTACH POINT POSITIONS WR/T CENTER OF MASS
      EQUIVALENCE (P(1128), RPXBT(1)  )
      EQUIVALENCE (P(1144), RPYBT(1)  )
      EQUIVALENCE (P(1160), RPZBT(1)  )

! ATTACH POINT FORCES (BODY FRAME)
      EQUIVALENCE (P(1176), RPFXBT(1) )
      EQUIVALENCE (P(1192), RPFYBT(1) )
      EQUIVALENCE (P(1208), RPFZBT(1) )

! ATTACH POINT COUPLES (BODY FRAME)
      EQUIVALENCE (P(1224), RPMXBT(1) )
      EQUIVALENCE (P(1240), RPMYBT(1) )
      EQUIVALENCE (P(1256), RPMZBT(1) )

! ATTACH POINT MOMENTS WR/T CG (BODY FRAME)
      EQUIVALENCE (P(1272), RPGXBT(1) )
      EQUIVALENCE (P(1288), RPGYBT(1) )
      EQUIVALENCE (P(1304), RPGZBT(1) )


! BELOW ARE THE "ORIGINAL INPUT VARIABLE NAMES" FOR HOST ATT PT LOCATIONS.
! DUE TO INCREASING THE NUMBER FROM 8 TO 16, AND, IN DEFERENCE TO NOT DISRUPTING
! EITHER ANY EXECUTIONAL GTOSS CODE, NOR INVALIDATING EXISTING INPUT FILES, A
! TRIAGE OF THE "NEW INPUT-ARRAY NAMES" TO THIS EXPANDED ARRAY IS DONE IN INITIA.F.
!---------------------------------------------------------------------------------
! CONSTANT REFFERENCE ATTACH POINT LOCATIONS IN REF PT BODY FRAME
      DIMENSION RPPXBT(16),   RPPYBT(16),   RPPZBT(16)
      EQUIVALENCE (P(1320), RPPXBT(1) )
      EQUIVALENCE (P(1336), RPPYBT(1) )
      EQUIVALENCE (P(1352), RPPZBT(1) )


!     EQUIVALENCE (P(1368),              OPEN  )
!          "
!     EQUIVALENCE (P(1500),              OPEN  )
