! COMDECK EQU_MMDS
! %Z%GTOSS %M% H.5 code v02.00
!              H.5 code v02.00 (baseline for vH.5 delivery)
!----------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!**********************************************************
! Storage for tether deployment scenario 6:
! Martin Marietta Deployer System.
! this version is a simplified MMDS model:
! no sensors, effectors, or failures.
!**********************************************************
      REAL FT2MT
      PARAMETER ( FT2MT = 0.3048 )
      INTEGER MNVRMX
      PARAMETER( MNVRMX = 16 )

      INTEGER DMMDSZ
      INTEGER FMMDSZ
      INTEGER LMMDSZ
      PARAMETER ( DMMDSZ = 120 )
      PARAMETER ( FMMDSZ =  30 )
      PARAMETER ( LMMDSZ =  20 )

      REAL DMMDS (DMMDSZ)
      REAL FMMDS (FMMDSZ)
      INTEGER LMMDS (LMMDSZ)

! REAL STORAGE
!-------------
      EQUIVALENCE  (DTOSZQ(       1), FMMDS(1) )
      EQUIVALENCE  (DTOSZQ(FMMDSZ+1), DMMDS(1) )

! INTEGER STORAGE
!----------------
      EQUIVALENCE  (JTOSVQ(1), LMMDS(1) )

      REAL T
      REAL TAU
      REAL ALC
      REAL ALDC
      REAL ALDDC
      REAL ALRC
      REAL CTH
      REAL CTHD
      REAL CTHDD
      REAL FC
      REAL FUN
      REAL R1
      REAL AJ1
      REAL RLOMM
      REAL RLODMM
      REAL AM1
      REAL AM2
      REAL AMST
      REAL TOM
      REAL H
      REAL CTHS
      REAL CTHDS
      REAL CTHDDS
      REAL T0
      REAL TOS
      REAL FUN0
      REAL ALDC0
      REAL TST
      REAL ALCST
      REAL OM2
      REAL SSTIME
      REAL R1TIME
      REAL R2TIME
      REAL R3TIME
      REAL BRTIME
      REAL TABL
      REAL AET
      REAL AJ10
      REAL ALC0
      REAL ALDF
      REAL ALF
      REAL ALMAX
      REAL AMO
      REAL AMS
      REAL AMT
      REAL CTHTOM
      REAL DD
      REAL DX
      REAL OM
      REAL R02
      REAL ROW
      REAL RF2
      REAL TOMS
      REAL Z2B

      INTEGER LR
      INTEGER MNVR
      INTEGER ISOFT
      INTEGER IR
      INTEGER IBRAKE
      INTEGER KEY
      INTEGER IST

!     primary output variables, of high interest to users
!     guidance time (s)
      EQUIVALENCE (DMMDS( 1),T)
!     time ratio in segment (nd)
      EQUIVALENCE (DMMDS( 2),TAU)
!     range cmd (m)
      EQUIVALENCE (DMMDS( 3),ALC)
!     length rate cmd (m/s)
      EQUIVALENCE (DMMDS( 4),ALDC)
!     accel cmd (m/s2)
      EQUIVALENCE (DMMDS( 5),ALDDC)
!     unstretched length cmd (m)
      EQUIVALENCE (DMMDS( 6),ALRC)
!     inplane angle cmd (rad)
      EQUIVALENCE (DMMDS( 7),CTH)
!     inplane rate cmd (rad/s)
      EQUIVALENCE (DMMDS( 8),CTHD)
!     inplane accel cmd (rad/s2)
      EQUIVALENCE (DMMDS( 9),CTHDD)
!     tension cmd (N)
      EQUIVALENCE (DMMDS(10),FC)
!     ldot/L cmd (1/s)
      EQUIVALENCE (DMMDS(11),FUN)
!     reel radius (m)
      EQUIVALENCE (DMMDS(12),R1)
!     reel inertia (kg-m2)
      EQUIVALENCE (DMMDS(13),AJ1)
!     tether length (m)
      EQUIVALENCE (DMMDS(14),RLOMM)
!     length rate (m/s)
      EQUIVALENCE (DMMDS(15),RLODMM)

!     secondary variables available for output
!     adjusted mass of deployer object (kg)
      EQUIVALENCE (DMMDS(16),AM1)
!     adjusted mass of deployed object (kg)
      EQUIVALENCE (DMMDS(17),AM2)
!     reduced system mass (kg)
      EQUIVALENCE (DMMDS(18),AMST)
!     time of maneuver (s)
      EQUIVALENCE (DMMDS(19),TOM)
!     integration step size (s)
      EQUIVALENCE (DMMDS(20),H)

!     saved values, of little interest to users
!     CTH at start of soft stop maneuver (rad)
      EQUIVALENCE (DMMDS(21),CTHS)
!     CTHD at start of soft stop maneuver (rad/s)
      EQUIVALENCE (DMMDS(22),CTHDS)
!     CTHDD at start of soft stop maneuver (rad/s2)
      EQUIVALENCE (DMMDS(23),CTHDDS)
!     T at start of maneuver (s)
      EQUIVALENCE (DMMDS(24),T0)
!     T at start of soft stop maneuver (s)
      EQUIVALENCE (DMMDS(25),TOS)
!     saved value of FUN (1/s)
      EQUIVALENCE (DMMDS(26),FUN0)
!     saved value of ALDC (m/s)
      EQUIVALENCE (DMMDS(27),ALDC0)
!     T at start of constant LDOT mode (s)
      EQUIVALENCE (DMMDS(28),TST)
!     ALC at start of constant LDOT mode (m)
      EQUIVALENCE (DMMDS(29),ALCST)
!     OM * OM
      EQUIVALENCE (DMMDS(30),OM2)
!     soft stop time (s)
      EQUIVALENCE (DMMDS(31),SSTIME)
!     resume to sta 1 time (s)
      EQUIVALENCE (DMMDS(32),R1TIME)
!     resume to sta 2 time (s)
      EQUIVALENCE (DMMDS(33),R2TIME)
!     resume to dock time (s)
      EQUIVALENCE (DMMDS(34),R3TIME)
!     brake on time (s)
      EQUIVALENCE (DMMDS(35),BRTIME)
!     mission profile table
      DIMENSION TABL(MNVRMX,4)
      EQUIVALENCE (DMMDS(36),TABL)

!----------------------------------

!     guidance constants
!     tether stiffness (N)
      EQUIVALENCE (FMMDS( 1),AET)
!     empty reel inertia (kg-m2)
      EQUIVALENCE (FMMDS( 2),AJ10)
!     initial range cmd (m)
      EQUIVALENCE (FMMDS( 3),ALC0)
!     final ldot cmd (m/s)
      EQUIVALENCE (FMMDS( 4),ALDF)
!     range threshold for constant ldot (m)
      EQUIVALENCE (FMMDS( 5),ALF)
!     total tether load (m)
      EQUIVALENCE (FMMDS( 6),ALMAX)
!     orbiter mass (kg)
      EQUIVALENCE (FMMDS( 7),AMO)
!     satellite mass (kg)
      EQUIVALENCE (FMMDS( 8),AMS)
!     finite tether mass factor (nd)
      EQUIVALENCE (FMMDS( 9),AMT)
!     tomlin maneuver angle (deg)
      EQUIVALENCE (FMMDS(10),CTHTOM)
!     tether length in mechanisms (m)
      EQUIVALENCE (FMMDS(11),DD)
!     boom length (m)
      EQUIVALENCE (FMMDS(12),DX)
!     orbit rate (rad/s)
      EQUIVALENCE (FMMDS(13),OM)
!     empty reel radius, squared (m2)
      EQUIVALENCE (FMMDS(14),R02)
!     tether lineal density (kg/m)
      EQUIVALENCE (FMMDS(15),ROW)
!     full reel radius, squared (m2)
      EQUIVALENCE (FMMDS(16),RF2)
!     soft stop maneuver duration (s)
      EQUIVALENCE (FMMDS(17),TOMS)
!     satellite radius (m)
      EQUIVALENCE (FMMDS(18),Z2B)

!----------------------------------

!     guidance integers
!     inputs
!     number of segments (1-MNVRMX)
      EQUIVALENCE (LMMDS( 1),LR)
!     maneuver number (1-MNVRMX)
      EQUIVALENCE (LMMDS( 2),MNVR)
!     soft stop (1=perform soft stop)
      EQUIVALENCE (LMMDS( 3),ISOFT)
!     resume (1=to sta 1, 2=to sta 2, 3=to dock)
      EQUIVALENCE (LMMDS( 4),IR)
!     brake (0=off, 1=on)
      EQUIVALENCE (LMMDS( 5),IBRAKE)

!     outputs
!     maneuver type (1-3)
      EQUIVALENCE (LMMDS( 6),KEY)

!     internal saved values
!     constant LDOT mode
      EQUIVALENCE (LMMDS( 7),IST)
