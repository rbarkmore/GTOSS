! ROUTINE: Y04POP
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!*****************************************
!*****************************************
!
      SUBROUTINE Y04POP(JFUNC,IPOINT)
!
!*****************************************
!*****************************************
! THIS ROUTINE MANAGES THE POPULATING OF THE
! LABEL ARRAY AND DATA ARRAY BY CALLING THE
! FORMAT ROUTINES WITH JFUNC=1 OR 2


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS

! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS




      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_ROSS.h"
      include "../../A_HDR/COM_UOSS.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"





!*********************************************************************
! SET IN DATA LABELS AND FIND TOTAL NUMBER OF DATA ITEMS TO BE WRITTEN
!*********************************************************************

!------------------------------------------------
! POPULATE APPROPRIATE ARRAY WITH RPS INFORMATION
!------------------------------------------------
      CALL Y04RPS(JFUNC,IPOINT)

!------------------------------------------------
! POPULATE APPROPRIATE ARRAY WITH APS INFORMATION
!------------------------------------------------
      CALL Y04APS(JFUNC,IPOINT)

!--------------------------------------------------------
! POPULATE APPROPRIATE ARRAY WITH TOSS OBJECT INFORMATION
!--------------------------------------------------------
       IF(LASOBJ .NE. 1) THEN

            DO 1002 JBODY=2,LASOBJ

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
               CALL TOSLDW(JBODY)

! SET IN OBJECT BASIC DATA TO WRITE ARRAY
               CALL Y04BST(JFUNC,IPOINT,JBODY)

1002       CONTINUE
      END IF

!--------------------------------------------------------
! POPULATE APPROPRIATE ARRAY WITH TOSS TETHER INFORMATION
!--------------------------------------------------------
! FOR EACH TOSS TETHER
       IF(NTETH .NE. 0) THEN
          DO 1003 JTT=1,NTETH

             CALL Y04TET(JFUNC,IPOINT,JTT)

1003      CONTINUE
       END IF

!-------------------------------------------------------
! POPULATE APPROPRIATE ARRAY WITH BEAD MODEL INFORMATION
!-------------------------------------------------------
! IF DATA IS NON-EXISTANT, GO TO NEXT
       IF(NOPSZB .NE. 0) THEN

! START LOOP FOR FINITE SOLNS IN RUN
       DO 1004 JFTETH=1,NFINIT

! DONT POPULATE IF THIS IS NOT AN ASSIGNED, BEAD MODEL SOLN
          IF( (JASIGN(JFTETH) .EQ. 0)   &
     &               .OR. (NFTYPE(JFTETH) .EQ. 0) ) GO TO 1004

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER WITH SPECIFIED FINITE SOLN
              CALL TISLDW(JFTETH)

! SET IN BEAD SOLN BASIC DATA TO WRITE ARRAY
              CALL Y04BSB(JFUNC,IPOINT,JFTETH)

1004  CONTINUE

      END IF

!------------------------------------------------------
! POPULATE APPROPRIATE ARRAY WITH FLEX BOOM INFORMATION
!------------------------------------------------------
! DONT DUMP IF DATA IS NON-EXISTANT
       IF(NOPSZK .NE. 0) CALL Y04BSM(JFUNC,IPOINT)


!--------------------------------------------------------
! POPULATE APPROPRIATE ARRAY WITH MISC REF PT INFORMATION
!--------------------------------------------------------
! NOTE: THE ROUTINE Y04BSG CAN BE MODIFIED TO ALLOW INCLUSION
!       OF ANY HOST SIMULATION DATA A USER MIGHT WANT TO VERIFY
!       AS A PART OF THE TOSS AUTO-VERIFICATION SCHEME

! INHIBIT HOST SIMULATION MISC DATA DUMP IF USER REQUESTS
      IF(HOSTOF .NE. -1.0) THEN

! DONT DUMP IF DATA IS NON-EXISTANT
           IF(NOPSZG .NE. 0) CALL Y04BSG(JFUNC,IPOINT)

      END IF

      RETURN
      END
