! ROUTINE: YFMT02
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!******************************************************************
!******************************************************************
!******************************************************************
                     SUBROUTINE YFMT02 (JFUNC)
!******************************************************************
!******************************************************************
!******************************************************************
! SAO COMPARISON STUDY SNAPSHOT-DUMP FORMAT 02


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS

! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_UOSS.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"

!      include "../../A_HDR/EQU_OBJ.i"  (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.h"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.h"



!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"





!---------------------------------
!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
!---------------------------------
! ARCHIVE: INITIALIZATION AND HEADER WRITE
      IF(JFUNC .EQ. 1) GO TO 1000

! ARCHIVE: DATA DEFINITION AND WRITING
      IF(JFUNC .EQ. 2) GO TO 2000

! RDB BUILD: INITIALIZATION AND HEADER EXTRACT
      IF(JFUNC .EQ. 3) GO TO 3000

! RDB BUILD: DATA EXTRACTION AND RDB WRITING
      IF(JFUNC .EQ. 4) GO TO 4000

! WARN USER OF ILLEGAL ARGUMENT
!------------------------------
      WRITE(IUERR,11)
11    FORMAT(' YFMT02: ILLEGAL ARGUMENT')
      STOP ' IN YFMT02-0'



! SAO COMPARISON STUDY SNAPSHOT-DUMP       FORMAT 02
!******************************************
!******************************************
!   START EXPORT INITIALIZATION SECTION
!******************************************
!******************************************
1000  CONTINUE

! WRITE A ONE TIME HEADER ON FILE
      WRITE(IUOUT,22)
22    FORMAT('SAO SNAPSHOT DATA BLOCK FORMAT (METERS):'//   &
     &       '   TIME,  # BEADS  (I6, I3)' /   &
     &       '   SHUTTLE  X,Y,Z     (3F11.2)'  /   &
     &       '   BEAD #1  X,Y,Z     (3F11.2)'  /   &
     &       '   BEAD #2  X,Y,Z     (3F11.2)'  /   &
     &       '     "        "            '     /   &
     &       '     "        "            '     /   &
     &       '   BEAD #N  X,Y,Z     (3F11.2)'  /   &
     &       '   SATELITE X,Y,Z     (3F11.2)'  //   &
     &       '         (A  BLANK LINE SEPARATES EACH SNAPSHOT)'/)

!******************************************
! RETURN FROM EXPORT INITIALIZATION SECTION
!******************************************
      RETURN



! SAO COMPARISON STUDY SNAPSHOT-DUMP           FORMAT 02
!**********************************************
!**********************************************
!   START EXPORT DEFINITION/WRITING SECTION
!**********************************************
!**********************************************
2000  CONTINUE

!--------------------------
! LOOK FOR SNAP EVENTS ONLY
!--------------------------
      IF(NSHPEV .EQ. 0) RETURN

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER WITH SPECIFIED FINITE SOLN
      CALL TISLDW(1)

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
      CALL TOSLDW (2)


!--------------------------------------------
! DEFINE FINITE SOLN TIME AND NUMBER OF BEADS
!--------------------------------------------
      OUTP( 1) = TISTIM
      OUTP( 2) = REAL(NBEAD)

!------------------------
! DEFINE ORBITER POSITION
!------------------------
      OUTP( 3) = 0.0
      OUTP( 4) = 0.0
      OUTP( 5) = 0.0

!-----------------------------------
! DEFINE BEAD POSITIONS IN SAO FRAME
!-----------------------------------
! FORM TETHER FRAME TO ORB FRAME (GOT) TRANSF (STASH AS SCRATCH)
      CALL MATMUL (0,RPGOI,GIT, TOSMX2)

! START A LOOP TO EVALUATE EACH SPECIFIED BEADS POSITION
!-------------------------------------------------------
      DO 20 JBEAD = 1,NBEAD

! FORM INDEX INTO THIS BEADS STATE (IN SYSTEM VECTOR)
            JB1 = 3*(JBEAD - 1) + 1
            JB2 = JB1+1
            JB3 = JB1+2

! FORM TETHER FRAME COMPONENTS OF POS VECTOR WR/T REF END
            TOSVX1(1) = BUT(JB1) + REAL(JBEAD)*ELSGX
            TOSVX1(2) = BUT(JB2)
            TOSVX1(3) = BUT(JB3)

! TRANSFORM THIS POINT TO OBJECT ORB FRAME (AT "X" END)
            CALL MATVEC (0,TOSMX2,TOSVX1, TOSVX2)

! CALCULATE INDEX INTO OUTPUT ARRAY
            JDUM = 3*(JBEAD-1) + 5

! DEFINE THE BEAD COMPONENTS WR/T SAO ORBIT FRAME
            OUTP(JDUM+1) = -TOSVX2(3)*XFTXMR
            OUTP(JDUM+2) =  TOSVX2(1)*XFTXMR
            OUTP(JDUM+3) = -TOSVX2(2)*XFTXMR

20    CONTINUE


!-----------------------------------------------------
! SATELLITE POS WR/T ORBITER (IN SAO COORDINATE FRAME)
!-----------------------------------------------------
      OUTP(3*NBEAD+5 +1) = -RBOR(3)*XFTXMR
      OUTP(3*NBEAD+5 +2) =  RBOR(1)*XFTXMR
      OUTP(3*NBEAD+5 +3) = -RBOR(2)*XFTXMR


!---------------------------------
! WRITE A BLOCK TO THE OUTPUT FILE
!---------------------------------
      WRITE(IUOUT,14) NINT(OUTP(1)), NINT(OUTP(2)),   &
     &                (OUTP(J), J=3,3*NBEAD+5+3)

14    FORMAT(I6, I3/ 50(3F11.2/) )

!*****************************************************
! RETURN FROM EXPORT DATA DEFINITION AND WRITE SECTION
!*****************************************************
      RETURN




! SAO COMPARISON STUDY SNAPSHOT-DUMP              FORMAT 02
!*************************************************
!*************************************************
!  START IMPORT INITIALIZATION AND HEADER READS
!*************************************************
!*************************************************
3000  CONTINUE

! THIS FORMAT CORRESPONDS TO A SNAP SHOT FORMAT,
! AND PROBABLY WILL NEVER BE MADE REVERSIBLE
      WRITE(IUERR,3011)
3011  FORMAT(' IMPORT FORMAT 02 IS NOT AVAILABLE')
      STOP 'IN YFMT02-1'

!******************************************
! RETURN FROM IMPORT INITIALIZATION SECTION
!******************************************
!CC      RETURN




! SAO COMPARISON STUDY SNAPSHOT-DUMP              FORMAT 02
!*************************************************
!*************************************************
!  START IMPORT DATA READS AND TOSS COMMON BUILD
!*************************************************
!*************************************************
4000  CONTINUE

! DUMMY DUMMY DUMMY DUMMY ROUTINE

!***********************************************
! RETURN FROM IMPORT READS AND TOSS COMMON BUILD
!***********************************************
      RETURN

      END
