! ROUTINE: YFMT53
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!******************************************************************
!******************************************************************
!******************************************************************
              SUBROUTINE YFMT53 (JFUNC)
!******************************************************************
!******************************************************************
!******************************************************************
! THIS IS AN AVAILABLE UTOSS EXPANSION FORMAT STUBB
! SEE FORMAT 50 FOR STRUCTURAL DETAILS


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_UOSS.h"




      WRITE(IUERR,1011)
1011  FORMAT(' YFMT53: EXPORT FORMAT 53 NOT IMPLEMENTED BY USER')
      IF(JFUNC .GE. 0 ) STOP ' IN YFMT53-1'

      RETURN

      END
