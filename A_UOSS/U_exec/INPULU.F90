! ROUTINE: INPULU
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!********************************
!********************************
!
      SUBROUTINE INPULU
!
!********************************
!********************************
! THIS SUBROUTINE SOLICITS INPUT FOR EACH DISPLAY
! POST PROCESSING RUN


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_UOSS.h"





! DEFINE ANSI STD INTERNAL FILE FOR BUFFERING INPUT STREAM
      CHARACTER *80 INFILE
! DEFINE INPUT CHARACTER BUFFER
      CHARACTER  INCHAR(80)


!**************************************************
! START EACH RUN BY READING INPUT FILE FOR RUN DEFN
!**************************************************

! BUT FIRST, SEE IF END OF FILE HAS ALREADY BEEN DETECTED
      IF(NOEN) GO TO 1

! NOTIFY ERROR FILE THERES NO MORE RUNS IN INPUT FILE
                WRITE (IUERR,201)

! NOTIFY INTERACTIVE USER OF SAME
                IF(LBATCC .EQ. 0) WRITE (IUCRT,201)

                GO TO 80



!--------------------------------------------------------
! READ A TEXT HEADER AS FIRST 2 LINES IN ASCII INPUT FILE
!--------------------------------------------------------

! GO FETCH A LINE OF PURIFIED INPUT CHARS
! (ARG = 1 IN CALL TO INCHEU TRIGGERS INCHEU TO START
!          SEEKING USERS ENTRY OF A NEW RDB NAME)

1     CALL INCHEU (INCHAR,EOFLG,PDBNAM,PDBNUM,NPMFLG,XFNAM,1)
           IF(EOFLG .GT. 0.) GO TO 200
           WRITE(INFILE,2) INCHAR
2          FORMAT(80A1)

      READ (INFILE,10) (DTEXT(J,1),J=1,72)

      CALL INCHEU (INCHAR,EOFLG,PDBNAM,PDBNUM,NPMFLG,XFNAM,0)
           IF(EOFLG .GT. 0.) GO TO 200
           WRITE(INFILE,2) INCHAR

      READ (INFILE,10) (DTEXT(J,2),J=1,72)

   10 FORMAT(72A1)
      GO TO 11

! GO HERE AT END OF FILE ON INPUT FILE
  200 WRITE (IUERR,201)
  201 FORMAT('NO MORE RUNS IN UTOSS INPUT FILE')

      NOEN = .FALSE.
      GO TO 80

! END OF RUN TEXT HEADER READ-IN
   11 CONTINUE



!--------------------------------------------
! LOOP TO READ PARAMETERS FROM THE INPUT FILE
!--------------------------------------------

! CHECK FOR END OF FILE
   20 CALL INCHEU (INCHAR,EOFLG,PDBNAM,PDBNUM,NPMFLG,XFNAM,0)

           IF(EOFLG .GT. 0.) THEN
! GO HERE IF EOF IS HIT DURING PARAMETER READ
                  WRITE (IUERR,301)
  301             FORMAT ('HIT END OF FILE TRYING TO READ J,',   &
     &                    'P(J) FOR DISPLAY RUN')
                  GO TO 80
           END IF

           WRITE(INFILE,2) INCHAR

      READ (INFILE,40) K, PAR
   40 FORMAT(I3, F15.5)



! IF K IS NEG. TERMINATE FURTHER EXECUTION
!-----------------------------------------
      IF(K .LT. 0) THEN

!  SESSION TERMINATION, CLOSE UTOSS INPUT STREAM FILE
           CLOSE(IUIN)

! CLOSE AUDIT-ERROR REPORT FILE
           CLOSE(IUERR,STATUS='KEEP')

!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
! MAKE A FILE CALLED FINISH INDICATING THAT GTOSS IS THROUGH
! (FOR USE BY APPLESCRIPT OPERATING IN LOCAL DIRECTORY MODE)
!@MAC           OPEN(IUIN, FILE = 'IFINISH',  STATUS='NEW')

! MATERIALIZE FILE VIA WRITING
!@MAC           WRITE(IUIN,*) 'UTOSS FINISHED'
!@MAC           ENDFILE(IUIN)
!@MAC           CLOSE(IUIN, STATUS='KEEP')
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@

           STOP 'INPULU: NORMAL UTOSS TERMINATION'
      END IF


! IF K = 0, START THIS NEW RUN
      IF(K .EQ. 0)  GO TO 80

      PR(K) = PAR
      GO TO 20



!******************************
! SET UP RUN CONTROL PARAMETERS
!******************************
80    NSKI = NINT(TSKI1)
      NCOUN = NSKI
      PROCE = .TRUE.
      RUNFL = .TRUE.
      NLIN = 0
      NCR = 0
      IULIN = 0
      NBR = NINT(BATCON)

      IF(BATCON .GT. 0.0) LBATCC = 0
      IF(BATCON .EQ. 0.0) LBATCC = 1


! IDENTIFY THIS VERSION OF UTOSS TO INTERACTIVE USER
!---------------------------------------------------
      IF(LBATCC .EQ. 0) WRITE(IUCRT,*)  UTSID, CHAR(13)

! WRITE TEXT LINES FROM INPUT HEADER TO CRT FOR INTERACTIVE USER
!---------------------------------------------------------------
      IF(LBATCC .EQ. 0) WRITE (IUCRT,10) (DTEXT(J,1),J=1,72)
      IF(LBATCC .EQ. 0) WRITE (IUCRT,10) (DTEXT(J,2),J=1,72)


!**************************************
! VISIT SOLUTION SPECIFIC INPUT SUPPORT
!**************************************
! IF APPROPRIATE, INITIALIZE DATA EXPORT OPERATION
      IF(CXFLAG .EQ. 0.0) CALL ICEXP

! IF APPROPRIATE, INITIALIZE DATA IMPORT OPERATION
      IF(CXFLAG .EQ. 1.0) CALL ICIMP


! VISIT PROGRAM TO DISPLAY THIS RUN DEFN TO INTERACTIVE USER
!-----------------------------------------------------------
      IF(LBATCC .EQ. 0) CALL CRTPUL

      RETURN
      END
