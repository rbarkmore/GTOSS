! ROUTINE: TOZELV
! %Z%GTOSS %M% H.10 code v01.10
!              H.10 code v01.10  New GGIC logic fixes bug w/deploy mass opt,
!                                plus, reorg of chain triage logic.
!              H.10 code v01.01  Improved iterator capability
!--------------------------------------------------------------------------
!              H.10 code v01.00 New program to support Space Elevator
!*************************************
!*************************************
!
      SUBROUTINE TOZELV(ICHRP)
!
!*************************************
!*************************************
!
! INPUT ARG:  ICHRP  INDICATES CHAIN TO BE PLANET-FIXED STABILIZED.
!             THIS REFERENCED CHAIN MUST CONTAIN THE REF POINT OBJ 1
!
! THE ROUTINE INITIALIZES A CHAIN OF TETHERS TO BE VERTICAALY STABLE
! UNDER THE ACTION OF BOTH GRAVITY AND CENTRIFUGAL FORCE. THIS SUBR
! ONLY APPLIES TO THE CASE OF THE REF POINT OBJECT BEING FIXED TO THE
! PLANET, AND THE CHAIN OF TETHERS EMANATING FROM THAT RELATED PLANET
! FIXED ATTACH POINT.
!
! IT WILL ALLOW FOR THE INITIALIZATION OF A CHAIN OF TETHERS ANCHORED
! TO THE PLANET (CONSTITUTING A STRUCTURE WHICH CAN BE CONSTRUED AS A
! SPACE ELEVATOR). A CHAIN OF TETHERS IS DEFINED IDENTICALLY AS IN THE
! GRAV GRADIENT IC OPTION (WITH ALL THE SAME TOPOLOGICAL RESTRICTIONS).
! ONLY 1 OF THE (MAXIMUM TOPOLOGICALLY ALLOWED) 4 CHAINS WILL BE IC'D,
! THAT ONE BEING THE CHAIN CONTAINING THE REF PT OBJECT (SINCE ONLY
! THE RP OBJECT CAN BE FIXED TO THE PLANET). A CHAIN MAY HAVE UP TO 3
! SEGS (CORRESPONDING TO 4 TOSS OBJECTS, ONE OF WHICH MUST BE THE
! REF PT OBJECT), AND THE ASSOCIATED 3 TETHERS CAN BE ANY MIXED
! COMBINATIONS OF EITHER MASSLESS OR FINITE TETHER TYPES.
!
! RESULTING TETHER-STATES SPECIFIED FOR STABILITY ARE BASED ON THE
! CHAIN OF TETHERS BEING ALIGNED ALONG THE LOCAL VERTICAL...DEVIATION
! FROM THIS ASSUMPTION WILL PRODUCE RESPONSES OF LESSOR STABILITY
! THIS ROUTINE USES CHAIN TOPOLOGY AND CERTAIN STATE PARAMETERS
! THAT HAVE BEEN DETERMINED IN THE (TOSQV) GGIC CALL SEQUENCE
! PRECEEDING THE CALL TO THIS ROUTINE (AND STORED IN THE COMMON
! ARRAY CALLED: "CHAIN")
!
! IT CALCULATES A DEPLOYED LENGTH REQUIRED TO CREATE A TENSION
! DISTRIBUTION IN EACH TETHER THAT WILL PRODUCE EQUILIBRIUM WITH DUE
! REGARD TO CENTRIFUGAL FORCE, GRAVITY GRADIENT, AND THE LONGITUDINAL
! TETHER MATERIAL DISPLACEMENTS UNDER THE INFLUENCE OF THESE FORCES.
!
! THIS ROUTINE MAINTAINS THE ORIGINAL USER SPECIFIED POSITIONS AND
! VELOCITIES OF THE TOSS OBJECTS LINKING THE SECTIONS (SEGMENTS) OF
! THE CHAIN. IT MODIFIES ONLY THE DEPLOYED LENGTH OF EACH TETHER,
! AND, IF A TETHER IS A FINITE SOLN, ITS RELATIVE BEAD DISPLACEMENTS.


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS

! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.h"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.h"



!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"





! TRANSIENT STORAGE LOCATIONS PERTAINING TO CHAIN SEGMENTS AND TETHERS
         DIMENSION DUMNAC(3), DUMRMG(3), DUMGB(3)


! DETERMINE THE NUMBER OF SEGMENTS IN THIS CHAIN
! (CHECK FOR 1ST CHAIN SEGMENT WITH NO TOSS TETH NUMBER)
20    ISGCNT = 0

      DO 22 JSG=1,3
           IF(CHAIN(ICHRP, JSG, 1) .NE. 0) ISGCNT = ISGCNT + 1
22    CONTINUE


!-------------------------------------------------------
!-------------------------------------------------------
! EXTRACT CERTAIN USEFUL PARAMETERS FROM THE CHAIN ARRAY
!-------------------------------------------------------
!-------------------------------------------------------
! FOR ALL SEGMENTS PRESENT IN THIS REF-PT CHAIN, EXTRACT VARIABLES
! AND CALC USEFUL PARAMETERS
!
!     DUMRMG(JCHSG) = MAG OF POS VEC TO OBJ AT UPPER END OF CHAIN SEG
!     DUMGB(JCHSG)  = ACCEL OF GRAVITY AT UPPER END OF CHAIN SEG
!     DUMNAC(JCHSG) = NET ACCEL AT UPPER OBJECT

      DO 33 JCHSG = 1,ISGCNT

! CALC TENS NEEDED TO SWING A FREE TOSS OBJ AT UPPER END OF THIS SEGMENT
!-----------------------------------------------------------------------
! FIND TOSS OBJECT AT UPPER END OF SEGMENT
         JOBJ = NINT( ABS(CHAIN(ICHRP,JCHSG,3)) )

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
         CALL TOSLDW(JOBJ)

! DETERMINE MAGNITUDE OF OBJECT POSITION VEC TO INER PT
         DUMRMG(JCHSG) = VECMAG(RI)

! NEXT FIND PLANET FRAME COMP OF MASS POSITION VECTOR
         CALL EITEF (LJULOP,TOSTIM,RI,   TOSVX1)

! GET PLANET FRAME COMP OF ACCELERATION OF GRAV AT MASS
         CALL GRAV (LGRVOP,TOSTIM,TOSVX1,    TOSVX2)

! GET MAGNITUDE OF GRAV ACCELERATION AT THIS POINT
         DUMGB(JCHSG) = VECMAG(TOSVX2)

! CALC NET ACCEL (CENTRIFUGAL - GRAVITY) AT THE UPPER OBJECT (THIS
! ACCEL x MASS = FORCE REQUIRED TO SWING UPPER OBJECT CENTRIFGUALLY)
         DUMNAC(JCHSG) = DUMRMG(JCHSG)*OMEGAE**2 - DUMGB(JCHSG)

33    CONTINUE
!-----------------------
!-----------------------
! END OF EXTRACTION LOOP
!-----------------------
!-----------------------

!---------------------------------------------------------------------
!---------------------------------------------------------------------
!---------------------------------------------------------------------
! START W/CHAIN SEG FARTHEST FROM PLANET, WORK PLANET-WARD DETERMINING
! DEPLOYED LENGTH STATES REQUIRED TO EQUILIBRATE EACH CHAIN SECTION
!
! THE PROCESS TERMINATES UPON FINDING THE DELOYED LENGTH FOR THE CHAIN
! SEGMENT ANCHORED TO THE PLANET, THAT WILL EQUILIBRATE THE PLANET
! SEGMENT IN FULL CONSIDERATION OF THE TENS REQUIRED TO EQUILIBRATE
! ALL OUTBOARD CHAIN SEGMENTS
!--------------------------------------------------------------------
!--------------------------------------------------------------------
!--------------------------------------------------------------------
! SINCE THERE ARE NO TETHERS PULLING OUTBOARD OF THE TOP-MOST TOSS
! OBJECT, AND SINCE THE CHAIN-SEGMENT LOOP BELOW STARTS WITH THE
! TOP-MOST CHAIN-SEGMENT, INITIALIZE OUTBOARD TENSION TO ZERO TO ENTER
      DUMOBT = 0.0



!****************************************************************
!****************************************************************
!****************************************************************
!****************************************************************
! START EXAMINING EACH SEGMENT STARTING FROM THE TOP-MOST SEGMENT
!****************************************************************
!****************************************************************
!****************************************************************
!****************************************************************
      DO 999 JCHSG = ISGCNT,1,-1


! INTIALIZE ITERATION PHASE SELECTOR
         IPHASE = 0

! GET OBJECT NO. AT TOP OF THIS CHAIN SEGMENT
         JOBJ = NINT( ABS(CHAIN(ICHRP,JCHSG,3)) )

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
         CALL TOSLDW(JOBJ)

! GET TETHER AND RELATED FINITE SOLN NUMBER FOR THIS CHAIN SEGMENT
         JTETH  = NINT(CHAIN(ICHRP,JCHSG,1))
         JFINIT = LASIGN(JTETH)

! CALC "NET TENS" TO SWING SEGMENT'S TOP MASS (AND ALL OUTBOARD)
! CENTRIFUGALLY. NOTE: FOR TOP MOST SEGMENT, THIS IS A FREE OBJ
! WITH NO OUTBOARD TETHERS, IE. DUMOBT = 0.0 ON 1ST TIME THRU.

! INCREMENT THE TENSION REQUIRED TO CENTRIFUGALLY-SWING THE FREE
! OBJECT (AT THE UPPER END OF THIS SEGMENT) BY THE AMOUNT OF ANY
! TETHER TENSION PULLING ON THE OBJECT FROM THE SEGMENT ABOVE
!----------------------------------------------------------------
         TENBLS = DMASS*DUMNAC(JCHSG) + DUMOBT

!---------------------------------------------------------------------
! DO DEFAULT DEPLOYED LENGTH CALC ASSUMING MASSLESS TETH FOR CHAIN SEG
!---------------------------------------------------------------------
! FIRST, CALCULATE DISTANCE BETWEEN ATTACH POINTS FOR THIS SEG
         JOX = NOBJX(JTETH)
         JAX = LATTX(JTETH)
         JOY = NOBJY(JTETH)
         JAY = LATTY(JTETH)

! FORM POSITION VECTOR BETWEEN ATT PTS OF SEGMENTS TOSS TETHER
         DO 44 J = 1,3
            TOSVX1(J) = APS(J+6, JAY, JOY) - APS(J+6, JAX, JOX)
44       CONTINUE

! FIND DISTANCE BETWEEN ATTACH POINTS
         DISDUM = VECMAG(TOSVX1)



! DETERMINE IF BEAD MODEL IS IN FORCE FOR THIS SEGMENT
! (IF NOT, THEN THE SOLUTION FOR THIS CHAIN-SEGMENT IS
!  COMPLETE, SO, GO TO END OF LOOP FOR NEXT CHAIN-SEGMENT)
!---------------------------------------------------------
! DEFAULT TO MASSLESS CALC IF ACTIVE, FINIT SOLN IS NOT ASSIGNED
         IF(((JFINIT .EQ. 0) .OR. (JFINIT .GT. NFINIT)) .OR.   &

! DEFAULT TO MASSLESS IF FINITE SOLN TYPE IS NOT A BEAD MODEL   &
     &           ( NFTYPE(JFINIT) .EQ. 0 ) ) THEN

! THEN CALCULATE REQUIRED DEPLOYED TETHER LENGTH
! (ALLOW FOR BOTH TYPES OF MASSLESS PROPERTIES DEFINITIONS)
            IF(TLENO(JTETH) .NE. 0.0) THEN
                CODUM = TSPRNO(JTETH)*TLENO(JTETH)
                TLENT(JTETH) = DISDUM*CODUM/(TENBLS + CODUM)
            ELSE
                TLENT(JTETH) = DISDUM*TTAE(JTETH)/(TENBLS +TTAE(JTETH))
                TLENO(JTETH) = TLENT(JTETH)
                TSPRNO(JTETH) = TTAE(JTETH) /TLENO(JTETH)
                TDAMPO(JTETH) = TTBETF(JTETH)*TSPRNO(JTETH)
            END IF

! SAVE THE TENSION AT THE BOTTOM END OF THIS MASSLESS CHAIN-SEGMENT,
! SINCE IT WILL BE THE TENSION EXERTED FROM ABOVE ON THE UPPER-MOST
! TOSS OBJECT OF THE CHAIN-SEGMENT JUST BELOW
            DUMOBT = TENBLS

            GO TO 999

          END IF


!-------------------------------------------------------------------
! THIS CHAIN SEG USES A FINITE TETHER SO PROCEED WITH ITERATIVE SOLN
!-------------------------------------------------------------------
! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
         CALL TISLDW(JFINIT)

! ELBSG(JS)  = SEGMENT LENGTH
! SEGLA(JS)  = DEPLOYED, UNDEFORMED LENGTH OF TETH IN A SEGMENT
! ELSGX      = X-AXIS UNIFORM SEG LENGTH BASED ON DIST BETWEEN ATT PTS
! EL         = INSTANTANEOUS DEPLOYED (UNDEFORMED) TETH LENGTH - WHOLE TETHER
! ELB        = DEPLOYED (UNDEFORMED) TETHER LENGTH             - BETWEEN 2 BEADS
! ELMG       = INSTANTANEOUS DISTANCE BETWEEN ATT PTS

! BEADKS(JS) = CURRENT EFFECTIVE SEGMENT SPRING RATE
! BMS(JB)    = CURRENT EQUIVALENT MASS OF A BEAD

! JB1  = 3*(JB-1) + 1 = X-COORD PTR INTO STATE OF BEAD JB
! JB1P = 3*(JB-2) + 1 = X-COORD PTR INTO STATE OF BEAD PREVIOUS TO JB

! RYENDI(3)  = POS WR/T INER PT OF Y-END OF BEAD SEG (INER FRAME) TNSPVS MUST BE CALLED

! TDIST(JTT) = INSTANTANEOUS DISTANCE BETWEEN ATTACH POINTS
! TLENT(JTT) = INSTANTANEOUS TETHER DEPLOYED LENGTH


!====================================================
! INITIALIZE DEPLOYED TETH LENGTH ITERATION VARIABLES
!====================================================
! CALC INITIAL DEPLOYED LENGTH
         EL = TDIST(JTETH)

! GIVE INITIAL VALUES TO SOME "PREVIOUS-VERSIONS" OF VARIABLES
!-------------------------------------------------------------
         TENASG = 0.0
         D_TENASG = 0.0


! LOAD CURRENT OBJECT MASS AT TOP OF CHAIN SEG
         JOBJ = NINT( ABS(CHAIN(ICHRP,JCHSG,3)) )

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
         CALL TOSLDW(JOBJ)


!**************************************************
!**************************************************
!**************************************************
! START THE BIG DEPLOYED LENGTH SOLN-ITERATION LOOP
!**************************************************
!**************************************************
!**************************************************
        DO 111 JCNTR = 1,400


! CALC TENSION REQUIRED TO SWING THIS MASS CENTRIFUGALLY + TENSION ABOVE
             TENBLS = DMASS*DUMNAC(JCHSG) + DUMOBT

! ZERO THE BEAD X-COORDS TO START EACH ITERATION
               DO 100 J=1,NBEAD
                  JBX = 3*(J-1) + 1
                  BUT(JBX)  = 0.0
                  BUDT(JBX) = 0.0
                  BUI(JBX)  = 0.0
                  BUDI(JBX) = 0.0
100            CONTINUE

! INITIALIZE SEGMENT LENGTH ACCUMULATOR
               SEGSUMDUM = 0.0

! INITIALIZE ACCUMULATOR FOR TENSION TO SWING ALL MASS OUTBOARD OF A
! FINITE-TETHER-SEGMENT. THE FIRST TETHER SEGMENT IN THE LOOP IS THE
! ONE IMMEDIATELY UNDER THE TOSS OBJ AT THE TOP OF THE CHAIN-SEGMENT.
!
! NOTE, EVENTUALLY, THE CALCULATION OF THE ACTUAL TENSION IN THE
!       FINITE-TETHER SEG AT THE LOWER-MOST END OF THE CHAIN-SEGMENT,
!       MUST END UP W/ THIS TENSION...THE ITER. CONVERGENCE CRITERIA
!--------------------------------------------------------------------
            TENOBM = TENBLS

! BASED ON DEPLOY LENGTH, CALC DEPLOYED, UNDEFORMED LENGTH BETWEEN BEADS
            ELB = EL*OBNSEG

! CALC BEAD SEGMENT EFFECTIVE SPRING FOR NEW DEPLOYED LENGTH
            CALL TNSMKS



!*********************************************************************
!*********************************************************************
! DETERMINE WHETHER THIS CHAIN SEGMENT TETHER HAS THE X- OR Y-END DOWN
!*********************************************************************
!*********************************************************************
            IF(CHAIN(ICHRP,JCHSG,3) .GT. 0.0) THEN

!======================================================================
!======================================================================
!
!  ----> USE BEAD GEOMETRY FOR Y-END DOWN FINITE TETH CHAIN SEG <----
!
!======================================================================
! CALC BEAD X-COORDS SO SEG TENSION WILL SWING OB MASS, THEN ACCUM TENS
!======================================================================
            DO 222 JBB = 1,NBEAD

! CALC INDEX INTO X-COORD STATE OF JBB AND THE BEAD OUTBOARD OF IT
                 JBX   = 3*(JBB-1) + 1
                 JBXOB = JBX - 3

! CALC PTR TO SEGMENT OUTBOARD OF THIS BEAD
                 JSOB = JBB

! ACTIVATE SUB TO CALC SEGLA VARIABLE
!------------------------------------
! (THIS CALL PROVIDES DEPLOYED, UNDEFORMED LENGTH OF TETH IN A SEGMENT)
                 CALL TNSHOK

! USE DUMMY VARS FOR NOTATION
                 DUMKS = BEADKS(JSOB)
                 DUMSL = SEGLA(JSOB)

! CALC SEGS LOWER BEAD X-DISPL TO PRODUCE TENS TO SWING OUTBOARD MASS
!--------------------------------------------------------------------
! TREAT SEGMENT ADJACENT TO UPPER TOSS OBJECT SEPARATELY
                 IF(JBB .EQ. 1)   &

     &             THEN
                     BUT(JBX) = TENOBM/DUMKS -ELSGX +DUMSL

! TREAT ALL OTHER SEGMENTS AS PER
                   ELSE
                     BUT(JBX) = TENOBM/DUMKS -ELSGX +DUMSL +BUT(JBXOB)

                 END IF

!===================================================
! CALCULATE THIS BEADS CONTRIBUTION TO OUTBOARD LOAD
!===================================================

! FIRST GET BEAD RADIUS VECTOR FOR CURRENT BEAD X-DISPL
!------------------------------------------------------
! (THESE CALLS SET UP AND PROVIDE BEAD INERTIAL POSITION, RYENDI)
                 CALL TNSBIT
                 CALL TNSSEG
                 CALL TNSVEL
                 CALL TNSPVS(JBB)


! SEGMENT'S LOWER BEAD CENTRIFUGAL FORCE
!---------------------------------------
                 DUMRBB = VECMAG(RYENDI)
                 DUMCF = BMS(JBB)*DUMRBB*OMEGAE**2

! CALC SEG'S LOWER BEAD GRAVITY FORCE
!------------------------------------
! NEXT FIND PLANET FRAME COMP OF BEADS POSITION VECTOR
                 CALL EITEF (LJULOP,TOSTIM,RYENDI,   TOSVX1)

! GET PLANET FRAME COMP OF ACCELERATION OF GRAV AT BEAD
                 CALL GRAV (LGRVOP,TOSTIM,TOSVX1,    TOSVX2)

! GET MAGNITUDE OF GRAVITY ACCELERATION
                 DUMACC = VECMAG(TOSVX2)
                 DUMALT = DUMRBB - RE

! GET MAGNITUDE OF GRAVITY FORCE
                 DUMGF = BMS(JBB)*DUMACC

! ACCUMULATE THESE FORCES FOR NEXT LOWER SEG TO SWING
!----------------------------------------------------
                 TENOBM = TENOBM + DUMCF - DUMGF

! ACCUMULATE THIS SEGMENT LENGTH CONTRIBUTION TO TOTAL
! (NOTE: CALC OF DIST BETWEEN UPPER OBJ AND LAST BEAD WILL HAVE BEEN
! ACCOMPLISHED AT THIS POINT VIA THE ACCUMULATION IN LOOP ABOVE)
                 SEGSUMDUM = SEGSUMDUM + ELBSG(JBB)

222         CONTINUE

! CAPTURE ACTUAL DISTANCE BETWEEN THE ANCHOR BEAD AND ITS ATT PT
            ASEGLDUM = ELBSG(NBEAD + 1)


!======================================================================
!======================================================================
! END OF Y-END DOWN BEAD LOOP TO ADJUST EACH BB X-COORD TO SWING OB MASS
!======================================================================
!======================================================================

            ELSE

!======================================================================
!======================================================================
!
!  ----> USE BEAD GEOMETRY FOR X-END DOWN FINITE TETH CHAIN SEG <----
!
!======================================================================
! CALC BEAD X-COORDS SO SEG TENSION WILL SWING OB MASS, THEN ACCUM TENS
!======================================================================
            DO 333 JBB = NBEAD,1,-1

! CALC INDEX INTO X-COORD STATE OF JBB AND THE BEAD OUTBOARD OF IT
                 JBX   = 3*(JBB-1) + 1
                 JBXOB = 3*JBB + 1

! CALC PTR TO SEGMENT OUTBOARD OF THIS BEAD
                 JSOB = JBB + 1

! ACTIVATE SUB TO CALC SEGLA VARIABLE
!------------------------------------
! (THIS CALL PROVIDES DEPLOYED, UNDEFORMED LENGTH OF TETH IN A SEGMENT)
                 CALL TNSHOK

! USE DUMMY VARS FOR NOTATION
                 DUMKS = BEADKS(JSOB)
                 DUMSL = SEGLA(JSOB)

! CALC SEGS LOWER BEAD X-DISPL TO PRODUCE TENS TO SWING OUTBOARD MASS
!--------------------------------------------------------------------
!-------------------------------------------------------
! FOR Y-END SEGMENT (ADJACENT TO UPPER TOSS OBJECT)
!           ELBSG(JSEG) = - BUI(JB1P) + ELSGX
!           BUT(JB1P)  = - TENOBM/DUMKS + ELSGX  - DUMSL
!-------------------------------------------------------
! FOR INTERIOR SEGMENTS
!           ELBSG(JSEG) = BUI(JB1) - BUI(JB1P) + ELSGX
!           BUT(JB1P)  = - TENOBM/DUMKS + BUT(JB1)  + ELSGX  - DUMSL
!-------------------------------------------------------
! TREAT SEGMENT ADJACENT TO UPPER TOSS OBJECT SEPARATELY
                 IF(JBB .EQ. NBEAD)   &

     &             THEN
                     BUT(JBX) = -TENOBM/DUMKS +ELSGX -DUMSL

! TREAT ALL OTHER SEGMENTS AS PER
                   ELSE
                     BUT(JBX) = -TENOBM/DUMKS +ELSGX -DUMSL +BUT(JBXOB)

                 END IF

!===================================================
! CALCULATE THIS BEADS CONTRIBUTION TO OUTBOARD LOAD
!===================================================

! FIRST GET BEAD RADIUS VECTOR FOR CURRENT BEAD X-DISPL
!------------------------------------------------------
! (THESE CALLS SET UP AND PROVIDE BEAD INERTIAL POSITION, RYENDI)
                 CALL TNSBIT
                 CALL TNSSEG
                 CALL TNSVEL
                 CALL TNSPVS(JBB)

! SEGMENT'S LOWER BEAD CENTRIFUGAL FORCE
!---------------------------------------
                 DUMRBB = VECMAG(RYENDI)
                 DUMCF = BMS(JBB)*DUMRBB*OMEGAE**2

! CALC SEG'S LOWER BEAD GRAVITY FORCE
!------------------------------------
! NEXT FIND PLANET FRAME COMP OF BEADS POSITION VECTOR
                 CALL EITEF (LJULOP,TOSTIM,RYENDI,   TOSVX1)

! GET PLANET FRAME COMP OF ACCELERATION OF GRAV AT BEAD
                 CALL GRAV (LGRVOP,TOSTIM,TOSVX1,    TOSVX2)

! GET MAGNITUDE OF GRAVITY ACCELERATION
                 DUMACC = VECMAG(TOSVX2)
                 DUMALT = DUMRBB - RE

! GET MAGNITUDE OF GRAVITY FORCE
                 DUMGF = BMS(JBB)*DUMACC

! ACCUMULATE THESE FORCES FOR NEXT LOWER SEG TO SWING
!----------------------------------------------------
                 TENOBM = TENOBM + DUMCF - DUMGF


! ACCUMULATE THIS SEGMENT LENGTH CONTRIBUTION TO TOTAL
                 SEGSUMDUM = SEGSUMDUM + ELBSG(JSOB)

333       CONTINUE

! CAPTURE ACTUAL DISTANCE BETWEEN THE ANCHOR BEAD AND ITS ATT PT
          ASEGLDUM = ELBSG(1)


!======================================================================
!======================================================================
! END OF X-END DOWN BEAD LOOP TO ADJUST EACH BB X-COORD TO SWING OB MASS
!======================================================================
!======================================================================

! END OF LOGIC FOR X-END -VS- Y-END SEGMENT TETHER DETERMINATION
          END IF


!------------------------------------------------------------
! NOW, SEE WHAT THE TENSION IN THE ANCHOR SEG TURNS OUT TO BE
!------------------------------------------------------------
! ACTIVATE SERIES OF SUPPORT ROUTINES TO RECONSTRUCT SEG TENSIONS
          CALL TNSBIT
          CALL TNSSEG
          CALL TNSHOK

! SAVE CURRENT VALUE OF ANCHOR TENSION FOR TREND DETERMINATION
          TENASGP = TENASG

! EXTRACT ANCHOR SEG TENSION BASED ON WHICH END IS DOWN
!------------------------------------------------------
          IF(CHAIN(ICHRP,JCHSG,3) .GT. 0.0) THEN
                TENASG = TENSEG(NBEAD+1)
          ELSE
                TENASG = TENSEG(1)
          END IF

! EVALUATE ANCH TENSION TREND (SETTING DEFACTO 1ST ITERATION PASS TREND)
          D_TENASGP = D_TENASG
          D_TENASG  = TENASG - TENASGP
          IF (JCNTR .EQ. 1) D_TENASG  = 0.0
          IF (JCNTR .EQ. 1) D_TENASGP = 0.0


!***********************************************************************
!***********************************************************************
! ITERATION LOGIC COMPARING ANCH-SEG TENS TO REQUIRED OUTBOARD SWING TEN
!***********************************************************************
!***********************************************************************

!************************************************************************
! PHASE 0: ASSESS STATE OF BOTTOM SEG SLACK RESULTING FROM INITIAL DEPLOY
!************************************************************************
! THIS PHASE DETERMINES:
!     1. WHICH ITERATION PHASE TO INVOKE NEXT, AND
!     2. THE INITIAL LENGTH INCREMENT FOR THAT PHASE (IF APPROPRIATE)
!
! SEGSUMDUM  = BOTTOM-BEAD'S DISTANCE BELOW UPPER OBJECT
! ASEGLDUM   = ACTUAL DISTANCE OF BOTTOM-BEAD FROM THE BOTTOM OBJECT

      IF(IPHASE .EQ. 0) THEN

! SEE WHETHER LAST BEAD IS ABOVE OR BELOW
!----------------------------------------
         IF(SEGSUMDUM .LT. TDIST(JTETH)) THEN

!-------------------------------------------------------------
! =====> EXECUTE PHASE 0 MODE SELECTION FOR "BEAD ABOVE LOGIC"
!--------------------------------------------------------
              IF(TENASG .GT. 0.0) THEN

! BEAD-ABOVE WITH NON-ZERO ANCHOR TENSION
!----------------------------------------
                 DELELL = 0.2*ELB
                 IPHASE = 3
              ELSE

! BEAD-ABOVE WITH ZERO ANCHOR TENSION
!------------------------------------
                  DELELL = ELB - ASEGLDUM
                  IF(DELELL .LT. 0.3*ELB) DELELL = 0.3*ELB
                  IPHASE = 3
              END IF

         ELSE

!--------------------------------------------------------
! =====> EXECUTE PHASE 0 MODE SELECTION FOR "BELOW LOGIC"
!--------------------------------------------------------
              IF(TENASG .EQ. 0.0) THEN

! BEAD-BELOW WITH ZERO ANCHOR TENSION
!------------------------------------
                  DELELL = ELB + ASEGLDUM
                  IPHASE = 3
              ELSE

! BEAD-BELOW WITH NON-ZERO ANCHOR TENSION
!----------------------------------------
                  IF(TENOBM .GE. 0.0) THEN

! CASE OF TENOBM POSITIVE
                      IF(TENASG .GE. TENOBM) THEN

! SUB-CASE OF TENOBM POSITIVE W/ ANCHOR TENSION GREATER THAN TENOBM
                             DELELL = ELB
                             D_TENASGP = -1.0
                             D_TENASG  = -1.0
                             IPHASE = 2
                      ELSE
! SUB-CASE OF TENOBM POSITIVE W/ ANCHOR TENSION LESS THAN TENOBM
                             DELELL = 3.0*ELB
                             DUMELI = DELELL
                             IPHASE = 4
                      END IF

                  ELSE

! CASE OF TENOBM NEGATIVE
                      DELELL = ELB
                      IPHASE = 1
                  END IF

              END IF

         END IF

      END IF


!**********************************************************************
! PHASE 1: TIGHTEN TETH UNTIL RELIABLE NEG. TENASG GRADIENT ESTABLISHED
!**********************************************************************
      IF(IPHASE .EQ. 1) THEN

! TEST FOR CURRENT AND PREVIOUS NEGATIVE TENASG CHANGE
          IF((D_TENASGP .LT. 0.0) .AND. (D_TENASG .LT. 0.0)) THEN

! TERMINATE THIS PHASE OF ITERATION AND START NEXT PHASE
                IPHASE = 2
          ELSE
                EL = EL - DELELL
! CONTINUE IN THIS ITERATION PHASE
                GO TO 444

          END IF

      END IF


!****************************************************
! PHASE 2: CRUISE UNTIL A MINIMUM IN TENASG IS SENSED
!****************************************************
      IF(IPHASE .EQ. 2) THEN

! TEST FOR EXTREMUM OCCURANCE IN TENASG
          IF((D_TENASGP .GE. 0.0) .OR. (D_TENASG .GE. 0.0)) THEN

! REVERSE (BACK UP) ITERATION BY 1 LENGTH INCREMENT STEP
                EL = EL + DELELL

! TERMINATE THIS ITERATION PHASE BUT RE-VISIT STATE-CALCS TO CONTINUE
                IPHASE = 3
                GO TO 444
          ELSE
                EL = EL - DELELL
                TENTRAP = TENASG

! CONTINUE IN THIS ITERATION PHASE
                GO TO 444

          END IF

      END IF



!******************************************************************
! PHASE 3: SLOWLY TAKE SLACK OUT OF ANCHOR SEGMENT USING FINE-STEPS
!******************************************************************
      IF(IPHASE .EQ. 3) THEN

! SET UP BASE INCREMENT FOR THIS PHASE
          DUMELI = 0.1*DELELL

! TEST FOR TENASG EXCEEDING TENOBM
          IF((TENOBM .GT. 0.0) .AND. (TENASG .GT. TENOBM)) THEN

! REVERSE THIS LAST LENGTH INCREMENT
                EL = EL + DUMELI

! TERMINATE THIS ITERATION PHASE BUT RE-VISIT STATE-CALCS TO CONTINUE
                IPHASE = 4

                GO TO 444

          ELSE
                EL = EL - DUMELI

! CONTINUE IN THIS ITERATION PHASE
                GO TO 444

          END IF

      END IF



!******************************************************************
! PHASE 4: HOME IN ON TENOBM WITH EVER DECREASING LENGTH INCREMENTS
!******************************************************************
! THIS PHASE IS THE END-OF-THE-LINE
      IF(IPHASE .EQ. 4) THEN

! FIRST CHECK FOR SUCCESSFUL ITERATION CONVERGENCE
!-------------------------------------------------
          IF(ABS(TENASG-TENOBM)/ABS(TENOBM) .LT. 0.0001) GO TO 400


! SEE WHICH SIDE OF TENOBM THAT TENASG LIES ON
          IF(TENASG .GT. TENOBM) THEN

! BACKUP THE DEPLOYED LENGTH TO ITS VALUE BEFORE THIS PASS
                EL = EL + DUMELI

! DECREASE DEPLOY-LENGTH INCREMENT STEP
                DUMELI = 0.2*DUMELI
!                DUMELI = 0.8*DUMELI
          ELSE

! TIGHTEN UP LENGTH A BIT
                EL = EL - DUMELI
          END IF

      END IF


!---------------------------------------------------------------------
! BEFORE NEXT ITERATION, INSURE END-OBJ MASS REFLECTS TETH MASS DEPLOY
!---------------------------------------------------------------------
444   CONTINUE

! SET IN THIS FINITE SOLN'S DEPLOYED MASS TO GENERAL ACCESS TOSS ARRAY
      TTMASS(JTETH) = BMSTOT

! PUT TTMASS VALUE INTO APS(32,...) ARRAY IF MASS DEPLOY EFFECTS ENABLED
      IF(LDOTEF .EQ. 1) THEN

           APS(32,JAX,JOX) = TTMASS(JTETH)

! UPDATE TOSS OBJ MASS AT "X-END (DEPLOY-END)" OF THIS FINITE SOLN
! (BYPASS IF THE X-END IS THE REF PT)
           IF(JOX .NE. 1)  THEN

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
                IF(JOBJ .NE. JOX) CALL TOSLDW(JOX)

                DUMINC = 0.0
                DO 10 JXAP = 1,NATPAT
                      DUMINC = DUMINC - APS(32,JXAP,JOX)
10              CONTINUE

                DMASS = FMASSO + DUMINC - DMASINC

! RESTORE ARRAY DATA FOR OBJECT AT TOP OF CHAIN SEGMENT
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
                CALL TOSLDW(JOBJ)

           END IF

      END IF



!***********************************
!***********************************
!***********************************
! END LOOP:  BIG SOLN-ITERATION LOOP
!***********************************
!***********************************
!***********************************
111   CONTINUE

! FALLING THRU LOOP CONSTITUTES FAILURE OF DEPLOY LENGTH ITERATION
!-----------------------------------------------------------------
      WRITE(LUE,3011) JFINIT, JCHSG
3011  FORMAT('TOZELV: FATAL; FINITE SOLN',I2,' ITERATION FAILURE',   &
     &       ' ON DEPLOYED LENGTH, CHAIN SEG',I2,   &
     &       ' SPACE ELEV GGIC OPTION')
      STOP 'IN TOZELV-1'




!-----------------------------------
! SUCCESSFUL FINITE TETHER ITERATION
!-----------------------------------
400   CONTINUE

! SET IN VALUE FOR DEPLOYED TOSS TETHER LENGTH
      TLENT(JTETH) = EL

! MOVE CERTAIN FINITE SOLN VARIABLES TO GENERAL TOSS COMMON
! (W/BEAD DISPL INITIALIZATION)
      CALL TISSTW(JFINIT)

! SAVE THE TENSION AT BOTTOM END OF THIS FINITE TETHER CHAIN-SEGMENT,
! SINCE IT WILL BE THE TENSION EXERTED FROM ABOVE ON THE UPPER-MOST
! TOSS OBJECT OF THE CHAIN-SEGMENT JUST BELOW
      DUMOBT = TENASG


999   CONTINUE
!****************************************************************
!****************************************************************
!****************************************************************
!****************************************************************
! END OF MASTER SEGMENT EQUILIBRATION LOOP
!****************************************************************
!****************************************************************
!****************************************************************
!****************************************************************

! STANDARD RETURN
1000  RETURN

      END

