! ROUTINE: TOSAE5
! %Z%GTOSS %M% H.10 code v01.0
!              H.10 code v01.2   (baseline for vH.10.1b7 delivery)
!**********************************
!**********************************
!
      SUBROUTINE TOSAE5
!
!**********************************
!**********************************
! THIS ROUTINE PERFORMS AERODYNAMIC CALCULATIONS PECULIAR TO
! 6 DOF FULL ANGLE OF ATTACK/SIDE-SLIP AERODYNAMICS
!
!  A SPECIFIC AERODYNAMIC DATA DEFINITION FOR ===> CC33_A  (SMALL-deccelerator)
!
! WHILE THIS APPLIES TO A SPECIFIC APPLICATION, IT ALSO
! SERVES AS A GENERIC ILLUSTRATION OF HOW A FULL NON-LINEAR
! AERODYNAMIC DATA SET CAN BE SIMULATED FOR A TOSS OBJECT


! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_TOSS.h"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.h"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.h"







      DIMENSION UCADUM(3), UCNDUM(3), UCMDUM(3), DUMIB(3)
      DIMENSION FASDUM(3), FNSDUM(3), CUPSDUM(3)
      DIMENSION FNDDUM(3), CUPDDUM(3), VRWDUM(3)
      DIMENSION OMPTDUM(3), UOMIDUM(3), UOMJDUM(3), UOMKDUM(3)

! AERO TABLE STORAGE
      DIMENSION CATAB(133), CNTAB(133), CMTAB(133)
      DIMENSION CMQTAB(381)

! STATIC AERO COEFFICIENT CA TABLE   ===> CC33_A (generically decelerated AERO config)
      DATA (CATAB(J),j=1,133) /   &
     &  11.0,  10.0,   &
     &           0.0,    5.0,     20.0,    30.0,    60.0,    90.0,   &
     &         120.0,   150.0,   165.0,   175.0,   180.0,   &
     &  0.0,      .9992,  1.0142,  1.1210,  1.2298,   .8742,  -.4899,   &
     &           -.8742,  -1.2298,  -1.1210,  -1.0142,   -.9992,   &
     &  0.1,      .9992,  1.0142,  1.1210,  1.2298,   .8742,  -.4899,   &
     &           -.8742,  -1.2298,  -1.1210,  -1.0142,   -.9992,   &
     &  0.5,      .9992,  1.0142,  1.1210,  1.2298,   .8742,  -.4899,   &
     &           -.8742,  -1.2298,  -1.1210,  -1.0142,   -.9992,   &
     &  0.8,     1.2332,  1.2484,  1.2974,  1.2977,   .7623,   .1759,   &
     &           -.7623,  -1.2977,  -1.2974,  -1.2484,  -1.2332,   &
     &  0.9,     1.3186,  1.3320,  1.3725,  1.3768,   .8664,   .3103,   &
     &           -.8664,  -1.3768,  -1.3725,  -1.3320,  -1.3186,   &
     &  1.1,     1.5216,  1.5347,  1.5849,  1.4972,  1.0447,   .1224,   &
     &           -1.0447,  -1.4972,  -1.5849,  -1.5347,  -1.5216,   &
     &  1.6,     1.6153,  1.6157,  1.6028,  1.4989,   .9193,   .1421,   &
     &           -.9193,  -1.4989,  -1.6028,  -1.6157,  -1.6153,   &
     &  2.0,     1.6129,  1.6136,  1.5858,  1.4762,   .8087,   .1674,   &
     &           -.8087,  -1.4762,  -1.5858,  -1.6136,  -1.6129,   &
     &  4.0,     1.6070,  1.6097,  1.5488,  1.4299,   .5499,   .2052,   &
     &           -.5499,  -1.4299,  -1.5488,  -1.6097,  -1.6070,   &
     &  99.,     1.6070,  1.6097,  1.5488,  1.4299,   .5499,   .2052,   &
     &           -.5499,  -1.4299,  -1.5488,  -1.6097,  -1.6070 /


! STATIC AERO COEFFICIENT CN TABLE   ===> CC33_A (generically decelerated AERO config)
      DATA (CNTAB(J),j=1,133) /   &
     & 11.0,  10.0,   &
     &          0.0,      5.0,    20.0,    30.0,    60.0,    90.0,   &
     &        120.0,    150.0,    165.0,    175.0,  180.0,   &
     &  0.0,  .0000,  -.1279,  -.2790,  -.1280,   .2040,   .4576,   &
     &        .2040,  -.1280,  -.2790,  -.1279,  .0000,   &
     &  0.1,  .0000,  -.1279,  -.2790,  -.1280,   .2040,   .4576,   &
     &        .2040,  -.1280,  -.2790,  -.1279,  .0000,   &
     &  0.5,  .0000,  -.1279,  -.2790,  -.1280,   .2040,   .4576,   &
     &        .2040,  -.1280,  -.2790,  -.1279,  .0000,   &
     &  0.8,  .0000,  -.1195,  -.2408,   .1081,   .7234,   .6892,   &
     &        .7234,   .1081,  -.2408,  -.1195,  .0000,   &
     &  0.9,  .0000,  -.0961,  -.2183,   .2508,   .9553,   .9497,   &
     &        .9553,   .2508,  -.2183,  -.0961,  .0000,   &
     &  1.1,  .0000,  -.0832,   .0536,   .6251,  1.2942,  1.3680,   &
     &        1.2942,   .6251,   .0536,  -.0832,  .0000,   &
     &  1.6,  .0000,   .0655,   .2234,   .5811,  1.2226,  1.4343,   &
     &        1.2226,   .5811,   .2234,   .0655,  .0000,   &
     &  2.0,  .0000,   .0624,   .2092,   .4981,  1.1281,  1.3978,   &
     &        1.1281,   .4981,   .2092,   .0624,  .0000,   &
     &  4.0,  .0000,   .0390,   .1258,   .3069,   .9231,  1.2767,   &
     &        .9231,   .3069,   .1258,   .0390,  .0000,   &
     &  99.,  .0000,   .0390,   .1258,   .3069,   .9231,  1.2767,   &
     &        .9231,   .3069,   .1258,   .0390,  .0000 /


! STATIC AERO COEFFICIENT Cm TABLE   ===> CC33_A (generically decelerated AERO config)
      DATA (CMTAB(J),j=1,133) /   &
     &  11.0,  10.0,   &
     &        0.0,      5.0,      20.0,     30.0,   60.0,     90.0,   &
     &        120.0,    150.0,    165.0,    175.0,  180.0,   &
     &  0.0,  .0000,  -.01020,  -.02230,  -.01020,  -.1088,   -.4210,   &
     &       -.2783,   .0759,   .2417,   .1276,  .0000,   &
     &  0.1,  .0000,  -.01020,  -.02230,  -.01020,  -.1088,   -.4210,   &
     &       -.2783,   .0759,   .2417,   .1276,  .0000,   &
     &  0.5,  .0000,  -.01023,  -.02232,  -.01024,  -.1088,   -.4210,   &
     &       -.2783,   .0759,   .2417,   .1276,  .0000,   &
     &  0.8,  .0000,  -.00956,  -.01926,   .00865,  -.3858,   -.5857,   &
     &       -.5864,  -.0516,   .2219,   .1098,  .0000,   &
     &  0.9,  .0000,  -.00769,  -.01746,   .02006,  -.5095,   -.7548,   &
     &       -.7391,  -.1465,   .2089,   .0909,  .0000,   &
     &  1.1,  .0000,  -.00666,   .00429,   .05001,  -.6902,   -.9990,   &
     &       -.9541,  -.4242,  -.0049,   .0826,  .0000,   &
     &  1.6,  .0000,   .00524,   .01787,   .04649,  -.7743,  -1.0224,   &
     &       -.8836,  -.3866,  -.1355,  -.0441,  .0000,   &
     &  2.0,  .0000,   .00499,   .01674,   .03985,  -.7520,   -.9891,   &
     &       -.8173,  -.3266,  -.1298,  -.0376,  .0000,   &
     &  4.0,  .0000,   .00312,   .01006,   .02455,  -.6154,   -.8943,   &
     &       -.6753,  -.1947,  -.0710,  -.0258,  .0000,   &
     &  99.,  .0000,   .00310,   .01010,   .02460,  -.6154,   -.8943,   &
     &       -.6753,  -.1947,  -.0710,  -.0258,  .0000 /




! AERO COEFFICIENT Cmq TABLE          ===> CC33_A (generically decelerated AERO config)
      DATA (CMQTAB(J),j=1,133) /   &
     &  11.0,  10.0,   &
     &        0.0,      5.0,      20.0,     30.0,   60.0,     90.0,   &
     &        120.0,    150.0,    165.0,    175.0,  180.0,   &
     &  0.0,   2.0000,  2.0000,    .2500,   -.5000,   .0000,   .0000,   &
     &          .0000,   -.5000,    .2500,  2.0000,   2.0000,   &
     &  0.1,   2.0000,  2.0000,    .2500,   -.5000,   .0000,   .0000,   &
     &          .0000,   -.5000,    .2500,  2.0000,   2.0000,   &
     &  0.5,   2.0000,  2.0000,    .2500,   -.5000,   .0000,   .0000,   &
     &          .0000,   -.5000,    .2500,  2.0000,   2.0000,   &
     &  0.8,   4.0000,  1.0000,   2.0000,    .0000,   .0000,   .0000,   &
     &          .0000,    .0000,   2.0000,  1.0000,   4.0000,   &
     &  0.9,   6.0000,  2.0000,   4.0000,    .0000,   .0000,   .0000,   &
     &          .0000,    .0000,   4.0000,  2.0000,   6.0000,   &
     &  1.1,  16.0000,   .2500,    .0000,  -4.0000,   .0000,   .0000,   &
     &          .0000,  -4.0000,    .0000,   .2500,  16.0000,   &
     &  1.6,   4.0000,   .5000,    .0000,  -1.0000,   .0000,   .0000,   &
     &          .0000,  -1.0000,    .0000,   .5000,   4.0000,   &
     &  2.0,  24.0000,   .2500,  -1.0000,  -1.0000,  2.0000,  2.0000,   &
     &          2.0000,  -1.0000,  -1.0000,   .2500,  24.0000,   &
     &  4.0,   4.0000,   .5000,  -1.0000,  -2.0000,   .0000,   .0000,   &
     &          .0000,  -2.0000,  -1.0000,   .5000,   4.0000,   &
     &  99.,   4.0000,   .5000,  -1.0000,  -2.0000,   .0000,   .0000,   &
     &          .0000,  -2.0000,  -1.0000,   .5000,   4.0000 /



!**************************************************************
! THIS SECTION DOES A NON-LINEAR SIMULATION OF AERODYNAMICS FOR
! A SYMMETRICAL BODY FOR WHICH TOTAL ANGLE-OF-ATTACK (ALP_OBJ)
! SUFFICES TO DESCRIBE STATIC AERODYNAMICS, AND DYNAMIC ANGLE OF
! ATTACK (ALP_DYN, IN A PLANE NORMAL TO THE GENERALIZED PITCHING
! RATE VECTOR, OMPTDUM) SUFFICES TO DESCRIBE DYNAMIC AERO LOADS
!**************************************************************
! NOTE 1: THIS USES VARIOUS QUANTITIES ALREADY CALCULATED IN
!         ROUTINE TOSAE (PREVIOUS TO THIS TOSAE4 CALL), AND
!         STORED IN OBJECT COMMON, AND IT RETURNS THE TOTAL
!         AIRLOAD AND COUPLE TO THE TOSS COMMON FOR THE OBJECT
!
! NOTE 2: THESE CALCULATIONS PREEMPTS THE SIMPLE DRAG CALCS WHICH
!         HAVE ALREADY BEEN CALCULATED PREVIOUSLY IN ROUTINE TOSAE

! FIRST CALC VARIOUS PARAMETERS NEEDED BY "STATIC" AERO LOAD MODEL
!-----------------------------------------------------------------
! TOTAL ANGLE OF ATTACH (IN DEG)
       ALP_OBJ = 0.0
       DUMNUM = SQRT(VRWB(2)**2 + VRWB(3)**2)
       IF(VRWB(1) .NE. 0.0) ALP_OBJ = RTDALL*ATAN2(DUMNUM,-VRWB(1))

! FORM THE UNIT AXIAL-AIRLOAD-ORIENTATION VECTOR "UCADUM"
       UCADUM(1) = -1.0
       UCADUM(2) = 0.0
       UCADUM(3) = 0.0

! FORM THE UNIT NORMAL-AIRLOAD-ORIENTATION VECTOR "UCN"
       UCNDUM(1) = 0.0
       UCNDUM(2) = VRWB(2)
       UCNDUM(3) = VRWB(3)

       IF(DUMNUM .NE. 0.0) CALL VECNRM(UCNDUM, UCNDUM)

! FORM THE COUPLE-ORIENTATION VECTOR "UCMDUM = IB X UCN"
       DUMIB(1) = 1.0
       DUMIB(2) = 0.0
       DUMIB(3) = 0.0

       CALL CROSS(DUMIB,UCNDUM,  UCMDUM)

       DUMVAL = VECMAG(UCMDUM)
       IF(DUMVAL .NE. 0.0) CALL VECNRM(UCMDUM, UCMDUM)


! NEXT CALC VARIOUS PARAMETERS NEEDED BY "DYNAMIC" AERO LOAD MODEL
!-----------------------------------------------------------------
! FORM TOTAL DYN-AERO ANGULAR VELOCITY VECTOR (EXPRESSED IN BODY FRAME)
       OMPTDUM(1) = 0.0
       OMPTDUM(2) = OMB(2)
       OMPTDUM(3) = OMB(3)

! CALC MAGNITUDE OF DYNANMIC AERO ANGULAR VELOCITY
       AERO_RATE = VECMAG(OMPTDUM)

! FORM X-AXIS UNIT VECTOR OF DYNAMIC AERO FRAME (EXPRESSED IN BODY FRAME)
! NOTE: THIS IS THE SAME AS THE BODY SYMMETRY AXIS, OR X AXIS
       UOMIDUM(1) = 1.0
       UOMIDUM(2) = 0.0
       UOMIDUM(3) = 0.0

! FORM Y-AXIS UNIT VECTOR OF DYNAMIC AERO FRAME (EXPRESSED IN BODY FRAME)
! NOTE: THIS A UNIT VECTOR IN DIRECTION OF TOTAL DYN-AREO ANG VEL VECTOR
! (GUARDING AGAINST A NULL ANGULAR VEL FOR THE NORMALIZATION PROCESS)
       UOMJDUM(1) = 1.0
       UOMJDUM(2) = 0.0
       UOMJDUM(3) = 0.0
       IF(AERO_RATE .NE. 0.0) CALL VECNRM(OMPTDUM, UOMJDUM)

! FORM Z-AXIS UNIT VECTOR OF DYNAMIC AERO FRAME (EXPRESSED IN BODY FRAME)
! NOTE: SIMPLY CORRESPONDS TO THE CROSS PRODUCT OF I AND J DEFINED ABOVE
! (GUARDING AGAINST A NULL ANGULAR VEL FOR THE NORMALIZATION PROCESS)
       UOMKDUM(1) = 1.0
       UOMKDUM(2) = 0.0
       UOMKDUM(3) = 0.0
       IF(AERO_RATE .NE. 0.0) THEN
           CALL CROSS(UOMIDUM, UOMJDUM,    UOMKDUM)
           CALL VECNRM(UOMKDUM,   UOMKDUM)
       END IF

! GET COMP OF REL WIND IN DYN AERO FRAME (ALL VEC INGREDIENTS IN BODY FRAME)
       VRWDUM(1) = DOT(VRWB,UOMIDUM)
       VRWDUM(2) = DOT(VRWB,UOMJDUM)
       VRWDUM(3) = DOT(VRWB,UOMKDUM)

! TOTAL ANGLE OF ATTACK IN PLANE NORM TO DYNAMIC AERO TOTAL ANG VEL (DEG)
       ALP_DYN = 0.0
       DUMNUM = SQRT(VRWDUM(2)**2 + VRWDUM(3)**2)
       IF(VRWDUM(1) .NE. 0.0) ALP_DYN = RTDALL*ATAN2(DUMNUM,-VRWDUM(1))


!********************************************************************
! FIND STATIC AERO DERIVS VIA DOUBLE TABLE LOOKUP OF ALP_OBJ AND MACH
!********************************************************************
! CALCULATE CA
!-------------
       CALL DLINT(CATAB, ALP_OBJ, DMACH,    CA_OBJ, IERDUM)
       IF(IERDUM .NE. 0.0) GO TO 2001

! CALCULATE CN
!-------------
       CALL DLINT(CNTAB, ALP_OBJ, DMACH,    CN_OBJ, IERDUM)
       IF(IERDUM .NE. 0.0) GO TO 2003

! CALCULATE CM
!-------------
       CALL DLINT(CMTAB, ALP_OBJ, DMACH,    CM_OBJ, IERDUM)
       IF(IERDUM .NE. 0.0) GO TO 2005


!*********************************************************************
! FIND DYNAMIC AERO DERIVS VIA DOUBLE TABLE LOOKUP OF ALP_DYN AND MACH
!*********************************************************************
! CALCULATE CNq
!--------------
! PRELIMINARY CONFIGURATION CC33_A ASSUMES ZERO CNq
       CNQ_OBJ = 0.0

! CALCULATE CMq
!--------------
       CALL DLINT(CMQTAB, ALP_DYN, DMACH,   CMQ_OBJ, IERDUM)
       IF(IERDUM .NE. 0.0) GO TO 2009


!-------------------------------------------------------------------
! DEFINE REF AREA AND LENGTH (FOR AERO DATA DEFINED IN ABOVE TABLES)
!-------------------------------------------------------------------
! These two parameters are what distinguish the aero simulations of the
! SMALL -vs- LARGE Deccelerator aero configurations; both use the identical
! aero coefficient tables (above)

! DEFINITION FOR -SMALL- DECELERATOR
       AREF = 220.35
       LREF = 16.75

! DEFINE THE PRODUCT OF REFERENCE AREA AND LENGTH W/DYN PRESSURE
       RAQDUM =        AREF * QBAR
       RLQDUM = LREF * AREF * QBAR

! EVALUATE DYN AERO ANGULAR-RATE-CHARACTERIZATION TERM
       QLO2V_DUM = AERO_RATE * LREF/(2.0 * VR)


!----------------------------------------------------------------
! CALC MAGNITUDES OF STATIC AERO COUPLE, AXIAL, AND NORMAL FORCES
!----------------------------------------------------------------
! NOTE: THESE MAGNITUDES ARE REFERENCED TO "STATIC AERO FRAME"
       FAMAG_OBJ  = RAQDUM * CA_OBJ
       FNMAG_OBJ  = RAQDUM * CN_OBJ
       ACMAG_OBJ  = RLQDUM * CM_OBJ

! FIND THE CORRESPONDING COMPONENTS OF EACH VECTOR IN BODY FRAME
!---------------------------------------------------------------
! STATIC AXIAL FORCE VECTOR
       CALL VECSCL(FAMAG_OBJ, UCADUM,        FASDUM)

! STATIC NORMAL FORCE VECTOR
       CALL VECSCL(FNMAG_OBJ, UCNDUM,        FNSDUM)

! STATIC COUPLE VECTOR
       CALL VECSCL(ACMAG_OBJ, UCMDUM,       CUPSDUM)



!-------------------------------------------------------------
! CALC MAGNITUDES OF DYNAMIC AERO COUPLE AND NORMAL FORCES DUE TO PITCH RATE
!-------------------------------------------------------------
! NOTE: THESE MAGNITUDES ARE REFERENCED TO "DYNAMIC AERO FRAME"
       FNQMAG_OBJ  = RAQDUM * CNQ_OBJ * QLO2V_DUM
       ACQMAG_OBJ  = RLQDUM * CMQ_OBJ * QLO2V_DUM


! FIND THE CORRESPONDING COMPONENTS OF EACH VECTOR IN BODY FRAME
!---------------------------------------------------------------
! DYNAMIC NORMAL FORCE VECTOR
        CALL VECSCL(FNQMAG_OBJ, UOMKDUM,       FNDDUM)

! DYNAMIC COUPLE VECTOR
        CALL VECSCL(ACQMAG_OBJ, UOMJDUM,      CUPDDUM)

! for now just zero the dynamic contributions.....
        CUPDDUM(1) = 0.0
        CUPDDUM(2) = 0.0
        CUPDDUM(3) = 0.0


!-------------------------------------------------------
! FORM THE TOTAL TOSS OBJECT AERO FORCE VECTOR "FDB"
!-------------------------------------------------------
! NOTE: THESE ARE STORED INTO THE OFFCIAL TOSS AIR LOAD LOCATION
!       FOR THIS OBJECT. TOSS SEES THIS AS THE TOTAL AIRLOAD AT
!       THE OBJECT'S AERO REF PT (FROM WHICH IT WILL PRODUCE A
!       MOMENT CORRESPONDING TO THE LOAD AT THE SPECIFIED REF PT.

! FIRST SUM STATIC AERO LOADS (NORMAL AND AXIAL CONTRIBUTIONS)
       CALL VECSUM(FASDUM, FNSDUM,       FDB)

! NOW ADD IN THE DYNAMIC AERO (NORMAL) CONTRIBUTION
       CALL VECSUM(FNDDUM, FDB,          FDB)


!------------------------------------------------------------
! NOW FORM THE TOTAL TOSS OBJECT AERO COUPLE VECTOR "CDB"
! (TOSS WANTS TO SEE THIS AS COUPLE ASSOCIATED W/AERO REF PT)
!------------------------------------------------------------
! CONTRIBUTIONS FROM BOTH STATIC AND DYNAMIC AERO MODELS
        CALL VECSUM(CUPSDUM, CUPDDUM,    CDB)


! STANDARD RETURN POINT
1000  RETURN


! IDENTIFY SOURCE OF AERO TABLE LOOKUP ERRORS
2001  WRITE (LUE,2002) IERDUM
2002  FORMAT('AERO CA TABLE LOOKUP FAILURE',I2)
      STOP 'IN TOSAE3-2002'

2003  WRITE (LUE,2004) IERDUM
2004  FORMAT('AERO CN TABLE LOOKUP FAILURE',I2)
      STOP 'IN TOSAE3-2004'

2005  WRITE (LUE,2006) IERDUM
2006  FORMAT('AERO CM TABLE LOOKUP FAILURE',I2)
      STOP 'IN TOSAE3-2006'

2007  WRITE (LUE,2008) IERDUM
2008  FORMAT('AERO CNQ TABLE LOOKUP FAILURE',I2)
      STOP 'IN TOSAE3-2008'

2009  WRITE (LUE,2010) IERDUM
2010  FORMAT('AERO CMQ TABLE LOOKUP FAILURE',I2)
      STOP 'IN TOSAE3-2010'

      END
