! ROUTINE: TOSAE3
! %Z%GTOSS %M% H.10 code v01.0
!              H.10 code v01.2   (baseline for vH.10.1b7 delivery)
!**********************************
!**********************************
!
      SUBROUTINE TOSAE3
!
!**********************************
!**********************************
! THIS ROUTINE PERFORMS AERODYNAMIC CALCULATIONS PECULIAR TO
! 6 DOF FULL ANGLE OF ATTACK/SIDE-SLIP AERODYNAMICS
!
!  A SPECIFIC AERODYNAMIC DATA DEFINITION FOR ===> CC33
!
! WHILE THIS APPLIES TO A SPECIFIC APPLICATION, IT ALSO
! SERVES AS A GENERIC ILLUSTRATION OF HOW A FULL NON-LINEAR
! AERODYNAMIC DATA SET CAN BE SIMULATED FOR A TOSS OBJECT


! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_TOSS.h"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.h"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.h"







      DIMENSION UCADUM(3), UCNDUM(3), UCMDUM(3), DUMIB(3)
      DIMENSION FASDUM(3), FNSDUM(3), CUPSDUM(3)
      DIMENSION FNDDUM(3), CUPDDUM(3), VRWDUM(3)
      DIMENSION OMPTDUM(3), UOMIDUM(3), UOMJDUM(3), UOMKDUM(3)

! AERO TABLE STORAGE
      DIMENSION CATAB(133), CNTAB(133), CMTAB(133)
      DIMENSION CNQTAB(381), CMQTAB(381)

! STATIC AERO COEFFICIENT CA TABLE      ==> CC33
      DATA (CATAB(J),j=1,133) /   &
     &  11.0,  10.0,   &
     &           0.0,    5.0,     20.0,    30.0,    60.0,    90.0,   &
     &         120.0,   150.0,   165.0,   175.0,   180.0,   &
     &  0.0,   0.3635, 0.3649, 0.3734, 0.3043, -0.1205, -0.4899,   &
     &        -0.8742, -1.2298, -1.1210, -1.0142, -1.1210,   &
     &  0.1,   0.3635, 0.3649, 0.3734, 0.3043, -0.1205, -0.4899,   &
     &        -0.8742, -1.2298, -1.1210, -1.0142, -1.1210,   &
     &  0.5,   0.3635, 0.3649, 0.3734, 0.3043, -0.1205, -0.4899,   &
     &        -0.8742, -1.2298, -1.1210, -1.0142, -1.1210,   &
     &  0.8,   0.3917, 0.3932, 0.4393, 0.3929,  0.0544,  0.1759,   &
     &        -0.7623, -1.2977, -1.2974, -1.2484, -1.2974,   &
     &  0.9,   0.5273, 0.5293, 0.5584, 0.5291,  0.1890,  0.3103,   &
     &        -0.8664, -1.3768, -1.3725, -1.3320, -1.3725,   &
     &  1.1,   0.8326, 0.8358, 0.8924, 0.8634,  0.5405,  0.1224,   &
     &        -1.0447, -1.4972, -1.5849, -1.5347, -1.5849,   &
     &  1.6,   0.8752, 0.8785, 0.8889, 0.8746,  0.6425,  0.1421,   &
     &        -0.9193, -1.4989, -1.6028, -1.6157, -1.6028,   &
     &  2.0,   0.8449, 0.8481, 0.8385, 0.8198,  0.6480,  0.1674,   &
     &        -0.8087, -1.4762, -1.5858, -1.6136, -1.5858,   &
     &  4.0,   0.7387, 0.7415, 0.7160, 0.6907,  0.5371,  0.2052,   &
     &        -0.5499, -1.4299, -1.5488, -1.6097, -1.5488,   &
     &  99.,   0.7387, 0.7415, 0.7160, 0.6907,  0.5371,  0.2052,   &
     &        -0.5499, -1.4299, -1.5488, -1.6097, -1.5488 /


! STATIC AERO COEFFICIENT CN TABLE      ==> CC33
      DATA (CNTAB(J),j=1,133) /   &
     & 11.0,  10.0,   &
     &          0.0,      5.0,    20.0,    30.0,    60.0,    90.0,   &
     &        120.0,    150.0,    165.0,    175.0,  180.0,   &
     &  0.0,   0.0,   0.1702,  0.5959,  0.7871,  0.8258,  0.4576,   &
     &         0.2040,  -0.1280,  -0.2790,  -0.1279,    0.0,   &
     &  0.1,   0.0,   0.1702,  0.5959,  0.7871,  0.8258,  0.4576,   &
     &         0.2040,  -0.1280,  -0.2790,  -0.1279,    0.0,   &
     &  0.5,   0.0,   0.1702,  0.5959,  0.7871,  0.8258,  0.4576,   &
     &         0.2040,  -0.1280,  -0.2790,  -0.1279,    0.0,   &
     &  0.8,   0.0,   0.1845,  0.7278,  1.0240,  1.1594,  0.6892,   &
     &         0.7234,   0.1081,  -0.2408,  -0.1195,    0.0,   &
     &  0.9,   0.0,   0.2076,  0.9252,  1.2786,  1.3075,  0.9497,   &
     &         0.9553,   0.2508,  -0.2183,  -0.0961,    0.0,   &
     &  1.1,   0.0,   0.2307,  0.8333,  1.1932,  1.6070,  1.3680,   &
     &         1.2942,   0.6251,   0.0536,  -0.0832,    0.0,   &
     &  1.6,   0.0,   0.1735,  0.6854,  0.9962,  1.5233,  1.4343,   &
     &         1.2226,   0.5811,   0.2234,   0.0655,    0.0,   &
     &  2.0,   0.0,   0.1488,  0.5953,  0.8913,  1.4440,  1.3978,   &
     &         1.1281,   0.4981,   0.2092,   0.0624,    0.0,   &
     &  4.0,   0.0,   0.1012,  0.4307,  0.6926,  1.2890,  1.2767,   &
     &         0.9231,   0.3069,   0.1258,   0.0390,    0.0,   &
     &  99.,   0.0,   0.1012,  0.4307,  0.6926,  1.2890,  1.2767,   &
     &         0.9231,   0.3069,   0.1258,   0.0390,    0.0 /


! STATIC AERO COEFFICIENT Cm TABLE    ==> CC33
      DATA (CMTAB(J),j=1,133) /   &
     &  11.0,  10.0,   &
     &        0.0,      5.0,      20.0,     30.0,   60.0,     90.0,   &
     &        120.0,    150.0,    165.0,    175.0,  180.0,   &
     &  0.0,  0.0,    -0.0874,  -0.3104,  -0.4090,  -0.4587,  -0.4210,   &
     &       -0.2783,   0.0759,   0.2417,   0.1276,    0.0,   &
     &  0.1,  0.0,    -0.0874,  -0.3104,  -0.4090,  -0.4587,  -0.4210,   &
     &       -0.2783,   0.0759,   0.2417,   0.1276,    0.0,   &
     &  0.5,  0.0,      -0.0874,  -0.3104,  -0.4090,  -0.4587, -0.4210,   &
     &       -0.2783,   0.0759,   0.2417,   0.1276,    0.0,   &
     &  0.8,  0.0,     -0.0907,   -0.3737,  -0.5393,  -0.7046, -0.5857,   &
     &       -0.5864,  -0.0516,   0.2219,   0.1098,    0.0,   &
     &  0.9,  0.0,    -0.1007,   -0.5364,  -0.7593,  -0.8143,  -0.7548,   &
     &       -0.7391,  -0.1465,   0.2089,   0.0909,    0.0,   &
     &  1.1,  0.0,    -0.1464,   -0.5070,  -0.7514,  -1.0335,  -0.9990,   &
     &       -0.9541,  -0.4242,  -0.0049,   0.0826,    0.0,   &
     &  1.6,  0.0,     -0.1122,  -0.4418,  -0.6394,  -1.0022,  -1.0224,   &
     &       -0.8836,  -0.3866,  -0.1355,  -0.0441,    0.0,   &
     &  2.0,  0.0,     -0.0950,  -0.3822,  -0.5751,  -0.9533,  -0.9891,   &
     &       -0.8173,  -0.3266,  -0.1298,  -0.0376,    0.0,   &
     &  4.0,  0.0,     -0.0608,  -0.2658,  -0.4403,  -0.8474,  -0.8943,   &
     &       -0.6753,  -0.1947,  -0.0710,  -0.0258,    0.0,   &
     &  99.,  0.0,     -0.0608,  -0.2658,  -0.4403,  -0.8474,  -0.8943,   &
     &       -0.6753,  -0.1947,  -0.0710,  -0.0258,    0.0 /


! DYNAMIC AERO COEFFICIENT CNq TABLE (NORMAL FORCE DUE TO PITCH RATE)  ====>  LAWN-DART
      DATA (CNQTAB(J),J=1,381) /   &
     &  37.0,  9.0,   &
     &          0.0,      2.86,    5.0,   10.0,   20.0,   30.0,   45.0,   &
     &         60.0,      75.0,   90.0,  105.0,  120.0,  135.0,  150.0,   &
     &        160.0,     170.0,  175.0,  177.0,  180.0,  183.0,  185.0,   &
     &        190.0,     200.0,  210.0,  225.0,  240.0,  255.0,  270.0,   &
     &        285.0,     300.0,  315.0,  330.0,  340.0,  350.0,  355.0,   &
     &       357.14,     360.0,   &
     & 0.0,  3.92,   3.891,   3.831, 3.0568, 1.2927, 1.8909, 2.0304,   &
     &       1.8609, 1.7727, 1.6784, 1.7728, 1.8612, 2.0325, 1.7958,   &
     &       1.6156, 3.1697, 3.4921, 3.5087, 3.5087, 3.5087, 3.4921,   &
     &       3.1697, 1.6156, 1.7958, 2.0325, 1.8612, 1.7728, 1.6784,   &
     &       1.7727, 2.0304, 1.8909, 1.8909, 1.2927, 3.0568, 3.831,   &
     &       3.891,  3.92,   &
     & 0.2,  3.92,   3.891,   3.831, 3.0568, 1.2927, 1.8909, 2.0304,   &
     &       1.8609, 1.7727, 1.6784, 1.7728, 1.8612, 2.0325, 1.7958,   &
     &       1.6156, 3.1697, 3.4921, 3.5087, 3.5087, 3.5087, 3.4921,   &
     &       3.1697, 1.6156, 1.7958, 2.0325, 1.8612, 1.7728, 1.6784,   &
     &       1.7727, 2.0304, 1.8909, 1.8909, 1.2927, 3.0568, 3.831,   &
     &       3.891,  3.92,   &
     & 0.5,  4.0635, 3.9846, 3.8885, 2.9541, 1.5137, 2.1048, 2.0395,   &
     &       1.8181, 1.7504, 1.6778, 1.7504, 1.8184, 2.0464, 2.0446,   &
     &       1.8054, 3.1375, 3.556,  3.5954, 3.6274, 3.5954, 3.556,   &
     &       3.1375, 1.8054, 2.0446, 2.0464, 1.8184, 1.7504, 1.6778,   &
     &       1.7504, 2.0395, 2.1048, 2.1048, 1.5137, 2.9541, 3.8885,   &
     &       3.9846, 4.0635,   &
     & 0.9,  4.3947, 4.2614, 4.1251, 3.2526, 2.0381, 2.132,  1.6349,   &
     &       1.8062, 1.6533, 1.4896, 1.6534, 1.8068, 1.6354, 2.1351,   &
     &       2.2146, 3.3568, 3.7395, 3.8,    3.8649, 3.8,    3.7395,   &
     &       3.3568, 2.2146, 2.1351, 1.6354, 1.8068, 1.6534, 1.4896,   &
     &       1.6533, 1.6349, 2.132,  2.132,  2.0381, 3.2526, 4.1251,   &
     &       4.2614, 4.3947,   &
     & 1.2,  8.3053, 8.1519,  8.007, 7.462,  6.6158, 6.2639, 5.4824,   &
     &       6.0333, 5.8277, 5.6079, 5.8277, 6.0335, 5.4789, 6.2981,   &
     &       6.6657, 7.3294, 7.5676, 7.6241, 7.6934, 7.6241, 7.5676,   &
     &       7.3294, 6.6657, 6.2981, 5.4789, 6.0335, 5.8277, 5.6079,   &
     &       5.8277, 5.4824, 6.2639, 6.2639, 6.6158, 7.462,  8.007,   &
     &       8.1519, 8.3053,   &
     & 1.5,  3.3966, 3.3325, 3.2325, 2.921,  2.4827, 2.065,  1.5663,   &
     &       1.5862, 1.5573, 1.5264, 1.5573, 1.5862, 1.5649, 2.0753,   &
     &       2.4824, 2.7868, 2.9231, 2.9517, 2.9651, 2.9517, 2.9231,   &
     &       2.7868, 2.4824, 2.0753, 1.5649, 1.5862, 1.5573, 1.5264,   &
     &       1.5573, 1.5663, 2.065,  2.065,  2.4827, 2.921,  3.2325,   &
     &       3.3325, 3.3966,   &
     & 2.0,  2.852,  2.8445, 2.8434, 2.741,  2.4704, 2.1885, 1.9289,   &
     &       1.7022, 1.6543, 1.6031, 1.6543, 1.7023, 1.9298, 2.2011,   &
     &       2.4461, 2.6096, 2.5789, 2.5683, 2.5617, 2.5683, 2.5789,   &
     &       2.6096, 2.4461, 2.2011, 1.9298, 1.7023, 1.6543, 1.6031,   &
     &       1.6543, 1.9289, 2.1885, 2.1885, 2.4704, 2.741,  2.8434,   &
     &       2.8445, 2.852,   &
     & 4.0,  2.2081, 2.2165, 2.2296, 2.2939, 2.4151, 2.2852, 2.0936,   &
     &       1.9244, 1.7862, 1.6387, 1.7862, 1.9244, 2.0941, 2.2851,   &
     &       2.3468, 2.164,  2.0982, 2.0851, 2.0745, 2.0851, 2.0982,   &
     &       2.164,  2.3468, 2.2851, 2.0941, 1.9244, 1.7862, 1.6387,   &
     &       1.7862, 2.0936, 2.2852, 2.2852, 2.4151, 2.2939, 2.2296,   &
     &       2.2165, 2.2081,   &
     & 99.,  2.2081, 2.2165, 2.2296, 2.2939, 2.4151, 2.2852, 2.0936,   &
     &       1.9244, 1.7862, 1.6387, 1.7862, 1.9244, 2.0941, 2.2851,   &
     &       2.3468, 2.164,  2.0982, 2.0851, 2.0745, 2.0851, 2.0982,   &
     &       2.164,  2.3468, 2.2851, 2.0941, 1.9244, 1.7862, 1.6387,   &
     &       1.7862, 2.0936, 2.2852, 2.2852, 2.4151, 2.2939, 2.2296,   &
     &       2.2165, 2.2081/



! AERO COEFFICIENT Cmq TABLE                   ====>  LAWN-DART
      DATA (CMQTAB(J),J=1,381) /   &
     &  37.0,  9.0,   &
     &          0.0,      2.86,    5.0,   10.0,   20.0,   30.0,   45.0,   &
     &         60.0,      75.0,   90.0,  105.0,  120.0,  135.0,  150.0,   &
     &        160.0,     170.0,  175.0,  177.0,  180.0,  183.0,  185.0,   &
     &        190.0,     200.0,  210.0,  225.0,  240.0,  255.0,  270.0,   &
     &        285.0,     300.0,  315.0,  330.0,  340.0,  350.0,  355.0,   &
     &       357.14,     360.0,   &
     & 0.0,  -2.2445, -2.2269, -2.1905, -1.725,  -0.6665, -1.025,   &
     &       -1.1084, -1.0069, -0.9541, -0.8977, -0.9542, -1.0071,   &
     &       -1.1096, -0.9679, -0.8601, -1.7901, -1.983,  -1.9929,   &
     &       -1.9929, -1.9929, -1.983,  -1.7901, -0.8601, -0.9679,   &
     &       -1.1096, -1.0071, -0.9542, -0.8977, -0.9541, -1.1084,   &
     &       -1.025,  -1.025,  -0.6665, -1.725,  -2.1905,  -2.2269,   &
     &       -2.2445,   &
     & 0.2,  -2.2445, -2.2269, -2.1905, -1.725,  -0.6665, -1.025,   &
     &       -1.1084, -1.0069, -0.9541, -0.8977, -0.9542, -1.0071,   &
     &       -1.1096, -0.9679, -0.8601, -1.7901, -1.983,  -1.9929,   &
     &       -1.9929, -1.9929, -1.983,  -1.7901, -0.8601, -0.9679,   &
     &       -1.1096, -1.0071, -0.9542, -0.8977, -0.9541, -1.1084,   &
     &       -1.025,  -1.025,  -0.6665, -1.725,  -2.1905,  -2.2269,   &
     &       -2.2445,   &
     & 0.5,  -2.4117, -2.3641, -2.3061, -1.7446, -0.8805, -1.2344,   &
     &       -1.1953, -1.0628, -1.0222, -0.9788, -1.0222, -1.0629,   &
     &       -1.1993, -1.1982, -1.0552, -1.852,  -2.1023, -2.1259,   &
     &       -2.145,  -2.1259, -2.1023, -1.852,  -1.0552, -1.1982,   &
     &       -1.1993, -1.0629, -1.0222, -0.9788, -1.0222, -1.1953,   &
     &       -1.2344, -1.2344, -0.8805, -1.7446, -2.3061,  -2.3641,   &
     &       -2.4117,   &
     & 0.9,  -2.9723, -2.8924, -2.8106, -2.2883, -1.5621, -1.6178,   &
     &       -1.3213, -1.4234, -1.3323, -1.2347, -1.3323, -1.4238,   &
     &       -1.3216, -1.6193, -1.6667, -2.3473, -2.5754, -2.6114,   &
     &       -2.6501, -2.6114, -2.5754, -2.3473, -1.6667, -1.6193,   &
     &       -1.3216, -1.4238, -1.3323, -1.2347, -1.3323, -1.3213,   &
     &       -1.6178, -1.6178, -1.5621, -2.2883, -2.8106,  -2.8924,   &
     &       -2.9723,   &
     & 1.2,  -3.3286, -3.2298, -3.1365, -2.7857, -2.2412, -2.0149,   &
     &       -1.5126, -1.8667, -1.7345, -1.5933, -1.7345, -1.8668,   &
     &       -1.5104, -2.0368, -2.273,  -2.6996, -2.8526, -2.889,   &
     &       -2.9335, -2.889,  -2.8526, -2.6996, -2.273,  -2.0368,   &
     &       -1.5104, -1.8668, -1.7345, -1.5933, -1.7345, -1.5126,   &
     &       -2.0149, -2.0149, -2.2412, -2.7857, -3.1365,  -3.2298,   &
     &       -3.3286,   &
     & 1.5,  -1.9948, -1.9523, -1.8861, -1.68,   -1.3901, -1.114,   &
     &       -0.7845, -0.7976, -0.7785, -0.7581, -0.7785, -0.7976,   &
     &       -0.7836, -1.1208, -1.3897, -1.5909, -1.6809, -1.6998,   &
     &       -1.7087, -1.6998, -1.6809, -1.5909, -1.3897, -1.1208,   &
     &       -0.7836, -0.7976, -0.7785, -0.7581, -0.7785, -0.7845,   &
     &       -1.114,  -1.114,  -1.3901, -1.68,   -1.8861,  -1.9523,   &
     &       -1.9948,   &
     & 2.0,  -1.6173, -1.6123, -1.6115, -1.543,  -1.3622, -1.1739,   &
     &       -1.0006, -0.8494, -0.8173, -0.7832, -0.8173, -0.8494,   &
     &       -1.0013, -1.1823, -1.3459, -1.455,  -1.4345, -1.4275,   &
     &       -1.423,  -1.4275, -1.4345, -1.455,  -1.3459, -1.1823,   &
     &       -1.0013, -0.8494, -0.8173, -0.7832, -0.8173, -1.0006,   &
     &       -1.1739, -1.1739, -1.3622, -1.543,  -1.6115,  -1.6123,   &
     &       -1.6173,   &
     & 4.0,  -1.1766, -1.1822, -1.191, -1.2342,  -1.3155, -1.2282,   &
     &       -1.0995, -0.9858, -0.8929, -0.7938, -0.8929, -0.9858,   &
     &       -1.0998, -1.2282, -1.2696, -1.1468, -1.1026, -1.0938,   &
     &       -1.0866, -1.0938, -1.1026, -1.1468, -1.2696, -1.2282,   &
     &       -1.0998, -0.9858, -0.8929, -0.7938, -0.8929, -1.0995,   &
     &       -1.2282, -1.2282, -1.3155, -1.2342, -1.191,   -1.1822,   &
     &       -1.1766,   &
     & 99.,  -1.1766, -1.1822, -1.191, -1.2342,  -1.3155, -1.2282,   &
     &       -1.0995, -0.9858, -0.8929, -0.7938, -0.8929, -0.9858,   &
     &       -1.0998, -1.2282, -1.2696, -1.1468, -1.1026, -1.0938,   &
     &       -1.0866, -1.0938, -1.1026, -1.1468, -1.2696, -1.2282,   &
     &       -1.0998, -0.9858, -0.8929, -0.7938, -0.8929, -1.0995,   &
     &       -1.2282, -1.2282, -1.3155, -1.2342, -1.191,   -1.1822,   &
     &       -1.1766/


!**************************************************************
! THIS SECTION DOES A NON-LINEAR SIMULATION OF AERODYNAMICS FOR
! A SYMMETRICAL BODY FOR WHICH TOTAL ANGLE-OF-ATTACK (ALP_OBJ)
! SUFFICES TO DESCRIBE STATIC AERODYNAMICS, AND DYNAMIC ANGLE OF
! ATTACK (ALP_DYN, IN A PLANE NORMAL TO THE GENERALIZED PITCHING
! RATE VECTOR, OMPTDUM) SUFFICES TO DESCRIBE DYNAMIC AERO LOADS
!**************************************************************
! NOTE 1: THIS USES VARIOUS QUANTITIES ALREADY CALCULATED IN
!         ROUTINE TOSAE (PREVIOUS TO THIS TOSAE4 CALL), AND
!         STORED IN OBJECT COMMON, AND IT RETURNS THE TOTAL
!         AIRLOAD AND COUPLE TO THE TOSS COMMON FOR THE OBJECT
!
! NOTE 2: THESE CALCULATIONS PREEMPTS THE SIMPLE DRAG CALCS WHICH
!         HAVE ALREADY BEEN CALCULATED PREVIOUSLY IN ROUTINE TOSAE

! FIRST CALC VARIOUS PARAMETERS NEEDED BY "STATIC" AERO LOAD MODEL
!-----------------------------------------------------------------
! TOTAL ANGLE OF ATTACH (IN DEG)
       ALP_OBJ = 0.0
       DUMNUM = SQRT(VRWB(2)**2 + VRWB(3)**2)
       IF(VRWB(1) .NE. 0.0) ALP_OBJ = RTDALL*ATAN2(DUMNUM,-VRWB(1))

! FORM THE UNIT AXIAL-AIRLOAD-ORIENTATION VECTOR "UCADUM"
       UCADUM(1) = -1.0
       UCADUM(2) = 0.0
       UCADUM(3) = 0.0

! FORM THE UNIT NORMAL-AIRLOAD-ORIENTATION VECTOR "UCN"
       UCNDUM(1) = 0.0
       UCNDUM(2) = VRWB(2)
       UCNDUM(3) = VRWB(3)

       IF(DUMNUM .NE. 0.0) CALL VECNRM(UCNDUM, UCNDUM)

! FORM THE COUPLE-ORIENTATION VECTOR "UCMDUM = IB X UCN"
       DUMIB(1) = 1.0
       DUMIB(2) = 0.0
       DUMIB(3) = 0.0

       CALL CROSS(DUMIB,UCNDUM,  UCMDUM)

       DUMVAL = VECMAG(UCMDUM)
       IF(DUMVAL .NE. 0.0) CALL VECNRM(UCMDUM, UCMDUM)


! NEXT CALC VARIOUS PARAMETERS NEEDED BY "DYNAMIC" AERO LOAD MODEL
!-----------------------------------------------------------------
! FORM TOTAL DYN-AERO ANGULAR VELOCITY VECTOR (EXPRESSED IN BODY FRAME)
       OMPTDUM(1) = 0.0
       OMPTDUM(2) = OMB(2)
       OMPTDUM(3) = OMB(3)

! CALC MAGNITUDE OF DYNANMIC AERO ANGULAR VELOCITY
       AERO_RATE = VECMAG(OMPTDUM)

! FORM X-AXIS UNIT VECTOR OF DYNAMIC AERO FRAME (EXPRESSED IN BODY FRAME)
! NOTE: THIS IS THE SAME AS THE BODY SYMMETRY AXIS, OR X AXIS
       UOMIDUM(1) = 1.0
       UOMIDUM(2) = 0.0
       UOMIDUM(3) = 0.0

! FORM Y-AXIS UNIT VECTOR OF DYNAMIC AERO FRAME (EXPRESSED IN BODY FRAME)
! NOTE: THIS A UNIT VECTOR IN DIRECTION OF TOTAL DYN-AREO ANG VEL VECTOR
! (GUARDING AGAINST A NULL ANGULAR VEL FOR THE NORMALIZATION PROCESS)
       UOMJDUM(1) = 1.0
       UOMJDUM(2) = 0.0
       UOMJDUM(3) = 0.0
       IF(AERO_RATE .NE. 0.0) CALL VECNRM(OMPTDUM, UOMJDUM)

! FORM Z-AXIS UNIT VECTOR OF DYNAMIC AERO FRAME (EXPRESSED IN BODY FRAME)
! NOTE: SIMPLY CORRESPONDS TO THE CROSS PRODUCT OF I AND J DEFINED ABOVE
! (GUARDING AGAINST A NULL ANGULAR VEL FOR THE NORMALIZATION PROCESS)
       UOMKDUM(1) = 1.0
       UOMKDUM(2) = 0.0
       UOMKDUM(3) = 0.0
       IF(AERO_RATE .NE. 0.0) THEN
           CALL CROSS(UOMIDUM, UOMJDUM,    UOMKDUM)
           CALL VECNRM(UOMKDUM,   UOMKDUM)
       END IF

! GET COMP OF REL WIND IN DYN AERO FRAME (ALL VEC INGREDIENTS IN BODY FRAME)
       VRWDUM(1) = DOT(VRWB,UOMIDUM)
       VRWDUM(2) = DOT(VRWB,UOMJDUM)
       VRWDUM(3) = DOT(VRWB,UOMKDUM)

! TOTAL ANGLE OF ATTACK IN PLANE NORM TO DYNAMIC AERO TOTAL ANG VEL (DEG)
       ALP_DYN = 0.0
       DUMNUM = SQRT(VRWDUM(2)**2 + VRWDUM(3)**2)
       IF(VRWDUM(1) .NE. 0.0) ALP_DYN = RTDALL*ATAN2(DUMNUM,-VRWDUM(1))


!********************************************************************
! FIND STATIC AERO DERIVS VIA DOUBLE TABLE LOOKUP OF ALP_OBJ AND MACH
!********************************************************************
! CALCULATE CA
!-------------
       CALL DLINT(CATAB, ALP_OBJ, DMACH,    CA_OBJ, IERDUM)
       IF(IERDUM .NE. 0.0) GO TO 2001

! CALCULATE CN
!-------------
       CALL DLINT(CNTAB, ALP_OBJ, DMACH,    CN_OBJ, IERDUM)
       IF(IERDUM .NE. 0.0) GO TO 2003

! CALCULATE CM
!-------------
       CALL DLINT(CMTAB, ALP_OBJ, DMACH,    CM_OBJ, IERDUM)
       IF(IERDUM .NE. 0.0) GO TO 2005


!*********************************************************************
! FIND DYNAMIC AERO DERIVS VIA DOUBLE TABLE LOOKUP OF ALP_DYN AND MACH
!*********************************************************************
! CALCULATE CNq
!--------------
       CALL DLINT(CNQTAB, ALP_DYN, DMACH,   CNQ_OBJ, IERDUM)
       IF(IERDUM .NE. 0.0) GO TO 2007

! CALCULATE CMq
!--------------
       CALL DLINT(CMQTAB, ALP_DYN, DMACH,   CMQ_OBJ, IERDUM)
       IF(IERDUM .NE. 0.0) GO TO 2009


!-------------------------------------------------------------------
! DEFINE REF AREA AND LENGTH (FOR AERO DATA DEFINED IN ABOVE TABLES)
!-------------------------------------------------------------------
       AREF = 122.72
       LREF = 12.5

! DEFINE THE PRODUCT OF REFERENCE AREA AND LENGTH W/DYN PRESSURE
       RAQDUM =        AREF * QBAR
       RLQDUM = LREF * AREF * QBAR

! EVALUATE DYN AERO ANGULAR-RATE-CHARACTERIZATION TERM
       QLO2V_DUM = AERO_RATE * LREF/(2.0 * VR)


!----------------------------------------------------------------
! CALC MAGNITUDES OF STATIC AERO COUPLE, AXIAL, AND NORMAL FORCES
!----------------------------------------------------------------
! NOTE: THESE MAGNITUDES ARE REFERENCED TO "STATIC AERO FRAME"
       FAMAG_OBJ  = RAQDUM * CA_OBJ
       FNMAG_OBJ  = RAQDUM * CN_OBJ
       ACMAG_OBJ  = RLQDUM * CM_OBJ

! FIND THE CORRESPONDING COMPONENTS OF EACH VECTOR IN BODY FRAME
!---------------------------------------------------------------
! STATIC AXIAL FORCE VECTOR
       CALL VECSCL(FAMAG_OBJ, UCADUM,        FASDUM)

! STATIC NORMAL FORCE VECTOR
       CALL VECSCL(FNMAG_OBJ, UCNDUM,        FNSDUM)

! STATIC COUPLE VECTOR
       CALL VECSCL(ACMAG_OBJ, UCMDUM,       CUPSDUM)



!-------------------------------------------------------------
! CALC MAGNITUDES OF DYNAMIC AERO COUPLE AND NORMAL FORCES DUE TO PITCH RATE
!-------------------------------------------------------------
! NOTE: THESE MAGNITUDES ARE REFERENCED TO "DYNAMIC AERO FRAME"
       FNQMAG_OBJ  = RAQDUM * CNQ_OBJ * QLO2V_DUM
       ACQMAG_OBJ  = RLQDUM * CMQ_OBJ * QLO2V_DUM


! FIND THE CORRESPONDING COMPONENTS OF EACH VECTOR IN BODY FRAME
!---------------------------------------------------------------
! DYNAMIC NORMAL FORCE VECTOR
        CALL VECSCL(FNQMAG_OBJ, UOMKDUM,       FNDDUM)

! DYNAMIC COUPLE VECTOR
        CALL VECSCL(ACQMAG_OBJ, UOMJDUM,      CUPDDUM)

! for now just zero the dynamic contributions.....
        CUPDDUM(1) = 0.0
        CUPDDUM(2) = 0.0
        CUPDDUM(3) = 0.0


!-------------------------------------------------------
! FORM THE TOTAL TOSS OBJECT AERO FORCE VECTOR "FDB"
!-------------------------------------------------------
! NOTE: THESE ARE STORED INTO THE OFFCIAL TOSS AIR LOAD LOCATION
!       FOR THIS OBJECT. TOSS SEES THIS AS THE TOTAL AIRLOAD AT
!       THE OBJECT'S AERO REF PT (FROM WHICH IT WILL PRODUCE A
!       MOMENT CORRESPONDING TO THE LOAD AT THE SPECIFIED REF PT.

! FIRST SUM STATIC AERO LOADS (NORMAL AND AXIAL CONTRIBUTIONS)
       CALL VECSUM(FASDUM, FNSDUM,       FDB)

! NOW ADD IN THE DYNAMIC AERO (NORMAL) CONTRIBUTION
       CALL VECSUM(FNDDUM, FDB,          FDB)


!------------------------------------------------------------
! NOW FORM THE TOTAL TOSS OBJECT AERO COUPLE VECTOR "CDB"
! (TOSS WANTS TO SEE THIS AS COUPLE ASSOCIATED W/AERO REF PT)
!------------------------------------------------------------
! CONTRIBUTIONS FROM BOTH STATIC AND DYNAMIC AERO MODELS
        CALL VECSUM(CUPSDUM, CUPDDUM,    CDB)


! STANDARD RETURN POINT
1000  RETURN


! IDENTIFY SOURCE OF AERO TABLE LOOKUP ERRORS
2001  WRITE (LUE,2002) IERDUM
2002  FORMAT('AERO CA TABLE LOOKUP FAILURE',I2)
      STOP 'IN TOSAE3-2002'

2003  WRITE (LUE,2004) IERDUM
2004  FORMAT('AERO CN TABLE LOOKUP FAILURE',I2)
      STOP 'IN TOSAE3-2004'

2005  WRITE (LUE,2006) IERDUM
2006  FORMAT('AERO CM TABLE LOOKUP FAILURE',I2)
      STOP 'IN TOSAE3-2006'

2007  WRITE (LUE,2008) IERDUM
2008  FORMAT('AERO CNQ TABLE LOOKUP FAILURE',I2)
      STOP 'IN TOSAE3-2008'

2009  WRITE (LUE,2010) IERDUM
2010  FORMAT('AERO CMQ TABLE LOOKUP FAILURE',I2)
      STOP 'IN TOSAE3-2010'

      END
