! ROUTINE: TOSRR1
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!****************************
!****************************
!
      SUBROUTINE TOSRR1
!
!****************************
!****************************
! THIS ROUTINE PRODUCES A POSITION VECTOR (AND ITS TIME
! DERIVATIVE) FROM THE TOSS REF PT TO THE TOTAL SYSTEM CG.
! THESE VECTORS ARE EXPRESSED IN THE TOSS INERTIAL FRAME.


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS

! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.h"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.h"



!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"





      DIMENSION DUMAI(3), DUMAID(3)


! GET OUT IF DYNAMICS VERIFICATION IS NOT ACTIVATED
!--------------------------------------------------
      IF(LVERFY .EQ. 0) RETURN


!******************************************
! FIND TOTAL SYSTEM CENTER OF MASS LOCATION
!******************************************

! INITIALIZE SYSTEM TOTAL MASS-POSITION-MOMENT ACCUMULATOR
      POSCGI(1) = 0.0
      POSCGI(2) = 0.0
      POSCGI(3) = 0.0

! INITIALIZE SYSTEM TOTAL MASS-VELOCITY-MOMENT ACCUMULATOR
      VELCGI(1) = 0.0
      VELCGI(2) = 0.0
      VELCGI(3) = 0.0

! INITIALIZE SYSTEM MASS TALLY WITH TOSS REF PT MASS
      TOTMAS = RPMAST

!------------------------------------------------------------
! COMPUTE AND TALLY MASS AND VELOCITY MOMENTS OF TOSS OBJECTS
!------------------------------------------------------------
! NOTE: REF PT HAS NO CONTRIBUTION TO VECTOR MOMENTS

      DO 100 JOBJ = 2,LASOBJ

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
             CALL TOSLDW(JOBJ)

! ADD THIS OBJECTS MASS TO SYSTEM MASS TOTAL
             TOTMAS = TOTMAS + DMASS

! FORM OBJECTS POSITION MOMENT WR/T REF PT
!-----------------------------------------
             CALL VECSCL(DMASS,RBI, TOSVX1)

! ADD IN THIS CONTRIBUTION TO MASS MOMENT TOTAL
             CALL VECSUM(POSCGI,TOSVX1,  POSCGI)

! FORM OBJECTS VELOCITY MOMENT WR/T REF PT
!-----------------------------------------
              CALL VECSCL(DMASS,RBID, TOSVX1)

! ADD IN THIS CONTRIBUTION TO VELOCITY MOMENT TOTAL
              CALL VECSUM(VELCGI,TOSVX1,  VELCGI)


100   CONTINUE


!-----------------------------------------------------
! COMPUTE AND TALLY MASS MOMENTS OF ALL FINITE TETHERS
!-----------------------------------------------------

! DO A SORT LOOP THROUGH ALL FINITE SOLNS LOOKING FOR BEAD MODELS
!----------------------------------------------------------------
      DO 500 JF = 1, NFINIT

! FIND ASSOCIATED TOSS TETHER NUMBER
             JT = JASIGN(JF)

! SKIP FINITE SOLN IF IT HAS NOT BEEN ASSIGNED
             IF(JT .EQ. 0) GO TO 500

! SKIP FINITE SOLN IF IT IS NOT A BEAD MODEL
             IF(NFTYPE(JF) .EQ. 0) GO TO 500

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
             CALL TISLDW(JF)

! FIND THE ATTACH POINT NUMBER AT X END OF THIS TETHER
             NXADUM = LATTX(JT)

! FIND TOSS OBJECT NUMBER AT X-END OF THIS TETHER
             NXODUM = NOBJX(JT)

! FIND POSITION VECTOR FROM REF PT TO X END OF TETHER (TOSS INER FRAME)
             DUMAI(1)  = APS( 7,NXADUM,NXODUM)
             DUMAI(2)  = APS( 8,NXADUM,NXODUM)
             DUMAI(3)  = APS( 9,NXADUM,NXODUM)

! FIND DERIV. OF VECTOR FROM REF PT TO X END OF TETHER (TOSS INER FRAME)
             DUMAID(1) = APS(10,NXADUM,NXODUM)
             DUMAID(2) = APS(11,NXADUM,NXODUM)
             DUMAID(3) = APS(12,NXADUM,NXODUM)


!-------------------------------------------
! SUM EACH BEADS MASS MOMENT FOR THIS TETHER
!-------------------------------------------
             DO 400 JBEAD = 1,NBEAD

! FORM INDEX INTO THIS BEADS POSITION COMPONENTS OF BEAD STATE VECTOR
                    JB1 = 3*(JBEAD - 1) + 1
                    JB2 = JB1 + 1
                    JB3 = JB1 + 2

! FETCH POSITION OF BEAD IN INERTIAL FRAME
!-----------------------------------------
                    TOSVX6(1) = RUBI(JB1)
                    TOSVX6(2) = RUBI(JB2)
                    TOSVX6(3) = RUBI(JB3)

! CALC POSITION OF BEAD WR/T REF PT (IN INERTIAL FRAME)
                    CALL VECSUM(DUMAI,TOSVX6,  TOSVX6)

! FORM THIS BEADS CONTRIBUTION TO MASS MOMENT
                    CALL VECSCL(BMS(JBEAD),TOSVX6,  TOSVX6)

! ADD IN CONTRIBUTION TO MASS MOMENT
                    CALL VECSUM(POSCGI,TOSVX6,  POSCGI)

! FETCH VELOCITY OF BEAD IN INERTIAL FRAME
!-----------------------------------------
                    TOSVX6(1) = VUBI(JB1)
                    TOSVX6(2) = VUBI(JB2)
                    TOSVX6(3) = VUBI(JB3)

! CALC VELOCITY OF BEAD WR/T REF PT (IN INERTIAL FRAME)
                    CALL VECSUM(DUMAID,TOSVX6,  TOSVX6)

! FORM THIS BEADS CONTRIBUTION TO MASS MOMENT
                    CALL VECSCL(BMS(JBEAD),TOSVX6,  TOSVX6)

! ADD IN CONTRIBUTION TO VELOCITY MOMENT
                    CALL VECSUM(VELCGI,TOSVX6,  VELCGI)


! ACCUMULATE MASS OF EACH UNIFORM BEAD TO TOTAL SYSTEM MASS
!----------------------------------------------------------
                    TOTMAS = TOTMAS + BMS(JBEAD)

! END OF BEAD SUMMATION LOOP
!---------------------------
400           CONTINUE


! END OF FINITE SOLN SEARCH LOOP
!-------------------------------
500   CONTINUE


!------------------------------------------------------------
! CALCULATE VECTORS FROM REF PT TO SYSTEM CG (INERTIAL FRAME)
!------------------------------------------------------------
      CALL VECSCL(1.0/TOTMAS, POSCGI,  POSCGI)
      CALL VECSCL(1.0/TOTMAS, VELCGI,  VELCGI)



!**************************************************************
! INITIALIZE SYSTEM ANG MOMENTUM ACCUMULATION W/RP CONTRIBUTION
!**************************************************************

! INITIALIZE TOTAL PARTICLE ANGULAR MOMENTUM WITH REF PT CONTRIBUTION
      CALL CROSS (POSCGI,VELCGI,   PAMOMI)
      CALL VECSCL(RPMAST,PAMOMI,   PAMOMI)

! INITIALIZE TOTAL GRAVITY MOMENT WITH REF PT CONTRIBUTION
      CALL CROSS ( POSCGI,RPACGI,  SYSMOG)
      CALL VECSCL(-RPMAST,SYSMOG,  SYSMOG)


      RETURN
      END
