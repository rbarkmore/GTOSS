! ROUTINE: TOSCND
! %Z%GTOSS %M% H.9 code v01.10
!              H.9 code v01.10 Removed force/mom zeroing (in TOSCN now)
!----------------------------------------------------------------------
!              H.6 code v01.02 Fixed TOS_XV_1 and ROFBBI (no dimension)
!                              quiescent state bugs
!----------------------------------------------------------------------
! %Z%GTOSS %M% H.3 code v01.01 (New routine for vH.3)
!              H.3 code v01.01 Split TOSCN forming TOSCND (skip damper)
!                              Added skip damper quiescent state
!                              Modified anchor tether tension
!                              calcs for both active and passive state
!********************************
!********************************
!
      SUBROUTINE TOSCND (JBODY)
!
!********************************
!********************************
! THIS CONTROL OPTION ALLOWS FOR THE SIMULATION OF A BOOM-TIP
! TYPE OF SKIP ROPE DAMPER TO BE SIMULATED USING A TOSS OBJECT
!
! SIMULATING A BOOM TIP DAMPER IS A COMPLEX ISSUE INVOLVING THE
! INTERPRETATION OF A TOSS OBJECT AS A SURROGATE BEAD IN THE DOMAIN
! OF THE BEAD MODEL TETHER. ISSUES SUCH AS BEAD MASS, SPRING RATES
! AND WHAT HAPPENS DURING DEPLOYMENT, ETC MUST BE UNDERSTOOD.
! PUULEASE CONSULT THE TOSS REFERENCE MANUAL TO SEE HOW THIS IS
! DONE BEFORE MESSING WITH THIS CODE, OR SETTING UP A RUN!
!
!
! THE SKIP DAMPER HAS AN ACTIVE AND A QUIESCENT STATE AS PER
! THE VARIABLE "JQUPSD" IN THE COMMON BLOCK EQU_TOSS.i
!
!       ANYTIME JQUPSD = 0 THEN DAMPER IS ACTIVE (THE DEFAULT STATE)
!
!       ANYTIME JQUPSD = 1 THEN DAMPER IS IN QUIESCENT STATE
!
!
! ===============> CONCERNING THE QUIESCENT STATE <===============
!
!  1. THE CHALLENGE
!
!     THE QUIESCENT STATE (QS) IS INTENDED TO ALLOW A SIMULATION TO
!     BE RUN WITH THE TOPOLOGY OF A SKIP DAMPER RUN (IE. A CENTERPIECE
!     ANCHOR TETHER, ETC) BUT WITHOUT THE SMALL STEPSIZES REQUIRED TO
!     SUCCESSFULLY INTEGRATE THE SMALL CENTERPIECE MASS. SINCE THE
!     NATURALLY DEFINED FORCES AND DYNAMICAL ADVANCEMENT STATE CANNOT BE
!     ALLOWED TO PREVAIL (ELSE, SMALL STEPSIZES WOULD BE REQUIRED) THE
!     QS DOES NOT HAVE A UNIQUE DYNAMICAL DEFINITION, BUT RATHER CAN BE
!     CONCEIVED TO EXHIBIT ANY NUMBER OF (SOMEWHAT) ARBITRARY ATTRIBUTES
!
!     TO WIT:
!
!     A. WHAT'S THE NET FORCE STATE ON THE CENTERPIECE (CP)?
!     B. HOW DOES THE FORCE AND MOMENT EFFECTS THAT THE APPROACH ANGLE
!        OF THE TETHER (TO THE DEPLOY ORIFICE) GET TRANSMITTED TO THE
!        DAMPER HOST OBJEVCT?
!     C. WHAT'S THE RELATIONSHIP BETWEEN DAMPED AND ANCHOR TETHER TENS?
!     D. WHAT'S THE POSITION OF THE CP?
!     E. WHAT'S THE VEL AND ACCEL OF THE CP?
!     F. HOW CONSISTENT MUST BE THE ACCEL/VEL/POS OF THE CP (WHICH IS
!        THE ATT PT FOR THE DAMPED TETHERS FINITE MODEL) TO INSURE
!        A REASONABLE SOLUTION FOR THE FINITE MODEL?
!     G. HOW SENSITIVE ARE THESE ISSUES TO THE SCALE OF THE
!        PRACTICAL PROBLEMS AT HAND (IE THE REAL GEOMETRY WHICH
!        WOULD BE STUDIED)?
!     H. HOW SIGNFICIANT WILL BE THE TRANSIENTS FROM QUIESECENT STATE TO
!        NORMAL STATE (THIS IS THE NORMALLY EXPECTED MODUS OPERANDI)?
!
!     GIVEN BELOW IS THE CURRENT SET OF CONVENTIONS AND ASSUMPTIONS
!     USED TO CONSTRUCT THE QUIESCENT STATE MODE.
!
!
!  2. CURRENT DEFINITION OF QUIESCENT STATE:
!
!      1. ALL DAMPER LINK ARM FORCES ARE ZERO
!         (NO CONTROL FORCES ON OBJECT)
!
!      2. INBOARD (ANCHOR TETHER) TENSION SET EQUAL TO OUTBOARD TENSION
!
!      3. THE DAMPER CENTERPIECE IS MAINTAINED IN A POSITION SUCH THAT:
!
!           A. CP IS ON A STRAIGHT LINE BETWEEN THE NEAREST BEAD IN THE
!              OUTBOARD (DAMPED FINITE SOLN) TETHER.
!
!           B. CP IS IN THE PLANE OF THE 1ST THREE DAMPER LINK ARMS
!              (IE. THE TOSS OBJECT STATE IS FORCED TO THIS LOCATION)
!
!              FROM A TOPOLOGICAL STAND-POINT, ITEMS A + B MAKES THE
!              CP/ANCHOR TETHER A NATURAL EXTENSION OF THE OUTBOARD
!              FINITE TETHER AS THOUGH THE OUTBOARD TETHER WERE
!              EMANATING DIRECTLY FROM THE DEPLOY ORIFICE WITH NO
!              INTERVENTION FROM THE DAMPING LINKS.
!
!           C. CP VELOCITY AND ACCELERATION IS THAT WHICH WOULD BE
!              EXPERIENCED BY A POINT ON A FINITE OUTBOARD TETHER
!
!              ITEM C. PROVIDES A DYNAMICALLY CONSISTENT ATTACH POINT
!              STATE TO THE OUTBOARD TETHER FINITE SOLN.
!
!           D. THE STATE OF THE CP TOSS OBJECT IS INHIBITED FROM
!              INTEGRATIVE ADVANCEMENT.


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS

! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.h"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.h"



!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"





! MISCELLANEOUS LOCAL VARIABLES...NOTE: THE USUAL TOSS NAMING CONVENTION
! FOR LOCAL VARIABLES IS NOT FOLLOWED HERE SO AS TO PROMOTE CLARITY
      DIMENSION  RAPI(3),    RAPID(3),    RAPIDD(3),   &
     &           RAOI(3),    RAOID(3),    UAOI(3),   &
     &           RANCA(3),   RANCAD(3),   RANCI(3),    RANCID(3),   &
     &           RLNI(3),    RLNID(3),    ULNI(3),   &
     &           OMA(3),     OMAD(3),     DUMFC(3),    DUMGC(3),   &
     &           FAPI1(3),   FAPI2(3),    UFAPI1(3),   UFAPI2(3)

! VARIABLES ASSOCIATED EXCLUSIVELY WITH THE QUIESCENT STATE
      DIMENSION  D1ANCA(3),  D2ANCA(3),   D3ANCA(3),   &
     &           D12DUM(3),  D13DUM(3),   DUMNPA(3),   &
     &           UII(3),     UIDI(3),     UIDDI(3),   &
     &           UBBI(3),    UBBA(3),   &
     &           DUMB(3),    DUMBD(3),    DUMBDD(3),   &
     &           RCPBBI(3),  RORFCP(3),   ROFBBI(3)


!-----------------------------------------------------------
! EXTRACT NEEDED DATA FROM THE ATTACH PT CARRYING THE DAMPER
!-----------------------------------------------------------
! ATTACH PT POSITION, VELOCITY, AND FRAME ANGULAR RATES
      DO 2011 J = 1,3

         RAPI(J)   = APS( 6+J, LAPPSR, LOBJSR)
         RAPID(J)  = APS( 9+J, LAPPSR, LOBJSR)
         RAPIDD(J) = APS(12+J, LAPPSR, LOBJSR)

         OMA(J)    = APS(24+J, LAPPSR, LOBJSR)
         OMAD(J)   = APS(27+J, LAPPSR, LOBJSR)

2011  CONTINUE

! ATTACH PT FRAME DIR COSINES WR/T INER FRAME
      GSDAI(1,1) = APS(16, LAPPSR, LOBJSR)
      GSDAI(2,1) = APS(17, LAPPSR, LOBJSR)
      GSDAI(3,1) = APS(18, LAPPSR, LOBJSR)

      GSDAI(1,2) = APS(19, LAPPSR, LOBJSR)
      GSDAI(2,2) = APS(20, LAPPSR, LOBJSR)
      GSDAI(3,2) = APS(21, LAPPSR, LOBJSR)

      GSDAI(1,3) = APS(22, LAPPSR, LOBJSR)
      GSDAI(2,3) = APS(23, LAPPSR, LOBJSR)
      GSDAI(3,3) = APS(24, LAPPSR, LOBJSR)


!******************************************
! DETERMINE IF ITS ACTIVE OR QUIESCENT MODE
!******************************************
      IF(JQUPSD .EQ. 1) GO TO 5000



!******************************************************************
!******************************************************************
!******************************************************************
!******************************************************************
! THIS DEFINES THE ACTIVE MODE OF OPERATION OF THE SKIP ROPE DAMPER
!******************************************************************
!******************************************************************
!******************************************************************
!******************************************************************

!-------------------------------------------------------------------
! CALC POS VEC (AND DERIV) FROM ATT PT TO THIS TOSS OBJ (INER FRAME)
!-------------------------------------------------------------------
       CALL VECDIF(RBI,   RAPI,   RAOI )
       CALL VECDIF(RBID,  RAPID,  RAOID)

! THEN FORM UNIT VECTOR FROM ATT PT TO THIS TOSS OBJECT
       CALL VECNRM(RAOI,  UAOI)


!****************************************************
!----------------------------------------------------
! START A BIG LOOP TO PROCESS EACH LINK OF THE DAMPER
!----------------------------------------------------
!****************************************************
! ZERO NET CONTROL FORCE TO ACCUMULATE EACH LINK FORCE CONTRIBUTION
       DUMFC(1) = 0.0
       DUMFC(2) = 0.0
       DUMFC(3) = 0.0

       DO 2200 J = 1,LNCHOR

! SET IN THIS DAMPER LINKS ANCHOR POINT (IN AP FRAME)
          RANCA(1) = XASRA(J)
          RANCA(2) = YASRA(J)
          RANCA(3) = ZASRA(J)

! CALC DERIVATIVE OF ANCHOR VECTOR (EXPRESSED IN AP FRAME)
! NOTE: ANCHOR COORDS ARE CONSTANT IN ANCHOR FRAME
          CALL CROSS(OMA,RANCA,    RANCAD)

! FIND ANCHOR VECTOR AND ITS DERIVATIVE IN INER FRAME
          CALL MATVEC(1,GSDAI, RANCA,      RANCI )
          CALL MATVEC(1,GSDAI, RANCAD,     RANCID)

! FORM DAMPER LINK VEC (+ DERIV) FROM CENTERPIECE TO ANCHOR (INER FRAME)
          CALL VECDIF(RANCI,   RAOI,     RLNI )
          CALL VECDIF(RANCID,  RAOID,    RLNID)

! FIND UNIT VECTOR ALONG LINK FROM OBJECT TO ANCHOR
          CALL VECNRM(RLNI, ULNI)

! FIND MAGNITUDE AND DERIV OF MAG OF LINK VECTOR
          DUMMG  = VECMAG(RLNI)
          DUMMGD = DOT(ULNI,RLNID)

! FIND THIS LINKS SPRING EXTENSION (RECKONED FROM REFERENCE LENGTH)
          DELDUM = DUMMG - REFSRL(J)


! DO PRELOAD HYSTERESIS LINK LOAD CALCULATIONS
!---------------------------------------------
! CALCULATE THIS LINKS BASE PRELOAD AMD MAX HYSTERESIS INCREMENT
          DUMBAS = 0.5*(1.0 + HYSRPL(J))*SRPL(J)
          DUMDLO = 0.5*(1.0 - HYSRPL(J))*SRPL(J)

! CALCULATE THE ACTUAL PRELOAD INCREMENT AS FUNC OF EXTENSION RATE
! (ONLY IF A NON-ZERO FULL-ONSET EXTENSION RATE IS SPECIFIED)
          IF(HYSONS .NE. 0.0) THEN
             DUMSDA = (ABS(DUMMGD) - HYSONS)/HYSONS
             DUMDEL = DUMDLO*(1.0 - DUMSDA**2)
          END IF

! BOUND THE ONSET EFFECT TO EXTENSION RATES LESS THAN FULL ONSET
          IF(ABS(DUMMGD) .GE. HYSONS) DUMDEL = DUMDLO

          IF(DUMMGD .LT. 0.0) DUMDEL = - DUMDEL

! CALCULATE HIS LINKS ACTUAL LOAD DUE TO PRELOAD HYSTERESIS
          DUMP  = DUMBAS + DUMDEL


! DO SPRING AND DAMPING HYSTERESIS LINK LOAD CALCULATIONS
!--------------------------------------------------------
! SET IN THIS LINKS SPRING/DAMPING CONSTANTS (FOR POS LINK EXTENSION)
          DUMS  = SRK(J)
          DUMSD = SRKD(J)

! DO HYSTERESIS DELIBERATIONS FOR LINKER FORCE COEFFICIENTS
          IF(DUMMGD .LT. 0.0) THEN
               DUMS  = HYSRK(J) *DUMS
               DUMSD = HYSRKD(J)*DUMSD
          END IF


! CALCULATE THIS LINKS NET DAMPER LOAD MAGNITUDE
!-----------------------------------------------
          ARMLOD(J) = DUMP + DUMS*DELDUM + DUMSD*DUMMGD

! FIND THE NET LOAD VECTOR FOR THIS LINK
          CALL VECSCL(ARMLOD(J), ULNI, TOSVX1)

! ADD THIS LINKS NET LOAD TO THE OTHER LINKS IN THIS DAMPER
          CALL VECSUM(DUMFC, TOSVX1,    DUMFC)

2200   CONTINUE

!****************************
! END OF DAMPER LINK ARM LOOP
!****************************


!------------------------------------------------------------------
! APPLY DAMPER EFFECTS TO THIS OBJECT UNDER GUISE OF CONTROL FORCES
!------------------------------------------------------------------
! TRANSFORM INERTIAL COMPONENTS TO BODY FRAME
      CALL MATVEC(0,GBI,DUMFC,    DUMFC)

! INCREMENT OBJECT BODY FRAME WITH DAMPER IMPOSED LOADS
      FCB(1) = FCB(1) + DUMFC(1)
      FCB(2) = FCB(2) + DUMFC(2)
      FCB(3) = FCB(3) + DUMFC(3)

! NOTE: THIS OBJECT MUST BE A PARTICLE, HENCE THERE ARE NO MOMENTS
!       RELATED TO THIS OBJECT DUE TO THE DAMPER. HOWEVER, NOTE THE
!       ATTACH POINT RELATED TO THIS DAMPER CAN EXPERIENCE A MOMENT
!       DUE TO DAMPER LOADS ACTING AT A DISTANCE (SEE CODE BELOW).


!*********************************************************************
! SAVE DAMPER EFFECT FOR LATER APPLICATION TO OFFICIAL APS ATT PT AREA
!*********************************************************************
! CALCULATE A NET MOMENT AT THE ATTACH PT ASSOCIATED WITH THIS DAMPER
       CALL CROSS(RAOI, DUMFC,    DUMGC)

! ADD (REVERSE) OF THIS DAMPERS FORCE AND COUPLE CONTRIBUTION TO ATTACH PT
      DO 2211 J = 1,3
         APS(36+J, LAPPSR,LOBJSR) = APS(36+J, LAPPSR,LOBJSR) - DUMFC(J)
         APS(39+J, LAPPSR,LOBJSR) = APS(39+J, LAPPSR,LOBJSR) - DUMGC(J)
2211  CONTINUE



!*********************************************************************
! DETERMINE THE FORCE EFFECTS OF THE TETHER ON THE CENTER-PIECE OBJECT
!*********************************************************************
! THIS IS ACCOMPLISHED BY ADJUSTING THE TENSION IN THE MASSLESS TETHER
! CONNECTING THE CENTERPIECE TO THE BUGLE SUCH THAT WHEN THE TWO TETHER
! FORCE VECTORS ARE SUMMED BY TOSS AND APPLIED TO THE CENTER-PIECE, THE
! NET EFFECT WILL BE THAT OF THE NORMAL AND FRICTION FORCE WHICH WOULD
! BE APPLIED TO THE CENTER-PIECE BY THE TETHER...SEE REF. MAN. ANALYSIS.

! ASSUME NO TETHER/CENTERPIECE RELATIVE MOTION
        DLFRIC = 0.0

! PROTECT TETHER FORCE CALCS FROM INITIAL NULL VECTORS
       IF( TOSTIM .GT. 0.0 ) THEN

! EXTRACT ATTACH PT FORCE VECTORS ON TETHERS SPANNING CENTER-PIECE
          FAPI1(1) = APS(1,1,JBODY)
          FAPI1(2) = APS(2,1,JBODY)
          FAPI1(3) = APS(3,1,JBODY)

          FAPI2(1) = APS(1,2,JBODY)
          FAPI2(2) = APS(2,2,JBODY)
          FAPI2(3) = APS(3,2,JBODY)

! FIND AVERAGE TENSION MAGNITUDE FROM BOTH THESE TENSION VECTORS
          DUMMT1 = VECMAG(FAPI1)
          DUMMT2 = VECMAG(FAPI2)

          DUMAVG = 0.5*(DUMMT1 + DUMMT2)

! CALL OFF THE TENSION TUG OF WAR IF EITHER TETHER IS SLACK
          IF((DUMMT1 .EQ. 0.0) .OR. (DUMMT2 .EQ. 0.0)) DUMAVG = 0.0

! FORM UNIT VECTORS IN DIRECTION OF TENSION
          IF(DUMMT1 .NE. 0.0)  CALL VECNRM(FAPI1,   UFAPI1)
          IF(DUMMT2 .NE. 0.0)  CALL VECNRM(FAPI2,   UFAPI2)

! FORM NEW TENSION VECTORS W/ IDENTICAL MAG (IE. THE AVER TENSION)
          CALL VECSCL(DUMAVG,UFAPI1,    TOSVX1)
          CALL VECSCL(DUMAVG,UFAPI2,    TOSVX2)

! SUM THESE TO GET AVERAGE NORMAL FORCE VECTOR
          CALL VECSUM(TOSVX1,TOSVX2,   TOSVX3)

! THE MAGNITUDE OF THIS VECTOR IS THE NORMAL FORCE ON CENTER-PIECE
          FRICNM = VECMAG(TOSVX3)

! FIND UNIT VECTOR IN DIRECTION OF FRICTION FORCE
! (ASSUMED NORMAL TO THE FORCE NORMAL, AND IN THE PLANE OF TENSION)
          CALL CROSS(TOSVX1,TOSVX2,    TOSVX4)
          CALL CROSS(TOSVX3,TOSVX4,    TOSVX5)

! PROTECT CALC OF FRICTION UNIT VECTOR IF TENSIONS HAVE GONE NULL
          IF(VECMAG(TOSVX5) .NE. 0.0)  CALL VECNRM(TOSVX5,  TOSVX5)

! CALC MAG OF FRICTION FORCE (IE. TENSILE FRIC DROP ACROSS CENTER-PIECE)
! (PAYING NO HEED TO THE SIGN OF RESULTING FRIC FORCE.... SEE NEXT STEP)
          DLFRIC = COEFMU*FRICNM*ABS( DOT(UAOI,TOSVX5) )

! DETERMINE THE SENSE OF THE TENSILE FRICTION DROP BY EXAMINING THE
! RELATIVE MOTION BETWEEN TETHER AND CENTER-PIECE ALONG LINE OF TETHER
!---------------------------------------------------------------------
! CALC RATE OF CHANGE OF DISTANCE BETWEEN CENTER-PIECE AND ATT PT
          DUMDSD = DOT(RAOID,UAOI)

! CALCULATE REL VELOCITY BETWEEN TETHER AND CENTERPIECE
          DUMREL = TLENTD(LTHDMP) - DUMDSD

! ASSIGN PROPER SENSE TO FRIC (DEPENDING ON CP-TETHER REL MOTION)
          IF(DUMREL .GT. 0.0) DLFRIC = - DLFRIC

! NOW SHAPE FRICTION ONSET AS FUNC OF REL MOTION BETWEEN CP AND TETHER
          IF(FRCONS .NE. 0.0) THEN
             DUMSDA = (ABS(DUMREL) - FRCONS)/FRCONS
             DUMFAC = 1.0 - DUMSDA**2
          END IF

! BOUND THE FRICTION ONSET EFFECT TO REL RATES LESS THAN FULL ONSET
          IF(ABS(DUMREL) .GE. FRCONS) DUMFAC = 1.0

          DLFRIC = DUMFAC*DLFRIC

       END IF



!***********************************************************************
! CALC DEPLOYED UNDEFORMED LENGTH CREATING TENS GRAD ACROSS CENTER-PIECE
! NOTE: THIS IS DONE ONLY FOR THE MASSLESS TETHER CONNECTED BETWEEN THE
!       DAMPER CENTER-PIECE AND THE ATTACH POINT CARRYING THE DAMPER
!***********************************************************************
! FIND TENS AT END OF TETHER-TO-BE-DAMPED THATS CONNECTED TO THIS OBJ
      DUMTEN = XELOAD(LTHDMP)
      IF(NOBJX(LTHDMP).NE.JBODY) DUMTEN = YELOAD(LTHDMP)

! ADJUST THIS TENSION FOR FRICTION GRADIENT
      DUMTEN = DUMTEN + DLFRIC

! CALC DEPLOYED AMOUNT TO ACHIEVE THIS TENSION
      FACDUM = TSPRNO(LTHBAS)*TLENO(LTHBAS)
      TLENT(LTHBAS) = FACDUM*TDIST(LTHBAS)/(DUMTEN + FACDUM)

! GUARD AGAINST IC SNAFFU
      IF(TLENT(LTHBAS) .EQ. 0.0) TLENT(LTHBAS) = TLENO(LTHBAS)

! MAKE SURE THIS TENS IS KNOWN TO SRD OBECT ATT PTS BEFORE OBJ ACCEL IS
! DETERMINED (A CRUCIAL FORCE BALANCE IN THE EYES OF FINITE HST SOLN DUE
! TO PRESENCE OF ACCEL FEEDBACK PATH NON-EXISTENT FOR STD BEAD MODELS)
!---------------------------------------------------------------------
      CALL TOSTHR(LTHBAS)

! DETERMINE CONNECTIVITY OF THE ANCHOR BASE TETHER
      IF(NOBJX(LTHBAS) .EQ. JBODY) THEN
          DUMTFX = TFXEND(1)
          DUMTFY = TFXEND(2)
          DUMTFZ = TFXEND(3)
          JADUM  = LATTX(LTHBAS)
      ELSE
          DUMTFX = TFYEND(1)
          DUMTFY = TFYEND(2)
          DUMTFZ = TFYEND(3)
          JADUM  = LATTY(LTHBAS)
      END IF

! DISTRIBUTE ASSOCIATED TENS FORCE TO ANCHOR ATT PT ON SRD OBJECT
      APS(1,JADUM,JBODY) = DUMTFX
      APS(2,JADUM,JBODY) = DUMTFY
      APS(3,JADUM,JBODY) = DUMTFZ



!**********************************
!**********************************
! END OF ACTIVE DAMPER CALCULATIONS
!**********************************
!**********************************
      GO TO 10000





!******************************************************************
!******************************************************************
!******************************************************************
!******************************************************************
! THIS DEFINES THE QUIESCENT MODE OF OPERATION OF SKIP ROPE DAMPER
!******************************************************************
!******************************************************************
!******************************************************************
!******************************************************************
5000  CONTINUE

!**************************************************************
! FORM AXIAL UNIT VEC ASSOCIATED WITH DMAPED TETHER FINITE SOLN
!**************************************************************
! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
! IE. THE DAMPED (OUTBOARD) TETHER
      JFTETH = LASIGN(LTHDMP)
      CALL TISLDW(JFTETH)


! MAKE AXIAL ATT PT VECTOR POINT FROM CP TO OPP END OF DAMPED TETHER
      CALL VECMOV(UITI,   UII   )
      CALL VECMOV(UITDI,  UIDI  )
      CALL VECMOV(UITDDI, UIDDI )
      IF(NOBJX(LTHDMP).NE.JBODY) THEN
          DUMS = -1.0
          CALL VECSCL(DUMS, UII,   UII   )
          CALL VECSCL(DUMS, UIDI,  UIDI  )
          CALL VECSCL(DUMS, UIDDI, UIDDI )
      END IF


! FIND BEAD NUMBER OF THE BEAD ADJACENT TO CP
      JBBDUM = 1
      IF(NOBJX(LTHDMP).NE.JBODY) JBBDUM = NBEAD


! FETCH THE BEAD COORDINATE STATES FOR BEAD ADJACENT TO CP
      JB1 = 3*(JBBDUM-1) + 1
      JB2 = JB1+1
      JB3 = JB1+2

      DUMB(1)   = BUI(JB1)
      DUMB(2)   = BUI(JB2)
      DUMB(3)   = BUI(JB3)

      DUMBD(1)  = BUDI(JB1)
      DUMBD(2)  = BUDI(JB2)
      DUMBD(3)  = BUDI(JB3)

      DUMBDD(1) = BUDDI(JB1)
      DUMBDD(2) = BUDDI(JB2)
      DUMBDD(3) = BUDDI(JB3)


! FIND A VEC POINTING FROM CP (IE. DAMPED TETH ATT PT) TO ADJACENT BEAD
      CALL VECSCL(ELSGX,UII,   TOSVX1)
      CALL VECSUM(TOSVX1, DUMB,   RCPBBI)


! FIND THE VECTOR FROM THE DEPLOY ORIFICE TO THE CP
      RORFCP(1) = RBI(1) - RAPI(1)
      RORFCP(2) = RBI(2) - RAPI(2)
      RORFCP(3) = RBI(3) - RAPI(3)


! NOW FIND A VECTOR FROM THE DEPLOY ORIFICE TO THE ADJACENT BEAD
      CALL VECSUM(RORFCP, RCPBBI,     ROFBBI)

! MAKE THIS VECTOR A UNIT VECTOR
      CALL VECNRM(ROFBBI,   UBBI)

! TRANSFORM THIS UNIT VECTOR INTO ATTACH POINT FRAME
      CALL MATVEC(0,GSDAI, UBBI,      UBBA)



!******************************************************
! DETERMINE THE PLANE DEFINED BY DAMPER LINK ANCHOR PTS
!******************************************************
! CHECK FOR AT LEAST 3 DAMPER LINKS (NEEDED TO MAKE CALC DETERMINANT)
      IF(LNCHOR .LT. 3) THEN
           WRITE (LUE,5001)
5001       FORMAT('DAMPER QUIESCENT MODE NEEDS AT LEAST 3 LINK ARMS')
           STOP 'IN TOSCND-1'
      END IF


! EXTRACT DAMPER LINK ANCHOR PT POS VECTORS (IN AP FRAME)
      D1ANCA(1) = XASRA(1)
      D1ANCA(2) = YASRA(1)
      D1ANCA(3) = ZASRA(1)

      D2ANCA(1) = XASRA(2)
      D2ANCA(2) = YASRA(2)
      D2ANCA(3) = ZASRA(2)

      D3ANCA(1) = XASRA(3)
      D3ANCA(2) = YASRA(3)
      D3ANCA(3) = ZASRA(3)

! FORM VECTORS FROM ANCHOR OF 1-LINK TO THE 2-LINK AND 3-LINK ANCHORS
      CALL VECDIF(D2ANCA,D1ANCA, D12DUM)
      CALL VECDIF(D3ANCA,D1ANCA, D13DUM)

! FORM VEC NORMAL TO PLANE DEFINED BY D12DUM,D13DUM (IN AP FRAME)
! (CALLED THE EXIT PLANE OF THE DAMPER)
      CALL CROSS(D12DUM, D13DUM,    DUMNPA)



!*********************************************
! FIND THE DISTANCE FROM THE ORIFICE TO THE CP
!*********************************************
! NOTE: THIS SCENARIO ASSUMES CP IS IN THE EXIT PLANE, AND ON A LINE OF
!       THE TETHER FRAME AXIAL UNIT VECTOR EMANATING FROM THE ORIFICE
!       TO THE ADJACENT BEAD OF THE DAMPED TETHER
!
! NOTE: THIS SUB-VARIANT SCHEME USES A FIXED LENGTH FOR ACPLEN,
!       WHICH IS DETERMINED DURING IC STAGE


! DETERMINE FIXED ORIFICE TO CP DISTANCE BASED ON IC CONDITIONS
!--------------------------------------------------------------
      IF(ICSTAG .NE. -1) THEN
! CALC DIST FROM ORIFICE TO CP ALONE AXIAL UNIT VECTOR (USE AP FRAME)
           ACPLEN = 0.0
           DUMDEN = DOT(UBBA,DUMNPA)
           IF(DUMDEN .NE. 0.0) ACPLEN = ABS(DOT(D1ANCA,DUMNPA)/DUMDEN)
      END IF


! FORM THE RATIO OF  ORIFICE-TO-CP   VS  ORIFICE-TO-BB  DISTANCE
!---------------------------------------------------------------
      RATDUM = ACPLEN/(ACPLEN + ELBSG(JBBDUM))


!****************************************************************
!****************************************************************
! THEN DETERMINE THE POSITION OF THE CP TOSS OBJECT WR/T RP
!****************************************************************
!****************************************************************
! (NOTE: THIS DEFINES TOSS OBJ POS STATE SINCE NO INTEGRATIONS ALLOWED)
      RBI(1) = RAPI(1) + ACPLEN*UBBI(1)
      RBI(2) = RAPI(2) + ACPLEN*UBBI(2)
      RBI(3) = RAPI(3) + ACPLEN*UBBI(3)



!****************************************************************
!****************************************************************
! THEN DETERMINE CONSISTENT VELOCITY OF CP (STATE OF TOSS OBJECT)
!****************************************************************
!****************************************************************
! THEN ADD THIS CONTRIBUTION TO THAT OF THE REF ANCHOR AP RATE
      RBID(1) = RAPID(1) + RATDUM*( ELSGX*UIDI(1) + DUMBD(1) )
      RBID(2) = RAPID(2) + RATDUM*( ELSGX*UIDI(2) + DUMBD(2) )
      RBID(3) = RAPID(3) + RATDUM*( ELSGX*UIDI(3) + DUMBD(3) )



!********************************************************************
!********************************************************************
! THEN DETERMINE CONSISTENT ACCELERATION OF CP (STATE OF TOSS OBJECT)
!********************************************************************
!********************************************************************
! THEN ADD THIS CONTRIBUTION TO THAT OF THE REF ANCHOR AP ACCEL
      RBIDD(1) = RAPIDD(1) + RATDUM*( ELSGX*UIDDI(1) + DUMBDD(1) )
      RBIDD(2) = RAPIDD(2) + RATDUM*( ELSGX*UIDDI(2) + DUMBDD(2) )
      RBIDD(3) = RAPIDD(3) + RATDUM*( ELSGX*UIDDI(3) + DUMBDD(3) )



!*******************************************
!*******************************************
! DETERMINE THE TENSION IN THE ANCHOR TETHER
!*******************************************
!*******************************************
! FIND TENS AT END OF TETHER-TO-BE-DAMPED THATS CONNECTED TO THIS OBJ
      DUMTEN = XELOAD(LTHDMP)
      IF(NOBJX(LTHDMP).NE.JBODY) DUMTEN = YELOAD(LTHDMP)

! SET IN DISTANCE BETWEEN ATTACH POINTS FOR THE ANCHOR TETHER
      TDIST(LTHBAS) = ACPLEN

! CALC DEPLOYED AMOUNT TO ACHIEVE THIS TENSION
      FACDUM = TSPRNO(LTHBAS)*TLENO(LTHBAS)

      TLENT(LTHBAS) = FACDUM*TDIST(LTHBAS)/(DUMTEN + FACDUM)

! GUARD AGAINST IC SNAFFU
      IF(TLENT(LTHBAS) .EQ. 0.0) TLENT(LTHBAS) = TLENO(LTHBAS)


! MAKE SURE THIS TENS IS KNOWN TO SRD OBECT ATT PTS BEFORE OBJ ACCEL IS
! DETERMINED (A CRUCIAL FORCE BALANCE IN THE EYES OF FINITE HST SOLN DUE
! TO PRESENCE OF ACCEL FEEDBACK PATH NON-EXISTENT FOR STD BEAD MODELS)
!---------------------------------------------------------------------
      CALL TOSTHR(LTHBAS)

! DETERMINE CONNECTIVITY OF THE ANCHOR BASE TETHER
      IF(NOBJX(LTHBAS) .EQ. JBODY) THEN
          DUMTFX = TFXEND(1)
          DUMTFY = TFXEND(2)
          DUMTFZ = TFXEND(3)
          JADUM  = LATTX(LTHBAS)
      ELSE
          DUMTFX = TFYEND(1)
          DUMTFY = TFYEND(2)
          DUMTFZ = TFYEND(3)
          JADUM  = LATTY(LTHBAS)
      END IF

! DISTRIBUTE ASSOCIATED TENS FORCE TO ANCHOR ATT PT ON SRD OBJECT
      APS(1,JADUM,JBODY) = DUMTFX
      APS(2,JADUM,JBODY) = DUMTFY
      APS(3,JADUM,JBODY) = DUMTFZ


! STANDARD RETURN
10000 CONTINUE

      RETURN
      END
