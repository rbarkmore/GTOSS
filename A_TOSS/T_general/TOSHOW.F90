! ROUTINE: TOSHOW
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!****************************
!****************************
!
      SUBROUTINE TOSHOW
!
!****************************
!****************************
! THIS ROUTINE PERFORMS CALCULATIONS OF INTERPRETATION

! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_RPS.h"





! LOAD TOSS OBJECT DATA AND DO INTERPRETIVE CALCS
      DO 100 KOBJ = 2,LASOBJ

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
             CALL TOSLDW(KOBJ)
             CALL TOSCAL

100   CONTINUE


      RETURN
      END
