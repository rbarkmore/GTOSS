! ROUTINE: TOSSF4
! %Z%GTOSS %M% H.5 code v01.02
!              H.5 code v01.02 Changed 6.28 to 2.0*PIIALL from COM_ALL.i
!                              Consolidated program-specific permanence
!                              variables:  F4LEN, TSLF4, FSLF4,
!                              into universal permanence variables
!----------------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************
!*************************************
!
      SUBROUTINE TOSSF4(JS,JTETH)
!
!*************************************
!*************************************
! THIS ROUTINE INTERPRETS A SCENARIO TO BE A
! (1-COS) PULSE LENGTH-RATE CONTROL DEPLOYMENT


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"





! INITIALIZE LENGTH BASE PRIOR TO INTEGRATION
!--------------------------------------------
      IF(QTIME .LE. TDSCON(JS))  FPLEN(JTETH) = TLENT(JTETH)


! INITIALIZE REDUCED-TIME PARAMETERS (TRIGGERED ON QTIME)
!--------------------------------------------------------
      IF(QTIME .EQ. 0.0) FSLFP(JTETH) = 0.0
      IF(QTIME .EQ. 0.0) TSLFP(JTETH) = DDINV1(JS) + DDINV2(JS)   &
     &                     + DDINV3(JS) + DDINV4(JS) + DDINV5(JS)


! DO ABSOLUTE-TIME START/STOP DETERMINATION
!------------------------------------------
! GO TO NEXT TETHER IF ITS NOT TIME TO START THIS SCENARIO
      IF(QTIME .LT. TDSCON(JS)) GO TO 1500

      TLENTD(JTETH) = 0.0
      TLENDD(JTETH) = 0.0

! GO TO NEXT TETHER IF TIME TO STOP THIS SCENARIO
! (ZERO STOP TIME DEFAULTS TO ENDLESS REPETITION)
      IF( (TDSCOF(JS) .NE. 0.0)  .AND.   &
     &               (QTIME .GT. TDSCOF(JS))  ) GO TO 1500

!-----------------------------------------------------
! START REDUCED TIME AND SCENARIO PERIOD DETERMINATION
!-----------------------------------------------------
      RTDUM = QTIME - TDSCON(JS) - FSLFP(JTETH)*TSLFP(JTETH)
      IF(RTDUM .GE. TSLFP(JTETH)) FSLFP(JTETH) = FSLFP(JTETH) + 1.0

! CAPTURE SCENARIO DATA FOR APPROPRIATE PERIOD
!---------------------------------------------
! ASSUME PERIOD 1
      BGDUM = BGVAL1(JS)
      ENDUM = ENVAL1(JS)
      DLDUM = DDINV1(JS)
      RLDUM = RTDUM
      IF(RLDUM .LE. DLDUM) GO TO 1100

! ASSUME PERIOD 2
      BGDUM = BGVAL2(JS)
      ENDUM = ENVAL2(JS)
      DLDUM = DDINV2(JS)
      RLDUM = RLDUM - DDINV1(JS)
      IF(RLDUM .LE. DLDUM) GO TO 1100

! ASSUME PERIOD 3
      BGDUM = BGVAL3(JS)
      ENDUM = ENVAL3(JS)
      DLDUM = DDINV3(JS)
      RLDUM = RLDUM - DDINV2(JS)
      IF(RLDUM .LE. DLDUM) GO TO 1100

! ASSUME PERIOD 4
      BGDUM = BGVAL4(JS)
      ENDUM = ENVAL4(JS)
      DLDUM = DDINV4(JS)
      RLDUM = RLDUM - DDINV3(JS)
      IF(RLDUM .LE. DLDUM) GO TO 1100

! ASSUME PERIOD 5
      BGDUM = BGVAL5(JS)
      ENDUM = ENVAL5(JS)
      DLDUM = DDINV5(JS)
      RLDUM = RLDUM - DDINV4(JS)

! END OF PERIOD DETERMINATION
1100  CONTINUE


!-------------------------------------
! PROTECT AGAINST SLOPPY CRAY NUMERICS
!-------------------------------------
      IF(DLDUM  .NE. 0.0) THEN

! DEFINE 1-COS COEF AS RATE OR AMOUNT OF TETHER LENGTH TO BE DEPLOYED
!--------------------------------------------------------------------
! (BY 1ST ASSUMING AN AMOUNT OF TETHER IS SPECIFIED VIA END VALUE)
         COFDUM = 0.0
         IF(DLDUM  .NE. 0.0) COFDUM = ENDUM/DLDUM
         IF(COFDUM .EQ. 0.0) COFDUM = 0.5*BGDUM


! DO DEPLOYMENT PULSE RATE DETERMINATION FOR THIS PERIOD
!-------------------------------------------------------
! DETERMINE RATE COMMANDED AT THIS POINT (VIA 1-COS FUNC SHAPE)
         ARGDUM = 2.0*PIIALL*RLDUM/DLDUM
         RATDUM = COFDUM*( 1.0 - COS(ARGDUM) )

! APPLY TOSS TETHER MULTIPLICATIVE FACTOR (0.0 DEFAULTS TO 1.0)
         IF(FACDEP(JTETH) .NE. 0.0) RATDUM = FACDEP(JTETH)*RATDUM


! ACCUM CHANGE TO INSTANTANEOUS UNDEFORMED LENGTH
!------------------------------------------------
         DUMINC = RATDUM*QDELT

! DONT DO TETHER LENGTH INTEGRATION UNTIL DYNAMICS ARE INTEGRATING
         IF(JINTEG .EQ. 1) FPLEN(JTETH) = FPLEN(JTETH) + DUMINC

! SET IN INSTANTANEOUS DEPLOY LENGTH AND DERIVATIVES
!---------------------------------------------------
! (DEPLOY LENGTH ACCEL IS ANALYTICALLY DERIVED FROM 1-COS FUNC)
         TLENT(JTETH)  = FPLEN(JTETH)
         TLENTD(JTETH) = RATDUM
         TLENDD(JTETH) = (COFDUM*2.0*PIIALL/DLDUM)*SIN(ARGDUM)


! SET IN COMMANDED PARAMETER VALUE FOR THIS TETHER
         TTNCOM(JTETH) = RATDUM

! END OF SLOPPY NUMERICS TRAP
      END IF


! STANDARD RETURN FOR SCENARIO
1500  CONTINUE

      RETURN
      END
