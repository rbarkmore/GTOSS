! ROUTINE: MMGSEL
! %Z%GTOSS %M% H.5 code v02.00
!              H.5 code v02.00 (baseline for vH.5, simple TSS deployer)
!----------------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!***********************************************************************
!***********************************************************************
      SUBROUTINE MMGSEL
!***********************************************************************
!***********************************************************************
!
!        LATEST VERSION ON: 11/21/90
!                           09:12:56
!**** PURPOSE
!
!     DEPLOYER GUIDANCE SYSTEM SOFTWARE
!     BASED ON THE MARTIN MARIETTA TSS DYNAMICS AND CONTROL DATA
!     FEBRUARY 1988

! COMMON INCLUDE STATEMENTS
!--------------------------

      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_MMDS.h"





! SOFT STOP FLAG (1 = ON, 0 = OFF)
      ISOFT = 0

! FLAG FOR CONSTANT LDOT INITIATION
      IST   = 0

! RESUME DISCRETE (0 = RESUME INACTIVE)
      IR = 0

! current maneuver number (1-16)
      MNVR = 1

! number of maneuvers in TABL (1-16)
      LR = 16

      RETURN
      END
