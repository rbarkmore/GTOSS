! ROUTINE: TOSBD
! %Z%GTOSS %M% H.11 code v02.0
!              H.11 code v02.0  Rework for compatiblity w/New TOSS Obj scheme
!----------------------------------------------------------------------------
!              H.10 code v01.02 Increased No. of Objects to 15.
!--------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************
!*************************************
!
      SUBROUTINE TOSBD(JBODY, JLATE)
!
!*************************************
!*************************************
! THIS ROUTINE FIRST ZEROS THE "D" ARRAY FOR THE SPECIFIED
! BODY, THEN INTERCEPTS THE INPUT FILE DATA STREAM
! TO ALLOW USER INPUT TO THE DTOS2, 3, .... ARRAYS

! THE ARGUMENT IS THE BODY NUMBER "JBODY"


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_TOSS.h"





! PERFORM (AND SET UP) NOBJ VALIDITY CHECKS
      IF( (JBODY .LT. 2) .OR. (JBODY .GT. NOBJ) ) GO TO 400

! THIS IS ARTIFACT CODE, DESIGNED TO ALLOW THE USER TO SET IN
! VALUES TO THE DYNAMIC INTEGER ARRAYS OF THE ORIGINAL TOSS OBJECT
! INPUT DESIGN. THIS IS IGNORED FOR THE TOSS OBJECT POINTER DESIGN

!******************************************
! START A LOOP TO READ IN MANY REAL NUMBERS
!******************************************

! SET IN DUMMY ARRAY LIMIT FOR THIS DUMMY READ ACCESS
      LMIN = 0
      LMAX = 2

! SPECIFY THAT ONE LINE OF TEXT IS TO BE READ (AND DISPLAYED)
      LTEXT = 2

! CALL ROUTINE THAT HANDLES REAL READ-IN
1     CALL TOSRIN (LUI,LUO,LUE,LUC, LMIN,LMAX, LTEXT,ISHOIN,   &
     &             LINDEX,RVALUE)

! SEE IF READ-IN WENT AWRIE
      IF(LINDEX .LT. 0) GO TO 300

! SEE IF ALL REALS ARE IN
      IF(LINDEX .EQ. 0) GO TO 1000

! A SUCCESSFUL READ-IN ,SO STORE IT ACCORDING TO BODY NUMBER
!-----------------------------------------------------------
! NOTE: THIS LINE-READ-IN IS PRESERVED TO MAKE ALL THE HERITAGE
!       INPUT DECKS COMPATIBLE WITH THIS NEW TOSS OBJECT POINTER
!       SCHEME BY READING IN, BUT IGNORING, TRADITIONAL LINES OF DATA

      RDUM = RVALUE

! GO DO ANOTHER REAL READ-IN
      GO TO 1


! STANDARD RETURN POINT
1000  RETURN



!************************
! ERROR REPORTING SECTION
!************************

! IDENTIFY SOURCE OF INPUT FILE ERROR COMMENTS (FROM TOSLIN)
300   WRITE (LUE,301) JBODY
301   FORMAT('=> => => => ABOVE APPLIES TO DATA INPUT FOR JBODY',I2)
! JUST STOP THE SHOW UNTIL DATA IS STRAIGHTENED OUT
      STOP 'IN TOSBD-1'

! APPARENTLY AN UNKNOWN BODY IS SPECIFIED, SO WARN AND STOP
400   WRITE (LUE,401) JBODY
401   FORMAT('BODY NUMBER UNKNOWN TO SUBR TOSBD, #=',I3 )
      STOP 'IN TOSBD-2'

! POSSIBLE NOBJ EXPANSION CODE OMISSION
500   WRITE (LUE,501) JBODY
501   FORMAT('POSSIBLE NOBJ EXPANSION ERROR IN TOSBD, #=',I3 )
      STOP 'IN TOSBD-3'

      END
