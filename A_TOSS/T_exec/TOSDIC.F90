! ROUTINE: TOSDIC
! %Z%GTOSS %M% H.12 code v02.1
!              H.12 code v02.1 Mod to fix the Late starts for TOSS Objects
!----------------------------------------------------------------------------
!              H.11 code v02.0  Rework for compatiblity w/New TOSS Obj scheme
!----------------------------------------------------------------------------
!              H.10 code v01.10  Added envir-call defn of RE, GE, OMEGAE
!-----------------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!*********************************
!*********************************
!
      SUBROUTINE TOSDIC(JLATE)
!
!*********************************
!*********************************
! THIS ROUTINE PERFORMS HIGH LEVEL INVOKEMENT
! OF TOSS DATA INPUT MODULES AND INITIALIZATION


! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_TOSS.h"





      DIMENSION PDUM(3), GDUM(3)

!-------------------------------------------------
! READ IN A LATE START DATA SNAP SHOT IF REQUESTED
!-------------------------------------------------
      IF(JLATE .EQ. 1) CALL XGRAB

!-----------------------------------------------
! FIRST, READ INPUT STREAM FOR GENERAL TOSS DATA
!-----------------------------------------------
      CALL TOSQL
      CALL TOSQJ(JLATE)
      CALL TOSQF
      CALL TOSQD(JLATE)


!----------------------------------------------------------------
! SECOND, READ INPUT STREAM FOR EACH TOSS OBJECT BEING CONSIDERED
!----------------------------------------------------------------
      DO 100 KOBJ = 2,LASOBJ

! ASSOCIATE GLOBAL TOSS OBJECT POINTER WITH SPECIFIED OBJECT
             CALL TOSLDW(KOBJ)

! FIRST ZERO COMMON INPUT ARRAY FOR NEW IC IMAGE
             DO J = 1,NLI_IN
                  L_TOBJ_IN(J) = 0
             END DO

             DO J = 1,NLR_IN
                  R_TOBJ_IN(J) = 0.0
             END DO

! IF IT'S A LATE START, THEN RESTORE OBJ INPUT IMAGE TO UTIL INPUT ARRAYS
             IF(JLATE .EQ. 1) CALL TOSS_OBJ_IN_REST

! READ IN ALL DATA FOR OBJECT
             CALL TOSBL(KOBJ)
             CALL TOSBJ(KOBJ, JLATE)
             CALL TOSBF(KOBJ)
             CALL TOSBD(KOBJ, JLATE)


! SET THE READ-IN DATA INTO THE INSTANTIATED TOSS OBJECT
             CALL TOSS_OBJ_IN

100   CONTINUE


!-------------------------------------------------------------------
! DEFINE BASIC ENVIRONMENTAL ITEMS OTHER'S FOR USE IN INITIALIZATION
!-------------------------------------------------------------------
      IF(JLATE .NE. 1) THEN

! FIND PLANET EQUATORIAL RADIUS (ARBITRARY RADIUS VEC TO FIND GEO-RADIUS)
          PDUM(1) = 100000000.0
          PDUM(2) = 0.0
          PDUM(3) = 0.0

          CALL GEOD (LGLBOP, QTIME, PDUM,    RE, DUMLAT, DUMALT)

! FIND ACCELERATION OF GRAV AT EQUATOR
          PDUM(1) = RE
          PDUM(2) = 0.0
          PDUM(3) = 0.0

          CALL GRAV (LGRVOP, QTIME, PDUM,     GDUM)

          GE = VECMAG(GDUM)

! FIND PLANET ROTATION RATE
          CALL SIDEREAL (LGRVOP,     OMEGAE)

      END IF


!-------------------------------------------------------------
! DO TOSS INITIALIZATION FOR EACH TOSS OBJECT BEING CONSIDERED
!-------------------------------------------------------------
          DO 200 KOBJ = 2,LASOBJ
                 CALL TOSBV(KOBJ, JLATE)
200       CONTINUE

!-------------------------------
! DO GENERAL TOSS INITIALIZATION
!-------------------------------
      CALL TOSQV(JLATE)

      RETURN
      END
