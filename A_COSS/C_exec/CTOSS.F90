! ROUTINE: CTOSS
! %Z%GTOSS %M% H.10 code v01.06
!              H.10 code v01.06 Changed vers number ennunciator to H.10
!----------------------------------------------------------------------
!              H.9 code v01.05 Changed version number ennunciator to H.9
!                              Post processors all moved to v9 because
!                              number of allowed finite solns increased
!-----------------------------------------------------------------------
!              H.7 code v01.04 Changed version number ennunciator to H.7
!-----------------------------------------------------------------------
!              H.6 code v01.03 Changed version number ennunciator to H.6
!-----------------------------------------------------------------------
!              H.5 code v01.02 Changed version number ennunciator to H.5
!-----------------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! MAIN PROGRAM DEFN FOR CYBER
!@NOS      PROGRAM CTOSS
      PROGRAM CTOSS

!*********************************************
!*********************************************
! THIS FILE CONTAINS THE HIERARCHICAL (BUT NOT
! FUNCTIONAL) CTOSS MAIN PROGRAM
!*********************************************
!*********************************************

! DECLARE COMMON TO INSURE PERMANENT FORTRAN 77 DYN DATA ALLOC

      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_HOST.h"
      include "../../A_HDR/COM_BOOM.h"
      include "../../A_HDR/COM_COSS.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_TOSS.h"
!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
      include "../../A_HDR/COM_ROSS.h"





! IDENTIFY THE VERSION
      CTSID = '[CTOSS: VERSION H.11 F90 Ptrs/more Entities   Mar 2007]'


! INVOKE THE FUNCTIONAL CTOSS MAIN PROGRAM
      CALL CTOSUB

      END
