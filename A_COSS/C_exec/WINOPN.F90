! ROUTINE: WINOPN
! %Z%GTOSS %M% H.10 code v01.1
!              H.10 code v01.1 Modified Path scheme to allow non-Mac apl
!-----------------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!***************************
!***************************
!
      SUBROUTINE WINOPN
!
!***************************
!***************************
! THIS ROUTINE OPENS THE COLUMN DELIMITED OUTPUT FILES


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_COSS.h"
      include "../../A_HDR/COM_ROSS.h"




! STORAGE FOR THE DATE CHAR STRING
      CHARACTER *8 MMDDYY

! STORAGE FOR MAIN-FRAME FILE NAME OPTION
      CHARACTER *7 C7NAME

!     pause 'enter WINOPN'


! EXTRACT PART OF STANDARD RTOSS DATE (COULD BE USED IN FILE NAME)
!  (MMDDYY CURRENTLY USED IN CTOSS, NEEDS TO SEE COM_ROSS IF USED)
!-----------------------------------------------------------------
      MMDDYY = CDATEC(1:2) //'/'// CDATEC(4:5) //'/'// CDATEC(9:10)


!*********************************************************
! FOR EACH COLUMN DELIMITED OUTPUT FILE REQUESTED THIS RUN
!*********************************************************
      DO 100 JWIN = 1,LIMPDS

! VISIT PROGRAM TO SET UP OUTPUT-SPECIFIC CONTROL, ID STRINGS, ETC
!-----------------------------------------------------------------
               CALL WINSET(JWIN)

! IF THIS OUTPUT FORMAT SELECTOR IS NOT ACTIVATED, TRY NEXT
!----------------------------------------------------------
               IF(NCHUZ .EQ. 0) GO TO 100


!----------------------------
! FORM NAMES FOR OUTPUT FILES
!----------------------------
! CREATE FORMAT-SPECIFIC OBJECT-TETHER UNIQUENESS STRING
               WRITE(IFCOL5,111) NOBJZ, NTTSHO
111            FORMAT('J',I1,'T',I2.2)


! CREATE FORMAT-SPECIFIC GENERALIZED UNIQUENESS STRING
               WRITE(IFCOL11,222) NFTSHO, NTPEUL, NTPLIB,   &
     &                            NINT(PTOBJ(1)),  NINT(PNBSHO(1)),   &
     &                            NINT(PNSEGO(1)), NINT(PTSHOW(1))
222            FORMAT('x', 4I1, 3I2.2)


! DEFINE 31 CHAR FORM OF THE NAME FOR COLUMN DELIMITED OUTPUT FILE
! EXAMPLE,  'TR1101_E008_J3T00_xSULOBBGGTT'
!-----------------------------------------------------------------
               CKFNAM = RDBNAM//RDBNUM//'_'//   &
     &                  FMUID4//'_'//IFCOL5//'_'//IFCOL11

! debug for path names, etc
!         pause 'b4 RDBNAM print'
!          call seetxt(RDBNAM)
!          call seetxt(RDBNUM)
!          call seetxt(CKFNAM)
!         pause 'after RDBNAM print'

!-----------------------------------------------------------
! CREATE COLUMN DELIMITED OUTPUT FILE-STYLE VIA USER REQUEST
!-----------------------------------------------------------
         IF(NAME7 .EQ. 1) GO TO 50

!              pause 'in long name logic'

! CREATE COLUMN DELIMITED OUTPUT FILE WITH 31 CHARACTER NAMES

! MAKE PATH TO THE OUTPUT FILE (USING A 31 CHAR NAME)
!----------------------------------------------------
      CALL MACPTH (AXROOT, CKFNAM//'  ',   MOPATH, IERDUM)

!                 call seetxt(MOPATH)
!                 pause 'return from MACPTH'

! CHECK SUCCESS OF ACQUIRING FILE PATH NAME
!------------------------------------------
      IF(IERDUM .EQ. 1) THEN

          WRITE(IUERR,56)
 56       FORMAT(' FATAL: MAC PATH NAME EXCEEDS 64 CHARS (OUTPUT ',   &
     &           'FILE ROOT NAME PROBABLY TOO LONG)' )

          STOP 'WINOPN'

      END IF

!           pause 'made it past IUERR logic'

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! NOW OPEN A COLUMN DELIMITED OUTPUT FILE WITH MACINTOSH NAME LENGTH
!-------------------------------------------------------------------
!@NOS            STOP 'WINOPN-2, LONG FILES NOT ALLOWED ON NOS'
            OPEN(IUOUT, FILE = MOPATH,  STATUS='UNKNOWN')
!@VAX            OPEN(IUOUT, FILE = MOPATH,  STATUS='UNKNOWN')
!@DOS            OPEN(IUOUT, FILE = MOPATH,  STATUS='UNKNOWN')
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
!@MAC            OPEN(IUOUT, FILE = MOPATH,  STATUS='NEW')
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@

            REWIND(IUOUT)

!               pause 'going to statement 90'

            GO TO 90




! CREATE COLUMN DELIMITED OUTPUT FILE FOR SYSTEMS WITH 7 CHAR FILE NAMES
!-----------------------------------------------------------------------
! NOTE: THIS OPTION PROVIDES A 7 CHAR FILE NAMING CONVENTION AND INSERTS
! THE 'REAL DESCRIPTIVE NAME' OF THE FILE AS A HEADER IN THE FILE ITSELF
! FOR LATER USE BY A CONVERSION PROGRAM OR TEXT-POST-PROCESSING BY USER

50          CONTINUE

! BUMP TOTAL OUTPUT FILE GENERATION COUNT FOR THIS CTOSS SESSION
            JRUNS = JRUNS + 1

! PROTECT AGAINST EXCESSIVE FILE COUNT
            IF(JRUNS .GT. 99) CALL CLEANO(-1)

! FORM THE MAIN-FRAME VERSION OF THE CREATION FILE NAME
            WRITE(C7NAME,122) JRUNS
122         FORMAT('XCRIK',I2.2)

! MAKE A FACILTY-DEPENDENT PATH TO THE OUTPUT FILE
!-------------------------------------------------
            CALL GETPTH (AXROOT, C7NAME,   AXPATH)

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! PURGE CYBER PERMANENT FILE BEFORE DEFINE
!@NOS            CALL PF('PURGE',  C7NAME,         'NA', 'DUMB')
!@NOS            CALL PF('DEFINE', C7NAME, C7NAME, 'NA', 'DUMB')

! NOW, CREATE COLUMN DELIMITED OUTPUT FILE
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! OPEN STATMENT MUST USE RECL=600 FOR CYBER
!@NOS            OPEN(IUOUT, FILE = C7NAME, STATUS='NEW', RECL=600)
            OPEN(IUOUT, FILE = AXPATH, STATUS='UNKNOWN')
!@VAX            OPEN(IUOUT, FILE = AXPATH, STATUS='UNKNOWN')
!@DOS            OPEN(IUOUT, FILE = AXPATH, STATUS='UNKNOWN')
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
!@MAC            OPEN(IUOUT, FILE = AXPATH, STATUS='NEW')
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@

            REWIND(IUOUT)

! IF USER DOESNT INHIBIT, HEAD FILE WITH $'REAL-FILE-NAME' LINE
!--------------------------------------------------------------
            IF(DOLLAR .NE. -1.0) THEN
               WRITE(IUOUT,114) '$', CKFNAM
114            FORMAT(A1,A30)
            END IF

!---------------------------------------------------------
! WRITE A SPECIAL CHARACTER TO OUTPUT FILE IF USER DESIRES
!---------------------------------------------------------
90    CONTINUE


!         pause 'b4 write at 90'


! SEE IF USER DESIRES SPECIAL CHARACTER
      IF(PNOAST .EQ. 1.0) THEN

! WRITE ASTERISK TO HEADER
           WRITE(IUOUT,91) AST
91         FORMAT(A1)

       END IF

!         pause 'after write at 90'

!*********************************
! END OF OUTPUT FILE CREATION LOOP
!*********************************
100   CONTINUE

!        pause 'leaving WINOPN'

      RETURN
      END
