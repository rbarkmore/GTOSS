! ROUTINE: YFM042
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM042 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! <BEAD MODEL TETHER>  BEAD TETHER SEGMENT LENGTH
!                      (AT SPECIFIED SEGMENTS): SOLN #


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_COSS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"




!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! DEFINE COLUMN HEADINGS
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE COLUMN DATA
      IF(JFUNC .EQ. 2) GO TO 2000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM042-0'


!******************************************
!******************************************
!     WRITE COLUMN HEADER TO PLOT FILE
!******************************************
!******************************************
1000  CONTINUE


! CHOOSE STANDARD OR TAG FORMAT
!------------------------------
      IF(TAGFLG .EQ. 1.0) THEN

! DEFINE SPECIAL TIME HEADER
      WRITE(COLIF,1001) TIMID6, NINT(PNSEGO(1)), NFTSHO, TAG4
1001  FORMAT(A6, '_',I2.2,I1, '_',A4)

      ECOLHD( 1) = COLIF
      MCOLHD( 1) = COLIF

! PUT HEADINGS ON COLUMNS FOR LEGITMATE BEAD NUMBERS
!---------------------------------------------------
      JDUM = 0
      DO 1011 J = 1,11
         JSGDUM = PNSEGO(J)
         IF((JSGDUM.LT.1).OR.(JSGDUM.GT.(NBEADS(NFTSHO)+1))) GO TO 1011

! CONCATENATE BEAD NUMBER AND FINITE SOLN
           WRITE(IFCOL11,1002) JSGDUM, NFTSHO, TAG4
1002       FORMAT('S',I3.3,'_',I1, '_',A4)

           ECOLHD(JDUM + 2) = 'L_F_'//IFCOL11
           MCOLHD(JDUM + 2) = 'L_M_'//IFCOL11

           JDUM = JDUM + 1

1011  CONTINUE

      ELSE


! DEFINE SPECIAL TIME HEADER
!---------------------------
      WRITE(COLIF,1003) TIMID6, NINT(PNSEGO(1)), FSNID4
1003  FORMAT(A6,'_GX',I2.2, A4)

      ECOLHD( 1) = COLIF
      MCOLHD( 1) = COLIF

! PUT HEADINGS ON COLUMNS FOR LEGITMATE BEAD NUMBERS
!---------------------------------------------------
      JDUM = 0
      DO 1022 J = 1,11
         JSGDUM = PNSEGO(J)
         IF((JSGDUM.LT.1).OR.(JSGDUM.GT.(NBEADS(NFTSHO)+1))) GO TO 1022

! CONCATENATE BEAD NUMBER AND FINITE SOLN
           WRITE(IFCOL9,1004) JSGDUM, FSNID4
1004       FORMAT('SG',I3.3, A4)

           ECOLHD(JDUM + 2) = '  '//'L_F_'//IFCOL9
           MCOLHD(JDUM + 2) = '  '//'L_M_'//IFCOL9

           JDUM = JDUM + 1

1022  CONTINUE

      END IF


! DEFINE NUMBER OF COLUMNS BEING DEFINED
      NUMCOL = JDUM + 1

      RETURN



!******************************************
!******************************************
!     CALCULATE DATA FOR PLOT FILE
!******************************************
!******************************************
2000  CONTINUE

! IF NON-VALID FINIT SOLN # IS SPEC FOR THIS PAGE, GET OUT
      IF( (NFTSHO.LT.1) .OR. (NFTSHO.GT.NFINIT) ) GO TO 100

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
      CALL TISLDW (NFTSHO)

! SET IN SIMULATION TIME FOR THIS TETHER
      OUTP( 1) = TISTIM

! START A LOOP TO EXTRACT LEGITIMATE SEGMENT TENSIONS
      JOPDUM = 0
      DO 20 J = 1,11

! DETERMINE WHICH SEGMENT TO DISPLAY
         JSEG = NINT(PNSEGO(J))

! EXTRACT SEGMENT LENGTHS
         IF( (JSEG.GE.1) .AND. (JSEG.LE.(NBEAD+1)) ) THEN
             JOPDUM = JOPDUM + 1
             OUTP(JOPDUM+1) = XXLNS * ELBSG(JSEG)
         END IF

20    CONTINUE

100   CONTINUE

      RETURN
      END
