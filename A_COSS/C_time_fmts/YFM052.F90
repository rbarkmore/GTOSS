! ROUTINE: YFM052
! %Z%GTOSS %M% H.9 code v01.00 (baseline for vH.9 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM052 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! <BEAD MODEL TETHER>  BEAD GENERALIZED BARE-WIRE OPS
!                      PARAMETERS:  SOLN #


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_COSS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"





!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! DEFINE COLUMN HEADINGS
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE COLUMN DATA
      IF(JFUNC .EQ. 2) GO TO 2000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM052-0'


!******************************************
!******************************************
!     WRITE COLUMN HEADER TO PLOT FILE
!******************************************
!******************************************
1000  CONTINUE


! CHOOSE STANDARD OR TAG FORMAT
!------------------------------
      IF(TAGFLG .EQ. 1.0) THEN

! DEFINE SPECIAL TIME HEADER
      WRITE(COLIF,1001) TIMID6, FSNID4, TAG4
1001  FORMAT(A6, A4, '_', A4)


! SET IN ENGLISH UNITS COLUMN HEADERS
      ECOLHD( 1) = COLIF

      ECOLHD( 2) = 'CUR_INTGRL_'//TAG4
      ECOLHD( 3) = 'ETERS_OHMS_'//TAG4
      ECOLHD( 4) = 'BAT_INT_HR_'//TAG4
      ECOLHD( 5) = 'EM_END_CUR_'//TAG4
      ECOLHD( 6) = 'NOT_IN_USE_'//TAG4
      ECOLHD( 7) = 'ELEC_DENS__'//TAG4
      ECOLHD( 8) = 'TETH_VOLTS_'//TAG4
      ECOLHD( 9) = 'AMP_LN_AVG_'//TAG4
      ECOLHD(10) = 'RP_MAGFLDX_'//TAG4
      ECOLHD(11) = 'RP_MAGFLDY_'//TAG4
      ECOLHD(12) = 'RP_MAGFLDZ_'//TAG4


! SET IN METRIC UNITS COLUMN HEADERS
      MCOLHD( 1) = COLIF

      MCOLHD( 2) = 'CUR_INTGRL_'//TAG4
      MCOLHD( 3) = 'ETERS_OHMS_'//TAG4
      MCOLHD( 4) = 'BAT_INT_HR_'//TAG4
      MCOLHD( 5) = 'EM_END_CUR_'//TAG4
      MCOLHD( 6) = 'NOT_IN_USE_'//TAG4
      MCOLHD( 7) = 'ELEC_DENS__'//TAG4
      MCOLHD( 8) = 'TETH_VOLTS_'//TAG4
      MCOLHD( 9) = 'AMP_LN_AVG_'//TAG4
      MCOLHD(10) = 'RP_MAGFLDX_'//TAG4
      MCOLHD(11) = 'RP_MAGFLDY_'//TAG4
      MCOLHD(12) = 'RP_MAGFLDZ_'//TAG4

      ELSE

! DEFINE SPECIAL TIME HEADER
!---------------------------
      WRITE(COLIF,1002) TIMID6, FSNID4
1002  FORMAT(A6, A4, '     ')


! SET IN ENGLISH UNITS COLUMN HEADERS
      ECOLHD( 1) = COLIF

      ECOLHD( 2) = 'CUR_INTGRL_'//FSNID4
      ECOLHD( 3) = 'ETERS_OHMS_'//FSNID4
      ECOLHD( 4) = 'BAT_INT_HR_'//FSNID4
      ECOLHD( 5) = 'EM_END_CUR_'//FSNID4
      ECOLHD( 6) = 'NOT_IN_USE_'//FSNID4
      ECOLHD( 7) = 'ELEC_DENS__'//FSNID4
      ECOLHD( 8) = 'TETH_VOLTS_'//FSNID4
      ECOLHD( 9) = 'AMP_LN_AVG_'//FSNID4
      ECOLHD(10) = 'RP_MAGFLDX_'//FSNID4
      ECOLHD(11) = 'RP_MAGFLDY_'//FSNID4
      ECOLHD(12) = 'RP_MAGFLDZ_'//FSNID4


! SET IN METRIC UNITS COLUMN HEADERS
      MCOLHD( 1) = COLIF

      MCOLHD( 2) = 'CUR_INTGRL_'//FSNID4
      MCOLHD( 3) = 'ETERS_OHMS_'//FSNID4
      MCOLHD( 4) = 'BAT_INT_HR_'//FSNID4
      MCOLHD( 5) = 'EM_END_CUR_'//FSNID4
      MCOLHD( 6) = 'NOT_IN_USE_'//FSNID4
      MCOLHD( 7) = 'ELEC_DENS__'//FSNID4
      MCOLHD( 8) = 'TETH_VOLTS_'//FSNID4
      MCOLHD( 9) = 'AMP_LN_AVG_'//FSNID4
      MCOLHD(10) = 'RP_MAGFLDX_'//FSNID4
      MCOLHD(11) = 'RP_MAGFLDY_'//FSNID4
      MCOLHD(12) = 'RP_MAGFLDZ_'//FSNID4

      END IF

! DEFINE NUMBER OF COLUMNS BEING DEFINED
      NUMCOL = 12
      RETURN



!******************************************
!******************************************
!     CALCULATE DATA FOR PLOT FILE
!******************************************
!******************************************
2000  CONTINUE

! IF NON-VALID FINIT SOLN # IS SPEC FOR THIS PAGE, GET OUT
      IF( (NFTSHO.LT.1) .OR. (NFTSHO.GT.NFINIT) ) GO TO 100

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
      CALL TISLDW(NFTSHO)

! SET IN SIMULATION TIME FOR THIS TETHER
      OUTP(1) = TISTIM

! THEN BARE WIRE OPERATIONS DATA
      unused = 0.0
      OUTP(2) = VOLTS_OUT
      OUTP(3) = TOTRIS
      OUTP(4) = POWER_OUT
      OUTP(5) = EMICUR
      OUTP(6) = unused
      OUTP(7) = EDENS(1)
      OUTP(8) = EMFTOT

! CALC LENGTH-AVERAGE OF CURRENT
      DUMAVG = 0.0
      DO 111 JSEG = 1, NBEAD+1
             DUMAVG = DUMAVG + CURRT + CURRTS(JSEG)
111   CONTINUE

      OUTP(9) = DUMAVG/REAL(NBEAD+1)

! DISPLAY PLANET MAGNETIC FIELD AT RP IN PLANET FRAME
      OUTP(10) = RPGAU(1)
      OUTP(11) = RPGAU(2)
      OUTP(12) = RPGAU(3)

100   CONTINUE

      RETURN
      END

