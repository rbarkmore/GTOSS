! ROUTINE: YFM031
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM031 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! <BEAD MODEL TETHER>  BEAD POSITION IN REF END ORB FRAME
!                     (Y-COMP AT SPECIFIED BEADS):  SOLN #


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_COSS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"




!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! DEFINE COLUMN HEADINGS
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE COLUMN DATA
      IF(JFUNC .EQ. 2) GO TO 2000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM031-0'


!******************************************
!******************************************
!     WRITE COLUMN HEADER TO PLOT FILE
!******************************************
!******************************************
1000  CONTINUE

! CHOOSE STANDARD OR TAG FORMAT
!------------------------------
      IF(TAGFLG .EQ. 1.0) THEN

! DEFINE SPECIAL TIME HEADER
      WRITE(COLIF,1001) TIMID6, NFTSHO, NINT(PNBSHO(1)), TAG4
1001  FORMAT(A6, '_', I1, I2.2, '_', A4)

      ECOLHD( 1) = COLIF
      MCOLHD( 1) = COLIF

! PUT HEADINGS ON COLUMNS FOR LEGITMATE BEAD NUMBERS
      JDUM = 0
      DO 1011 J = 1,11
         JBBDUM = PNBSHO(J)
         IF((JBBDUM.LT.1) .OR. (JBBDUM.GT.NBEADS(NFTSHO)) ) GO TO 1011

! CHARACTERIZE BEAD NUMBER
           WRITE(IFCOL9,1002) JBBDUM, TAG4
1002       FORMAT('B',I3.3, '_', A4)

           ECOLHD(JDUM + 2) = 'YOR_F_'//IFCOL9
           MCOLHD(JDUM + 2) = 'YOR_M_'//IFCOL9

           JDUM = JDUM + 1

1011  CONTINUE

      ELSE


! DEFINE SPECIAL TIME HEADER
!---------------------------
      WRITE(COLIF,1003) TIMID6, FSNID4, NINT(PNBSHO(1))
1003  FORMAT(A6, A4,'_BX',I2.2)

      ECOLHD( 1) = COLIF
      MCOLHD( 1) = COLIF

! PUT HEADINGS ON COLUMNS FOR LEGITMATE BEAD NUMBERS
!---------------------------------------------------
      JDUM = 0
      DO 1022 J = 1,11
         JBBDUM = PNBSHO(J)
         IF((JBBDUM.LT.1) .OR. (JBBDUM.GT.NBEADS(NFTSHO)) ) GO TO 1022

! CHARACTERIZE BEAD NUMBER
           WRITE(BBNID4,1004) JBBDUM
1004       FORMAT('B',I3.3)

! CONCATENATE BEAD NUMBER AND FINITE SOLN
           IFCOL8 = BBNID4//FSNID4

           ECOLHD(JDUM + 2) = 'Y_ORB_F_'//IFCOL8
           MCOLHD(JDUM + 2) = 'Y_ORB_M_'//IFCOL8

           JDUM = JDUM + 1

1022  CONTINUE

      END IF

! DEFINE NUMBER OF COLUMNS BEING DEFINED
      NUMCOL = JDUM + 1

      RETURN


!******************************************
!******************************************
!     CALCULATE DATA FOR PLOT FILE
!******************************************
!******************************************
2000  CONTINUE

! MAKE CALL TO GET ORB FRAME POS COMP OF SELECTED BEADS
      CALL YPDFBB(2)

      RETURN
      END
