! ROUTINE: CHAINALS
! %Z%GTOSS %M% H.10 code v01.01
!              H.10 code v01.01 Fixed bug in 2-Object chain detection
!                               Fixed strain calc to reflect thermal option
!--------------------------------------------------------------------------
!              H.10 code v01.00 (baseline for vH.10 delivery)
!**********************************************************************
!**********************************************************************
!**********************************************************************
        SUBROUTINE CHAINALS (JOBDUM,  JCNTDUM, DLDUM, DATDUM)
!**********************************************************************
!**********************************************************************
!**********************************************************************
! THIS ROUTINE IS CALLED WITH AN ARGUMENT THAT IS A TOSS OBJECT NUMBER.
! IT WILL RETURN INFORMATION ABOUT THE TETHER-CHAIN THAT CONTAINS THAT
! TOSS OBJECT. IF NO CHAIN CONTAINS THE CANDIDATE TOSS OBJECT, THEN
! THE ELEMENT-COUNT-VALUE OF THE RETURNED RESULTS ARRAYS IS NEGATIVE.
!
! SPECIFICALLY, THE FOLLOWING ARGUMENTS ARE PASSED:
!
! INPUT
!------
!
!      JOBDUM      A TOSS OBJECT NUMBER (FOR WHICH CHAIN DATA IS SOUGHT)
!
!
! OUTPUT (FOR THAT CHAIN CONTAINING TOSS OBJECT "JOBDUM"):
!-------
!      JCNTDUM     THE NUMBER OF DATA ITEMS RETURNED IN EACH OUTPUT ARRAY
!                  NOTE: THIS MEANS ALTDUM(JCNTDUM), AND DATDUM(JCNTDUM,4)
!                  NOTE: IF NO CHAIN CONTAINS THE SPECIFIED TOSS OBJECT, THIS
!                        IS RETURNED AS -1
!
!      DLDUM       THE ARRAY CONTAINING THE DISTANCES (STARTING FROM THE END OF
!                  THE CHAIN NEAREST THE PLANETS SURFACE) CORRESPONDING TO EACH
!                  OF THE RETURNED DATA PARAMETERS IN DATDUM (THIS WOULD BE THE
!                  "X-AXIS" REQUIRED TO PLOT THE SHAPE DATA).
!
!                  NOTE: DLDUM IS "PARAMETRICALLY DIMENSIONED" VIA THE CTOSS
!                        PARAMETER "NCOSSEG", WHICH RENDERS ARRAY SIZES SELF-
!                        MAINTAINING RELATIVE TO THE  TOSS MAXBBS PARAMETER.
!                        CTOSS ROUTINES CALLING THIS PROGRAM MUST ALSO
!                        DIMENSION THEIR RECEIVING ARRAY IDENTICALLY.
!
!      DATDUM      THE ARRAY CONTAINING THESE PARAMETERS FOR THE CHAIN
!
!                      DATDUM(NCOSSEG,1): SEG TENSIONS (LBF)    [TENSEG(JS)]
!                      DATDUM(NCOSSEG,2): SEG ELAS AREA (FT**2) [BEADELA(JS)]
!                      DATDUM(NCOSSEG,3): SEG STRESS (PSF)      [TENSEG(JS)/BEADELA(JS)]
!                      DATDUM(NCOSSEG,4): SEG STRAIN (ND)       [CALCULATED]
!
!                  NOTE: DATDUM IS VARIABLY DIMENSIONED SIMILAR TO DLDUM ABOVE
!
!
! NOTES ON IMPLEMENTED DISPLAY CONVENTIONS:
!
! 1. THE "SIZE-EXTENT" OF TOSS OBJECTS IS ASSUMED INSIGNIFICANT RELATIVE TO
!    THE DIMENSIONAL EXTENT OF TETHERS, AND ARE THUS IGNORED IN DEFINING THE
!    "X-AXIS" CORRESPONDING TO THE SHAPE DATA. THUS, EVEN THOUGH THE ATTACH
!    POINTS ON A TOSS OBJECT CAN HAVE PHYSICAL SEPARATION, IT IS NOT PORTRAYED.
!
! 2. THE REFERENCE (OR ZERO VALUE) FOR THE "X-AXIS" CORRESPONDS TO THE POINT
!    ON THE CHAIN NEAREST THE PLANET SURFACE (LOWEST ALTITUDE).
!
! 3. THE X-AXIS VALUES CORRESPOND TO NODE POSITIONS; FOR INNER NODES, DISPLAY
!    ATTRIBUTES CORRESPOND TO THE ATTRIBUTE'S AVERAGE OF THE TWO ADJACENT
!    SEGMENTS. THE "END VALUES" SIMPLY REFLECT THEIR ASSOCIATED SEGMENT.
!
! 4. FOR THE BENEFIT OF PLOTTING PROGRAMS THAT WILL BE USED TO GRAPH RESULTS,
!    X-AXIS VALUES ARE CHOSEN AT THE INNER POINTS OF A CHAIN (WHERE 2 TETHER
!    "TOUCH") SUCH THAT DISCONTINUITIES IN PARAMETERS CAN BE DEPICTED ACROSS
!    THE "MASS" OF  OBJECTS INTERPOSED BETWEEN THE TWO TETHERS. THIS IS DONE
!    BY PRESENTING ADJACENT VALUES AT TWO POINTS SEPARATED BY A SMALL VALUE
!    IN THE X-AXIS.
!
! 5. THE X-AXIS REPRESENTS EFFECTIVELY "ARC LENGTH" ALONG THE TETHERS.
!
! 6. IF A TETHER IN THE CHAIN IS A MASSLESS TETHER, THEN FOR ANY PAREMETER (THAT
!    CAN HAVE MEANING), TWO IDENTICAL VALUES ARE DISPLAYED AND GIVEN AN ASSOCIATION
!    WITH LENGTH POSITIONS CORRESPONDING TO BOTH ENDS OF THE MASSLESS TETHER, THUS
!    YIELDING A "FLAT VALUE". PARAMETERS WITH NO MEANING WILL BE RETURNED AS "ZERO".


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS


! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_HOST.h"
      include "../../A_HDR/EQU_HOST.h"
      include "../../A_HDR/COM_COSS.h"
      include "../../A_HDR/COM_ROSS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"

!      include "../../A_HDR/EQU_OBJ.i"  (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.h"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.h"



!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"





! ARGUMENT DATA STORAGE (SELF MAINTAINS WR/T MAXBBS, VIA COM_FOSS PARAM)
      DIMENSION DLDUM(NCOSSEG), DATDUM(NCOSSEG,4)

! ARRAYS DEFINING TOSS TETHER AND OBJECT SEQUENCES IN THE CHAIN
      DIMENSION JTORDER(3)

! ARRAY DEFINING SEGMENT STRAINS
      DIMENSION STRNDUM(MAXSEG)


!*************************************************************
!*************************************************************
!    DETERMINE THE DISPLAY TOPOLOGY OF THE SPECIFIED CHAIN
!*************************************************************
!*************************************************************
!
! CHAINFOS USES THESE PARAMETERS TO DEFINE THE CHAIN TOPOLOGY
!
!    JCHNOBZ = CHAIN NUMBER THAT CONTAINS THE SPECIFIED TOSS OBJECT
!
!    LSEGCNT = NUMBER OF SEGMENTS PRESENT IN THIS CHAIN
!
!    JOBLOW  = THE TOSS OBJECT NUMBER NEAREST PLANET SURFACE
!
!    JSEGLOW = SEGMENT NO. CONTAINING TOSS OBJECT NEAREST PLANET SURFACE
!
!    JTORDER(3)  1ST ELEMENT IS TOSS TETH NUMBER IN JSEGLOW
!                2ND ELEMENT IS TOSS TETH NUMBER IN JSEGLOW + 1
!                3RD ELEMENT IS TOSS TETH NUMBER IN JSEGLOW + 2
!
!          NOTE: A POS VALUE MEANS TETHER'S X-END IS NEAREST THE PLANET
!                A NEG VALUE MEANS TETHER'S Y-END IS NEAREST THE PLANET


!--------------------------------------------------------------------
! NOW, TRY TO FIND THAT CHAIN CONTAINING THE INPUT-SPECIFIED TOSS OBJ
!--------------------------------------------------------------------
       DO 22 JCH = 1, 4
             JCHNOBZ = JCH

             DO 11 JSG = 1,3
               IF(ABS(NINT(CHAIN(JCH,JSG, 2))) .EQ. JOBDUM ) GO TO 1200
               IF(ABS(NINT(CHAIN(JCH,JSG, 3))) .EQ. JOBDUM ) GO TO 1200
11           CONTINUE

22     CONTINUE

! FALLING THRU LOOP ONLY HAPPENS IF CHAIN SEARCH WAS UNSUCCESSFUL
!----------------------------------------------------------------
! SET FLAG TO SO INDICATE AND GET OUT
       JCNTDUM = -1

       GO TO 4000


!---------------------------------------------------------------------
! NOW, FIND OBJECT NEAREST PLANET SURFACE (IE. HAVING LOWEST ALTITUDE)
!---------------------------------------------------------------------
1200   CONTINUE

! INITIALIZE "SEGMENTS PRESENT" COUNTER
       LSEGCNT = 0

! FOR CHOSEN CHAIN, CHECK OTHER 2 SEGS LOOKING FOR OBJ NEAREST PLANET
       DO 44 JSG = 1,3

! SKIP THIS SEGMENT IF IT DOESN'T HAVE A TETHER ASSOCIATED WITH IT
             IF(CHAIN(JCHNOBZ, JSG, 1) .EQ. 0.0) GO TO 44

! TALLY A "PRESENT" SEGMENT
             LSEGCNT = LSEGCNT + 1

! GET ALTITUDES OF OBJECTS BOUNDING SEGMENT 1
!--------------------------------------------
             JOB2 = ABS(NINT(CHAIN(JCHNOBZ, JSG, 2)))
             JOB3 = ABS(NINT(CHAIN(JCHNOBZ, JSG, 3)))

! GET ALT FOR TOSS OBJECT OR REF PT OBJECT (ASSUME REF PT FIRST)
             ALTDUM2 = RPALT
             IF(JOB2 .NE. 1) THEN
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
                 CALL TOSLDW(JOB2)
                 ALTDUM2 = ALT
             END IF

             ALTDUM3 = RPALT
             IF(JOB3 .NE. 1) THEN
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
                 CALL TOSLDW(JOB3)
                 ALTDUM3 = ALT
             END IF


! ON 1ST PASS, SEE WHICH END OF THIS SEGMENT IS THE LOWER OBJECT
!---------------------------------------------------------------
             IF(JSG .EQ. 1) THEN
                JOBLOW  = JOB2
                ALTPDUM = ALTDUM2

                IF(ALTDUM3 .LT. ALTDUM2) THEN
                   JOBLOW  = JOB3
                   ALTPDUM = ALTDUM3
                END IF

! FOR NOW LET THIS SEGMENT CLAIM THE LOWEST OBJECT
                JSEGLOW = JSG

             ELSE

! ON OTHER PASSES, SEE IF EITHER END OF THIS SEG IS LOWER THAN OTHER SEGS
                IF(ALTDUM2 .LT. ALTPDUM) THEN
                   JOBLOW  = JOB2
                   ALTPDUM = ALTDUM2
                   JSEGLOW = JSG
                END IF

                IF(ALTDUM3 .LT. ALTPDUM) THEN
                   JOBLOW  = JOB3
                   ALTPDUM = ALTDUM3
                   JSEGLOW = JSG
                END IF

             END IF

44     CONTINUE



!-----------------------------------------------------------------
! FINALLY, FORM SEQUENCE OF TOSS TETHER NUMBERS CONSTITUTING CHAIN
!-----------------------------------------------------------------
! NOTE: ELEMENT-1 IS THE STARTING TETHER (IE. ONE NEAREST PLANET SURFACE)

! INITIALIZE, FETCH RAW TOSS TETHER NO'S, AND FIGURE OUT WHICH END IS UP
!-----------------------------------------------------------------------
       JTORDER(1) = 0
       JTORDER(2) = 0
       JTORDER(3) = 0

! SEE WHETHER LOWEST SEG IS ORIGIN END OF CHAIN, AND PROCEED APPROPRIATELY
       IF(JSEGLOW .EQ. 1) THEN

! SORT OUT TETHER ORIENTATION PROGRESSING UP FROM ORIGIN-END OF CHAIN
           DO 66 JS = 1,LSEGCNT
              JTORDER(JS) = NINT(CHAIN(JCHNOBZ, JS, 1))
              IF(CHAIN(JCHNOBZ,JS,2) .LT. 0.) JTORDER(JS)=-JTORDER(JS)
66         CONTINUE

       ELSE

! SORT OUT TETHER ORIENTATION PROGRESSING UP FROM OTHER-END OF CHAIN
           DO 55 JS = LSEGCNT,1,-1
              JTORDER(JS) = NINT(CHAIN(JCHNOBZ, JS, 1))
              IF(CHAIN(JCHNOBZ,JS,2) .GT. 0.) JTORDER(JS)=-JTORDER(JS)
55         CONTINUE

       END IF


!==========================================
! debug code to confirm chain topology
!       call seeint('JCHNOBZ',JCHNOBZ)
!       call seeint('LSEGCNT',LSEGCNT)
!       call seeint('JOBLOW',JOBLOW)
!       call seeint('JSEGLOW',JSEGLOW)
!       call seeint('JTORDER(1)',JTORDER(1))
!       call seeint('JTORDER(2)',JTORDER(2))
!       call seeint('JTORDER(3)',JTORDER(3))
!
!       pause 'CHAIN topology by CHAINFOS'
!===========================================


! ELBSG(JS)  = SEGMENT LENGTH (MUST HAVE INGOSS AUX RDB DATA TURNED ON FOR FINITE SOLNS)
! SEGLA(JS)  = DEPLOYED, UNDEFORMED LENGTH OF TETH IN A SEGMENT (REFLECTS THERMAL)
! ELSGX      = X-AXIS UNIFORM SEG LENGTH BASED ON DIST BETWEEN ATT PTS
! EL         = INSTANTANEOUS DEPLOYED (UNDEFORMED) TETH LENGTH - WHOLE TETHER
! ELB        = DEPLOYED (UNDEFORMED) TETHER LENGTH             - BETWEEN 2 BEADS
! ELMG       = INSTANTANEOUS DISTANCE BETWEEN ATT PTS

! JB1  = 3*(JB-1) + 1 = X-COORD PTR INTO STATE OF BEAD JB
! JB1P = 3*(JB-2) + 1 = X-COORD PTR INTO STATE OF BEAD PREVIOUS TO JB

! TDIST(JTT) = INSTANTANEOUS DISTANCE BETWEEN ATTACH POINTS
! XELOAD(JTT) = X-END LOAD OF A TOSS TETHER

! LASIGN(JTT) = FINITE SOLN NUMBER ASSIGNED TO THIS TOSS TETHER


!----------------------------------------------------------------
! PREPARE FOR RETURNING RESULTS BY INITIALIZING RETURN ARG ARRAYS
!----------------------------------------------------------------
      DO 666 J = 1,NCOSSEG
             DLDUM(J)  = 0.0

             DO 665 K=1,4
665             DATDUM(J,K) = 0.0
             CONTINUE

666   CONTINUE


!***********************************************************
!***********************************************************
!***********************************************************
!   FORM THE RESULTS ARRAY FOR "LENGTH" ALONG THE CHAIN
!***********************************************************
!***********************************************************
!***********************************************************
! INITIALIZE RETURN ARG FOR FUNCTION (AND LENGTH ARRAY INDEX)
      JLEN = 0


! SET IN LENGTHS FOR EACH CHAIN SEGMENT
!**************************************
      DO 777 J = 1,LSEGCNT


! SEE IF AN ACTIVE, FINITE SOLN IS ASSIGNED TO THIS TOSS TETHER
!--------------------------------------------------------------
         JTTN = ABS(JTORDER(J))
         JFTN = LASIGN(JTTN)
         IF((JFTN .EQ. 0) .OR. (NFINIT .LT. JFTN)) THEN

!***********************************************************
!    THIS CHAIN SEGMENT CONSISTS OF A "MASSLESS" TETHER
!***********************************************************
                 JLEN = JLEN + 1

! BY DEFN, 1ST LENGTH VALUE IS ZERO CORRESPONDING TO LOW-END OBJECT
                 IF(J .EQ. 1) THEN
                     DLDUM(JLEN) = 0.0
                 ELSE

! (FRACTIONAL INCREMENT COMPENSATES FOR PREV LENGTH DECREMENT)
                     DLDUM(JLEN) = (1.0 + 0.000001)*DLDUM(JLEN-1)
                 END IF

! SET IN A SINGLE VALUE FOR "SEGMENT LENGTH"
                 JLEN = JLEN + 1
                 DLDUM(JLEN) = DLDUM(JLEN-1) + TDIST(JTTN)

         ELSE


!***********************************************************
!    THIS CHAIN SEGMENT CONSISTS OF A  "FINITE"  TETHER
!***********************************************************
! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
! GET ITS SEGMENT DATA
                 CALL TISLDW(JFTN)

! SET IN ATTRIBUTE VALUES ASSOCIATED WITH LENGTH STATES AT THE BEADS
                 DO 771 JBBCNT = 1,NBEAD

! CREATE AN INDEX THAT ACCOUNTS FOR REF-END ORIENTATION OF TETHER
!----------------------------------------------------------------
! NOTE: JFS_NXT IS INDEX USED FOR PARAMETERS THAT ARE AVERAGED
                    JXEND = JBBCNT
                    JYEND = NBEAD + 1 - JBBCNT

                    IF(JTORDER(J) .GT. 0) THEN
                           JFS = JXEND
                           JFS_NXT = JFS + 1
                    END IF

                    IF(JTORDER(J) .LT. 0) THEN
                           JFS = JYEND + 1
                           JFS_NXT = JFS - 1
                    END IF

! SET IN VALUES FOR THE STARTING-END AND THEN 1ST BEAD POSITION
!--------------------------------------------------------------
                    IF(JBBCNT .EQ. 1) THEN

! SET IN ORIGIN LENGTH VALUE FOR THIS FINITE SOLN
! (FRACTIONAL INCREMENT COMPENSATES FOR PREV LENGTH DECREMENT)
                        JLEN = JLEN + 1
                        IF(J .EQ. 1) THEN
                            DLDUM(JLEN) = 0.0
                        ELSE
                            DLDUM(JLEN) = (1.0 + 0.000001)*DLDUM(JLEN-1)
                        END IF

! SET IN BEAD 1 LENGTH VALUE FOR THIS FINITE SOLN
                        JLEN = JLEN + 1
                        DLDUM(JLEN) = DLDUM(JLEN-1) + ELBSG(JFS)
                    END IF


! SET IN VALUES FOR THE INTERIOR POINTS
!--------------------------------------
                    IF((JBBCNT .GT. 1) .AND. (JBBCNT .LT. NBEAD)) THEN
                        JLEN = JLEN + 1
                        DLDUM(JLEN) = DLDUM(JLEN-1) + ELBSG(JFS)
                    END IF

! SET IN VALUES FOR THE LAST BEAD POSITION AND THE OPPOSITE END
!--------------------------------------------------------------
                    IF(JBBCNT .EQ. NBEAD) THEN
                        JLEN = JLEN + 1
                        DLDUM(JLEN) = DLDUM(JLEN-1) + ELBSG(JFS)

                        JLEN = JLEN + 1
                        DLDUM(JLEN) = DLDUM(JLEN-1) + ELBSG(JFS_NXT)
                    END IF

771              CONTINUE

         END IF
! END OF MASSLESS-VS-FINITE TETHER TRIAGE LOGIC


! GETTING TO HERE MEANS THIS CHAIN-SEGMENT IS FINISHED, SO TO ALLOW
! DISCONTINUTY DISPLAY, DECREMENT LAST LENGTH BY A SMALL AMOUNT
              DLDUM(JLEN) = (1.0 - 0.000001)*DLDUM(JLEN)

777   CONTINUE



!-------------------------------------------------------------------------
! VALUE OF ARRAY POINTER FROM ABOVE LOOP CONTAINS "THE NUMBER OF ELEMENTS"
! STORED IN RESULTS ARRAY; SAVE IT AS THE ARRAY CONTENT-SIZE OUTPUT ARG
!-------------------------------------------------------------------------
      JCNTDUM = JLEN






!********************************************************************
!********************************************************************
!********************************************************************
!    GIGANTIC LOOP TO SELECT DATA TYPE AND POPULATE RESULTS ARRAY
!********************************************************************
!********************************************************************
!********************************************************************
! INITIALIZE A LENGTH ARRAY INDEX
      JLEN = 0

! SET IN DATA PARAMETERS FOR EACH CHAIN-SEGMENT
!----------------------------------------------
      DO 888 J = 1,LSEGCNT

! SEE IF AN ACTIVE, FINITE SOLN IS ASSIGNED TO THIS TOSS TETHER
!--------------------------------------------------------------
         JTTN = ABS(JTORDER(J))
         JFTN = LASIGN(JTTN)
         IF((JFTN .EQ. 0) .OR. (NFINIT .LT. JFTN)) THEN

!***************************************************
!***************************************************
! THIS CHAIN-SEGMENT CONSISTS OF A "MASSLESS" TETHER
!***************************************************
!***************************************************

! POPULATE RESULTS WITH "MASSLESS TETHER DATA" AT BEGINNING OF CHAIN SEG
!=======================================================================
                 JLEN = JLEN + 1

! SET IN A SINGLE VALUE FOR MASSLESS TENSION AT BEGINNING OF CHAIN SEG
                 DATDUM(JLEN,1) = XELOAD(JTTN)
! SET IN A SINGLE VALUE FOR MASSLESS ELASTIC AREA AT BEGINNING OF CHAIN SEG
                 DATDUM(JLEN,2) = 0.0
! SET IN A SINGLE VALUE FOR MASSLESS STRESS AT BEGINNING OF CHAIN SEG
                 DATDUM(JLEN,3) = 0.0
! SET IN A SINGLE VALUE FOR MASSLESS STRAIN AT BEGINNING OF CHAIN SEG
                 DATDUM(JLEN,4) = (TDIST(JTTN) -TLENT(JTTN))/TLENT(JTTN)


! POPULATE RESULTS WITH "MASSLESS TETHER DATA" AT END OF CHAIN SEG
!=================================================================
                 JLEN = JLEN + 1

! SET IN A SINGLE VALUE FOR MASSLESS TENSION AT END OF CHAIN SEG
                 DATDUM(JLEN,1) = XELOAD(JTTN)
! SET IN A SINGLE VALUE FOR MASSLESS ELASTIC AREA AT END OF CHAIN SEG
                 DATDUM(JLEN,2) = 0.0
! SET IN A SINGLE VALUE FOR MASSLESS STRESS AT END OF CHAIN SEG
                 DATDUM(JLEN,3) = 0.0
! SET IN A SINGLE VALUE FOR MASSLESS STRAIN AT END OF CHAIN SEG
                 DATDUM(JLEN,4) = (TDIST(JTTN) -TLENT(JTTN))/TLENT(JTTN)

         ELSE

!*************************************************************
!*************************************************************
!     THIS CHAIN SEGMENT CONSISTS OF A  "FINITE"  TETHER
!*************************************************************
!*************************************************************
! EVALUATE FLAG INDICATING IF ANY THERMAL CALCS ARE ACTIVE
             JDUMTH = LQSOLR + LQALB  + LQEBB + LQAERO + LQOHMS   &
     &              + LQTRAD + LQCOND + LEXPND

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
! GET ITS SEGMENT DATA
                 CALL TISLDW(JFTN)

! DO A SUB-LOOP TO EXTRACT EACH SEGMENT STRAIN FOR LATER USE
!-----------------------------------------------------------
                 DO 333 JSEG = 1,NBEAD+1

! IF THERMAL CALCS ACTIVE, USE TEMPERATURE-COMPENSATED SEG LENGTHS
                       IF(JDUMTH .GT. 0) THEN
                           SEGDUM = SEGLA(JSEG)
                       ELSE
                           SEGDUM = SEGLAT/REAL(NBEAD+1)
                       END IF

! FORM VALUE FOR SEGMENT STRAIN
                       DELSG  = ELBSG(JSEG) - SEGDUM
                       DUMSTR = DELSG/SEGDUM

! STASH THIS IN OUTPUT ARRAY
                       STRNDUM(JSEG) = DUMSTR

333   CONTINUE


! SET IN ATTRIBUTE VALUES ASSOCIATED WITH LENGTH STATES AT THE BEADS
                 DO 881 JBBCNT = 1,NBEAD

! CREATE AN INDEX ACCOUNTING FOR REF-END ORIENTATION OF TETHER
!-------------------------------------------------------------
! NOTE: JFS_NXT IS INDEX USED FOR PARAMETERS THAT ARE AVERAGED
                    JXEND = JBBCNT
                    JYEND = NBEAD + 1 - JBBCNT

                    IF(JTORDER(J) .GT. 0) THEN
                           JFS = JXEND
                           JFS_NXT = JFS + 1
                    END IF

                    IF(JTORDER(J) .LT. 0) THEN
                           JFS = JYEND + 1
                           JFS_NXT = JFS - 1
                    END IF

! CALCULATE CURRENT SEGMENT AND NEXT_SEGMENT STRESS VALUES
                    STRDUM     = TENSEG(JFS)    /BEADELA(JFS)
                    STRDUM_NXT = TENSEG(JFS_NXT)/BEADELA(JFS_NXT)


!=======================================
!   THIS SECTION POPULATES RESULTS ARRAY
!=======================================
! SET IN VALUES FOR THE FIRST BEAD POSITION AND THE STARTING END
!---------------------------------------------------------------
                    IF(JBBCNT .EQ. 1) THEN
! STARTING END
                        JLEN = JLEN + 1

                        DATDUM(JLEN,1) = TENSEG(JFS)
                        DATDUM(JLEN,2) = BEADELA(JFS)
                        DATDUM(JLEN,3) = STRDUM
                        DATDUM(JLEN,4) = STRNDUM(JFS)

! FIRST BEAD POSITION
                        JLEN = JLEN + 1

                        DELDUM = TENSEG(JFS_NXT) - TENSEG(JFS)
                        DATDUM(JLEN,1) = TENSEG(JFS) + 0.750*DELDUM

                        DELDUM = BEADELA(JFS_NXT) - BEADELA(JFS)
                        DATDUM(JLEN,2) = BEADELA(JFS) + 0.750*DELDUM

                        DELDUM = STRDUM_NXT - STRDUM
                        DATDUM(JLEN,3) = STRDUM + 0.750*DELDUM

                        DELDUM = STRNDUM(JFS_NXT) - STRNDUM(JFS)
                        DATDUM(JLEN,4) = STRNDUM(JFS) + 0.750*DELDUM
                    END IF

! SET IN VALUES FOR THE INTERIOR POINTS
!--------------------------------------
                    IF((JBBCNT .GT. 1) .AND. (JBBCNT .LT. NBEAD)) THEN

                        JLEN = JLEN + 1

                        AVDUM = 0.5*(TENSEG(JFS) + TENSEG(JFS_NXT))
                        DATDUM(JLEN,1) = AVDUM

                        AVDUM = 0.5*(BEADELA(JFS) + BEADELA(JFS_NXT))
                        DATDUM(JLEN,2) = AVDUM

                        AVDUM = 0.5*(STRDUM + STRDUM_NXT)
                        DATDUM(JLEN,3) = AVDUM

                        AVDUM = 0.5*(STRNDUM(JFS) + STRNDUM(JFS_NXT))
                        DATDUM(JLEN,4) = AVDUM
                    END IF

! SET IN VALUES FOR THE LAST BEAD POSITION AND THE OPPOSITE END
!--------------------------------------------------------------
                    IF(JBBCNT .EQ. NBEAD) THEN

! LAST BEAD POSITION
                        JLEN = JLEN + 1

                        DELDUM = TENSEG(JFS_NXT) - TENSEG(JFS)
                        DATDUM(JLEN,1) = TENSEG(JFS) + 0.250*DELDUM

                        DELDUM = BEADELA(JFS_NXT) - BEADELA(JFS)
                        DATDUM(JLEN,2) = BEADELA(JFS) + 0.250*DELDUM

                        DELDUM = STRDUM_NXT - STRDUM
                        DATDUM(JLEN,3) = STRDUM + 0.250*DELDUM

                        DELDUM = STRNDUM(JFS_NXT) - STRNDUM(JFS)
                        DATDUM(JLEN,4) = STRNDUM(JFS) + 0.250*DELDUM

! OTHER END
                        JLEN = JLEN + 1

                        DATDUM(JLEN,1) = TENSEG(JFS_NXT)
                        DATDUM(JLEN,2) = BEADELA(JFS_NXT)
                        DATDUM(JLEN,3) = STRDUM_NXT
                        DATDUM(JLEN,4) = STRNDUM(JFS_NXT)
                    END IF


! END OF FINITE SOLN BEAD LOOP
!-----------------------------
881              CONTINUE
!-----------------------------


! END OF MASSLESS/FINITE SOLN TRIAGE IF STATEMENT
         END IF

! END OF CHAIN SEGMENT LOOP
!-----------------------------
888      CONTINUE
!-----------------------------

! STANDARD RETURN
4000  CONTINUE

      RETURN

      END
