! ROUTINE: YFM020
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM020 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! <BEAD MODEL TETHER>  LINEAR MOMENTUM AND DYNAMIC EQUILIBRIUM

! (USEFUL FOR ASSESSING NUMERICAL CONVERGENCE)


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_COSS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"





!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! DEFINE COLUMN HEADINGS
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE COLUMN DATA
      IF(JFUNC .EQ. 2) GO TO 2000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM020-0'


!******************************************
!******************************************
!     WRITE COLUMN HEADER TO PLOT FILE
!******************************************
!******************************************
1000  CONTINUE

! CHOOSE STANDARD OR TAG FORMAT
!------------------------------
      IF(TAGFLG .EQ. 1.0) THEN

! DEFINE SPECIAL TIME HEADER
      WRITE(COLIF,1001) TIMID6, FSNID4, TAG4
1001  FORMAT(A6, A4, '_', A4)

! SET IN ENGLISH UNITS COLUMN HEADERS
      ECOLHD( 1) = COLIF

      ECOLHD( 2) = 'PXI_BLM_E_'//TAG4//' '
      ECOLHD( 3) = 'PYI_BLM_E_'//TAG4//' '
      ECOLHD( 4) = 'PZI_BLM_E_'//TAG4//' '

      ECOLHD( 5) = 'PXID_BLM_E_'//TAG4
      ECOLHD( 6) = 'PYID_BLM_E_'//TAG4
      ECOLHD( 7) = 'PZID_BLM_E_'//TAG4

      ECOLHD( 8) = 'FXI_BEXT_E_'//TAG4
      ECOLHD( 9) = 'FYI_BEXT_E_'//TAG4
      ECOLHD(10) = 'FZI_BEXT_E_'//TAG4

      ECOLHD(11) = 'ER_BPXID_E_'//TAG4
      ECOLHD(12) = 'ER_BPYID_E_'//TAG4
      ECOLHD(13) = 'ER_BPZID_E_'//TAG4

      ECOLHD(14) = 'MG_BPID_E_'//TAG4//' '
      ECOLHD(15) = 'MG_BFEXT_E_'//TAG4
      ECOLHD(16) = 'ERR_BPID_E_'//TAG4

! SET IN METRIC UNITS COLUMN HEADERS
      MCOLHD( 1) = COLIF
      MCOLHD( 2) = 'PXI_BLM_M_'//TAG4//' '
      MCOLHD( 3) = 'PYI_BLM_M_'//TAG4//' '
      MCOLHD( 4) = 'PZI_BLM_M_'//TAG4//' '

      MCOLHD( 5) = 'PXID_BLM_M_'//TAG4
      MCOLHD( 6) = 'PYID_BLM_M_'//TAG4
      MCOLHD( 7) = 'PZID_BLM_M_'//TAG4

      MCOLHD( 8) = 'FXI_BEXT_M_'//TAG4
      MCOLHD( 9) = 'FYI_BEXT_M_'//TAG4
      MCOLHD(10) = 'FZI_BEXT_M_'//TAG4

      MCOLHD(11) = 'ER_BPXID_M_'//TAG4
      MCOLHD(12) = 'ER_BPYID_M_'//TAG4
      MCOLHD(13) = 'ER_BPZID_M_'//TAG4

      MCOLHD(14) = 'MG_BPID_M_'//TAG4//' '
      MCOLHD(15) = 'MG_BFEXT_M_'//TAG4
      MCOLHD(16) = 'ER_BBPID_M_'//TAG4

      ELSE

! DEFINE SPECIAL TIME HEADER
!---------------------------
      WRITE(COLIF,1003) TIMID6, FSNID4
1003  FORMAT(A6, A4, '     ')

! SET IN ENGLISH UNITS COLUMN HEADERS
      ECOLHD( 1) = COLIF

      ECOLHD( 2) = 'PXI_BBLM_E'//FSNID4//' '
      ECOLHD( 3) = 'PYI_BBLM_E'//FSNID4//' '
      ECOLHD( 4) = 'PZI_BBLM_E'//FSNID4//' '

      ECOLHD( 5) = 'PXID_BBLM_E'//FSNID4
      ECOLHD( 6) = 'PYID_BBLM_E'//FSNID4
      ECOLHD( 7) = 'PZID_BBLM_E'//FSNID4

      ECOLHD( 8) = 'FXI_BBEXT_E'//FSNID4
      ECOLHD( 9) = 'FYI_BBEXT_E'//FSNID4
      ECOLHD(10) = 'FZI_BBEXT_E'//FSNID4

      ECOLHD(11) = 'ERR_BPXID_E'//FSNID4
      ECOLHD(12) = 'ERR_BPYID_E'//FSNID4
      ECOLHD(13) = 'ERR_BPZID_E'//FSNID4

      ECOLHD(14) = 'MG_BBPID_E'//FSNID4//' '
      ECOLHD(15) = 'MG_BBFEXT_E'//FSNID4
      ECOLHD(16) = 'ERR_BBPID_E'//FSNID4

! SET IN METRIC UNITS COLUMN HEADERS
      MCOLHD( 1) = COLIF
      MCOLHD( 2) = 'PXI_BBLM_M'//FSNID4//' '
      MCOLHD( 3) = 'PYI_BBLM_M'//FSNID4//' '
      MCOLHD( 4) = 'PZI_BBLM_M'//FSNID4//' '

      MCOLHD( 5) = 'PXID_BBLM_M'//FSNID4
      MCOLHD( 6) = 'PYID_BBLM_M'//FSNID4
      MCOLHD( 7) = 'PZID_BBLM_M'//FSNID4

      MCOLHD( 8) = 'FXI_BBEXT_M'//FSNID4
      MCOLHD( 9) = 'FYI_BBEXT_M'//FSNID4
      MCOLHD(10) = 'FZI_BBEXT_M'//FSNID4

      MCOLHD(11) = 'ERR_BPXID_M'//FSNID4
      MCOLHD(12) = 'ERR_BPYID_M'//FSNID4
      MCOLHD(13) = 'ERR_BPZID_M'//FSNID4

      MCOLHD(14) = 'MG_BBPID_M'//FSNID4//' '
      MCOLHD(15) = 'MG_BBFEXT_M'//FSNID4
      MCOLHD(16) = 'ERR_BBPID_M'//FSNID4

      END IF


! DEFINE NUMBER OF COLUMNS BEING DEFINED
      NUMCOL = 16

      RETURN



!******************************************
!******************************************
!     CALCULATE DATA FOR PLOT FILE
!******************************************
!******************************************
2000  CONTINUE

! IF NON-VALID FINIT SOLN # IS SPEC FOR THIS PAGE, GET OUT
      IF( (NFTSHO.LT.1) .OR. (NFTSHO.GT.NFINIT) ) GO TO 100

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
      CALL TISLDW(NFTSHO)

! SET IN SIMULATION TIME FOR THIS TETHER
      OUTP( 1) = TISTIM

! SET IN MOMENTUM ATTRIBUTES
      OUTP( 2) = (XXMS1*XXLNS) * PBBI(1)
      OUTP( 3) = (XXMS1*XXLNS) * PBBI(2)
      OUTP( 4) = (XXMS1*XXLNS) * PBBI(3)

      OUTP( 5) = XXFORS * PBBID(1)
      OUTP( 6) = XXFORS * PBBID(2)
      OUTP( 7) = XXFORS * PBBID(3)

      OUTP( 8) = XXFORS * FEBBI(1)
      OUTP( 9) = XXFORS * FEBBI(2)
      OUTP(10) = XXFORS * FEBBI(3)

      OUTP(11) = ERRPBB(1)
      OUTP(12) = ERRPBB(2)
      OUTP(13) = ERRPBB(3)

      OUTP(14) = XXFORS * VECMAG(PBBID)
      OUTP(15) = XXFORS * VECMAG(FEBBI)

      OUTP(16) = VECMAG(PBBID) - VECMAG(FEBBI)

100   CONTINUE

      RETURN
      END
