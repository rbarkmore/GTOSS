! ROUTINE: YPHDBO
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!***************************
!***************************
      SUBROUTINE YPHDBO
!***************************
!***************************
! THIS ROUTINE CREATES THE TETHER SHAPE SCRATCH FILE
! AND WRITES THE HEADER RECORD (ONLY) FOR ALL THE ALL-BEAD
! TYPE ORBIT-FRAME TETHER SHAPE FACILITY SCRATCH FILES


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_COSS.h"
      include "../../A_HDR/COM_ROSS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"





! IF NON-VALID FINIT SOLN # IS SPEC FOR THIS PAGE, GET OUT
      IF( (NFTSHO.LT.1) .OR. (NFTSHO.GT.NFINIT) ) GO TO 100

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
      CALL TISLDW(NFTSHO)

! SET UP THE BEAD COUNT INDEX (TIME + 0.0 + NBEAD + END OBJ)
      NBBX = NBEAD + 3

! OPEN THE SCRATCH FILE
!----------------------
      OPEN(IUSHAP, STATUS='SCRATCH',   ACCESS='DIRECT',   &
     &             FORM='UNFORMATTED', RECL= NRBYTE*NBBX)


! POPULATE THE HEADER RECORD
!---------------------------
! THE 1,1 ELEMENT IS NOT USED
      OUTP(1) = 0.0

! SET IN INDEX NUMBER OF ZERO-TH BEAD (IE. ORB FRAME ORIGIN)
      OUTP(2) = 0.0

! SET IN BEAD NUMBERS WHICH ARE DISPLAYED (CALL END OBJ =NBEAD+2)
! (NOTE: NBBX-2 ACCOUNTS FOR + NBEADS + END OBJ ITEM)
      DO 20 J = 1,NBBX-2
         OUTP(J+2) = REAL(J)
20    CONTINUE


! WRITE THE HEADER RECORD
!------------------------
! SET THE RECORD POINTER TO 1ST RECORD
      NEXSHP = 1

! (NOTE: NBBX+1 ACCOUNTS FOR UNUSED + 0.0 + NBEADS + END OBJ ITEM)
      WRITE(IUSHAP,REC=NEXSHP) (OUTP(J),J=1,NBBX)

100   RETURN
      END
