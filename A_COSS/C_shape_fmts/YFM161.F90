! ROUTINE: YFM161
! %Z%GTOSS %M% H.10 code v01.00 (baseline for vH.10 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM161 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! DISPLAYS SEGMENT "+NORTH RELATIVE WIND" SNAPSHOTS -VS- "ALTITUDE
! ABOVE THE PLANET SURFACE" FOR MULTIPLE TETHERS THAT CONSTITUTE A
! SINGLE CHAIN. THESE RESULTS ARE EAST-NORTH REFERENCED TO THE PLANET-
! FIXED TOPO-FRAME CORRESPONDING TO THE TOSS REF POINT'S POSITION AT
! TIME = 0.
!
!
!       TETHER-CHAIN SNAP-SHOT SHAPE FMT
!       --------------------------------
!
! SPECIFICALLY, THE TETHER-CHAIN THAT WILL BE DISPLAYED IS CHOSEN BY
! THIS FORMAT TO BE THAT CHAIN CONTAINING TOSS OBJECT 1 ONLY. THIS MAKES
! THE FORMAT ESSENTIALLY HAVE REAL USE ONLY FOR SPACE-ELEVATOR TYPE
! RESULTS INTERPRETATION.
!
! IF A TETHER IN THE CHAIN IS A MASSLESS TETHER, THEN ZERO  VALUES ARE
! DISPLAYED TO REMIND THE USER THAT AERO SIMULATION FOR MASSLESS TETHERS
! IS NOT CONSIDERED TO BE NECESSARILY REPRESENTATIVE OF FACT.


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_COSS.h"
      include "../../A_HDR/COM_ROSS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"





! STORAGE FOR ARG DATA (SELF MAINTAINING WR/T MAXBBS, VIA FOSS PARAM)
      DIMENSION ALTIDUM(NCOSSEG), DATADUM(NCOSSEG,9)


!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! OPEN SCRATCH FILE AND WRITE FIRST RECORD
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE DATA FOR SNAP-SHOT, WRITE SNAPSHOT TO SCRATCH FILE
      IF(JFUNC .EQ. 2) GO TO 2000

! DECOMMUTATE SCRATCH FILE, WRITE COL-DELIMITED SNAPSHOT FILE
      IF(JFUNC .EQ. 3) GO TO 3000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM161-0'


!******************************************
!******************************************
!     WRITE COLUMN HEADER TO PLOT FILE
!******************************************
!******************************************
1000  CONTINUE

! FOR DUAL SNAP-SHOT STRUCTURE, THIS SECTION CREATES THE
! RELATIVE-WIND-SHAPE SCRATCH FILE


! FETCH RELATED DATA FOR THIS CHAIN
!----------------------------------
      CALL CHAINARO (JNPDUM, ALTIDUM, DATADUM)


! STOP IF NO CHAIN EXISTS THAT CONTAINS SPECIFIED OBJECT NUMBER
      IF(JNPDUM .LT. 0) STOP ' IN YFM161-1, NO CHAIN CONTAINS OBJECT 1'

! OPEN THE SCRATCH FILE
!----------------------
      JDAT = JNPDUM + 1
      OPEN(IUSHAP, STATUS='SCRATCH',   ACCESS='DIRECT',   &
     &             FORM='UNFORMATTED', RECL= NRBYTE*JDAT)

! INITIALIZE RECORD POINTER FOR OTHERS
!-------------------------------------
      NEXSHP = 0

100   RETURN


!******************************************
!******************************************
!     CALCULATE DATA FOR PLOT FILE
!******************************************
!******************************************
2000  CONTINUE

! THIS SECTION WRITES BOTH RELATIVE-WIND-SHAPES, AND THE
! CORRESPONDING ALTITUDE PROFILES OF ALL SEGMENTS IN A
! TETHER CHAIN, TO A SHAPE SCRATCH FILE, IF SO REQUESTED BY
! THE SHAPE EVENT FLAG NSHPEV

! IF SHAPE RECORDING IS NOT REQUESTED, GET OUT
      IF(NSHPEV .EQ. 0) RETURN

! FETCH RELATED DATA FOR THIS CHAIN
!----------------------------------
      CALL CHAINARO (JNPDUM, ALTIDUM, DATADUM)

!-----------------------------------------------------------------
! EXTRACT ALTITUDES CORRESPONDING TO EACH SHAPE DISPLACEMENT SAVED
!-----------------------------------------------------------------
! FIRST ELEMENT IS NOT USED FOR ALTITUDE PROFILE
      OUTP(1) = QTIME

      DO 25 J = 1,JNPDUM
         OUTP(J+1) = XXLNS * ALTIDUM(J)
25    CONTINUE

! PREPARE TO WRITE THE NEXT RECORD TO THE SHAPE SCRATCH FILE
!-----------------------------------------------------------
! BUMP ASSOCIATED RECORD COUNTER
      NEXSHP = NEXSHP + 1

! WRITE THIS ALTITUDE PROFILE TO ASSOCIATED SCRATCH FILE
      WRITE(IUSHAP,REC=NEXSHP) (OUTP(J),J=1,JNPDUM+1)



!-----------------------------------
! EXTRACT EACH SEGMENT RELATIVE WIND
!-----------------------------------
! SAVE SIMULATION TIME FOR THIS SHAPE EVENT
      OUTP(1) = QTIME

      DO 30 J = 1,JNPDUM
            OUTP(J+1) = XXLNS * DATADUM(J,2)
30    CONTINUE


! PREPARE TO WRITE THE NEXT RECORD TO THE SHAPE SCRATCH FILE
!-----------------------------------------------------------
! BUMP ASSOCIATED RECORD COUNTER
      NEXSHP = NEXSHP + 1

! WRITE THIS RELATIVE WIND-SHAPE TO ASSOCIATED SCRATCH FILE
      WRITE(IUSHAP,REC=NEXSHP) (OUTP(J),J=1,JNPDUM+1)

300   RETURN


!******************************************
!******************************************
!     WRITE LINE OF DATA TO PLOT FILE
!******************************************
!******************************************
3000  CONTINUE


! THIS ROUTINE CONVERTS AN ASSOCIATED SEGMENT RELATIVE WINDS
! SHAPE SCRATCH FILE TO A COLUMN DELIMITED OUTPUT FILE
! FOR THE TETHER-CHAIN SHAPE FORMATS

! FETCH RELATED DATA FOR THIS CHAIN
!----------------------------------
      CALL CHAINARO (JNPDUM, ALTIDUM, DATADUM)


!************************************************************
! OPEN GENERAL PURPOSE UTILITY FILE AND WRITE IT FULL OF JUNK
!************************************************************
      OPEN(ISUTIL, STATUS='SCRATCH',   ACCESS='DIRECT',   &
     &             FORM='UNFORMATTED', RECL= NRBYTE*NEXSHP)

      DO 1 JREC = 1,JNPDUM+1
          WRITE(ISUTIL,REC=JREC) (OUTP(J),J=1,NEXSHP)
1     CONTINUE



!****************************************************
! TRANSFORM SHAPE-SCRATCH FILE INTO UTILITY STRUCTURE
!****************************************************
      DO 50 JSREC = 1,NEXSHP

! READ A RECORD OF THE SCRATCH FILE
         READ(IUSHAP,REC=JSREC) (OUTP(J),J=1,JNPDUM+1)

!-----------------------------------------------------
! TEAR IT A PART AND POPULATE THE UTILITY SCRATCH FILE
! (USE RDBOUT ARRAY TO HELP HERE)
            DO 55 JUREC = 1,JNPDUM+1

                  READ (ISUTIL,REC=JUREC) (RDBOUT(J),J=1,NEXSHP)
                  RDBOUT(JSREC) = OUTP(JUREC)
                  WRITE(ISUTIL,REC=JUREC) (RDBOUT(J),J=1,NEXSHP)

55          CONTINUE
! END OF UTILITY SCRATCH FILE WRITE
!----------------------------------

50      CONTINUE
! END OF TETHER-SHAPE FACILITY SCRATCH FILE READ
!***********************************************



!***********************************************
! START WRITING THE COLUMN DELIMITED OUTPUT FILE
!     (BY RETRIEVING DATA FROM UTILITY FILE)
!***********************************************

!-----------------------------------------------------------------
! FIRST, PUT THE COLUMN HEADER ON THE COLUMN DELIMITED OUTPUT FILE
!-----------------------------------------------------------------

! FIND THE TIMES AT WHICH RELATIVE WINDS WERE SAVED
      READ(ISUTIL,REC=1) (OUTP(J),J=1,NEXSHP)


! CHOOSE STANDARD OR TAG FORMAT
!------------------------------
      IF(TAGFLG .EQ. 1.0) THEN

! TAG FORMAT HEADER DEFINITION
!*****************************
! ACTIVATE FORMAT OF UNITS CHOICE AND
! WRITE HEADER TO DATA OUTPUT FILE

      IF(NUNIT .EQ. 1) WRITE(IUOUT,11) (HT, FMNID3,TAG4,OUTP(J),  HT, TAG4,OUTP(J),  J=1,NEXSHP,2)
      IF(NUNIT .EQ. 2) WRITE(IUOUT,12) (HT, FMNID3,TAG4,OUTP(J),  HT, TAG4,OUTP(J),  J=1,NEXSHP,2)

! ENGLISH UNITS
11    FORMAT(   &
     &    50(A1,'ALT_FT',A3,'_',A4,E8.3, A1,'N_REL_W_FPS_',A4, E8.3))

! METRIC UNITS
12    FORMAT(   &
     &    50(A1,'ALT_M',A3,'_',A4,E8.3, A1,'N_REL_W_MPS_',A4, E8.3))

      ELSE



! REGULAR FORMAT HEADER DEFINITION
!*********************************
! ACTIVATE FORMAT OF UNITS CHOICE AND
! WRITE HEADER TO DATA OUTPUT FILE

      IF(NUNIT .EQ. 1) WRITE(IUOUT,13) (HT,FMNID3,OUTP(J), HT,OUTP(J), J=1,NEXSHP,2)
      IF(NUNIT .EQ. 2) WRITE(IUOUT,14) (HT,FMNID3,OUTP(J), HT,OUTP(J), J=1,NEXSHP,2)

! ENGLISH UNITS
13    FORMAT( 50( A1,'ALT_FT',A3,'_',E8.3, A1,'N_REL_W_FPS',E8.3) )

! METRIC UNITS
14    FORMAT( 50( A1,'ALT_M',A3,'_',E8.3, A1,'N_REL_W_MPS',E8.3) )

      END IF


! IN FORMATS ABOVE, REP FACTOR OF 50 IS ARBITRARY AS LONG
! AS IT IS .GT. MAX NUMBER OF SHAPE SNAPSHOTS


!------------------------------------------------
! THEN, PUT OUT A POSITIONS AND ITS RELATIVE WINDS
!------------------------------------------------
      DO 400 JBREC = 2,JNPDUM+1

! READ RECORDS CONTAINING POSITION + ASSOCIATED RELATIVE WINDS AT EACH TIME
          READ(ISUTIL,REC=JBREC) (OUTP(J),J=1,NEXSHP)


! WRITE SEGMENTS ALTITUDE AND DATA
          WRITE(IUOUT,22) (HT,OUTP(J),  HT,OUTP(J+1), J=1,NEXSHP,2)
22        FORMAT( 50(A1,1PE15.8,  A1,E15.8) )

400   CONTINUE


! CLOSE ASSOCIATED TETHER-SHAPE SCRATCH AND UTILITY FILE
!-------------------------------------------------------
      CLOSE(IUSHAP)
      CLOSE(ISUTIL)

999   RETURN

      END
