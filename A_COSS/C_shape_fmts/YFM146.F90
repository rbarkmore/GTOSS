! ROUTINE: YFM146
! %Z%GTOSS %M% H.10 code v01.01 (baseline for vH.10 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM146 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! THIS SHAPE FORMAT PROVIDES SEGMENT ELASTIC STRAIN


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_COSS.h"
      include "../../A_HDR/COM_ROSS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"





!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! WRITE HEADING TO PRINTOUT PAGE
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE DATA FOR PRINTOUT PAGE
      IF(JFUNC .EQ. 2) GO TO 2000

! PUT OUT A LINE OF DATA TO PRINTOUT PAGE
      IF(JFUNC .EQ. 3) GO TO 3000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM146-0'


!******************************************
!******************************************
!     WRITE COLUMN HEADER TO PLOT FILE
!******************************************
!******************************************
1000  CONTINUE

! THIS SECTION CREATES THE SEGMENT-STRAIN-SHAPE SCRATCH
! FILE AND WRITES THE HEADER RECORD (ONLY) FOR THE
! ALL-BEAD TYPE STRAIN SCRATCH FILE

! IF NON-VALID FINIT SOLN # IS SPEC FOR THIS PAGE, GET OUT
      IF( (NFTSHO.LT.1) .OR. (NFTSHO.GT.NFINIT) ) GO TO 100

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
      CALL TISLDW(NFTSHO)

! SET UP THE SEGMENT COUNT INDEX
      NBBX = NBEAD + 2

! OPEN THE SCRATCH FILE
!----------------------
      OPEN(IUSHAP, STATUS='SCRATCH',   ACCESS='DIRECT',   &
     &             FORM='UNFORMATTED', RECL= NRBYTE*NBBX)


! POPULATE THE HEADER RECORD
!---------------------------
! THE 1,1 ELEMENT IS NOT USED
      OUTP(1) = 0.0

! SET IN SELECTED SEGMENT NUMBERS WHICH WILL BE DISPLAYED
      DO 20 J = 1,NBBX-1
         OUTP(J+1) = REAL(J)
20    CONTINUE


! WRITE THE HEADER RECORD
!------------------------
! SET THE RECORD POINTER TO 1ST RECORD
      NEXSHP = 1

      WRITE(IUSHAP,REC=NEXSHP) (OUTP(J),J=1,NBBX)

100   RETURN


!******************************************
!******************************************
!     CALCULATE DATA FOR PLOT FILE
!******************************************
!******************************************
2000  CONTINUE

! THIS ROUTINE WRITES THE SEGMENT STRAIN-SHAPE
! OF ALL SEGMENTS IN A BEAD MODEL TETHER, TO A
! SHAPE SCRATCH FILE, IF SO REQUESTED BY THE SHAPE
! EVENT FLAG NSHPEV

! IF SHAPE RECORDING IS NOT REQUESTED, GET OUT
      IF(NSHPEV .EQ. 0) RETURN

! IF NON-VALID FINIT SOLN # IS SPEC FOR THIS PAGE, GET OUT
      IF( (NFTSHO.LT.1) .OR. (NFTSHO.GT.NFINIT) ) GO TO 300

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
      CALL TISLDW(NFTSHO)

! SAVE SIMULATION TIME FOR THIS SHAPE EVENT
      OUTP(1) = TISTIM

! EVALUATE FLAG INDICATING IF ANY THERMAL CALCS ARE ACTIVE
             JDUMTH = LQSOLR + LQALB  + LQEBB + LQAERO + LQOHMS   &
     &              + LQTRAD + LQCOND + LEXPND

! START A LOOP TO EXTRACT EACH SEGMENT STRAIN
!--------------------------------------------
      DO 333 JSEG = 1,NBEAD+1

! IF THERMAL CALCS ACTIVE, USE TEMPERATURE-COMPENSATED SEG LENGTHS
             IF(JDUMTH .GT. 0) THEN
                 SEGDUM = SEGLA(JSEG)
             ELSE
                 SEGDUM = SEGLAT/REAL(NBEAD+1)
             END IF


! FORM VALUE FOR SEGMENT STRAIN
            DELSG  = ELBSG(JSEG) - SEGDUM
            DUMSTR = DELSG/SEGDUM

! STASH THIS IN OUTPUT ARRAY
            OUTP(JSEG+1) = DUMSTR

333   CONTINUE


! PREPARE TO WRITE THE NEXT RECORD TO THE SHAPE SCRATCH FILE
!-----------------------------------------------------------
! BUMP ASSOCIATED RECORD COUNTER
      NEXSHP = NEXSHP + 1

! WRITE THIS STRAIN-SHAPE TO ASSOCIATED SCRATCH FILE
      WRITE(IUSHAP,REC=NEXSHP) (OUTP(J),J=1,NBEAD+2)

300   RETURN


!******************************************
!******************************************
!     WRITE LINE OF DATA TO PLOT FILE
!******************************************
!******************************************
3000  CONTINUE

! THIS ROUTINE CONVERTS AN ASSOCIATED SEGMENT STRAIN
! SHAPE SCRATCH FILE TO A COLUMN DELIMITED OUTPUT FILE
! FOR THE ALL-BEAD SHAPE FORMATS

! IF NON-VALID FINIT SOLN # IS SPEC FOR THIS PAGE, GET OUT
      IF( (NFTSHO.LT.1) .OR. (NFTSHO.GT.NFINIT) ) GO TO 999

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
! (ONLY TO GET NBEAD)
      CALL TISLDW(NFTSHO)

! SET UP BEAD COUNT INDEX
      NBBX = NBEAD + 2

!************************************************************
! OPEN GENERAL PURPOSE UTILITY FILE AND WRITE IT FULL OF JUNK
!************************************************************
      OPEN(ISUTIL, STATUS='SCRATCH',   ACCESS='DIRECT',   &
     &             FORM='UNFORMATTED', RECL= NRBYTE*NEXSHP)

      DO 1 JREC = 1,NBBX
          WRITE(ISUTIL,REC=JREC) (OUTP(J),J=1,NEXSHP)
1     CONTINUE



!****************************************************
! TRANSFORM SHAPE-SCRATCH FILE INTO UTILITY STRUCTURE
!****************************************************
      DO 50 JSREC = 1,NEXSHP

! READ A RECORD OF THE SCRATCH FILE
         READ(IUSHAP,REC=JSREC) (OUTP(J),J=1,NBBX)

!-----------------------------------------------------
! TEAR IT A PART AND POPULATE THE UTILITY SCRATCH FILE
! (USE RDBOUT ARRAY TO HELP HERE)
            DO 55 JUREC = 1,NBBX

                  READ (ISUTIL,REC=JUREC) (RDBOUT(J),J=1,NEXSHP)
                  RDBOUT(JSREC) = OUTP(JUREC)
                  WRITE(ISUTIL,REC=JUREC) (RDBOUT(J),J=1,NEXSHP)

55          CONTINUE
! END OF UTILITY SCRATCH FILE WRITE
!----------------------------------

50      CONTINUE
! END OF TETHER-SHAPE FACILITY SCRATCH FILE READ
!***********************************************



!***********************************************
! START WRITING THE COLUMN DELIMITED OUTPUT FILE
!     (BY RETRIEVING DATA FROM UTILITY FILE)
!***********************************************

!-----------------------------------------------------------------
! FIRST, PUT THE COLUMN HEADER ON THE COLUMN DELIMITED OUTPUT FILE
!-----------------------------------------------------------------

! FIND THE TIMES AT WHICH BEAD DISPLACEMENTS WERE SAVED
      READ(ISUTIL,REC=1) (OUTP(J),J=1,NEXSHP)


! CHOOSE STANDARD OR TAG FORMAT
!------------------------------
      IF(TAGFLG .EQ. 1.0) THEN

! ACTIVATE FORMAT OF UNITS CHOICE AND
! WRITE HEADER TO DATA OUTPUT FILE

      IF(NUNIT .EQ. 1) WRITE(IUOUT,11) FMNID3, TAG4, (HT, OUTP(J), TAG4, J=2,NEXSHP)
      IF(NUNIT .EQ. 2) WRITE(IUOUT,12) FMNID3, TAG4, (HT, OUTP(J), TAG4, J=2,NEXSHP)

! ENGLISH UNITS
11    FORMAT('SEGSTR',A3,'_',A4,' ',  50(A1,'ND',E8.3,'_',A4) )

! METRIC UNITS
12    FORMAT('SEGSTR',A3,'_',A4,' ',  50(A1,'ND',E8.3,'_',A4) )

      ELSE

! ACTIVATE FORMAT OF UNITS CHOICE AND
! WRITE HEADER TO DATA OUTPUT FILE

      IF(NUNIT .EQ. 1) WRITE(IUOUT,13) FMNID3, (HT, OUTP(J), J=2,NEXSHP)
      IF(NUNIT .EQ. 2) WRITE(IUOUT,14) FMNID3, (HT, OUTP(J), J=2,NEXSHP)

! ENGLISH UNITS
13    FORMAT('SEG_LEN_FRAC',A3, 50(A1,'STRAIN_',E8.3) )

! METRIC UNITS
14    FORMAT('SEG_LEN_FRAC',A3, 50(A1,'STRAIN_',E8.3) )

      END IF


! IN FORMATS ABOVE, REP FACTOR OF 50 IS ARBITRARY AS LONG
! AS IT IS .GT. MAX NUMBER NEEDED


!-----------------------------------------------
! THEN, PUT OUT A SEGMENT NUMBER AND ITS STRAIN
!-----------------------------------------------
      DO 400 JBREC = 2,NBEAD+2

! READ RECORDS CONTAINING A BEAD NO. AND ITS POSITION FOR EACH TIME
          READ(ISUTIL,REC=JBREC) (OUTP(J),J=1,NEXSHP)

! CALC THIS SEGMENTS EFFECTIVE FRACTIONAL LENGTH POSITION
          FDUM = (OUTP(1)-.5)/REAL(NBEAD+1)

! WRITE SEGMENTS FRACT LENGTH POSITION AND STRAINS

          WRITE(IUOUT,22) FDUM, (HT,OUTP(J), J=2,NEXSHP)
22        FORMAT(1PE15.8, 50(A1,1PE15.8) )

400   CONTINUE


! CLOSE ASSOCIATED TETHER-SHAPE SCRATCH AND UTILITY FILE
!-------------------------------------------------------
      CLOSE(IUSHAP)
      CLOSE(ISUTIL)

999   RETURN

      END
