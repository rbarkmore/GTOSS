! ROUTINE: YFMT50
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!******************************************************************
!******************************************************************
!******************************************************************
                    SUBROUTINE YFMT50 (JFUNC)
!******************************************************************
!******************************************************************
!******************************************************************
! THIS IS THE FIRST USER IMPLEMENTABLE FORMAT (SEE YFMT01 AS A
! TEMPLATE, AND YFMT04 AS A REAL EXAMPLE)


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_VOSS.h"




!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
      IF(JFUNC .EQ. 1) GO TO 1000
      IF(JFUNC .EQ. 2) GO TO 2000
      IF(JFUNC .EQ. 3) GO TO 3000
      IF(JFUNC .EQ. 4) GO TO 4000
      IF(JFUNC .EQ. 5) GO TO 5000

      WRITE(IUERR,11)
11    FORMAT(' YFMT50: ILLEGAL ARGUMENT')
      STOP ' IN YFMT50-0'

!******************************************
!      START INITIALIZATION SECTION
!******************************************
1000  CONTINUE
      RETURN

!**********************************************
!     START REAL DATA BLOCK READ SECTION
!**********************************************
2000  CONTINUE
!CC   RETURN

!*************************************************
!        START BLOCK HEADERS REPORT WRITE
!*************************************************
3000  CONTINUE
!CC   RETURN

!*************************************************
!     START DISCREPANCY DETECT/RESULT WRITE
!*************************************************
4000  CONTINUE
!CC   RETURN

!*************************************************
!     START SUMMARY RESULTS WRITE FOR SESSION
!*************************************************
5000  CONTINUE

      WRITE(IUERR,5011)
5011  FORMAT(' YFMT50: FORMAT 01 IS NOT IMPLEMENTED')
      STOP ' IN YFMT50-1'

!CC   RETURN
      END
