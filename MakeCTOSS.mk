
INCLUDE =A_HDR
FC =gfortran
FFLAGS = 
LDFLAGS = 
CPPFLAGS =-I$(INCLUDE) 

modfiles=\
A_MODS/Finite_Solution_Data_Structure.o \
A_MODS/Finite_Solutions.o \
A_MODS/TOSS_Object_Data_Structure.o \
A_MODS/TOSS_Object_Solutions.o

envrfiles=\
A_ENVR/cira.o \
A_ENVR/ATM62.o \
A_ENVR/ATM76.o \
A_ENVR/ATMO3V.o \
A_ENVR/ATMOS2.o \
A_ENVR/ATMOS3.o \
A_ENVR/ATMOS.o \
A_ENVR/EFTEI.o \
A_ENVR/EITEF.o \
A_ENVR/GAUSS2.o \
A_ENVR/GAUSSD.o \
A_ENVR/GAUSS.o \
A_ENVR/GAUSSG.o \
A_ENVR/GEOD.o \
A_ENVR/GRAV4.o \
A_ENVR/GRAV.o \
A_ENVR/INTERP.o \
A_ENVR/J20FRM.o \
A_ENVR/JACHIA.o \
A_ENVR/JULIAN.o \
A_ENVR/M50EF.o \
A_ENVR/M50EFV.o \
A_ENVR/M50FRM.o \
A_ENVR/MONDAY.o \
A_ENVR/MONSEC.o \
A_ENVR/PLASMA.o \
A_ENVR/SOLEPH.o \
A_ENVR/SUNPOS.o \
A_ENVR/WINDS.o \
A_ENVR/WINDP01.o \
A_ENVR/WINDP02.o \
A_ENVR/WINDP03.o \
A_ENVR/WINDP04.o \
A_ENVR/WINDP05.o \
A_ENVR/WINDP06.o \
A_ENVR/SIDEREAL.o \
A_ENVR/irine.o \
A_ENVR/iritecne.o \
A_ENVR/irifun.o

utilfiles=\
A_UTIL/ADAM.o \
A_UTIL/CROSS.o \
A_UTIL/DAYNIT.o \
A_UTIL/DLINT.o \
A_UTIL/DMOUSE.o \
A_UTIL/DOT.o \
A_UTIL/EULICS.o \
A_UTIL/EULINT.o \
A_UTIL/EULMAT.o \
A_UTIL/LIBXYZ.o \
A_UTIL/MATDIF.o \
A_UTIL/MATEUL.o \
A_UTIL/MATFIX.o \
A_UTIL/MATMOV.o \
A_UTIL/MATMUL.o \
A_UTIL/MATSCL.o \
A_UTIL/MATSNV.o \
A_UTIL/MATSUM.o \
A_UTIL/MATVEC.o \
A_UTIL/NCROSS.o \
A_UTIL/NEWPAG.o \
A_UTIL/ORBFRM.o \
A_UTIL/RIBXYZ.o \
A_UTIL/QLTERP.o \
A_UTIL/SHOERR.o \
A_UTIL/SLINT.o \
A_UTIL/TCROSS.o \
A_UTIL/TDOT.o \
A_UTIL/TOPOGA.o \
A_UTIL/TOPOGR.o \
A_UTIL/TR3SOL.o \
A_UTIL/TR4SOL.o \
A_UTIL/TR5SOL.o \
A_UTIL/VECDIF.o \
A_UTIL/VECMAG.o \
A_UTIL/VECMAT.o \
A_UTIL/VECMOV.o \
A_UTIL/VECNRM.o \
A_UTIL/VECSCL.o \
A_UTIL/VECSUM.o \
A_UTIL/XYZLIB.o \
A_UTIL/SEEVECMAT.o \
A_UTIL/XYZRIB.o

foss1files=\
A_FOSS/F_general/TISFRM.o \
A_FOSS/F_general/TISUTL.o \
A_FOSS/F_general/TNSAD1.o \
A_FOSS/F_general/TNSARO.o \
A_FOSS/F_general/TNSBIT.o \
A_FOSS/F_general/TNSBRK.o \
A_FOSS/F_general/TNSBTH.o \
A_FOSS/F_general/TNSBTI.o \
A_FOSS/F_general/TNSCUP.o \
A_FOSS/F_general/TNSELC.o \
A_FOSS/F_general/TNSFOR.o \
A_FOSS/F_general/TNSFSI.o \
A_FOSS/F_general/TNSHOK.o \
A_FOSS/F_general/TNSHST.o \
A_FOSS/F_general/TNSMKS.o \
A_FOSS/F_general/TNSPRG.o \
A_FOSS/F_general/TNSPVS.o \
A_FOSS/F_general/TNSPVT.o \
A_FOSS/F_general/TNSSEG.o \
A_FOSS/F_general/TNSSGT.o \
A_FOSS/F_general/TNSTHR.o \
A_FOSS/F_general/TNSVEL.o \
A_FOSS/F_general/TNSXET.o \
A_FOSS/F_general/TNSYET.o \
A_FOSS/F_general/TOSSFM.o

foss2files=\
A_FOSS/F_init/TISSV.o \
A_FOSS/F_init/TISZZ.o \
A_FOSS/F_init/TNSBMV.o \
A_FOSS/F_init/TNSHIC.o \
A_FOSS/F_init/TNSKPV.o \
A_FOSS/F_init/TNSWAV.o

foss3files=\
A_FOSS/F_thermal/QDALBN.o \
A_FOSS/F_thermal/QDEBBN.o \
A_FOSS/F_thermal/QDSOLN.o \
A_FOSS/F_thermal/TNSQD.o \
A_FOSS/F_thermal/TNSQDS.o


goss4files=\
A_BOSS/B_general/BOMDER.o \
A_BOSS/B_general/BOMDAT.o \
A_BOSS/B_general/BOMLOD.o \
A_BOSS/B_general/BOMTIP.o \
A_BOSS/B_general/BOOMID.o

goss3files=\
A_GOSS/G_exec/COMGRB.o \
A_GOSS/G_exec/COMLAT.o \
A_GOSS/G_exec/COMSNP.o \
A_GOSS/G_exec/CRTDOT.o \
A_GOSS/G_exec/CRTHD.o \
A_GOSS/G_exec/CRTRUN.o \
A_GOSS/G_exec/HOSSTG.o \
A_GOSS/G_exec/INPUT.o \
A_GOSS/G_exec/INTEST.o \
A_GOSS/G_exec/LOOKHD.o \
A_GOSS/G_exec/LOOKLN.o \
A_GOSS/G_exec/OUTPUT.o \
A_GOSS/G_exec/POPREF.o \
A_GOSS/G_exec/GANIMO.o \
A_GOSS/G_exec/GHAINALD.o \
A_GOSS/G_exec/GHAINALS.o

goss1files=\
A_GOSS/G_general/ADAM1D.o \
A_GOSS/G_general/APSGET.o \
A_GOSS/G_general/APSPUT.o \
A_GOSS/G_general/BSMOVR.o \
A_GOSS/G_general/DERIV.o \
A_GOSS/G_general/DERMAS.o \
A_GOSS/G_general/DERROT.o \
A_GOSS/G_general/DRAGO.o \
A_GOSS/G_general/EVLVIR.o \
A_GOSS/G_general/GGTORK.o \
A_GOSS/G_general/NEWRPS.o \
A_GOSS/G_general/PGCALC.o \
A_GOSS/G_general/PLNFIX.o \
A_GOSS/G_general/RPAFOR.o \
A_GOSS/G_general/RPAMOM.o \
A_GOSS/G_general/RPATTC.o

goss2files=\
A_GOSS/G_init/INITIA.o \
A_GOSS/G_init/INITSS.o \
A_GOSS/G_init/INIVIR.o \
A_GOSS/G_init/INTROT.o \
A_GOSS/G_init/INTTRN.o

ross0files=\
A_ROSS/R_aux/PULAXA.o \
A_ROSS/R_aux/PULAXB.o \
A_ROSS/R_aux/PULAXF.o \
A_ROSS/R_aux/PULAXG.o \
A_ROSS/R_aux/PULAXJ.o \
A_ROSS/R_aux/PULAXK.o \
A_ROSS/R_aux/PULAXT.o \
A_ROSS/R_basic/PULBSA.o \
A_ROSS/R_basic/PULBSB.o \
A_ROSS/R_basic/PULBST.o \
A_ROSS/R_exec/PULCLO.o \
A_ROSS/R_exec/PULDB.o \
A_ROSS/R_exec/PULNAM.o \
A_ROSS/R_exec/PULOPN.o \
A_ROSS/R_wild/PULBSF.o \
A_ROSS/R_wild/PULBSG.o \
A_ROSS/R_wild/PULBSJ.o \
A_ROSS/R_wild/PULBSK.o \
A_ROSS/R_wild/PULF1.o \
A_ROSS/R_wild/PULG1.o \
A_ROSS/R_wild/PULJ1.o \
A_ROSS/R_wild/PULK1.o

ross1files=\
A_ROSS/R_ascii/POPIDB.o \
A_ROSS/R_ascii/POPIDO.o \
A_ROSS/R_ascii/POPIDS.o \
A_ROSS/R_ascii/POPIDT.o

ross2files=\
A_ROSS/R_aux/POPAXA.o \
A_ROSS/R_aux/POPAXB.o \
A_ROSS/R_aux/POPAXF.o \
A_ROSS/R_aux/POPAXG.o \
A_ROSS/R_aux/POPAXJ.o \
A_ROSS/R_aux/POPAXK.o \
A_ROSS/R_aux/POPAXT.o

ross3files=\
A_ROSS/R_basic/POPBSA.o \
A_ROSS/R_basic/POPBSB.o \
A_ROSS/R_basic/POPBST.o

ross4files=\
A_ROSS/R_exec/FIXPTH.o \
A_ROSS/R_exec/GETPTH.o \
A_ROSS/R_exec/GTROOT.o \
A_ROSS/R_exec/MACPTH.o \
A_ROSS/R_exec/NEWNAM.o \
A_ROSS/R_exec/POPDB.o \
A_ROSS/R_exec/POPDBO.o \
A_ROSS/R_exec/POPID.o \
A_ROSS/R_exec/POPIDX.o \
A_ROSS/R_exec/RDBCLO.o \
A_ROSS/R_exec/RDBNME.o \
A_ROSS/R_exec/RDBOPN.o

ross5files=\
A_ROSS/R_init/RDBIC.o \
A_ROSS/R_init/RDBSET.o \
A_ROSS/R_init/RDBZZ.o

ross6files=\
A_ROSS/R_wild/POPBSF.o \
A_ROSS/R_wild/POPBSG.o \
A_ROSS/R_wild/POPBSJ.o \
A_ROSS/R_wild/POPBSK.o \
A_ROSS/R_wild/POPF1.o \
A_ROSS/R_wild/POPG1.o \
A_ROSS/R_wild/POPJ1.o \
A_ROSS/R_wild/POPK1.o

toss1files=\
A_TOSS/T_exec/TOSBD.o \
A_TOSS/T_exec/TOSBF.o \
A_TOSS/T_exec/TOSBJ.o \
A_TOSS/T_exec/TOSBL.o \
A_TOSS/T_exec/TOSDIC.o \
A_TOSS/T_exec/TOSIN.o \
A_TOSS/T_exec/TOSLIN.o \
A_TOSS/T_exec/TOSQD.o \
A_TOSS/T_exec/TOSQF.o \
A_TOSS/T_exec/TOSQJ.o \
A_TOSS/T_exec/TOSQL.o \
A_TOSS/T_exec/TOSRIN.o

toss2files=\
A_TOSS/T_general/TOSAE.o \
A_TOSS/T_general/TOSAE2.o \
A_TOSS/T_general/TOSAE3.o \
A_TOSS/T_general/TOSAE4.o \
A_TOSS/T_general/TOSAE5.o \
A_TOSS/T_general/TOSAE4.o \
A_TOSS/T_general/TOSAE6.o \
A_TOSS/T_general/TOSAE7.o \
A_TOSS/T_general/TOSATT.o \
A_TOSS/T_general/TOSCAL.o \
A_TOSS/T_general/TOSCNTH.o \
A_TOSS/T_general/TOSCNA.o \
A_TOSS/T_general/TOSCNAP.o \
A_TOSS/T_general/TOSCND.o \
A_TOSS/T_general/TOSCN.o \
A_TOSS/T_general/TOSCNF.o \
A_TOSS/T_general/TOSCNL.o \
A_TOSS/T_general/TOSCNO.o \
A_TOSS/T_general/TOSCNVT.o \
A_TOSS/T_general/TOSCNVZ.o \
A_TOSS/T_general/TOSCNHT.o \
A_TOSS/T_general/TOSCNTT.o \
A_TOSS/T_general/TOSCN_SK_LABW_SENSOR.o \
A_TOSS/T_general/TOSCN_SK_LABW_SECTOR.o \
A_TOSS/T_general/TOSCN_SK_LABW_TRAV.o \
A_TOSS/T_general/TOSCN_SK_LABW_SAG1.o \
A_TOSS/T_general/TOSCN_SK_LABW_MASSEV.o \
A_TOSS/T_general/TOSCN_SK_LABW_MAIN.o \
A_TOSS/T_general/TOSCN_SK_LABW_T.o \
A_TOSS/T_general/TOSCN_SK_LABW_K.o \
A_TOSS/T_general/TOSCN_CRANE.o \
A_TOSS/T_general/TOSEXE.o \
A_TOSS/T_general/TOSGET.o \
A_TOSS/T_general/TOSGR.o \
A_TOSS/T_general/TOGRAV.o \
A_TOSS/T_general/TOSHOW.o \
A_TOSS/T_general/TOSMS2.o \
A_TOSS/T_general/TOSMS.o \
A_TOSS/T_general/TOSPUT.o \
A_TOSS/T_general/TOSROT.o \
A_TOSS/T_general/TOSRR1.o \
A_TOSS/T_general/TOSRR2.o \
A_TOSS/T_general/TOSRR3.o \
A_TOSS/T_general/TOSRR4.o \
A_TOSS/T_general/TOSSA.o \
A_TOSS/T_general/TOSSET.o \
A_TOSS/T_general/TOSSTEP.o \
A_TOSS/T_general/TOSSRP.o \
A_TOSS/T_general/TOSTEN.o \
A_TOSS/T_general/TOSTHR.o

toss3files=\
A_TOSS/T_ggic/TOZCON.o \
A_TOSS/T_ggic/TOZDBB.o \
A_TOSS/T_ggic/TOZDIF.o \
A_TOSS/T_ggic/TOZELV.o \
A_TOSS/T_ggic/TOZGGT.o \
A_TOSS/T_ggic/TOZGGV.o \
A_TOSS/T_ggic/TOZTTN.o \
A_TOSS/T_ggic/TOZTTS.o

toss4files=\
A_TOSS/T_init/TOSS_OBJ_IN.o \
A_TOSS/T_init/TOSS_OBJ_IN_REST.o \
A_TOSS/T_init/TOSS_CONSOL.o \
A_TOSS/T_init/TOSBVA.o \
A_TOSS/T_init/TOSBV.o \
A_TOSS/T_init/TOSBVT.o \
A_TOSS/T_init/TOSQV.o \
A_TOSS/T_init/TOSZZ.o

toss5files=\
A_TOSS/T_late_start/XGAPS.o \
A_TOSS/T_late_start/XGRAB.o \
A_TOSS/T_late_start/XGRABI.o \
A_TOSS/T_late_start/XGRABL.o \
A_TOSS/T_late_start/XGRABS.o \
A_TOSS/T_late_start/XSAPS.o \
A_TOSS/T_late_start/XSNAP.o \
A_TOSS/T_late_start/XSNAPI.o \
A_TOSS/T_late_start/XSNAPL.o \
A_TOSS/T_late_start/XSNAPS.o

toss6files=\
A_TOSS/T_scenario/CMODE1.o \
A_TOSS/T_scenario/CMODE2.o \
A_TOSS/T_scenario/CMODE3.o \
A_TOSS/T_scenario/CMODE4.o \
A_TOSS/T_scenario/CURCAL.o \
A_TOSS/T_scenario/TOSSF1.o \
A_TOSS/T_scenario/TOSSF2.o \
A_TOSS/T_scenario/TOSSF3.o \
A_TOSS/T_scenario/TOSSF4.o \
A_TOSS/T_scenario/TOSSF5.o \
A_TOSS/T_scenario/TOSSF6.o \
A_TOSS/T_scenario/TOSSF7.o \
A_TOSS/T_scenario/TOSSF8.o \
A_TOSS/T_scenario/TOSSF9.o \
A_TOSS/T_scenario/TOSSF10.o \
A_TOSS/T_scenario/TOSSF11.o \
A_TOSS/T_scenario/TOSSF12.o \
A_TOSS/T_scenario/TOSSF13.o \
A_TOSS/T_scenario/TOSSF14.o \
A_TOSS/T_scenario/TOSSFQ.o \
A_TOSS/T_scenario/TOSSFU.o \
A_TOSS/T_scenario/TOSSH1.o \
A_TOSS/T_scenario/TOSSH2.o \
A_TOSS/T_scenario/TOSSH3.o \
A_TOSS/T_scenario/TOSSH4.o \
A_TOSS/T_scenario/TOSSH5.o \
A_TOSS/T_scenario/TOSSH6.o \
A_TOSS/T_scenario/TOSSHU.o

toss7files=\
A_TOSS/T_tss_deploy/FOFT.o \
A_TOSS/T_tss_deploy/MMD.o \
A_TOSS/T_tss_deploy/MMF.o \
A_TOSS/T_tss_deploy/MMGSED.o \
A_TOSS/T_tss_deploy/MMGSE.o \
A_TOSS/T_tss_deploy/MMGSEF.o \
A_TOSS/T_tss_deploy/MMGSEL.o \
A_TOSS/T_tss_deploy/MMGSEV.o \
A_TOSS/T_tss_deploy/MMI.o \
A_TOSS/T_tss_deploy/MML.o \
A_TOSS/T_tss_deploy/MMO.o \
A_TOSS/T_tss_deploy/MMV.o \
A_TOSS/T_tss_deploy/RESUME.o \
A_TOSS/T_tss_deploy/TERP.o

bossfiles=\
A_BOSS/B_general/BOMDAT.o \
A_BOSS/B_general/BOMDER.o \
A_BOSS/B_general/BOMLOD.o \
A_BOSS/B_general/BOMTIP.o \
A_BOSS/B_general/BOOMID.o

cossfiles=\
A_COSS/C_exec/BSEGVU.o \
A_COSS/C_exec/CLEANO.o \
A_COSS/C_exec/CRTDIS.o \
A_COSS/C_exec/CRTPUL.o \
A_COSS/C_exec/CTOSUB.o \
A_COSS/C_exec/DECIDE.o \
A_COSS/C_exec/INCHEK.o \
A_COSS/C_exec/INPUL.o \
A_COSS/C_exec/SHAPEV.o \
A_COSS/C_exec/SHPSAV.o \
A_COSS/C_exec/WINCLO.o \
A_COSS/C_exec/WINOPN.o \
A_COSS/C_exec/WINSET.o \
A_COSS/C_exec/XFMT.o \
A_COSS/C_exec/XROWPO.o \
A_COSS/C_exec/XUNITS.o \
A_COSS/C_exec/XWINDO.o \
A_COSS/C_shape_fmts/CHAINARO.o \
A_COSS/C_shape_fmts/CHAINFOD.o \
A_COSS/C_shape_fmts/CHAINFOS.o \
A_COSS/C_shape_fmts/CHAINARA.o \
A_COSS/C_shape_fmts/YFM140.o \
A_COSS/C_shape_fmts/YFM141.o \
A_COSS/C_shape_fmts/YFM142.o \
A_COSS/C_shape_fmts/YFM143.o \
A_COSS/C_shape_fmts/YFM144.o \
A_COSS/C_shape_fmts/YFM145.o \
A_COSS/C_shape_fmts/YFM146.o \
A_COSS/C_shape_fmts/YFM147.o \
A_COSS/C_shape_fmts/YFM148.o \
A_COSS/C_shape_fmts/YFM149.o \
A_COSS/C_shape_fmts/YFM150.o \
A_COSS/C_shape_fmts/YFM151.o \
A_COSS/C_shape_fmts/YFM152.o \
A_COSS/C_shape_fmts/YFM153.o \
A_COSS/C_shape_fmts/YFM154.o \
A_COSS/C_shape_fmts/YFM155.o \
A_COSS/C_shape_fmts/YFM156.o \
A_COSS/C_shape_fmts/YFM157.o \
A_COSS/C_shape_fmts/YFM158.o \
A_COSS/C_shape_fmts/YFM159.o \
A_COSS/C_shape_fmts/YFM160.o \
A_COSS/C_shape_fmts/YFM161.o \
A_COSS/C_shape_fmts/YFM162.o \
A_COSS/C_shape_fmts/YFM163.o \
A_COSS/C_shape_fmts/YFM164.o \
A_COSS/C_shape_fmts/YFM165.o \
A_COSS/C_shape_fmts/YFM166.o \
A_COSS/C_shape_fmts/YFM167.o \
A_COSS/C_shape_fmts/YFM168.o \
A_COSS/C_shape_fmts/YFM169.o \
A_COSS/C_shape_fmts/YPHDBB.o \
A_COSS/C_shape_fmts/YPHDBO.o \
A_COSS/C_shape_fmts/YPHDSB.o \
A_COSS/C_shape_fmts/YPLNBB.o \
A_COSS/C_shape_fmts/YPLNBO.o \
A_COSS/C_shape_fmts/YPLNSB.o \
A_COSS/C_shape_fmts/YPSHBB.o \
A_COSS/C_shape_fmts/YPSHBO.o \
A_COSS/C_shape_fmts/YPSHSB.o \
A_COSS/C_time_fmts/YFM001.o \
A_COSS/C_time_fmts/YFM002.o \
A_COSS/C_time_fmts/YFM003.o \
A_COSS/C_time_fmts/YFM004.o \
A_COSS/C_time_fmts/YFM005.o \
A_COSS/C_time_fmts/YFM006.o \
A_COSS/C_time_fmts/YFM007.o \
A_COSS/C_time_fmts/YFM008.o \
A_COSS/C_time_fmts/YFM009.o \
A_COSS/C_time_fmts/YFM010.o \
A_COSS/C_time_fmts/YFM011.o \
A_COSS/C_time_fmts/YFM012.o \
A_COSS/C_time_fmts/YFM013.o \
A_COSS/C_time_fmts/YFM014.o \
A_COSS/C_time_fmts/YFM015.o \
A_COSS/C_time_fmts/YFM016.o \
A_COSS/C_time_fmts/YFM017.o \
A_COSS/C_time_fmts/YFM018.o \
A_COSS/C_time_fmts/YFM019.o \
A_COSS/C_time_fmts/YFM020.o \
A_COSS/C_time_fmts/YFM021.o \
A_COSS/C_time_fmts/YFM022.o \
A_COSS/C_time_fmts/YFM023.o \
A_COSS/C_time_fmts/YFM024.o \
A_COSS/C_time_fmts/YFM025.o \
A_COSS/C_time_fmts/YFM026.o \
A_COSS/C_time_fmts/YFM027.o \
A_COSS/C_time_fmts/YFM028.o \
A_COSS/C_time_fmts/YFM029.o \
A_COSS/C_time_fmts/YFM030.o \
A_COSS/C_time_fmts/YFM031.o \
A_COSS/C_time_fmts/YFM032.o \
A_COSS/C_time_fmts/YFM033.o \
A_COSS/C_time_fmts/YFM034.o \
A_COSS/C_time_fmts/YFM035.o \
A_COSS/C_time_fmts/YFM036.o \
A_COSS/C_time_fmts/YFM037.o \
A_COSS/C_time_fmts/YFM038.o \
A_COSS/C_time_fmts/YFM039.o \
A_COSS/C_time_fmts/YFM040.o \
A_COSS/C_time_fmts/YFM041.o \
A_COSS/C_time_fmts/YFM042.o \
A_COSS/C_time_fmts/YFM043.o \
A_COSS/C_time_fmts/YFM044.o \
A_COSS/C_time_fmts/YFM045.o \
A_COSS/C_time_fmts/YFM046.o \
A_COSS/C_time_fmts/YFM047.o \
A_COSS/C_time_fmts/YFM048.o \
A_COSS/C_time_fmts/YFM049.o \
A_COSS/C_time_fmts/YFM050.o \
A_COSS/C_time_fmts/YFM051.o \
A_COSS/C_time_fmts/YFM052.o \
A_COSS/C_time_fmts/YFM053.o \
A_COSS/C_time_fmts/YFM054.o \
A_COSS/C_time_fmts/YFM055.o \
A_COSS/C_time_fmts/YFM056.o \
A_COSS/C_time_fmts/YFM057.o \
A_COSS/C_time_fmts/YFM058.o \
A_COSS/C_time_fmts/YFM059.o \
A_COSS/C_time_fmts/YFM060.o \
A_COSS/C_time_fmts/YFM061.o \
A_COSS/C_time_fmts/YFM062.o \
A_COSS/C_time_fmts/YFM200.o \
A_COSS/C_time_fmts/YFM201.o \
A_COSS/C_time_fmts/YFM202.o \
A_COSS/C_time_fmts/YFM203.o \
A_COSS/C_time_fmts/YFM204.o \
A_COSS/C_time_fmts/YFM205.o \
A_COSS/C_time_fmts/YFM206.o \
A_COSS/C_time_fmts/YFM207.o \
A_COSS/C_time_fmts/YPDFBB.o \
A_COSS/C_time_fmts/YPDFBC.o \
A_COSS/C_time_fmts/YPDFDP.o \
A_COSS/C_time_fmts/YPDFTT.o \
A_COSS/C_time_fmts/CHAINALS.o \
A_COSS/C_time_fmts/CHAINTHRM.o \
A_COSS/C_time_fmts/CHAINALD.o

# Declare new suffix relationship
.SUFFIXES : .o .F90 

# Specify source compilation via new suffix rule
.F90.o :
	$(FC) -s -O1  -c -o $@ $?

all: CTOSSvH12

CTOSSvH12 : A_COSS/C_exec/CTOSS.o \
$(modfiles) $(cossfiles) \
$(goss1files) $(goss2files) $(goss3files) $(goss4files) \
$(ross0files) \
$(ross1files) $(ross2files) $(ross3files) \
$(ross4files) \
$(ross5files) $(ross6files) $(ross7files) \
$(toss1files) $(toss2files) $(toss3files) \
$(toss4files) \
$(toss5files) $(toss6files) $(toss7files) \
$(foss1files) $(foss2files) $(foss3files) \
$(utilfiles) $(envrfiles)
	$(FC) $(LDFLAGS) -o CTOSSvH12 $^

clean:
	/bin/rm -f *.o CTOSSvH12
cleanwell:
	@/bin/rm -f A_COSS/C_exec/CTOSS.o
	@/bin/rm -f $(cossfiles)

