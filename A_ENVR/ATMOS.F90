! ROUTINE: ATMOS
! %Z%GTOSS %M% H.7 code v01.02
!              H.7 code v01.02  Activated Jacchia atmosphere
!-----------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!****************************************************************
!****************************************************************
!
      SUBROUTINE ATMOS (LATM, LGEO, SIMTIM, POSE,  RHO,SS,TEMPK)
!
!****************************************************************
!****************************************************************
! PURPOSE OF SUBROUTINE
!       GENERALIZED BUFFER ROUTINE TO PROVIDE A TOSS INTERFACE TO
!       VARIOUS ATMOSPHERIC MODELS.

! INPUT ARGUMENTS ARE:
!       LATM   -  ATMOSPHERIC MODEL SIMULATION FIDELITY FLAG
!       LGEO   -  PLANET GLOBE MODEL SIMULATION FIDELITY FLAG
!       SIMTIM -  TIME INTO SIMULATION (FOR USE IN DIURNAL EFFECTS
!                 WHEN THEY ARE PRESENT AS A SIMULATION FEATURE)

!       POSE   -  POSITION VECTOR (X,Y,Z - FT) OF POINT WR/T
!                 PLANET FIXED FRAME DEFINED AS:
!                                  X - OUT GREENWICH
!                                  Z - GEODETIC NORTH
!                                  Y - COMPLETING TRIAD
!
! NOTE. IF AN ATMOSPHERE MODEL NEEDS CALENDER-DATE TYPE INFO, IT
!       IS AVAILABLE IN THE MASTER COMMON BLOCK, COM_RPS.i
!
!
! OUTPUT ARGUMENTS ARE:
!       RHO - DENSITY IN SLUG/FT**3
!       SS  - SPEED OF SOUND IN FT/SEC
!       TEMPK - TEMPERATURE IN DEGREES KELVIN

      include "../A_HDR/COM_ALL.h"

      DIMENSION POSE(3)

!--------------------------------------
!--------------------------------------
! DECIDE WHICH ATMOS MODEL IS REQUESTED
!--------------------------------------
!--------------------------------------
! SEE IF 1976 STANDARD ATMOSPHERE IS REQUESTED
      IF(LATM .EQ. 0)  GO TO 100

! SEE IF 1962 STANDARD ATMOSPHERE IS REQUESTED
      IF(LATM .EQ. 1)  GO TO 200

! SEE IF JACCHIA ATMOSPHERE IS REQUESTED
      IF(LATM .EQ. 2)  GO TO 300

! APPARENTLY NO KNOWN ATMOSPHERE IS CALLED, JUST LEAVE
      GO TO 1000



! VISIT 1976 STANDARD ATMOSPHERE
!-------------------------------
100   CALL ATM76 (LGEO, SIMTIM, POSE,  RHO,SS,TEMPK)

      GO TO 1000


! VISIT 1962 STANDARD ATMOSPHERE
!-------------------------------
200   CALL ATM62 (LGEO, SIMTIM, POSE,  RHO,SS,TEMPK)

      GO TO 1000


! VISIT A JACCHIA ATMOSPHERE
!---------------------------
300   CALL JACHIA (LGEO, SIMTIM, POSE,  RHO,SS,TEMPK)


! STANDARD RETURN
1000  CONTINUE

      RETURN
      END
