! ROUTINE: ATM76
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************************************
!*************************************************************
!
      SUBROUTINE ATM76 (LGEO, SIMTIM, POSE,  RHO,SS,TEMPK)
!
!*************************************************************
!*************************************************************
! THIS ROUTINE PROVIDES THE STANDARD ARDC 1976 ATMOSPHERE MODEL

      include "../A_HDR/COM_ALL.h"


      DIMENSION POSE(3)

! FEET TO KILOMETER CONVERSION
      DATA FTTOKM /0.3048E-3/

! SPECIFY SLUG-TO-LBS CONVERSION FACTOR
!--------------------------------------
      SLUGTOP = 32.1740487

! GET ALTITUDE ABOVE PLANET GLOBE
      CALL GEOD (LGEO,SIMTIM,POSE,  RGDUM,GLDUM,ALTGEO)

! CONVERT ALTITUDE TO KILOMETERS
      ALTKM = ALTGEO*FTTOKM

!--------------------------------
! INVOKE 1976 STANDARD ATMOSPHERE
!--------------------------------
! DEFINE ALLOWED DENSITY VARIATION (1.0 + PER CENT/100.)
      RHOERR = 0.0

! DEFINE BASE DENSITY AT 200 KM (LB/FT**3)
      RHOZRO = 1.5863E-11

      CALL ATMOS2(ALTKM,RHOZRO,RHOERR,   TEMPK,SS,RHOLBM)

! CONVERT DENSITY TO SLUG/FT**3
      RHO = RHOLBM/SLUGTOP

      RETURN
      END
