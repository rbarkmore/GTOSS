! ROUTINE: BOOMID
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!******************************************
!******************************************
!
      SUBROUTINE BOOMID (IUSDUM, IULDUM)
!
!******************************************
!******************************************
! THIS ROUTINE POPULATES THE RDB/GTOSS ASCII SCRATCH
! RUN ID FILE, AND THE GTOSS QUICK LOOK FILE WITH
! FLEX BOOM CONFIGURATION DATA

! INPUT ARGUMENTS
!
!       IUSDUM   I/O UINT NUMBER OF ASCII SCRATCH FILE
!       IULDUM   I/O UINT NUMBER OF QUICK LOOK FILE
!
! OUTPUT ARGUMENTS (NONE)



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_BOOM.h"





! PUT FLEX BOOM DATA ON SEPARATE PAGE
!------------------------------------
! ASSUME IMBEDDED FORM FEED IS ACCEPTABLE (FOR NOW)
      LFDUM = 0
      CALL NEWPAG(LFDUM, IUSDUM)
      CALL NEWPAG(LFDUM, IULDUM)

!****************************
! PUT TITLE ON FLEX BOOM PAGE
!****************************
      WRITE(IUSDUM,10)
      WRITE(IULDUM,10)
10    FORMAT(/5X,'>>>>>>>>>>>>>>>>>>  REFERENCE POINT FLEXIBLE',   &
     &            ' BOOM PARAMETERS  <<<<<<<<<<<<<<<<<<' )


!***********************
! DISPLAY MISC BOOM DATA
!***********************
      WRITE(IUSDUM,20) DELTBM, NAPBOM, NBOMY, NBOMZ, NOPT12, NOPTXY,   &
     &   BOOMAS, ELBOOM, CRU1, CRU2,   &
     &   THTBOM, PHIBOM, PSIBOM,   RBASEB(1), RBASEB(2), RBASEB(3)

      WRITE(IULDUM,20) DELTBM, NAPBOM, NBOMY, NBOMZ, NOPT12, NOPTXY,   &
     &   BOOMAS, ELBOOM, CRU1, CRU2,   &
     &   THTBOM, PHIBOM, PSIBOM,   RBASEB(1), RBASEB(2), RBASEB(3)

20    FORMAT(/5X,'CONTROL PARAMETERS:',//,   &
     &        5X, 8X, 'INTEG STEPSIZE (SEC) =',F11.5,   &
     &           11X, 'ATT PT SERVICED BY BOOM MODEL =',I2,//,   &
     &        5X, 8X, 'NUMBER OF Y MODAL COORDINATES =',I2,   &
     &           11X, 'NUMBER OF Z MODAL COORDINATES =',I2,//,   &
     &        5X, 8X, 'DUAL STIFFNESS OPT (ACTIVE=1):',I3,   &
     &           11X, 'Z->Y AXIS DATA OPT(ACTIVE =1):',I3,//,   &
     &        5X, 8X, 'BOOM TOTAL MASS (SLUGS) =',F8.2,   &
     &           11X, 'BOOM LENGTH (FT) =',F8.2,//,   &
     &        5X, 8X, '1ST MASS MOM (SLUG-FT) =',F9.1,   &
     &           11X, '2ND MASS MOM (SLUG-FT**2) =',F9.1//,   &
     &        5X, 8X, 'BOOM EULER ANGLES (DEG):',10X,'THT =',F8.2,   &
     &            5X, 'PHI =',F8.2, 5X, 'PSI =',F8.2,//,   &
     &        5X, 8X, 'BODY FRAME BOOM BASE COORDS (FT): XB  =',F8.2,&
     &            5X, 'YB  =',F8.2, 5X, 'ZB  =', F8.2 )



!****************************
! DISPLAY MODAL GEOMETRY DATA
!****************************
      WRITE(IUSDUM,30)
      WRITE(IULDUM,30)
30    FORMAT(//5X,'RESUME OF MODAL COORDINATE PROPERTIES:',//,   &

     &  5X, 21X, ' SMALL-DISPLACEMENT    LARGE-DISPLACEMENT   ',   &
     &           'COORDINATE    TIP MODAL    TIP MODAL   COUPLING',   &
     &           '    COUPLING',/,   &

     &  5X, 21X, '  FREQ     DAMPING      FREQ     DAMPING    ',   &
     &           'TRANSITION   DISPLACMENT     SLOPE     TERM CMM',   &
     &           '    TERM CMU',/,   &

     &  5X, 21X, '  (HZ)    (% CRIT)      (HZ)    (% CRIT)    ',   &
     &           '(NO UNITS)      (FT)        (FT/FT)   (SLUG-FT)',   &
     &           '   (SLUG-FT)',/,   &

     &  5X, 21X, '-------  ---------     -------  --------    ',   &
     &           '----------    ---------    --------    --------',   &
     &           '    --------',/)

! 1ST MODAL COORDINATE
!---------------------
      WRITE(IUSDUM,40) FREQY1(1), ZETAY1(1), FREQY2(1), ZETAY2(1),   &
     &                 QTRANY(1), HYL1(1), SYL1(1),  CMMY1(1), CMMUY1(1)
      WRITE(IULDUM,40) FREQY1(1), ZETAY1(1), FREQY2(1), ZETAY2(1),   &
     &                 QTRANY(1), HYL1(1), SYL1(1),  CMMY1(1), CMMUY1(1)
40    FORMAT(5X, 8X, 'Y MODE 1 =>', F9.3, F10.3, F13.3, F10.3, F12.2,   &
     &                              F13.3, F13.4, F11.2, F13.2 )

      WRITE(IUSDUM,41) FREQZ1(1), ZETAZ1(1), FREQZ2(1), ZETAZ2(1),   &
     &                 QTRANZ(1), HZL1(1), SZL1(1),  CMMZ1(1), CMMUZ1(1)
      WRITE(IULDUM,41) FREQZ1(1), ZETAZ1(1), FREQZ2(1), ZETAZ2(1),   &
     &                 QTRANZ(1), HZL1(1), SZL1(1),  CMMZ1(1), CMMUZ1(1)
41    FORMAT(5X, 8X, 'Z MODE 1 =>', F9.3, F10.3, F13.3, F10.3, F12.2,   &
     &                              F13.3, F13.4, F11.2, F13.2, / )


! 2ND MODAL COORDINATE
!---------------------
      WRITE(IUSDUM,50) FREQY1(2), ZETAY1(2), FREQY2(2), ZETAY2(2),   &
     &                 QTRANY(2), HYL1(2), SYL1(2),  CMMY1(2), CMMUY1(2)
      WRITE(IULDUM,50) FREQY1(2), ZETAY1(2), FREQY2(2), ZETAY2(2),   &
     &                 QTRANY(2), HYL1(2), SYL1(2),  CMMY1(2), CMMUY1(2)
50    FORMAT(5X, 8X, 'Y MODE 2 =>', F9.3, F10.3, F13.3, F10.3, F12.2,   &
     &                              F13.3, F13.4, F11.2, F13.2 )

      WRITE(IUSDUM,51) FREQZ1(2), ZETAZ1(2), FREQZ2(2), ZETAZ2(2),   &
     &                 QTRANZ(2), HZL1(2), SZL1(2),  CMMZ1(2), CMMUZ1(2)
      WRITE(IULDUM,51) FREQZ1(2), ZETAZ1(2), FREQZ2(2), ZETAZ2(2),   &
     &                 QTRANZ(2), HZL1(2), SZL1(2),  CMMZ1(2), CMMUZ1(2)
51    FORMAT(5X, 8X, 'Z MODE 2 =>', F9.3, F10.3, F13.3, F10.3, F12.2,   &
     &                              F13.3, F13.4, F11.2, F13.2, / )


! 3RD MODAL COORDINATE
!---------------------
      WRITE(IUSDUM,60) FREQY1(3), ZETAY1(3), FREQY2(3), ZETAY2(3),   &
     &                 QTRANY(3), HYL1(3), SYL1(3),  CMMY1(3), CMMUY1(3)
      WRITE(IULDUM,60) FREQY1(3), ZETAY1(3), FREQY2(3), ZETAY2(3),   &
     &                 QTRANY(3), HYL1(3), SYL1(3),  CMMY1(3), CMMUY1(3)
60    FORMAT(5X, 8X, 'Y MODE 3 =>', F9.3, F10.3, F13.3, F10.3, F12.2,   &
     &                              F13.3, F13.4, F11.2, F13.2 )

      WRITE(IUSDUM,61) FREQZ1(3), ZETAZ1(3), FREQZ2(3), ZETAZ2(3),   &
     &                 QTRANZ(3), HZL1(3), SZL1(3),  CMMZ1(3), CMMUZ1(3)
      WRITE(IULDUM,61) FREQZ1(3), ZETAZ1(3), FREQZ2(3), ZETAZ2(3),   &
     &                 QTRANZ(3), HZL1(3), SZL1(3),  CMMZ1(3), CMMUZ1(3)
61    FORMAT(5X, 8X, 'Z MODE 3 =>', F9.3, F10.3, F13.3, F10.3, F12.2,   &
     &                              F13.3, F13.4, F11.2, F13.2, / )


! 4TH MODAL COORDINATE
!---------------------
      WRITE(IUSDUM,70) FREQY1(4), ZETAY1(4), FREQY2(4), ZETAY2(4),   &
     &                 QTRANY(4), HYL1(4), SYL1(4),  CMMY1(4), CMMUY1(4)
      WRITE(IULDUM,70) FREQY1(4), ZETAY1(4), FREQY2(4), ZETAY2(4),   &
     &                 QTRANY(4), HYL1(4), SYL1(4),  CMMY1(4), CMMUY1(4)
70    FORMAT(5X, 8X, 'Y MODE 4 =>', F9.3, F10.3, F13.3, F10.3, F12.2,   &
     &                              F13.3, F13.4, F11.2, F13.2 )

      WRITE(IUSDUM,71) FREQZ1(4), ZETAZ1(4), FREQZ2(4), ZETAZ2(4),   &
     &                 QTRANZ(4), HZL1(4), SZL1(4),  CMMZ1(4), CMMUZ1(4)
      WRITE(IULDUM,71) FREQZ1(4), ZETAZ1(4), FREQZ2(4), ZETAZ2(4),   &
     &                 QTRANZ(4), HZL1(4), SZL1(4),  CMMZ1(4), CMMUZ1(4)
71    FORMAT(5X, 8X, 'Z MODE 4 =>', F9.3, F10.3, F13.3, F10.3, F12.2,   &
     &                              F13.3, F13.4, F11.2, F13.2, / )


! 5TH MODAL COORDINATE
!---------------------
      WRITE(IUSDUM,80) FREQY1(5), ZETAY1(5), FREQY2(5), ZETAY2(5),   &
     &                 QTRANY(5), HYL1(5), SYL1(5),  CMMY1(5), CMMUY1(5)
      WRITE(IULDUM,80) FREQY1(5), ZETAY1(5), FREQY2(5), ZETAY2(5),   &
     &                 QTRANY(5), HYL1(5), SYL1(5),  CMMY1(5), CMMUY1(5)
80    FORMAT(5X, 8X, 'Y MODE 5 =>', F9.3, F10.3, F13.3, F10.3, F12.2,   &
     &                              F13.3, F13.4, F11.2, F13.2 )

      WRITE(IUSDUM,81) FREQZ1(5), ZETAZ1(5), FREQZ2(5), ZETAZ2(5),   &
     &                 QTRANZ(5), HZL1(5), SZL1(5),  CMMZ1(5), CMMUZ1(5)
      WRITE(IULDUM,81) FREQZ1(5), ZETAZ1(5), FREQZ2(5), ZETAZ2(5),   &
     &                 QTRANZ(5), HZL1(5), SZL1(5),  CMMZ1(5), CMMUZ1(5)
81    FORMAT(5X, 8X, 'Z MODE 5 =>', F9.3, F10.3, F13.3, F10.3, F12.2,   &
     &                              F13.3, F13.4, F11.2, F13.2, / )


!******************************
! DISPLAY MODAL COUPLING MATRIX
!******************************
      WRITE(IUSDUM,100) ((CMMYZ1(I,J),J=1,5),I=1,5)
      WRITE(IULDUM,100) ((CMMYZ1(I,J),J=1,5),I=1,5)
100   FORMAT(/5X,'MODAL COUPLING MATRIX: CMMYZ(I,J)',//,   &
     &  5X, 26X, '(1,1)         (1,2)         (1,3)         (1,4)',   &
     &           '         (1,5)',//,   &
     &  5X, 6X,  'CMMYZ(1,1) ->', 5F14.5,/   &
     &  5X, 6X,  'CMMYZ(2,1) ->', 5F14.5,/   &
     &  5X, 6X,  'CMMYZ(3,1) ->', 5F14.5,/   &
     &  5X, 6X,  'CMMYZ(4,1) ->', 5F14.5,/   &
     &  5X, 6X,  'CMMYZ(5,1) ->', 5F14.5  )

      RETURN
      END
