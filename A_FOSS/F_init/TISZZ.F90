! ROUTINE: TISZZ
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************
!*************************************
!
      SUBROUTINE TISZZ
!
!*************************************
!*************************************
! THIS ROUTINE PERFORMS ONCE-ONLY INITIALIZATION
! FOR FINITE TETHER SOLUTIONS


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"

      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"



!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"






! SET LOWER ARRAY LIMIT TO START ZEROING (WORKING ARRAY)
!------------------------------------------------------
      NFJZZW = 4

! SET LOWER ARRAY LIMIT TO START ZEROING (FINITE SOL 1-9)
!-------------------------------------------------------
      NFJZZ1 = 4
      NFJZZ2 = 4
      NFJZZ3 = 4
      NFJZZ4 = 4
      NFJZZ5 = 4
      NFJZZ6 = 4
      NFJZZ7 = 4
      NFJZZ8 = 4
      NFJZZ9 = 4


!************************************************
! ZERO FINITE TETHER OFFICIAL STATE ARRAYS      *
!************************************************
      DO JFTETH = 1,NFSOLN

! ZERO THE FINITE TETHER WORKING AREA
! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
             CALL TISLDW(JFTETH)
             CALL ZERO_FIN_SOLN
      END DO

      RETURN
      END
