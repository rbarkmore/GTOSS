! ROUTINE: TISSV
! %Z%GTOSS %M% H.12 code v04.40
!              H.12 code v04.30 Pulled CALL TISLDW out of Late-Start logic
!-------------------------------------------------------------------------
!              H.11 code v04.30 Additional Finite Soln assign checks added
!---------------------------------------------------------------------------
!              H.10 code v04.20 Added non-uniform data initialization
!                               to support 15 non-uniform segs.
!                               Added backward compatibility consistency check.
!-----------------------------------------------------------------------------
!              H.9 code v04.10 Added initialization for resistivity.
!                              Added initialization for plasma circum.
!                              Changed units on RHOELC
!----------------------------------------------------------------------
!              H.7 code v04.02 Eliminated unused variable STGDUM
!                              Added code zeroing seg-specific current
!---------------------------------------------------------------------
!              H.5 code v04.01 Changed 3.14 to PIIALL from COM_ALL.i
!                              Fixed err in HST type 3 end select logic
!----------------------------------------------------------------------
!              H.4 code v04.00 Incorporated vG HST model into vH
!                              added evaluation of NXYHST (vG HST)
!                              added bead count test (vG HST model)
!------------------------------------------------------------------
!              H.3 code v03.00 removed FTOSS-local stepsize IC stuff
!-------------------------------------------------------------------
!              H.2 code v02.02 added ether factor initialization
!---------------------------------------------------------------
!              H.1 code v02.01 (baseline for vH delivery)
!--------------------------------------------------------
!                  code v01.01 (baseline for vG delivery)
!*************************************
!*************************************
!
      SUBROUTINE TISSV(JLATE)
!
!*************************************
!*************************************
! THIS ROUTINE PRESIDES OVER INITIALIZATION OF DYNAMIC
! AND OTHER ATTRIBUTES OF THE FINITE TETHER SOLUTIONS
! PRIOR TO EACH RUN

! THE FIRST SECTION OF THIS ROUTINE PERFORMS
! INITIALIZATION FUNCTIONS WHICH TRANSCEND SPECIFIC
! FINITE TETHERS (AND SOLN TYPES) BUT WHICH CAN DEPEND
! UPON RUN SPECIFIC INPUT DATA

! THE SECOND SECTION PERFORMS INITIALIZATION
! SPECIFIC TO EACH FINITE TETHER (AND THUS TO SOLN TYPE).
! TO DO THIS, IT STORES ALL DATA INTO THE FINITE TETHER
! WORKING ARRAY, ALL WITHIN A GIANT LOOP.  AT THE
! END OF THIS LOOP, IT STORES THE WORKING IMAGE
! INTO THE OFFICIAL AREA FOR THE REFERENCED FINITE SOLN.

! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS

! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS

      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.h"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.h"



!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"



! SPECIFY SLUG-TO-LBS CONVERSION FACTOR
!--------------------------------------
      SLUGTOP = 32.1740487

!---------------------------------------------
! CHECK VARIOUS ARRAY-SIZE-CRITICAL PARAMETERS
! AGAINST EXCEEDANCE OF CONFIGURED LIMITS
!---------------------------------------------
! CHECK AGAINST EXCEEDING MAX FINITE TETHER SOLUTIONS
      IF(NFINIT .GT. NFSOLN) GO TO 2000

! CHECK EXCEEDANCE OF FINITE SOLN PARAMETERS
!-------------------------------------------
      DO 2 JF=1,NFINIT
         JFDUM = JF
         IF(NBEADS(JF) .GT. MAXBBS) GO TO 2012
2     CONTINUE


!---------------------------------------------------
!---------------------------------------------------
! THIS SECTION DOES NON-SOLN SPECIFIC INITIALIZATION
!---------------------------------------------------
!---------------------------------------------------
! DONT EXECUTE DURING A LATE START INITIALIZATION
      IF(JLATE .NE. 1) THEN

! INVERT FINITE SOLN ASSIGNMENT MAP "LASIGN" (W/CHECKS)
         NFCNT = 0
         NFSUM = 0
         DO 10 JTETH = 1,NTETH
            IF(LASIGN(JTETH) .EQ. 0)  GO TO 10
                 JASIGN( LASIGN(JTETH) ) = JTETH
                 NFCNT = NFCNT + 1
                 NFSUM = NFSUM + LASIGN(JTETH)
10       CONTINUE

! CHECK CONSISTENCY OF FINITE TETHER SOLN ASSIGNMENT
         IF(NFCNT .GT. NFSOLN) GO TO 2002

! IS SUM OF ASSIGMENTS .GT. SUM OF 1ST NFSOLN INTEGERS
         NFCHK = NFSOLN*(NFSOLN+1)/2
         IF(NFSUM .GT. NFCHK) GO TO 2004

! ARE THERE ANY REPEATED ASSIGNMENTS
         DO 20 J = 1,NFINIT
         DO 20 K = 1,NFINIT
            IF(J .EQ. K) GO TO 20
               IF(JASIGN(J) .EQ. JASIGN(K)) GO TO 2006
20       CONTINUE

! HAS AN IN-ACTIVE FINITE SOLN BEEN ASSIGNED TO A TOSS TETHER
        DO 21 JTETH = 1,NTETH
           IF(LASIGN(JTETH) .GT. NFINIT) GO TO 2022
21       CONTINUE

! HAS THE SAME FINITE SOLN BEEN ASSIGNED TO MORE THAN ONE TOSS TETHER
        DO 22 JT1 = 1,NTETH
           DO 22 JT2 = 1,NTETH
              IF(JT1 .NE. JT2) THEN
                 IF((LASIGN(JT1).NE.0) .AND. (LASIGN(JT2).NE.0)) THEN
                    IF(LASIGN(JT1) .EQ. LASIGN(JT2)) GO TO 2024
                 END IF
              END IF
22       CONTINUE

      END IF


!-----------------------------------------------
!-----------------------------------------------
! THIS SECTION DOES SOLN SPECIFIC INITIALIZATION
!-----------------------------------------------
!-----------------------------------------------

!************************************************************
! DO BIG LOOP TO INITIALIZE EACH ACTIVE, ASSIGNED FINITE SOLN
!************************************************************
      DO 1000 JFTETH=1,NFINIT

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
         CALL TISLDW(JFTETH)

! FIND THE TOSS TETHER TO WHICH THIS FINITE SOLN IS ASSIGNED
         JTETH = JASIGN(JFTETH)

! IF IT IS NOT ASSIGNED TO A TOSS TETHER, THEN IGNORE IT
         IF(JTETH .EQ. 0) GO TO 1000

! ZERO THE FINITE TETHER WORKING AREA
! DONT EXECUTE THIS DURING A LATE START INITIALIZATION
!-----------------------------------------------------
         IF(JLATE .NE. 1) THEN
             CALL ZERO_FIN_SOLN
         END IF


!-----------------------------------------------------
! THIS SECTION TRANSFERS CONFIGURATION DATA FROM THE
! COM_TOSS INPUT ARRAYS TO THE CORRESPONDING FINITE SOLN
!-----------------------------------------------------

!-------------------------------------------------
! GEOMETRY DATA APPLICABLE TO ALL FINITE SOLUTIONS
!-------------------------------------------------
! SET IN FINITE SOLN TYPE FOR THIS SOLN
         NFTYP = NFTYPE(JFTETH)

! DISTRIBUTED EXTERNAL FORCE EVAL OPTIONS
         LOPGRV = LOPG
         LOPARO = LOPA
         LOPELC = LOPE

! DYNAMIC INITIALIZATION OPTIONS
         LFIC   = LFTIC(JFTETH)
         LSIC   = LSKRIC(JFTETH)
         LSMODE = LSKMOD(JFTETH)

         IF(LSMODE .EQ. 0) LSMODE = 1


!---------------------------------------------------------------
! DECIDE ON INPUT DATA TO DEFINE FINITE SOLN PHYSICAL PROPERTIES
!---------------------------------------------------------------
! FAILURE TO SPECIFY ANY ESSENTIAL DATA IS CONSIDERED GROUNDS FOR
! USING REGION 1 NON-UNIFORM DATA SPECS (FOR BACKWARD COMPATIBILITY)

! FORM SUM OF (ALL POSITIVE) ESSENTIAL PARAMETERS
         DUMTESJ = TTRHOF(JTETH) + TTDIEF(JTETH) + TTYNGF(JTETH)

         DUMTESF = RHOF(JFTETH) + DIAEFF(JFTETH) + YOUNGF(JFTETH)


! CHECK FOR POTENTIAL AMBIGUOUS USER INPUTS AND STOP THE SHOW IF NEEDED
         IF((DUMTESJ .NE. 0.0) .AND. (DUMTESF .NE. 0.0)) THEN

             IF(LNUPR(JTETH) .EQ. 0) GO TO 2020

          END IF


! TRIAGE PROPERTIES FOR BACKWARD COMPATIBILITY
         IF(DUMTESJ .EQ. 0.0) THEN

! USE REGION 1 NON-UNIFORM FINITE SOLN DATA TO DEFINE A UNIFORM TETHER
             RHOT   =     RHOF(JFTETH)/(SLUGTOP*1000.0)
             AREAT  = ((DIAEFF(JFTETH)/12.0)**2)*PIIALL/4.0
             YOUNGT =   YOUNGF(JFTETH)*144.0
             BETAT  =    BETAF(JFTETH)
             DIAROT =   DIAROF(JFTETH)/12.0
         ELSE

! USE ASSOCIATED TOSS TETHER NUMBERED DATA TO DEFINE A UNIFORM TETHER
! (NOTE:  TOSS TETHER RELATED DATA ALLOWS NON-LINEAR STIFFNESS TERMS)
             RHOT   =   TTRHOF(JTETH)/(SLUGTOP*1000.0)
             AREAT  = ((TTDIEF(JTETH)/12.0)**2)*PIIALL/4.0
             YOUNGT =   TTYNGF(JTETH)*144.0
             BETAT  =   TTBETF(JTETH)
             DIAROT =   TTDIAF(JTETH)/12.0
         END IF

! IN ANY CIRCUMSTANCE, ALLOW NON-LINEAR ELASTICITY TO BE USED
         DNLCS  =   TTNLCS(JTETH)
         DNLEX  =   TTNLEX(JTETH)

!--------------------------------
! SET IN OTHER MISCELLANEOUS DATA
!--------------------------------
! NOMINAL AREA*MODULUS (MAY BE PREEMPTED BY NON-UNIFORM-PROPERTIES)
         AYOUNG = AREAT*YOUNGT

! DONT EXECUTE DURING A LATE START INITIALIZATION
         IF(JLATE .NE. 1) THEN

! SET IN INITIAL VALUES OF DEPLOYED UNDEFORMED LENGTH DATA
              EL    = TLENO(JTETH)
              ELD   = 0.0
              ELDD  = 0.0

! SET IN INITIAL VALUE OF UNIFORM-CURRENT AMPERAGE COMPONENT
              CURRT = CURRO(JTETH)

! ZERO THE NON-UNIFORM COMPONENT OF SEG-SPECIFIC CURRENT
              DO 66 JSEG = 1,NBEAD+1
                    CURRTS(JSEG) = 0.0
66            CONTINUE

! SET IN TOSS-TETHER BREAK CRITERIA RELATED TO THIS FINITE SOLN
              NBRKTP = LBRKTP(JTETH)
              VALBRK = BRKVAL(JTETH)

! SET IN INITIAL VALUE OF ETHER DAMPING
              EFACT = EFACO(JFTETH)

          END IF


! SET IN INTEGRATION STEPSIZE
      DELTIS = RPDELT


!*****************************************
! DO GENERAL INITIALIZATION FOR BEAD MODEL
!*****************************************
! SET IN NUMBER OF BEADS FOR THIS FINITE SOLN
         NBEAD = NBEADS(JFTETH)

! CALC SOME OFT USED BUT CONSTANT PARAMETERS
         N3BEAD = 3*NBEAD
         BNSEG  = REAL(NBEAD+1)
         OBNSEG = 1.0/BNSEG

! SET IN SEG NUMBERS WHICH MAY SUFFER TETHER BREAK
         NS1BRK = NSBRK1(JFTETH)
         NS2BRK = NSBRK2(JFTETH)

! SET IN TIMES AT WHICH BREAK MAY OCCUR
         TS1BRK = TSBRK1(JFTETH)
         TS2BRK = TSBRK2(JFTETH)


!---------------------------------------------------
! DO STUFF RELATED TO HOMOGENEOUS STRAIN SOLN OPTION
!---------------------------------------------------

! WARN USER OF LESS THAN 4 BEAD REQUEST FOR HOMOGENEOUS STRAIN OPTION
!--------------------------------------------------------------------
         JFDUM = JFTETH
         IF((NFTYP .EQ. 3) .AND. (NBEAD.LT.4)) GO TO 2016


! SET FRAC ERROR LIMIT FOR SEG LEN RECONCILE FAILURE REPORT
! (ALLOWING USER INPUT TO PREEMPT)
!---------------------------------
         HSTFRL = 0.01
         IF(HSTLIM(JFTETH) .NE. 0.0) HSTFRL = HSTLIM(JFTETH)


! SET HST CONSTRAINT ERROR CONTROL FEEDBACK GAINS
!------------------------------------------------
! (TO ASSIST IN USERS INTERPRETATION, NORMALIZE USER INPUT
!  TO UNITY, AND ALLOW A SINGLE USER INPUT TO PREEMPT ALL SOLNS)
         DUMULE = 1.0
         DUMULR = 1.0
         IF(HSTFBE .NE. 0.0) DUMULE = HSTFBE
         IF(HSTFBR .NE. 0.0) DUMULR = HSTFBR

! NOTE: THE .6 AND .25 FACTORS WERE IMPIRICALLY DETERMINED
         HSTGNE = 0.60*DUMULE
         HSTGNR = 0.25*DUMULR


! FIGURE OUT THE TENSION REF END FOR THE HST SOLN
!------------------------------------------------
! FIND THE CONNECTIVITY DEFINITION FOR THIS TOSS TETHER
         JOX = NOBJX(JTETH)
         JOY = NOBJY(JTETH)

! FETCH MASS ON X-END OF THIS FINITE TETHER
         DUMASX = RPMAST
         IF(JOX .GT. 1) THEN
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
            CALL TOSLDW(JOX)
            DUMASX = DMASS
         END IF

! FETCH MASS ON Y-END OF THIS FINITE TETHER
         DUMASY = RPMAST
         IF(JOY .GT. 1) THEN
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
            CALL TOSLDW(JOY)
            DUMASY = DMASS
         END IF

! SPECIFY THE TENSION REF END TO BE AT THE END OF LEAST MASS
! (ASSUME OBJECT AT X-END OF TETHER HAS GREATEST MASS)
         IF(DUMASX .LE. DUMASY) NXYHST = 0
         IF(DUMASX .GT. DUMASY) NXYHST = 1

! ALLOW USER PREEMPT OF AUTO-ASSIGNMENT (NOT WIDELY ADVERTISED TO USER)
         IF(LHSTXY(JFTETH) .NE. 0) THEN
            IF(LHSTXY(JFTETH) .EQ. 1)   NXYHST = 0
            IF(LHSTXY(JFTETH) .EQ. 2)   NXYHST = 1
         END IF


!---------------------------------------------------------
! SET IN NON-UNIFORM TETHER PROPERTY DATA REGIONS (A UNION
! OF THE BASE VALUES AND MULTI-REGION DEFINITIONS)
!---------------------------------------------------------
! NUMBER OF NON-UNIFORM REGIONS USER CLAIMS TO HAVE DEFINED
         NNUPR = LNUPR(JFTETH)

! SET IN REQUESTED INTERPOLATION TYPE FOR EACH TYPE OF NON-UNIFORM DATA
         NNUITD = LNUIND(JFTETH)
         NNUITE = LNUINE(JFTETH)
         NNUITA = LNUINA(JFTETH)

! SET IN REGION-LENGTH REFERENCE FOR RECKONING NON-UNIFORM-PROPERTIES
         NNUREF = LNUPREF(JFTETH)

! MAKE SURE QUADRATIC INTERPOLATION IS NOT REQUESTED FOR 1 REGION
        IF(NNUPR .EQ. 1) THEN
           IF(NNUITD .EQ. 1) GO TO 2018
           IF(NNUITE .EQ. 1) GO TO 2018
           IF(NNUITA .EQ. 1) GO TO 2018
        END IF


! REGION 1 DATA
! NOTE: OLD FINITE SOLN DATA DEFINITIONS FOR UNIFORM TETHERS
!       ARE A PART OF THE REGION 1 DEFINITION
         TLETR1 = TLENR1(JFTETH)
         YNGTR1 = YOUNGF(JFTETH)*144.0
         BETFR1 =  BETAF(JFTETH)
         RHOTB1 =   RHOF(JFTETH)/(SLUGTOP*1000.0)
         RHOTE1 = RHOFE1(JFTETH)/(SLUGTOP*1000.0)
         ARELB1 = PIIALL*( (DIAEFF(JFTETH)/12.0)**2 )/4.0
         ARELE1 = PIIALL*( (DIELE1(JFTETH)/12.0)**2 )/4.0
         DAROB1 = DIAROF(JFTETH)/12.0
         DAROE1 = DIARE1(JFTETH)/12.0

! REGION 2
         TLETR2 = TLENR2(JFTETH)
         YNGTR2 = YONGR2(JFTETH)*144.0
         BETFR2 = BETAR2(JFTETH)
         RHOTB2 = RHOFB2(JFTETH)/(SLUGTOP*1000.0)
         RHOTE2 = RHOFE2(JFTETH)/(SLUGTOP*1000.0)
         ARELB2 = PIIALL*( (DIELB2(JFTETH)/12.0)**2 )/4.0
         ARELE2 = PIIALL*( (DIELE2(JFTETH)/12.0)**2 )/4.0
         DAROB2 = DIARB2(JFTETH)/12.0
         DAROE2 = DIARE2(JFTETH)/12.0

! REGION 3
         TLETR3 = TLENR3(JFTETH)
         YNGTR3 = YONGR3(JFTETH)*144.0
         BETFR3 = BETAR3(JFTETH)
         RHOTB3 = RHOFB3(JFTETH)/(SLUGTOP*1000.0)
         RHOTE3 = RHOFE3(JFTETH)/(SLUGTOP*1000.0)
         ARELB3 = PIIALL*( (DIELB3(JFTETH)/12.0)**2 )/4.0
         ARELE3 = PIIALL*( (DIELE3(JFTETH)/12.0)**2 )/4.0
         DAROB3 = DIARB3(JFTETH)/12.0
         DAROE3 = DIARE3(JFTETH)/12.0

! REGION 4
         TLETR4 = TLENR4(JFTETH)
         YNGTR4 = YONGR4(JFTETH)*144.0
         BETFR4 = BETAR4(JFTETH)
         RHOTB4 = RHOFB4(JFTETH)/(SLUGTOP*1000.0)
         RHOTE4 = RHOFE4(JFTETH)/(SLUGTOP*1000.0)
         ARELB4 = PIIALL*( (DIELB4(JFTETH)/12.0)**2 )/4.0
         ARELE4 = PIIALL*( (DIELE4(JFTETH)/12.0)**2 )/4.0
         DAROB4 = DIARB4(JFTETH)/12.0
         DAROE4 = DIARE4(JFTETH)/12.0

! REGION 5
         TLETR5 = TLENR5(JFTETH)
         YNGTR5 = YONGR5(JFTETH)*144.0
         BETFR5 = BETAR5(JFTETH)
         RHOTB5 = RHOFB5(JFTETH)/(SLUGTOP*1000.0)
         RHOTE5 = RHOFE5(JFTETH)/(SLUGTOP*1000.0)
         ARELB5 = PIIALL*( (DIELB5(JFTETH)/12.0)**2 )/4.0
         ARELE5 = PIIALL*( (DIELE5(JFTETH)/12.0)**2 )/4.0
         DAROB5 = DIARB5(JFTETH)/12.0
         DAROE5 = DIARE5(JFTETH)/12.0

! REGION 6
         TLETR6 = TLENR6(JFTETH)
         YNGTR6 = YONGR6(JFTETH)*144.0
         BETFR6 = BETAR6(JFTETH)
         RHOTB6 = RHOFB6(JFTETH)/(SLUGTOP*1000.0)
         RHOTE6 = RHOFE6(JFTETH)/(SLUGTOP*1000.0)
         ARELB6 = PIIALL*( (DIELB6(JFTETH)/12.0)**2 )/4.0
         ARELE6 = PIIALL*( (DIELE6(JFTETH)/12.0)**2 )/4.0
         DAROB6 = DIARB6(JFTETH)/12.0
         DAROE6 = DIARE6(JFTETH)/12.0

! REGION 7
         TLETR7 = TLENR7(JFTETH)
         YNGTR7 = YONGR7(JFTETH)*144.0
         BETFR7 = BETAR7(JFTETH)
         RHOTB7 = RHOFB7(JFTETH)/(SLUGTOP*1000.0)
         RHOTE7 = RHOFE7(JFTETH)/(SLUGTOP*1000.0)
         ARELB7 = PIIALL*( (DIELB7(JFTETH)/12.0)**2 )/4.0
         ARELE7 = PIIALL*( (DIELE7(JFTETH)/12.0)**2 )/4.0
         DAROB7 = DIARB7(JFTETH)/12.0
         DAROE7 = DIARE7(JFTETH)/12.0

! REGION 8
         TLETR8 = TLENR8(JFTETH)
         YNGTR8 = YONGR8(JFTETH)*144.0
         BETFR8 = BETAR8(JFTETH)
         RHOTB8 = RHOFB8(JFTETH)/(SLUGTOP*1000.0)
         RHOTE8 = RHOFE8(JFTETH)/(SLUGTOP*1000.0)
         ARELB8 = PIIALL*( (DIELB8(JFTETH)/12.0)**2 )/4.0
         ARELE8 = PIIALL*( (DIELE8(JFTETH)/12.0)**2 )/4.0
         DAROB8 = DIARB8(JFTETH)/12.0
         DAROE8 = DIARE8(JFTETH)/12.0

! REGION 9
         TLETR9 = TLENR9(JFTETH)
         YNGTR9 = YONGR9(JFTETH)*144.0
         BETFR9 = BETAR9(JFTETH)
         RHOTB9 = RHOFB9(JFTETH)/(SLUGTOP*1000.0)
         RHOTE9 = RHOFE9(JFTETH)/(SLUGTOP*1000.0)
         ARELB9 = PIIALL*( (DIELB9(JFTETH)/12.0)**2 )/4.0
         ARELE9 = PIIALL*( (DIELE9(JFTETH)/12.0)**2 )/4.0
         DAROB9 = DIARB9(JFTETH)/12.0
         DAROE9 = DIARE9(JFTETH)/12.0

! REGION 10
         TLETR10 = TLENR10(JFTETH)
         YNGTR10 = YONGR10(JFTETH)*144.0
         BETFR10 = BETAR10(JFTETH)
         RHOTB10 = RHOFB10(JFTETH)/(SLUGTOP*1000.0)
         RHOTE10 = RHOFE10(JFTETH)/(SLUGTOP*1000.0)
         ARELB10 = PIIALL*( (DIELB10(JFTETH)/12.0)**2 )/4.0
         ARELE10 = PIIALL*( (DIELE10(JFTETH)/12.0)**2 )/4.0
         DAROB10 = DIARB10(JFTETH)/12.0
         DAROE10 = DIARE10(JFTETH)/12.0

! REGION 11
         TLETR11 = TLENR11(JFTETH)
         YNGTR11 = YONGR11(JFTETH)*144.0
         BETFR11 = BETAR11(JFTETH)
         RHOTB11 = RHOFB11(JFTETH)/(SLUGTOP*1000.0)
         RHOTE11 = RHOFE11(JFTETH)/(SLUGTOP*1000.0)
         ARELB11 = PIIALL*( (DIELB11(JFTETH)/12.0)**2 )/4.0
         ARELE11 = PIIALL*( (DIELE11(JFTETH)/12.0)**2 )/4.0
         DAROB11 = DIARB11(JFTETH)/12.0
         DAROE11 = DIARE11(JFTETH)/12.0

! REGION 12
         TLETR12 = TLENR12(JFTETH)
         YNGTR12 = YONGR12(JFTETH)*144.0
         BETFR12 = BETAR12(JFTETH)
         RHOTB12 = RHOFB12(JFTETH)/(SLUGTOP*1000.0)
         RHOTE12 = RHOFE12(JFTETH)/(SLUGTOP*1000.0)
         ARELB12 = PIIALL*( (DIELB12(JFTETH)/12.0)**2 )/4.0
         ARELE12 = PIIALL*( (DIELE12(JFTETH)/12.0)**2 )/4.0
         DAROB12 = DIARB12(JFTETH)/12.0
         DAROE12 = DIARE12(JFTETH)/12.0

! REGION 13
         TLETR13 = TLENR13(JFTETH)
         YNGTR13 = YONGR13(JFTETH)*144.0
         BETFR13 = BETAR13(JFTETH)
         RHOTB13 = RHOFB13(JFTETH)/(SLUGTOP*1000.0)
         RHOTE13 = RHOFE13(JFTETH)/(SLUGTOP*1000.0)
         ARELB13 = PIIALL*( (DIELB13(JFTETH)/12.0)**2 )/4.0
         ARELE13 = PIIALL*( (DIELE13(JFTETH)/12.0)**2 )/4.0
         DAROB13 = DIARB13(JFTETH)/12.0
         DAROE13 = DIARE13(JFTETH)/12.0

! REGION 14
         TLETR14 = TLENR14(JFTETH)
         YNGTR14 = YONGR14(JFTETH)*144.0
         BETFR14 = BETAR14(JFTETH)
         RHOTB14 = RHOFB14(JFTETH)/(SLUGTOP*1000.0)
         RHOTE14 = RHOFE14(JFTETH)/(SLUGTOP*1000.0)
         ARELB14 = PIIALL*( (DIELB14(JFTETH)/12.0)**2 )/4.0
         ARELE14 = PIIALL*( (DIELE14(JFTETH)/12.0)**2 )/4.0
         DAROB14 = DIARB14(JFTETH)/12.0
         DAROE14 = DIARE14(JFTETH)/12.0

! REGION 15
         TLETR15 = TLENR15(JFTETH)
         YNGTR15 = YONGR15(JFTETH)*144.0
         BETFR15 = BETAR15(JFTETH)
         RHOTB15 = RHOFB15(JFTETH)/(SLUGTOP*1000.0)
         RHOTE15 = RHOFE15(JFTETH)/(SLUGTOP*1000.0)
         ARELB15 = PIIALL*( (DIELB15(JFTETH)/12.0)**2 )/4.0
         ARELE15 = PIIALL*( (DIELE15(JFTETH)/12.0)**2 )/4.0
         DAROB15 = DIARB15(JFTETH)/12.0
         DAROE15 = DIARE15(JFTETH)/12.0

! ENFORCE CONSISTENT USER DEFINITION NON-UNIFORM REGIONS
!-------------------------------------------------------

! CALC ERROR DETECTORS USED TO DETECT BOGUS REGION DATA ATTRIBUTES
        DUMR1  = TLETR1  *YNGTR1  *RHOTB1  *RHOTE1  *ARELB1  *ARELE1
        DUMR2  = TLETR2  *YNGTR2  *RHOTB2  *RHOTE2  *ARELB2  *ARELE2
        DUMR3  = TLETR3  *YNGTR3  *RHOTB3  *RHOTE3  *ARELB3  *ARELE3
        DUMR4  = TLETR4  *YNGTR4  *RHOTB4  *RHOTE4  *ARELB4  *ARELE4
        DUMR5  = TLETR5  *YNGTR5  *RHOTB5  *RHOTE5  *ARELB5  *ARELE5
        DUMR6  = TLETR6  *YNGTR6  *RHOTB6  *RHOTE6  *ARELB6  *ARELE6
        DUMR7  = TLETR7  *YNGTR7  *RHOTB7  *RHOTE7  *ARELB7  *ARELE7
        DUMR8  = TLETR8  *YNGTR8  *RHOTB8  *RHOTE8  *ARELB8  *ARELE8
        DUMR9  = TLETR9  *YNGTR9  *RHOTB9  *RHOTE9  *ARELB9  *ARELE9
        DUMR10 = TLETR10 *YNGTR10 *RHOTB10 *RHOTE10 *ARELB10 *ARELE10
        DUMR11 = TLETR11 *YNGTR11 *RHOTB11 *RHOTE11 *ARELB11 *ARELE11
        DUMR12 = TLETR12 *YNGTR12 *RHOTB12 *RHOTE12 *ARELB12 *ARELE12
        DUMR13 = TLETR13 *YNGTR13 *RHOTB13 *RHOTE13 *ARELB13 *ARELE13
        DUMR14 = TLETR14 *YNGTR14 *RHOTB14 *RHOTE14 *ARELB14 *ARELE14
        DUMR15 = TLETR15 *YNGTR15 *RHOTB15 *RHOTE15 *ARELB15 *ARELE15

! NOW CHECK REGION DATA FOR CONSISTENCY (MAYBE WRITE ERROR MESSAGE)
        NRDUM = 1
        IF((NNUPR.GT.0) .AND. (DUMR1 .LE. 0.0)) GO TO 2014
        NRDUM = 2
        IF((NNUPR.GT.1) .AND. (DUMR2 .LE. 0.0)) GO TO 2014
        NRDUM = 3
        IF((NNUPR.GT.2) .AND. (DUMR3 .LE. 0.0)) GO TO 2014
        NRDUM = 4
        IF((NNUPR.GT.3) .AND. (DUMR4 .LE. 0.0)) GO TO 2014
        NRDUM = 5
        IF((NNUPR.GT.4) .AND. (DUMR5 .LE. 0.0)) GO TO 2014
        NRDUM = 6
        IF((NNUPR.GT.5) .AND. (DUMR6 .LE. 0.0)) GO TO 2014
        NRDUM = 7
        IF((NNUPR.GT.6) .AND. (DUMR7 .LE. 0.0)) GO TO 2014
        NRDUM = 8
        IF((NNUPR.GT.7) .AND. (DUMR8 .LE. 0.0)) GO TO 2014
        NRDUM = 9
        IF((NNUPR.GT.8) .AND. (DUMR9 .LE. 0.0)) GO TO 2014
        NRDUM = 10
        IF((NNUPR.GT.9) .AND. (DUMR10 .LE. 0.0)) GO TO 2014
        NRDUM = 11
        IF((NNUPR.GT.10) .AND. (DUMR11 .LE. 0.0)) GO TO 2014
        NRDUM = 12
        IF((NNUPR.GT.11) .AND. (DUMR12 .LE. 0.0)) GO TO 2014
        NRDUM = 13
        IF((NNUPR.GT.12) .AND. (DUMR13 .LE. 0.0)) GO TO 2014
        NRDUM = 14
        IF((NNUPR.GT.13) .AND. (DUMR14 .LE. 0.0)) GO TO 2014
        NRDUM = 15
        IF((NNUPR.GT.14) .AND. (DUMR15 .LE. 0.0)) GO TO 2014


! SET IN FINITE SOLN DRAG COEF MULTIPLIER FOR SUBSONIC AERO
!----------------------------------------------------------
! DEFAULT DRAG COEFFICIENT TO 1.0 IF USER DOESN'T ENTER A VALUE
      DRGMUL = 1.0
      IF(FACSDRG(JFTETH) .NE. 0.0) DRGMUL = FACSDRG(JFTETH)



! SET IN THERMAL AND ELECTRODYNAMIC PROPERTIES
!---------------------------------------------
! HEAT CONDUCTIVITY OF TETHER PER UNIT AREA
      AKHC = AKHCI(JFTETH)

! COEFF OF THERMAL EXPANSION (LINEAR NOT VOLUMETRIC)
      ALPHAL = ALPHIL(JFTETH)

! ABSORPTIVITY IN SOLAR SPECTRUM
      ALPHAS = ALPHIS(JFTETH)

! EMISSIVITY IN RADIANT SPECTRUM
      EPSS = EPSSI(JFTETH)

! SPECIFIC HEAT PER UNIT MASS
      CVMASS = CVMASI(JFTETH)

! SLOPE OF SPECIFIC HEAT WR/T TEMP
      DCVMDT = DCVMDI(JFTETH)

! SLOPE OF YOUNGS MODULUS WR/T TEMP
      DYMDT = DYMDTI(JFTETH)

! SLOPE OF DAMPING WR/T TEMP
      DBTADT = DBTADI(JFTETH)


! CONVERT INTRINSIC RESISTIVITY TO RESISTANCE PER UNIT LENGTH
!------------------------------------------------------------
! NOTE: CONSISTENT WITH THE THERMAL/ELECTRIC DATA, THIS IS ENTERED
!       AS METRIC, BUT CONVERTED HERE TO ENGLISH FOR INTERNAL USE.
!       THIS IS THE EXCEPTION FOR THE ELECTRODYNAMIC DATA.

      RHOELC = 0.3048*RHOELI(JFTETH)/ACONDI(JFTETH)

! INITIALIZE TEMPERATURE COMPENSATED ARRAY IN CASE THERMAL IS NOT ON
      DO JSEG = 1,NBEAD+1
         RHOELT(JSEG) = RHOELC
      END DO


! SLOPE OF RESISTIVITY WR/T TEMP
      DRHODT = DRHODI(JFTETH)

! HEAT CONDUCTIVITY OF X-END ATTACH POINT TO TETHER
      AKHCX = AKHCXI(JFTETH)

! HEAT CONDUCTIVITY OF Y-END ATTACH POINT TO TETHER
      AKHCY = AKHCYI(JFTETH)

! THERMAL PROPERTIES BASELINE TEMPERATURE
      TREF = TREFI(JFTETH)


! ELECTRODYNAMIC PLASMA CONTACT CIRCUMFERENCE (LENGTH)
!-----------------------------------------------------
      EDYNC = EDYNCI(JFTETH)

! INITIALIZE TEMPERATAURE COMPENSATED ARRAY IN CASE THERMAL IS NOT ON
      DO JSEG = 1,NBEAD+1
         EDYNCT(JSEG) = EDYNC
      END DO



! INITIALIZE SEGMENT TEMPERATURES (IF NOT A LATE START)
      IF(JLATE .NE. 1) THEN
          DO 77 JSEG = 1,NBEAD+1
                STEMPK(JSEG) = TREF
77        CONTINUE
      END IF



! SET UP TETHER FRAME (AND GEOMETRY NEEDED FOR FURTHER IC WORK)
!--------------------------------------------------------------
! (ORDER OF CALLS HERE IS IMPORTANT)
        CALL TISUTL (JTETH)
        CALL TISFRM
        CALL TNSMKS

!--------------------------------------------------------------
! ZERO ALL BEAD STATE COORDS FOR OTHER IC OPTIONS TO MANIPULATE
!--------------------------------------------------------------
! DONT EXECUTE DURING A LATE START INITIALIZATION
       IF(JLATE .NE. 1) THEN
             DO 100 J=1,N3BEAD
                BUT(J)  = 0.0
                BUDT(J) = 0.0
                BUI(J)  = 0.0
                BUDI(J) = 0.0
100          CONTINUE
       END IF


!--------------------------------------------------
! MOVE CERTAIN FINITE SOLN VARIABLES TO GENERAL TOSS COMMON
! DO THIS AS LAST ACT IN THIS LOOP
!--------------------------------------------------
      CALL TISSTW(JFTETH)

!**********************
! END OF GINORMOUS LOOP
!**********************
1000  CONTINUE

      RETURN


!--------------------------------------
! INPUT DATA ERROR DIAGNOSTIC REPORTING
!--------------------------------------
2000  WRITE(LUE,2001) NFSOLN
2001  FORMAT('TISSV: FATAL; LARGEST ACTIVE FINITE SOLN NO.',   &
     &       ' EXCEEDS MAX ALLOWED OF',I2)
      STOP 'IN TISSV-1'

2002  WRITE(LUE,2003) NFSOLN
2003  FORMAT('TISSV: FATAL; MORE FINITE SOLNS ARE ASSIGNED THAN',   &
     &       ' MAX ALLOWED OF',I2)
      STOP 'IN TISSV-2'

2004  WRITE(LUE,2005)
2005  FORMAT('TISSV: FATAL; GOT AN ANOMOLY IN YOUR ',   &
     &       'FINITE SOLN ASSIGMENT TO TOSS TETHERS')
      STOP 'IN TISSV-3'

2006  WRITE(LUE,2007)
2007  FORMAT('TISSV: FATAL; AN ACTIVE FINITE SOLN HAS BEEN',   &
     &       ' ASSIGNED TO MORE THAN 1 TOSS TETHER')
      STOP 'IN TISSV-4'

2012  WRITE(LUE,2013) JFDUM, MAXBBS
2013  FORMAT('TISSV: FATAL; FINITE SOLN',I2,' HAS EXCEEDED',   &
     &       ' MAX NO. BEADS OF',I3)
      STOP 'IN TISSV-7'

2014  WRITE(LUE,2015) JFDUM, NRDUM
2015  FORMAT('TISSV: FATAL; FINITE SOLN',I2,' BAD NON-UNIFORM',   &
     &       ' TETHER DEFN, REGION',I3)
      STOP 'IN TISSV-8'

2016  WRITE(LUE,2017) JFDUM
2017  FORMAT('TISSV: FATAL; FINITE SOLN',I2,' TYPE 3 HST SOLN ',   &
     &       ' OPTION REQUIRES AT LEAST 4 BEADS ')
      STOP 'IN TISSV-9'

2018  WRITE(LUE,2019) JFDUM
2019  FORMAT('TISSV: FATAL; FINITE SOLN',I2,' QUADRATIC INTERPOLATION',   &
     &      ' REQUEST W/ONLY 1 NON-UNIFORM REGION DEFINITION',/   &
     &  13X,'QUADRATIC INTERP REQUIRES AT LEAST 2 NON-UNIFORM REGIONS')
      STOP 'IN TISSV-10'

2020  WRITE(LUE,2021) JFDUM
2021  FORMAT('TISSV: FATAL; FINITE SOLN',I2,' BOTH MASSLESS & FINITE',   &
     &      ' SOLUTION TETHER CHARACTERISTICS SHOULD NOT BE ENTERED',/   &
     &  13X,' AS IT IS AMBIGUOUS FOR FTOSS')
      STOP 'IN TISSV-11'

2022  WRITE(LUE,2023)
2023  FORMAT('TISSV: FATAL; AN INACTIVE FINITE SOLUTION HAS BEEN',   &
     &       ' ASSIGNED TO A TOSS TETHER' )
      STOP 'IN TISSV-12'

2024  WRITE(LUE,2025)
2025  FORMAT('TISSV: FATAL; THE SAME FINITE SOLUTION HAS BEEN ',   &
     &       'ASSIGNED TO MORE THAN ONE TOSS TETHER' )
      STOP 'IN TISSV-13'


      END
