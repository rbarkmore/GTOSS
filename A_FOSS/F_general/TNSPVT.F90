! ROUTINE: TNSPVT
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vH1 inertial bead model)
!********************************
!********************************
!
      SUBROUTINE TNSPVT(JBEAD)
!
!********************************
!********************************
! THIS ROUTINE CALCS THE POS AND VEL OF A BEAD
! WR/T TO THE INERTIAL POINT AND EXPRESSED AS
! COMPONENTS IN THE INERTIAL FRAME

!------------------------------------------
! THIS ROUTINE ASSUMES BEAD STATE IS IN THE
! TETHER FRAME AND IS USED ONLY DURING GGIC
!------------------------------------------

! INPUT: JBEAD = THE BEADS NUMBER
!
! THE ROUTINE ALSO USES GENERAL BEAD STATE COMMON
! AS INPUT

! OUTPUT: RYENDI = THE POSITION OF THE BEAD WR/T
!                  THE INERTIAL PT EXPRESSED AS
!                  COMPONENTS IN INER FRAME
!
! OUTPUT: VYENDI = THE VELOCITY OF THE BEAD WR/T
!                  THE INERTIAL PT EXPRESSED AS
!                  COMPONENTS IN INER FRAME
!
! NOTE: JBEAD IS ALLOWED TO EQUAL NBEAD+1 (THERE
!       IS NO BEAD HERE, BUT IT WOULD CONCEPTUALLY
!       BE THE Y-END ATTACH PT, HENCE THE ROUTINE
!       RETURNS THE Y-END ATTACH PT STATE IN THIS CASE)


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"





      DIMENSION DUMRUT(3), DUMVUT(3)

! FIRST, CALC POINTERS TO BEAD STATE CORRESPONDING TO JBEAD
!----------------------------------------------------------
      JB1 = 3*(JBEAD-1) + 1
      JB2 = JB1+1
      JB3 = JB1+2

!------------------------------------------------------------
! NOW START FORMING POS,VEL WR/T INER PT FOR Y-END OF SEGMENT
!------------------------------------------------------------

! CALC SEGMENTS Y-END POS WR/T TETHER REF PT
!-------------------------------------------
! SEE IF CALLER WANTS FICTIOUS BEAD POS AT EXTREME Y-END
      IF(JBEAD .EQ. NBEAD+1) THEN

! USE Y-END ATTACH PT POSITION FOR FICTIOUS BEAD AT Y-END
           DUMRUT(1) = ELMG
           DUMRUT(2) = 0.0
           DUMRUT(3) = 0.0

! INTERIOR POINT POS CALC
      ELSE
           DUMRUT(1) = BUT(JB1) + REAL(JBEAD)*ELSGX
           DUMRUT(2) = BUT(JB2)
           DUMRUT(3) = BUT(JB3)

      END IF


! CALC SEGMENTS Y-END VEL WR/T TETHER REF PT
!-------------------------------------------
! FIRST, GET TETHER FRAME ROTATION COMP OF VEL (SAME FOR ALL CASES)
      CALL CROSS(OMT,DUMRUT,  DUMVUT)

! SEE IF CALLER WANTS FICTIOUS BEAD VEL AT EXTREME Y-END
      IF(JBEAD .EQ. NBEAD+1) THEN

! USE Y-END ATTACH PT VEL FOR FICTIOUS BEAD AT Y-END
           DUMVUT(1) = DUMVUT(1) + ELMGD

! INTERIOR POINT VEL CALC
      ELSE
           DUMVUT(1) = DUMVUT(1) + BUDT(JB1) + REAL(JBEAD)*ELSGXD
           DUMVUT(2) = DUMVUT(2) + BUDT(JB2)
           DUMVUT(3) = DUMVUT(3) + BUDT(JB3)

      END IF


! CALC SEGMENTS Y-END POS,VEL WR/T INER PT (INER FRAME)
!------------------------------------------------------
      CALL MATVEC(0,GIT,DUMRUT,     RUI)
      CALL VECSUM(RUI,ARI,    RYENDI)

      CALL MATVEC(0,GIT,DUMVUT,     VUI)
      CALL VECSUM(VUI,AVI,    VYENDI)


      RETURN
      END
