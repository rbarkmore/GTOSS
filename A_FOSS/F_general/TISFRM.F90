! ROUTINE: TISFRM
! %Z%GTOSS %M% H.1 code v02.01 (baseline for vH1 inertial bead model)
!*************************************
!*************************************
!
      SUBROUTINE TISFRM
!
!*************************************
!*************************************
! THIS ROUTINE PERFORMS CALCULATION TO DEFINE THE
! TETHER FRAME AND ITS KINEMATIC ATTRIBUTES

! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS

      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_RPS.h"
      include "../../A_HDR/COM_TOSS.h"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"



!*********************************************************
! FORM TETHER FRAME UNIT VECTORS AND TRANSFORMATION MATRIX
!*********************************************************
! NORMALIZE POS VECTOR TO FORM TETHER FRAME "I" UNIT VECTOR
       CALL VECNRM (ELI,  UITI)

! EXTRACT COMP OF UNIT VEC NORMAL TO TOSS REF PT ORBITAL PLANE
       UJOI(1) = RPGOI(2,1)
       UJOI(2) = RPGOI(2,2)
       UJOI(3) = RPGOI(2,3)

! FORM VECTOR IN DIRECTION OF TETHER FRAME "K" UNIT VECTOR
       CALL CROSS (UITI,UJOI,  UKTI)

! SAVE MAGNITUDE FOR LATER USE
       DUMZDA = VECMAG (UKTI)

! CHECK FOR PATHOLOGICAL AXIS ALIGNMENTS
       IF(DUMZDA .EQ. 0.0) GO TO 2000

! NORMALIZE TO FORM TETHER FRAME "K" UNIT VECTOR
       CALL VECNRM (UKTI,  UKTI)

! FORM TETHER FRAME "J" UNIT VECTOR
       CALL CROSS (UKTI,UITI,  UJTI)

! FORM ELEMENTS OF TETHER FRAME TO INER FRAME XFORMATION
       GIT(1,1) = UITI(1)
       GIT(2,1) = UITI(2)
       GIT(3,1) = UITI(3)

       GIT(1,3) = UKTI(1)
       GIT(2,3) = UKTI(2)
       GIT(3,3) = UKTI(3)

       GIT(1,2) = UJTI(1)
       GIT(2,2) = UJTI(2)
       GIT(3,2) = UJTI(3)


!**********************************************
! EVALUATE MISCELLANEOUS ATTACH POINT VARIABLES
!**********************************************
! FIND DISTANCE BETWEEN ATT PTS (AND DERIVATIVES)
       ELMG = VECMAG(ELI)
       OOLMG  = 1.0/ELMG
       ELMGD  = DOT (ELI,ELDI) * OOLMG
       ELMGDD = ( DOT(ELDI,ELDI) + DOT(ELI,ELDDI) - ELMGD**2 ) * OOLMG

! FIND GROSS (LINE-OF-SIGHT) STRETCH
       DELMEL = ELMG - EL

! FIND TETHER FRAME COMP OF REL VEL AND ACCEL BETWEEN ATT PTS
       CALL MATVEC (1,GIT,ELDI,   ELDT)

! FORM TETHER FRAME TO TOSS REF PT ORB FRAME XFORMATION
       CALL MATMUL (3,GIT,RPGOI,   GTO)



!*****************************************************
! EVALUATE ANGULAR VELOCITY COMPONENTS OF TETHER FRAME
!*****************************************************
! EVAL ANGULAR MOMENTUM DERIVATIVE TERM
       CALL CROSS (RPI,RPIDD,  RVDI)

! TRANSFORM THIS TO TOSS REF PT ORB FRAME
       CALL MATVEC (0,RPGOI,RVDI,    RVDO)


! FORM DERIVATIVE OF TOSS REF PT "JO" UNIT VECTOR
!------------------------------------------------
! FIND MAG OF TOSS REF PT ANGULAR MOMENTUM (IN SCRATCH AREA)
      CALL CROSS (RPI,RPID,  TOSVX1)
      DUMMAG = VECMAG (TOSVX1)

! FORM COMPONENTS OF DERV OF "JO" UNIT VEC
      UJDO(1) = - RVDO(1)/DUMMAG
      UJDO(2) =   0.0
      UJDO(3) = - RVDO(3)/DUMMAG

! GET TETHER FRAME COMPONENTS OF DERV OF "JO"
      CALL MATVEC (0,GTO,UJDO,   UJDT)

! FINALLY, THE TETHER FRAME ANGULAR RATES
      OMT(2) = -ELDT(3) * OOLMG
      OMT(3) =  ELDT(2) * OOLMG
      OMT(1) =  (GTO(1,2)*OMT(2) + UJDT(3))/DUMZDA


!************************************************************
! FORM DERIVATIVES OF UNIT VECTOR ALONG LINE OF ATTACH POINTS
!************************************************************

! FORM DERIVATIVE OF VECTOR, UITI
!--------------------------------
      CALL VECSCL (-ELMGD, UITI,    TOSVX1)
      CALL VECSUM (ELDI, TOSVX1,    TOSVX2)
      CALL VECSCL (OOLMG,TOSVX2,    UITDI)

! FORM DERIVATIVE OF VECTOR, UITDI
!---------------------------------
      CALL VECSCL (-2.0*ELMGD , UITDI,   TOSVX1)
      CALL VECSCL (    -ELMGDD, UITI ,   TOSVX2)

      CALL VECMOV (ELDDI,               UITDDI)
      CALL VECSUM (UITDDI, TOSVX1,      UITDDI)
      CALL VECSUM (UITDDI, TOSVX2,      UITDDI)

      CALL VECSCL (OOLMG,UITDDI,        UITDDI)


!--------------------------------------------------------
! CALC X-AXIS UNIFORM SEG LENGTH/DERIV (BASED ON AP DIST)
!--------------------------------------------------------
      ELSGX  = ELMG*OBNSEG
      ELSGXD = ELMGD*OBNSEG

      RETURN

!---------------------------
! ERROR DIAGNOSTIC REPORTING
!---------------------------
2000  WRITE(LUE,2001)
2001  FORMAT('TISFRM: FATAL; APPARENTLY CERTAIN AXES HAVE ALIGNED', &
     &       ' SO AS TO DEFEAT THE GEOMETRY OF TETH FRAME DEFN',I2)
      STOP 'IN TISFRM-1'


      END
