! ROUTINE: TNSAD1
! %Z%GTOSS %M% H.10 code v03.02
!              H.10 code v03.02 Protected possible singular unit vec calc.
!                               Add code to find and save max prop seg rate.
!---------------------------------------------------------------------------
!              H.4 code v03.01  modified IC logic for type 2 or 3 HST
!--------------------------------------------------------------------
!              H.3 code v03.00  modified scheme for integration of time
!                               allowing host-preemptive stepsize staging
!------------------------------------------------------------------------
!              H.1 code v02.01 (baseline for vH1 inertial bead model)
!*************************************
!*************************************
!
      SUBROUTINE TNSAD1 (JTETH,JFTETH)
!
!*************************************
!*************************************
! THIS ROUTINE ADVANCES THE STATE OF THE BEAD
! MODEL BY DIRECT INTEGRATION


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.h"





      DIMENSION DUMTU(3), DUMTUD(3), DUDC(3)
      DIMENSION DUDCD(3), DUDCC(3),  DTDU(3)


!********************************************************************
! RECONCILE TETHER STRAIN W/HST ASSUMPTION JUST B4 INTEGRATOR RELEASE
!********************************************************************
! BUT MINIMIZE LOGIC OVERHEAD VIA ONE SHOT LOGIC
      IF(JONCE .NE. 1) THEN
          IF( (JINTEG.EQ.-1) .AND. (ICSTAG.EQ.-1) ) THEN
              IF((NFTYP .EQ. 2) .OR. (NFTYP .EQ. 3)) THEN
                  CALL TNSHIC(JTETH, JFTETH)
              END IF
              JONCE = 1
          END IF
      END IF


! CALCULATE INTER-BEAD EQUIVALENT SPRING FORCES
!----------------------------------------------
      CALL TNSPRG (JTETH,JFTETH)


!*******************************************************
! EVALUATE BEAD COORDINATE ACCELERATIONS AND INGREDIENTS
!*******************************************************
      DO 50 JBEAD = 1,NBEAD

! CALC POINTERS TO THIS BEADS STATE
         JB1 = 3*(JBEAD-1) + 1
         JB2 = JB1+1
         JB3 = JB1+2


! INITIALIZE ACCEL ACCUM W/SPRING FORCE AND LOW SENSTIVITY TERMS
!---------------------------------------------------------------
         OOBMS = 1.0/BMS(JBEAD)
         BUDDI(JB1) = OOBMS*BFSI(JB1) + BCINS(JB1)
         BUDDI(JB2) = OOBMS*BFSI(JB2) + BCINS(JB2)
         BUDDI(JB3) = OOBMS*BFSI(JB3) + BCINS(JB3)




!-------------------------------------------------------------
! CALCULATE TERMS DUE TO DEPLOYMENT FLOW DERIVATIVE COMPONENTS
!-------------------------------------------------------------
! CALCULATE THESE ONLY IN PRESENCE OF DEPLOYMENT RATE AND ACCEL
         IF((ELD .EQ. 0.0) .AND. (ELDD .EQ. 0.0)) GO TO 50

! INITIALIZE THE ACCUMULATOR VECTOR
            TOSVX6(1) = 0.0
            TOSVX6(2) = 0.0
            TOSVX6(3) = 0.0

! CALC AVER UNIT TANGENT VECTOR TO SPACE CURVE (SUM AND NORMALIZE)
!-----------------------------------------------------------------
            DUMTU(1) = BBTUI(JBEAD,1) + BBTUI(JBEAD+1,1)
            DUMTU(2) = BBTUI(JBEAD,2) + BBTUI(JBEAD+1,2)
            DUMTU(3) = BBTUI(JBEAD,3) + BBTUI(JBEAD+1,3)

! CALC VECTOR MAGNITUDE AND PROTECT POSSIBLE SINGULAR CALC
            DUMG0 = VECMAG(DUMTU)

            IF(DUMG0 .EQ. 0.0) THEN
                 DUMTU(1) = 0.0
                 DUMTU(2) = 0.0
                 DUMTU(3) = 0.0
            ELSE
                 DUMTU(1) = DUMTU(1)/DUMG0
                 DUMTU(2) = DUMTU(2)/DUMG0
                 DUMTU(3) = DUMTU(3)/DUMG0
            END IF

! CALC AVERAGE TIME DERIVATIVE OF UNIT TANGENT
!---------------------------------------------
            DUMTUD(1) = 0.5*( BBTUDI(JBEAD,1) + BBTUDI(JBEAD+1,1) )
            DUMTUD(2) = 0.5*( BBTUDI(JBEAD,2) + BBTUDI(JBEAD+1,2) )
            DUMTUD(3) = 0.5*( BBTUDI(JBEAD,3) + BBTUDI(JBEAD+1,3) )

! CALC AVER PARTIAL OF DEFORMED ARC LENGTH WR/T UNDEFORMED MATERIAL
            DUMDS   = 0.5*( DSDU(JBEAD) + DSDU(JBEAD+1)  )

! CALC AVER TIME DERIV OF PARTIAL OF ARC LENGTH
            DUMDSD  = 0.5*( DSDUD(JBEAD) + DSDUD(JBEAD+1) )

! FORM THE NORMALIZED MATERIAL VARIABLE
            DUMCEE = REAL(NBEAD-JBEAD+1)*OBNSEG



! 1ST TERM CONTRIBUTION (MUST ALWAYS EVALUATE IF THIS POINT IS REACHED)
!**********************************************************************
! FORM PARTIAL OF U VECTOR WR/T NORMALIZED SPACE VARIABLE
            DUM = DUMDS*EL

            DUDC(1) = DUM*DUMTU(1) + ELMG*UITI(1)
            DUDC(2) = DUM*DUMTU(2) + ELMG*UITI(2)
            DUDC(3) = DUM*DUMTU(3) + ELMG*UITI(3)

! CALCULATE 1ST TERM
            DUMSCL = DUMCEE*(  2.0*(ELD/EL)**2 - ELDD/EL  )
            CALL VECSCL(DUMSCL, DUDC,   TOSVX1)

! ACCUMULATE THIS INCREMENT
            CALL VECSUM(TOSVX6, TOSVX1,    TOSVX6)




! 2ND TERM CONTRIBUTION (DO ONLY IF DEPLOY RATE NOT ZERO)
!********************************************************
            IF(ELD .NE. 0.0) THEN

! NUMERICALLY DIFFERENTIATE DSDU WR/T MATERIAL VARIABLE MU
! (NOTE, NUMERATOR DIFFERENCE REFLECTS MU DEFINITION FROM FAR END)
                DSDUU = ( DSDU(JBEAD) - DSDU(JBEAD+1) )/ELB

! NUMERICALLY DIFFERENTIATE UNIT TANGENT VEC WR/T MATERIAL VARIABLE MU
! (NOTE, NUMERATOR DIFFERENCE REFLECTS MU DEFINITION FROM FAR END)
                DTDU(1) = ( BBTUI(JBEAD,1) - BBTUI(JBEAD+1,1) )/ELB
                DTDU(2) = ( BBTUI(JBEAD,2) - BBTUI(JBEAD+1,2) )/ELB
                DTDU(3) = ( BBTUI(JBEAD,3) - BBTUI(JBEAD+1,3) )/ELB

! FORM 2ND DERIV OF U VECTOR WR/T NORMALIZED VARIABLE (DELETING L**2)
                DUDCC(1) = DSDUU*DUMTU(1) + DUMDS*DTDU(1)
                DUDCC(2) = DSDUU*DUMTU(2) + DUMDS*DTDU(2)
                DUDCC(3) = DSDUU*DUMTU(3) + DUMDS*DTDU(3)

! CALCULATE 2ND TERM (1/L**2 TERM DELETED FROM DUMSCL -SEE LINE ABOVE)
                DUMSCL = (DUMCEE*ELD)**2
                CALL VECSCL(DUMSCL, DUDCC,   TOSVX1)

! ACCUMULATE THIS INCREMENT
                CALL VECSUM(TOSVX6, TOSVX1,    TOSVX6)



! 3RD TERM CONTRIBUTION (DO ONLY IF 2ND TERM IS DONE)
!****************************************************
! FORM TIME DERIV OF PARTIAL OF U VECTOR WR/T NORMALIZED SPACE VARIABLE
                A = DUMDSD*EL + DUMDS*ELD
                B = DUMDS*EL
                C = ELMGD
                D = ELMG

                DUDCD(1)= A*DUMTU(1) +B*DUMTUD(1) +C*UITI(1) +D*UITDI(1)
                DUDCD(2)= A*DUMTU(2) +B*DUMTUD(2) +C*UITI(2) +D*UITDI(2)
                DUDCD(3)= A*DUMTU(3) +B*DUMTUD(3) +C*UITI(3) +D*UITDI(3)

! CALCULATE 3RD TERM
                DUMSCL = - 2.0*DUMCEE*ELD/EL
                CALL VECSCL(DUMSCL, DUDCD,   TOSVX1)

! ACCUMULATE THIS INCREMENT
                CALL VECSUM(TOSVX6, TOSVX1,    TOSVX6)

            END IF



! ADD IN THESE CONTRIBUTIONS TO BEAD ACCELERATION
!------------------------------------------------
         BUDDI(JB1) = BUDDI(JB1) - TOSVX6(1)
         BUDDI(JB2) = BUDDI(JB2) - TOSVX6(2)
         BUDDI(JB3) = BUDDI(JB3) - TOSVX6(3)

!-----------------------------------------------------
! END OF LOOP TO EVALUATE ALL BEAD COORD ACCELERATIONS
!-----------------------------------------------------
50    CONTINUE



!****************************************************************
!****************************************************************
! ADVANCE STATE OF BEAD TETHER (AFTER INITIALIZATION IS COMPLETE)
!****************************************************************
!****************************************************************
       IF(JINTEG .EQ. 1) THEN

!--------------------------------------------------------------
! NOTE: ACCEL IS ADVANCED BEFORE INTEGRATING RATE TO POSITION
!      IN ORDER TO INSERT INTEGRATION LEAD TO MAINTAIN STABILITY
!      OF 2ND ORDER UN-DAMPED SYSTEM RESPONSE (ABBOTTS METHOD?)
!--------------------------------------------------------------

! ADVANCE ALL BEAD ACCELERATIONS TO RATE
            CALL EULINT (DELTIS,N3BEAD,BUDDI,   BUDI)

! ADVANCE RATES TO POSITIONS (MUST PRECEDE ACCEL TO RATE?)
            CALL EULINT (DELTIS,N3BEAD,BUDI,     BUI)

       END IF


!------------------------------------------------------------
! CALCULATE TIME VARIABLE AND ALLOW FOR HOST STEPSIZE STAGING
!------------------------------------------------------------
! FIRST SEE IF INTEGRATION STEPSIZE HAS CHANGED
       IF(DELTIS .NE. DELTPF) THEN
            DELTPF = DELTIS
            NFTIME = 0
            TBIASF = TISTIM
       END IF

! ADVANCE TIME COUNTER
      IF(JINTEG .EQ. 1) NFTIME = NFTIME + 1

! CALCULATE THE VALUE OF TIME FOR THE FINITE SOLN
      TISTIM = DELTIS*REAL(NFTIME) + TBIASF


! FIND MAX SEGD/SEG FOR THIS TOSS TETH (FOR SUGGESTING STEPSIZE)
!---------------------------------------------------------------
      SDOSMAX(JTETH) = 0.0

      DO 100 JSEG = 1,NBEAD+1
         DUMRAT = ABS(ELBSGD(JSEG))/ELBSG(JSEG)
         IF(DUMRAT .GT. SDOSMAX(JTETH)) SDOSMAX(JTETH) = DUMRAT
100   CONTINUE


      RETURN
      END
