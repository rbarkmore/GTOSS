! ROUTINE: POPID
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!********************************************
!********************************************
!
      SUBROUTINE POPID (IOSCR,IOQLK,LSTOBJ)
!
!********************************************
!********************************************
! THIS SUBROUTINE POPULATES THE RDB ASCII I-FILE

! IT ALLOWS A SIMULATION WHICH IS INCORPORATING
! TOSS AND ITS RDB TO DEFINE AN ASCII RUN DEFINTION
! OF ITS OWN, AND SUPPORTS A QUICKLOOK TYPE OUTPUT
! OF ASCII RUN DATA BY WRITING ALL TOSS ASCII RUN
! DESCRIPTION TO I/O-UNITS DEFINED AS ARGUMENTS.


! ARG IOSCR  TELLS THE RDB SUB-SYSTEM THE FILE UNIT (IF ANY)
!            ON WHICH RUN ID DATA FROM THE HOST SIM CAN BE FOUND

! ARG IOQLK  TELLS THE RDB SUB-SYSTEM THE FILE UNIT (IF ANY)
!            TO WHICH TOSS SHOULD WRITE ITS RUN ID DATA FOR
!            THE BENEFIT OF THE HOST

! >>>>>>>>>>>>>>>>>>>>                 <<<<<<<<<<<<<<<<<<<<
! READS/WRITES TO ARG FILES IS INHIBITED IF ARG UNITS = -1
! >>>>>>>>>>>>>>>>>>>>                 <<<<<<<<<<<<<<<<<<<<


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_ROSS.h"




!****************************************
! READ REF PT DATA FROM HOST SCRATCH FILE
! WRITE REF PT DATA TO RDB SYSTEM I-FILE
!***************************************
      CALL POPIDX(IOSCR)


!*********************************************
! WRITE TOSS GENERAL DATA AND TOSS TETHER DATA
!*********************************************
      CALL POPIDT(IOQLK)


!**************************************
! WRITE BEAD MODEL FINITE SOLUTION DATA
!**************************************
      CALL POPIDB(IOQLK)


!*************************************
! WRITE POWER GEN/DEPLOYMENT SCENARIOS
!*************************************
      CALL POPIDS(IOQLK)


!*********************************************
! WRITE TOSS OBJECT SPECIFIC DATA FOR THIS RUN
!*********************************************
      IF(LSTOBJ .LT. 2) GO TO 1000

! DO A LOOP TO DISPLAY EACH OBJECT (IF ANY)
!------------------------------------------
      DO 61 JBODY = 2,LSTOBJ

! START NEW PAGE FOR EACH TOSS OBJECT OVER 2
            IF(IOQLK .NE. -1) CALL NEWPAG(LRFAC,IOQLK)
            CALL NEWPAG(LRFAC,IOUXXI)

            CALL POPIDO(IOQLK,JBODY)

61    CONTINUE


! STANDARD RETURN
1000  CONTINUE

!------------------------------------------------
! WRITE END OF RECORD ON ASCII RDB FILE AND CLOSE
!------------------------------------------------
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! SOME ANSI FORTRANS MAY NOT LIKE ENDFILE
      ENDFILE(IOUXXI)

      CLOSE(IOUXXI, STATUS='KEEP')


      RETURN
      END
