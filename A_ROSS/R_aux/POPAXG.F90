! ROUTINE: POPAXG
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************
!*************************************
!
      SUBROUTINE POPAXG(NPOP)
!
!*************************************
!*************************************

! THIS SUBROUTINE ALLOWS FOR A USER DEFINED ADDITION
! TO BE MADE TO THE ALREADY WRITTEN BASIC RDB FILE
! ASSOCIATED WITH THE WILD CARD G-FILE


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_ROSS.h"




!-----------------------------------------------------
! IT IS  THE RESPONSIBILITY  OF THIS ROUTINE  TO STATE
! ITS CONTRIBUTION TO ITS ASSOCIATED RDB FILES RECORD
! SIZE IN SUCH A WAY THAT  IF NPOP=0, THEN THE SIZE IS
! STATED, AND NO  UNINITIALIZED DATA  WILL BE HANDLED.
! IF NPOP=1, DATA ARRAYS ARE POPULATED AS USUAL
!-----------------------------------------------------

!******************************************
! RESPOND TO RTOSS REQUEST FOR RECORD COUNT
!******************************************
      IF(NPOP .EQ. 0) THEN

! STATE RECORD WORD COUNT REQUIRED TO CONTAIN YOUR DATA
!        NINCXG = NXG      SEE BELOW FOR DEFN OF NXG
         NINCXG = 0

         RETURN

      END IF

!***********************
! SET IN AUX G-FILE DATA
!***********************
! NO FUNCTION AT PRESENT

!     RDBOUT(NBASG+1) = ??????
!
!     RDBOUT(NBASG+NXG) = ??????

      RETURN
      END
