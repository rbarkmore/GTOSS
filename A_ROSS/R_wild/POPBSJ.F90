! ROUTINE: POPBSJ
! %Z%GTOSS %M% H.5 code v02.01
!              H.5 code v02.01 Activates code to archive mmds deployer
!---------------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************
!*************************************
!
      SUBROUTINE POPBSJ(NPOP)
!
!*************************************
!*************************************
! THIS SUBROUTINE POPULATES THE WILD-CARD J-FILE

! IN THIS CASE (GTOSS), IT IS USED TO ARCHIVE DATA
! PERTAINING TO THE TSS TYPE DEPLOYER SCENARIO


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_ROSS.h"
      include "../../A_HDR/COM_TOSS.h"
      include "../../A_HDR/EQU_TOSS.h"
      include "../../A_HDR/EQU_MMDS.h"





!-----------------------------------------------------
! IT IS  THE RESPONSIBILITY  OF THIS ROUTINE  TO STATE
! ITS CONTRIBUTION TO ITS ASSOCIATED RDB FILES RECORD
! SIZE IN SUCH A WAY THAT  IF NPOP=0, THEN THE SIZE IS
! STATED, AND NO  UNINITIALIZED DATA  WILL BE HANDLED.
! IF NPOP=1, DATA ARRAYS ARE POPULATED AS USUAL
!-----------------------------------------------------

!=======================================================================
!=======================================================================
!
!               !!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!
!
! THIS RDB ARCHIVE IS NOT GENERAL IN NATURE IN THAT ONLY ONE INSTANCE OF
! OF A TYPE 6 DEPLOYMENT SCENARIO WILL EVER BE ARCHIVED. THUS EVEN
! THOUGH TOSS WOULD LOGICALLY ALLOW MANY TETHERS TO BE ASSIGNED A TYPE 6
! DEPLOYMENT SCHEME, ONLY ONE WOULD BE ARCHIVED IN THE RDB. IT WOULD BE
! THE LAST ONE INVOKED!....THIS NOT-WITHSTANDING, THERE ARE OTHER
! PROBLEMS ASSOCIATED WITH TYPE 6 ASSIGNMENTS WHICH WOULD RENDER
! INCORRECT EXECUTION!
!=======================================================================
!=======================================================================


!****************************************
! DO NOTHING IF SCENARIO 6 IS NOT INVOKED
!****************************************
! NOTE: THAT THIS FLAG IS SET IN TOSSFQ.F, WHICH IS NATURALLY PRIVY TO
! WHETHER A TYPE 6 DEPLOY SCENARIOS HAVE BEEN INVOKED

      IF(JTYPD6 .EQ. 0) THEN
               NBASJ = 0
               RETURN
      END IF


! INITIALIZE NEXT AVAILABLE ARRAY ELEMENT POINTER
!------------------------------------------------
      NBASJ = 1

! ONLY TALLY THE ARRAY COUNT IF ITS OPEN-SIZE CALL
!-------------------------------------------------
      IF(NPOP .EQ. 1) THEN

         RDBOUT( 1) = T
         RDBOUT( 2) = TAU
         RDBOUT( 3) = ALC
         RDBOUT( 4) = ALDC
         RDBOUT( 5) = ALDDC
         RDBOUT( 6) = ALRC
         RDBOUT( 7) = CTH
         RDBOUT( 8) = CTHD
         RDBOUT( 9) = CTHDD
         RDBOUT(10) = FC
         RDBOUT(11) = FUN
         RDBOUT(12) = R1
         RDBOUT(13) = AJ1
         RDBOUT(14) = RLOMM
         RDBOUT(15) = RLODMM
         RDBOUT(16) = AM1
         RDBOUT(17) = AM2
         RDBOUT(18) = AMST
         RDBOUT(19) = TOM

      END IF

! TALLY THIS BLOCK UTILIZATION
      NBASJ = NBASJ + 19

      RETURN
      END
