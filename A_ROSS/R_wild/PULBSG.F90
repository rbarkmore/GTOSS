! ROUTINE: PULBSG
! %Z%GTOSS %M% H.10 code v01.1
!              H.10 code v01.1 Added data for RP-Fixed base motion
!-----------------------------------------------------------------
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************
!*************************************
!
      SUBROUTINE PULBSG
!
!*************************************
!*************************************
! THIS SUBROUTINE EXTRACTS BASIC DATA FROM RDB G-FILE

! THIS ROUTINE IS AN EXACT FUNCTIONAL RECIPROCAL
! OF ROUTINE "POPBSG"


      include "../../A_HDR/COM_ALL.h"
      include "../../A_HDR/COM_ROSS.h"
      include "../../A_HDR/COM_HOST.h"
      include "../../A_HDR/EQU_HOST.h"
      include "../../A_HDR/COM_RPS.h"




!***********************************************************
! EXTRACT BASIC GTOSS DATA FROM READ ARRAY TO WORKING ARRAYS
!***********************************************************
      T = RDBOUT( 1)

      RPRI(1) = RDBOUT( 2)
      RPRI(2) = RDBOUT( 3)
      RPRI(3) = RDBOUT( 4)

      RPRID(1) = RDBOUT( 5)
      RPRID(2) = RDBOUT( 6)
      RPRID(3) = RDBOUT( 7)

      RPRIDD(1) = RDBOUT( 8)
      RPRIDD(2) = RDBOUT( 9)
      RPRIDD(3) = RDBOUT(10)

      RPFI(1)  = RDBOUT(11)
      RPFI(2)  = RDBOUT(12)
      RPFI(3)  = RDBOUT(13)

      RPAGI(1) = RDBOUT(14)
      RPAGI(2) = RDBOUT(15)
      RPAGI(3) = RDBOUT(16)

      RPFAI(1) = RDBOUT(17)
      RPFAI(2) = RDBOUT(18)
      RPFAI(3) = RDBOUT(19)

      RPFCI(1) = RDBOUT(20)
      RPFCI(2) = RDBOUT(21)
      RPFCI(3) = RDBOUT(22)

      RPFEI(1) = RDBOUT(23)
      RPFEI(2) = RDBOUT(24)
      RPFEI(3) = RDBOUT(25)

      RPFTI(1) = RDBOUT(26)
      RPFTI(2) = RDBOUT(27)
      RPFTI(3) = RDBOUT(28)

      RPGB(1) = RDBOUT(29)
      RPGB(2) = RDBOUT(30)
      RPGB(3) = RDBOUT(31)

      RPGTB(1) = RDBOUT(32)
      RPGTB(2) = RDBOUT(33)
      RPGTB(3) = RDBOUT(34)

      RPGAB(1) = RDBOUT(35)
      RPGAB(2) = RDBOUT(36)
      RPGAB(3) = RDBOUT(37)

      RPGCB(1) = RDBOUT(38)
      RPGCB(2) = RDBOUT(39)
      RPGCB(3) = RDBOUT(40)

      RPGEB(1) = RDBOUT(41)
      RPGEB(2) = RDBOUT(42)
      RPGEB(3) = RDBOUT(43)

      RPGGB(1) = RDBOUT(44)
      RPGGB(2) = RDBOUT(45)
      RPGGB(3) = RDBOUT(46)

      RPOM(1) = RDBOUT(47)
      RPOM(2) = RDBOUT(48)
      RPOM(3) = RDBOUT(49)

      RPOMD(1) = RDBOUT(50)
      RPOMD(2) = RDBOUT(51)
      RPOMD(3) = RDBOUT(52)

      RPVWI(1) = RDBOUT(53)
      RPVWI(2) = RDBOUT(54)
      RPVWI(3) = RDBOUT(55)

      RPVWP(1) = RDBOUT(56)
      RPVWP(2) = RDBOUT(57)
      RPVWP(3) = RDBOUT(58)

      RPRIP(1) = RDBOUT(59)
      RPRIP(2) = RDBOUT(60)
      RPRIP(3) = RDBOUT(61)

      RFPALT = RDBOUT(62)
      RPVELI = RDBOUT(63)
      RPHD   = RDBOUT(64)

      RPQBAR = RDBOUT(65)
      RPDENS = RDBOUT(66)
      RPSSS  = RDBOUT(67)
      RPHOT  = RDBOUT(68)

      RPMASS = RDBOUT(69)

      DEGMUI = RDBOUT(70)
      DEGLAI = RDBOUT(71)
      DEGMUE = RDBOUT(72)
      DEGLAE = RDBOUT(73)

      RPGEOD = RDBOUT(74)
      RPGLAT = RDBOUT(75)

! ARTIFACT FROM WHEN RPGAU WAS IN EQU_HOST ARRAY (NOW ITS IN RPS)
! (THIS PLACEHOLDING ACTIVITY MAINTAINS RDB COMPATIBILITY)
!      RPGAU(1) = RDBOUT(76)
!      RPGAU(2) = RDBOUT(77)
!      RPGAU(3) = RDBOUT(78)

      RPAMI(1) = RDBOUT(79)
      RPAMI(2) = RDBOUT(80)
      RPAMI(3) = RDBOUT(81)

      RPIXX = RDBOUT(82)
      RPIYY = RDBOUT(83)
      RPIZZ = RDBOUT(84)

      RPIXY = RDBOUT(85)
      RPIXZ = RDBOUT(86)
      RPIYZ = RDBOUT(87)

      RPXBCG = RDBOUT(88)
      RPYBCG = RDBOUT(89)
      RPZBCG = RDBOUT(90)

      BSPER(1) = RDBOUT(91)
      BSPER(2) = RDBOUT(92)
      BSPER(3) = RDBOUT(93)

      BSPERD(1) = RDBOUT(94)
      BSPERD(2) = RDBOUT(95)
      BSPERD(3) = RDBOUT(96)

      BSPERDD(1) = RDBOUT(97)
      BSPERDD(2) = RDBOUT(98)
      BSPERDD(3) = RDBOUT(99)

      RETURN
      END
