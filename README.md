# GTOSS Generalized Tether Object Simulation System

source code repository

build using make scripts, such as 'MakeALL.sh'

tested with Gfortran and Mingw in Windows 10

See the GTOSS Reference Documents at
https://gitlab.com/rbarkmore/gtoss-documents
